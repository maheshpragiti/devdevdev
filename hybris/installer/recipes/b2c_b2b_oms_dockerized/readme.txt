Scenario for dockerized OMS Standalone

To perform follow this scenario:
================================

-> ./install.sh -r b2c_b2b_oms_dockerized createImagesStructure
-> cd work/output_images/b2c_b2b_oms_dockerized/
-> ./build-images.sh
-> cd - && cd recipes/b2c_b2b_oms_dockerized

# Initialize the platform.
-> docker-compose up -d platform_admin_init
# To view init logs
-> docker-compose logs -f platform_admin_init

# To start has with OMS web services on port 9002:
-> docker-compose up -d platform_webservices
# To view web services logs
-> docker-compose logs -f platform_webservices