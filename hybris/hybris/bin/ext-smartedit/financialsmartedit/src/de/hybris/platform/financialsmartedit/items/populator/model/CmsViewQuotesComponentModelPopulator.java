/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialsmartedit.items.populator.model;

import de.hybris.platform.cmsfacades.data.CMSViewQuotesComponentData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.financialservices.model.components.CMSViewQuotesComponentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Default cms view quotes component implementation of {@link Populator}.
 */
public class CmsViewQuotesComponentModelPopulator implements Populator<CMSViewQuotesComponentModel, CMSViewQuotesComponentData> {

    @Override
    public void populate(final CMSViewQuotesComponentModel source, final CMSViewQuotesComponentData target) throws ConversionException
    {
        if (source.getNumberOfQuotesToDisplay() != null)
        {
            target.setNumberOfQuotesToDisplay(String.valueOf(source.getNumberOfQuotesToDisplay()));
        }
    }
}
