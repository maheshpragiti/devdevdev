/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialsmartedit.items.populator.data;

import de.hybris.platform.cmsfacades.data.CMSViewPoliciesComponentData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.financialservices.model.components.CMSViewPoliciesComponentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang3.StringUtils;


/**
 * This populator will populate the {@link CMSViewPoliciesComponentModel#setNumberOfPoliciesToDisplay(int} with the attribute
 * number of Policies from the {@link CMSViewPoliciesComponentData}.
 */
public class CmsViewPoliciesComponentDataPopulator implements Populator<CMSViewPoliciesComponentData, CMSViewPoliciesComponentModel>
{

	@Override
	public void populate(CMSViewPoliciesComponentData source, CMSViewPoliciesComponentModel target) throws ConversionException {
		target.setNumberOfPoliciesToDisplay(StringUtils.isNotBlank(source.getNumberOfPoliciesToDisplay()) ? Integer.parseInt(source.getNumberOfPoliciesToDisplay()) : null);
	}

}
