/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialsmartedit.constants;

@SuppressWarnings("PMD")
public class FinancialsmarteditConstants extends GeneratedFinancialsmarteditConstants
{
	public static final String EXTENSIONNAME = "financialsmartedit";

	public static final String FIELD_REQUIRED = "field.required";
	public static final String FIELD_NOT_POSITIVE_INTEGER = "field.positive.integer.invalid";

	private FinancialsmarteditConstants()
	{
		//empty
	}
	
	
}
