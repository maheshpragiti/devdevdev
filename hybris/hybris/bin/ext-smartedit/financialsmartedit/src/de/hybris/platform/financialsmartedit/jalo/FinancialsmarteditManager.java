/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialsmartedit.jalo;

import de.hybris.platform.financialsmartedit.constants.FinancialsmarteditConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class FinancialsmarteditManager extends GeneratedFinancialsmarteditManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger( FinancialsmarteditManager.class.getName() );
	
	public static final FinancialsmarteditManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (FinancialsmarteditManager) em.getExtension(FinancialsmarteditConstants.EXTENSIONNAME);
	}
	
}
