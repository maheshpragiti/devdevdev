/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialsmartedit.items.populator.data;

import de.hybris.platform.cmsfacades.data.CMSViewQuotesComponentData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.financialservices.model.components.CMSViewQuotesComponentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang3.StringUtils;

/**
 * This populator will populate the {@link CMSViewQuotesComponentModel#setNumberOfQuotesToDisplay(int} with the attribute
 * number of quotes from the {@link CMSViewQuotesComponentData}.
 */
public class CmsViewQuotesComponentDataPopulator implements Populator<CMSViewQuotesComponentData, CMSViewQuotesComponentModel>
{

	@Override
	public void populate(CMSViewQuotesComponentData source, CMSViewQuotesComponentModel target) throws ConversionException {
		target.setNumberOfQuotesToDisplay(StringUtils.isNotBlank(source.getNumberOfQuotesToDisplay()) ? Integer.parseInt(source.getNumberOfQuotesToDisplay()) : null);
	}

}
