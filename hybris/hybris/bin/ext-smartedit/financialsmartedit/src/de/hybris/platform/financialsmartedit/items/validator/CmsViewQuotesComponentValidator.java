/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialsmartedit.items.validator;

import de.hybris.platform.financialsmartedit.constants.FinancialsmarteditConstants;
import de.hybris.platform.cmsfacades.data.CMSViewQuotesComponentData;

import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


/**
 * Validates integer field.
 */
public class CmsViewQuotesComponentValidator implements Validator
{
    private static final String NUMBER_OF_QUOTES_TO_DISPLAY = "numberOfQuotesToDisplay";

    private Predicate<String> validPositiveIntegerPredicate;

    @Override
    public boolean supports(final Class<?> clazz)
    {
        return CMSViewQuotesComponentData.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object obj, final Errors errors)
    {
        final CMSViewQuotesComponentData target = (CMSViewQuotesComponentData) obj;

        ValidationUtils.rejectIfEmpty(errors, NUMBER_OF_QUOTES_TO_DISPLAY, FinancialsmarteditConstants.FIELD_REQUIRED);

        if (!getValidPositiveIntegerPredicate().test(target.getNumberOfQuotesToDisplay()))
        {
            errors.rejectValue(NUMBER_OF_QUOTES_TO_DISPLAY, FinancialsmarteditConstants.FIELD_NOT_POSITIVE_INTEGER);
        }

    }

    public Predicate<String> getValidPositiveIntegerPredicate()
    {
        return validPositiveIntegerPredicate;
    }

    @Required
    public void setValidPositiveIntegerPredicate(final Predicate<String> validPositiveIntegerPredicate)
    {
        this.validPositiveIntegerPredicate = validPositiveIntegerPredicate;
    }
}
