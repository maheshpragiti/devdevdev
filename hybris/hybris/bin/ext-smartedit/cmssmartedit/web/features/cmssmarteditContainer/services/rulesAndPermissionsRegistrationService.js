/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('rulesAndPermissionsRegistrationModule', [
    'permissionServiceModule',
    'catalogVersionPermissionModule'
]).run(function($q, permissionService, catalogVersionPermissionService) {
    // Rules
    permissionService.registerRule({
        names: ['se.write.page', 'se.write.slot', 'se.write.component'],
        verify: function(permissionNameObjs) {
            var promises = permissionNameObjs.map(function(permissionNameObject) {
                if (permissionNameObject.context) {
                    return catalogVersionPermissionService.hasWritePermission(
                        permissionNameObject.context.catalogId,
                        permissionNameObject.context.catalogVersion
                    );
                } else {
                    return catalogVersionPermissionService.hasWritePermissionOnCurrent();
                }
            });

            var onSuccess = function(result) {
                return result.reduce(function(acc, val) {
                    return acc && val;
                }, true) === true;
            };

            var onError = function() {
                return false;
            };

            return $q.all(promises).then(onSuccess, onError);
        }
    });

    permissionService.registerRule({
        names: ['se.read.page', 'se.read.slot', 'se.read.component'],
        verify: function(permissionNameObjs) {
            return catalogVersionPermissionService.hasReadPermissionOnCurrent();
        }
    });

    // Permissions
    permissionService.registerPermission({
        aliases: ['se.add.component', 'se.context.menu.drag.and.drop.component'],
        rules: ['se.write.slot', 'se.write.component']
    });

    permissionService.registerPermission({
        aliases: ['se.read.restriction', 'se.read.page'],
        rules: ['se.read.page']
    });

    permissionService.registerPermission({
        aliases: ['se.edit.page'],
        rules: ['se.write.page']
    });

    permissionService.registerPermission({
        aliases: ['se.sync.slot.context.menu', 'se.sync.catalog', 'se.sync.page'],
        rules: ['se.write.page', 'se.write.slot', 'se.write.component']
    });

    permissionService.registerPermission({
        aliases: ['se.context.menu.edit.component', 'se.edit.navigation'],
        rules: ['se.write.component']
    });

    permissionService.registerPermission({
        aliases: ['se.context.menu.remove.component'],
        rules: ['se.write.slot']
    });

    permissionService.registerPermission({
        aliases: ['se.slot.context.menu.shared.icon'],
        rules: ['se.read.slot']
    });

    permissionService.registerPermission({
        aliases: ['se.slot.context.menu.visibility'],
        rules: ['se.read.slot', 'se.write.component']
    });
});
