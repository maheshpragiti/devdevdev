/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name restrictionsTableModule
 * @requires l10nModule
 * @requires pageRestrictionsModule
 * @description
 * This module defines the {@link restrictionsTableModule.directive:restrictionsTable restrictionsTable} component
 */
angular.module('restrictionsTableModule', [
    'l10nModule',
    'pageRestrictionsModule'
])

.controller('restrictionsTableController', function(
    pageRestrictionsFacade
) {

    this.removeRestriction = function(restriction) {
        this.restrictions.splice(this.restrictions.indexOf(restriction), 1);
    }.bind(this);

    this.editRestriction = function(restriction) {
        this.onClickOnEdit(restriction);
    }.bind(this);


    this.criteriaClicked = function() {
        this.onCriteriaSelected(this.restrictionCriteria);
    };

    this.removeAllRestrictions = function() {
        this.restrictions = [];
    };

    this.showRemoveAllButton = function() {
        return this.restrictions && this.restrictions.length > 0 && this.editable;
    };

    this.$onInit = function() {

        this.criteriaOptions = pageRestrictionsFacade.getRestrictionCriteriaOptions();
        if (!this.restrictionCriteria) { //default if none is provided
            this.restrictionCriteria = this.criteriaOptions[0];
        }

        this.actions = [{
            key: 'page.restrictions.item.edit',
            callback: this.editRestriction
        }, {
            key: 'page.restrictions.item.remove',
            callback: this.removeRestriction
        }];
    };

})

/**
 * @ngdoc directive
 * @name restrictionsTableModule.directive:restrictionsTable
 * @restrict E
 * @scope
 * @param {= String} customClass The name of the CSS class.
 * @param {= Boolean} editable States whether the restrictions table could be modified.
 * @param {< Function=} onClickOnEdit Triggers the custom on edit event.
 * @param {= Function} onCriteriaSelected Function that accepts the selected value of criteria.
 * @param {= Function} onSelect Triggers the custom on select event.
 * @param {= Object} restrictions The object of restrictions.
 * @param {= Object=} restrictionCriteria The object that contains information about criteria.
 * @description
 * Directive that can render a list of restrictions and provides callback functions such as onSelect and onCriteriaSelected. *
 */
.component('restrictionsTable', {
    templateUrl: 'restrictionsTableTemplate.html',
    controller: 'restrictionsTableController',
    controllerAs: '$ctrl',
    bindings: {
        customClass: '=',
        editable: '=',
        onClickOnEdit: '<?',
        onCriteriaSelected: '=',
        onSelect: '=',
        restrictions: '=',
        restrictionCriteria: '=?'
    }
});
