/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageRestrictionsEditorModule
 * @description
 * This module contains the {@link pageRestrictionsEditorModule.pageRestrictionsEditor pageRestrictionsEditor} component.
 */
angular.module('pageRestrictionsEditorModule', [
    'cmssmarteditContainerTemplates',
    'clientPagedListModule',
    "eventServiceModule",
    'pageRestrictionsModule',
    'restrictionsTableModule',
    'restServiceFactoryModule',
    "restrictionPickerModule",
    "sliderPanelModule",
    "yMessageModule",
    "yLoDashModule"
])

.controller('pageRestrictionsEditorController', function(
    $q, $log, lodash,
    GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT,
    CONTEXTUAL_PAGES_RESOURCE_URI,
    PAGEINFO_RESOURCE_URI,
    pageRestrictionsFacade,
    restServiceFactory,
    systemEventService,
    restrictionPickerConfig) {

    var getPageInfoRestResource = restServiceFactory.get(PAGEINFO_RESOURCE_URI);
    var updatePageInfoRestResource = restServiceFactory.get(CONTEXTUAL_PAGES_RESOURCE_URI, "pageUid");
    var self = this;

    function setSliderConfigForAddOrCreate() {
        self.sliderPanelConfiguration.modal.title = "se.cms.page.restriction.management.panel.title.add";
        self.sliderPanelConfiguration.modal.save.label = "se.cms.page.restriction.management.panel.button.add";
        self.sliderPanelConfiguration.modal.save.isDisabledFn = function() {
            if (self.restrictionManagement.isDirtyFn) {
                return !self.restrictionManagement.isDirtyFn();
            }
            return true; // disable save until save FN is bound byt restriction management component
        };
        self.sliderPanelConfiguration.modal.save.onClick = function() {
            self.restrictionManagement.submitFn().then(function(restriction) {
                self.restrictions.push(restriction);
                self.sliderPanelHide();
            });
        };
    }

    function setSliderConfigForEditing() {
        self.sliderPanelConfiguration.modal.title = "se.cms.page.restriction.management.panel.title.edit";
        self.sliderPanelConfiguration.modal.save.label = "se.cms.page.restriction.management.panel.button.save";
        self.sliderPanelConfiguration.modal.save.isDisabledFn = function() {
            if (self.restrictionManagement.isDirtyFn) {
                return !self.restrictionManagement.isDirtyFn();
            }
            return true; // disable save until save FN is bound byt restriction management component
        };
        self.sliderPanelConfiguration.modal.save.onClick = function() {
            self.restrictionManagement.submitFn().then(function(restrictionEdited) {
                var index = self.restrictions.findIndex(function(restriction) {
                    return restriction.uuid === restrictionEdited.uuid;
                });
                if (index !== -1) {
                    self.restrictions[index] = restrictionEdited;
                } else {
                    throw "pageRestrictionsEditorController - edited restriction not found in list: " + restrictionEdited;
                }
                self.sliderPanelHide();
            });
        };
    }

    this.showPageRestrictionPicker = false;
    this.restrictions = [];
    this.orrigRestrictionIds = [];
    this.restrictionsArrayIsDirty = false;
    this.criteria = {};
    this.orrigCriteria = {};
    this.matchCriteriaIsDirty = false;
    this.oldRestrictions = [];

    this.sliderPanelConfiguration = {
        modal: {
            showDismissButton: true,
            cancel: {
                label: "se.cms.page.restriction.management.panel.button.cancel",
                onClick: function() {
                    self.sliderPanelHide();
                }
            },
            save: {}
        },
        cssSelector: "#y-modal-dialog"
    };

    this.onClickOnAdd = function() {
        setSliderConfigForAddOrCreate();
        this.restrictionManagement.operation = restrictionPickerConfig.getConfigForSelecting(this.page.typeCode, this.restrictions);
        this.sliderPanelShow();
    }.bind(this);

    this.onClickOnEdit = function(restriction) {
        setSliderConfigForEditing();
        this.restrictionManagement.operation = restrictionPickerConfig.getConfigForEditing(restriction.uuid);
        this.sliderPanelShow();
    }.bind(this);

    this.matchCriteriaChanged = function(criteriaSelected) {
        this.criteria = criteriaSelected;
        this.matchCriteriaIsDirty = this.criteria != this.orrigCriteria;
        this.updateRestrictionsData();
    }.bind(this);

    this.setupResults = function(results) {
        this.restrictions = results;
        this.updateRestrictionsData();

        results.forEach(function(restriction) {
            this.orrigRestrictionIds.push(restriction.uuid);
        }.bind(this));
    };

    this.updateRestrictionsData = function() {
        if (this.onRestrictionsChanged) {
            this.onRestrictionsChanged({
                $onlyOneRestrictionMustApply: this.criteria.value,
                $restrictions: this.restrictions
            });
        }
    };

    this.$onInit = function() {

        this.restrictionCriteria = pageRestrictionsFacade.getRestrictionCriteriaOptionFromPage(this.page);

        if (self.initialRestrictions) {
            this.setupResults([this.initialRestrictions]);
        } else {
            if (this.page.uuid) {
                // fetch restrictions for this page and setup the dirty state watchers
                pageRestrictionsFacade.getRestrictionsByPageUUID(this.page.uuid).then(
                    function(results) {
                        this.setupResults(results.response);
                    }.bind(this)
                );
            } else {
                this.setupResults([]);
            }
        }

        self.criteria = pageRestrictionsFacade.getRestrictionCriteriaOptionFromPage(self.page);
        self.orrigCriteria = self.criteria;
        self.restrictionManagement = {
            uriContext: self.page.uriContext
        };

        // It is necessary to put this function inside $onInit. Otherwise, the editor is never marked as dirty.
        this.isDirtyFn = function() {
            return self.restrictionsArrayIsDirty || (self.matchCriteriaIsDirty && self.restrictions.length >= 2);
        };

        this.resetFn = function() {
            return true;
        };

        this.cancelFn = function() {
            return $q.when(true);
        };

    };

    this.$doCheck = function() {

        if (JSON.stringify(this.oldRestrictions) !== JSON.stringify(this.restrictions)) {
            this.oldRestrictions = lodash.cloneDeep(this.restrictions);

            if (this.restrictions.length != this.orrigRestrictionIds.length) {
                this.restrictionsArrayIsDirty = true;
                this.updateRestrictionsData();
                return;
            }

            var isDirty = false;
            this.restrictions.forEach(function(element, index) {
                if (element.uuid !== this.orrigRestrictionIds[index]) {
                    isDirty = true;
                }
            }.bind(this));
            this.restrictionsArrayIsDirty = isDirty;
            this.updateRestrictionsData();

        }

    };
})

/**
 * @ngdoc directive
 * @name pageRestrictionsEditorModule.pageRestrictionsEditor
 * @restrict E
 * @scope
 * @description
 * The purpose of this directive is to allow the user to manage the restrictions for a given page. The restrictionsEditor has an editable and non-editable mode.
 * It uses the restrictionsTable to display the list of restrictions and it uses the restrictionsPicker to add or remove the restrictions.
 * 
 * @param {= Object} page The page object for the page you want to manage restrictions.
 * @param {Boolean} page.onlyOneRestrictionMustApply The page object for the page you want to manage restrictions.
 * @param {String} page.uuid The uuid of the page. Required if not passing initialRestrictions. Used to fetch and update restrictions for the page.
 * @param {Object} page.uriContext the {@link resourceLocationsModule.object:UriContext uriContext}
 * @param {= Boolean} editable Boolean to determine whether the editor is enabled.
 * @param {= Array=} initialRestrictions An array of initial restrictions to be loaded in the restrictions editor. If initialRestrictions is provided, then the initial fetching of restrictions for the page is prevented, and this array is used instead.
 * @param {= Function=} resetFn Function that returns true. This function is defined in the restrictionsEditor controller and exists only to provide an external callback.
 * @param {= Function=} cancelFn Function that returns a promise. This function is defined in the restrictionsEditor controller and exists only to provide an external callback.
 * @param {= Function=} isDirtyFn Function that returns a boolean. This function is defined in the restrictionsEditor controller and exists only to provide an external callback.
 * @param {& Expression=} onRestrictionsChanged Function that passes '$onlyOneRestrictionMustApply' boolean and an array of '$restrictions' as arguments. The invoker can bind this to a custom function to fetch these values and perform other operations.
 */
.component('pageRestrictionsEditor', {
    templateUrl: 'pageRestrictionsEditorTemplate.html',
    controller: 'pageRestrictionsEditorController',
    scope: {},
    bindings: {
        page: '=',
        editable: '=',
        initialRestrictions: '=?',
        resetFn: '=?',
        cancelFn: '=?',
        isDirtyFn: '=?',
        onRestrictionsChanged: '&?'
    }
});
