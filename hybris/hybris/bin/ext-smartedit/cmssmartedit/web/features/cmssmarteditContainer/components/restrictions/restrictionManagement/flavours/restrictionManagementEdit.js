/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name restrictionManagementEditModule
 * @requires alertServiceModule
 * @requires pageRestrictionsModule
 * @requires restrictionsServiceModule
 * @description
 * This module defines the {@link restrictionManagementEditModule.directive:restrictionManagementEdit restrictionManagementEdit} component.
 **/
angular.module('restrictionManagementEditModule', [
    'alertServiceModule',
    'pageRestrictionsModule',
    'restrictionsServiceModule',
    'functionsModule',
    'componentHandlerServiceModule'
])

.controller('RestrictionManagementEditController', function(
    $q,
    alertService,
    pageRestrictionsFacade,
    restrictionsService,
    URIBuilder,
    cmsitemsUri,
    cmsitemsRestService
) {

    this.ready = false;
    this.restriction = {};
    this.itemManagementMode = 'edit';
    this.contentApi = new URIBuilder(cmsitemsUri).replaceParams(this.uriContext).build();
    this.structureApi = restrictionsService.getStructureApiUri(this.itemManagementMode);

    this._internalInit = function(isRestrictionTypeSupported) {
        this.isTypeSupported = isRestrictionTypeSupported;
        if (isRestrictionTypeSupported) {
            this.submitFn = function() {
                return this.submitInternal().then(function(itemResponse) {

                    alertService.showSuccess({
                        message: 'se.cms.restriction.saved.successful',
                    });

                    return cmsitemsRestService.getById(this.restrictionId).then(function(restriction) {
                        this.restriction = restriction;
                        return $q.when(this.restriction);
                    });

                }.bind(this));
            }.bind(this);
        } else {
            // type not supported, disable the save button always
            this.submitFn = function() {};
            this.isDirtyFn = function() {
                return false;
            };
        }
        this.ready = true;
    }.bind(this);

    this.$onInit = function $onInit() {

        cmsitemsRestService.getById(this.restrictionId).then(function(restriction) {
            this.restriction = restriction;
            return pageRestrictionsFacade.isRestrictionTypeSupported(this.restriction.itemtype).then(function(isSupported) {
                this._internalInit(isSupported);
            }.bind(this));
        }.bind(this));

    };

})

/**
 * @ngdoc directive
 * @name restrictionManagementEditModule.directive:restrictionManagementEdit
 * @restrict E
 * @scope
 * @param {? Function=} isDirtyFn Function returning the dirtiness status of the component.
 * @param {< String} restrictionId Identifier for a given restriction.
 * @param {? Function=} submitFn Function defined in outer scope to validate restriction edit.
 * @param {< Object} uriContext The {@link resourceLocationsModule.object:UriContext uriContext}, as defined on the resourceLocationModule.
 * @description
 * The restrictionManagementEdit Angular component is designed to be able to edit restrictions.
 */
.component('restrictionManagementEdit', {
    controller: 'RestrictionManagementEditController',
    templateUrl: 'restrictionManagementEditTemplate.html',
    bindings: {
        isDirtyFn: '=?',
        restrictionId: '<',
        submitFn: '=?',
        uriContext: '<'
    }
});
