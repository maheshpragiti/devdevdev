/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentMenuModule', ['slotRestrictionsServiceModule', 'componentItemModule', 'componentTypeModule', 'componentSearchModule', 'componentsModule', 'componentServiceModule', 'assetsServiceModule', 'cmsDragAndDropServiceModule'])
    .controller('ComponentMenuController', function($timeout, ComponentService, cmsDragAndDropService, slotRestrictionsService, smartEditBootstrapGateway) {

        this.$onInit = function() {
            this.types = {};

            ComponentService.loadComponentTypes().then(function(response) {
                slotRestrictionsService.getAllComponentTypesSupportedOnPage().then(function(supportedTypes) {
                    response.componentTypes = response.componentTypes.filter(function(componentType) {
                        return supportedTypes.indexOf(componentType.code) > -1;
                    });
                    this.types = response;
                }.bind(this));
            }.bind(this));

        }.bind(this);

        this.loadComponentItems = function(mask, pageSize, currentPage) {
            return ComponentService.loadPagedComponentItems(mask, pageSize, currentPage).then(function(page) {
                $timeout(function() {
                    // Necessary to ensure that elements are draggable. This is especially important after a search.
                    cmsDragAndDropService.update();
                }, 0);
                page.results = page.response;
                delete page.response;
                return page;
            });
        };

        this.preventDefault = function(oEvent) {
            oEvent.stopPropagation();
        };

        this.refreshComponentTypesListener = smartEditBootstrapGateway.subscribe("smartEditReady", function(eventId, data) {
            this.$onInit();
            return $q.when();
        }.bind(this));

        this.$onDestroy = function() {
            this.refreshComponentTypesListener();
        };
    })
    .filter('nameFilter', function() {
        return function(components, criteria) {
            var filterResult = [];
            if (!criteria || criteria.length < 3)
                return components;

            criteria = criteria.toLowerCase();
            var criteriaList = criteria.split(" ");

            (components || []).forEach(function(component) {
                var match = true;
                var term = component.name.toLowerCase();

                criteriaList.forEach(function(item) {
                    if (term.indexOf(item) == -1) {
                        match = false;
                        return false;
                    }
                });

                if (match && filterResult.indexOf(component) == -1) {
                    filterResult.push(component);
                }
            });
            return filterResult;
        };
    })
    /**
     * @ngdoc directive
     * @name componentMenuModule.directive:componentMenu
     * @scope
     * @restrict E
     * @element ANY
     *
     * @description
     * Component Menu widget that shows all the component types and customized components.
     */
    .directive('componentMenu', function($rootScope, systemEventService, $q, $document, assetsService, cmsDragAndDropService, DRAG_AND_DROP_EVENTS) {
        return {
            templateUrl: 'componentMenuTemplate.html',
            restrict: 'E',
            transclude: true,
            $scope: {},
            link: function($scope, elem, attrs) {

                $scope.parentBtn = elem.closest('.ySEHybridAction');

                $scope.status = {
                    isopen: false
                };

                $scope.updateIcon = function() {
                    if ($scope.status.isopen) {
                        $scope.parentBtn.addClass("ySEOpenComponent");

                        cmsDragAndDropService.update();
                    } else {
                        $scope.parentBtn.removeClass("ySEOpenComponent");
                    }
                };

                systemEventService.registerEventHandler('ySEComponentMenuClose', function() {
                    $scope.status.isopen = false;
                    return $q.when();
                });

                $scope.$watch('status.isopen', $scope.updateIcon);

                $scope.preventDefault = function(oEvent) {
                    oEvent.stopPropagation();
                };

                $document.on('click', function(event) {

                    if ($(event.target).parents('.ySEComponentMenuW').length <= 0 && $scope.status.isopen) {

                        $scope.status.isopen = false;
                        $scope.$apply();
                    }
                });

                var dndUnRegFn = systemEventService.registerEventHandler(DRAG_AND_DROP_EVENTS.DRAG_STARTED, function() {
                    $scope.status.isopen = false;
                });

                var overlayUnRegFn = systemEventService.registerEventHandler('OVERLAY_DISABLED', function() {
                    $scope.status.isopen = false;
                });

                $scope.$on('$destroy', function() {
                    dndUnRegFn();
                    overlayUnRegFn();
                });
            }
        };
    });
