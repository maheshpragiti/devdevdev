/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('editorModalServiceModule', ['genericEditorModalServiceModule', 'gatewayProxyModule', 'typeStructureRestServiceModule', 'adminTabModule', 'basicTabModule', 'visibilityTabModule', 'genericTabModule', 'renderServiceModule', 'componentEditorModule', 'cmsitemsRestServiceModule'])
    .factory('editorModalService', function($q, genericEditorModalService, gatewayProxy, renderService, cmsitemsRestService) {

        function EditorModalService() {
            this.gatewayId = 'EditorModal';
            gatewayProxy.initForService(this, ["open", "openAndRerenderSlot"]);
        }

        var tabs = [{
            id: 'genericTab',
            title: 'editortabset.generictab.title',
            templateUrl: 'genericTabTemplate.html'
        }, {
            id: 'basicTab',
            title: 'editortabset.basictab.title',
            templateUrl: 'basicTabTemplate.html'
        }, {
            id: 'adminTab',
            title: 'editortabset.admintab.title',
            templateUrl: 'adminTabTemplate.html',
        }, {
            id: 'visibilityTab',
            title: 'editortabset.visibilitytab.title',
            templateUrl: 'visibilityTabTemplate.html'
        }];

        var _createComponentData = function(componentAttributes, targetSlotId, position) {
            var type;
            try {
                type = componentAttributes.smarteditComponentType.toLowerCase();
            } catch (e) {
                throw "editorModalService._createComponentData - invalid component type in componentAttributes." + e;
            }
            return {
                componentId: componentAttributes.smarteditComponentId,
                componentUuid: componentAttributes.smarteditComponentUuid,
                componentType: componentAttributes.smarteditComponentType,
                title: 'type.' + type + '.name',
                catalogVersionUuid: componentAttributes.catalogVersionUuid,
                targetSlotId: targetSlotId,
                position: targetSlotId ? position : undefined
            };
        };

        // remove admin tab if creating a component
        var _filterTabs = function(componentId) {
            return tabs.filter(function(tab) {
                return componentId || tab.id !== 'adminTab';
            });
        };

        EditorModalService.prototype.openAndRerenderSlot = function(componentType, componentUuid, slotId) {

            var componentAttributes = {
                smarteditComponentType: componentType,
                smarteditComponentUuid: componentUuid
            };

            var componentData = _createComponentData(componentAttributes);
            var filteredTabs = _filterTabs(componentUuid);

            return genericEditorModalService.open(componentData, filteredTabs, function() {
                renderService.renderSlots(slotId);
            });

        };

        EditorModalService.prototype.open = function(_componentAttributes, _targetSlotId, position) {

            return legacyMode(_componentAttributes, _targetSlotId, position).then(function(componentAttributes) {
                var componentData = _createComponentData(componentAttributes, componentAttributes.targetSlotId, position);
                return genericEditorModalService.open(componentData, tabs, function() {
                    if (componentData.componentId) {
                        renderService.renderComponent(componentData.componentId, componentData.componentType);
                    }
                });

            });
        };


        //@deprecated since 6.4
        //old signature of EditorModalService.prototype.open was componentType, componentId
        function legacyMode(_componentAttributes /*is componentType in legacy mode*/ , _targetSlotId /*is componentId in legacy mode*/ , position) {

            var retunedValue;
            if (typeof _componentAttributes == 'string') {

                var componentType = _componentAttributes;
                var componentUID = _targetSlotId !== 0 ? _targetSlotId : undefined;
                if (componentUID) {
                    retunedValue = cmsitemsRestService._getByUIdAndType(componentUID, _componentAttributes).then(function(component) {
                        return {
                            smarteditComponentType: componentType,
                            smarteditComponentId: component.uid,
                            smarteditComponentUuid: component.uuid
                        };
                    });
                } else {
                    retunedValue = {
                        smarteditComponentType: componentType,
                    };
                }
            } else {
                retunedValue = _componentAttributes;
                retunedValue.targetSlotId = _targetSlotId;
            }

            return $q.when(retunedValue);
        }


        return new EditorModalService();
    });
