/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentSearchModule', [])
    .controller('componentSearchController', function($scope, iframeClickDetectionService) {

        this.$onInit = function() {

            this.showResetButton = false;

            iframeClickDetectionService.registerCallback('closeComponentSearch', function() {
                this.closeComponentSearch();
                this.searchTerm = "";
            }.bind(this));

        };

        this.$onChanges = function(changes) {

            if (changes.status && changes.status.isopen) {
                this.searchTerm = "";
            }
        };

        this.$doCheck = function() {

            var oldSearchTerm;
            if (oldSearchTerm !== this.searchTerm) {
                oldSearchTerm = this.searchTerm;
                this.showResetButton = this.searchTerm !== "";
            }
        };

        this.selectSearch = function(oEvent) {
            oEvent.stopPropagation();
        };

        this.preventDefault = function(oEvent) {
            oEvent.stopPropagation();
        };

        this.closeComponentSearch = function(oEvent) {
            if (oEvent) {
                oEvent.preventDefault();
                oEvent.stopPropagation();
            }
            this.status.isopen = false;
        };

    })
    .component('componentSearch', {
        templateUrl: 'componentSearchTemplate.html',
        controller: 'componentSearchController',
        bindings: {
            status: '=',
            searchTerm: '=',
            onSearchFocus: '&',
            onSearchReset: '&'
        }
    });
