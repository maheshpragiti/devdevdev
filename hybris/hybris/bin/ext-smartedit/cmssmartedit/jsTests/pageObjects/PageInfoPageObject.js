/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
module.exports = function() {

    var pageObject = {};

    pageObject.elements = {
        getPageType: function() {
            return browser.findElement('.page-type-code', true);
        },
        getPageTemplate: function() {
            return browser.findElement('.page-template', true);
        },
        getPageInfoMenuToolbarItem: function() {
            return browser.findElement('page-info-menu-toolbar-item', true);
        },
        getPageInfoMenuButton: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('button.ySEComponentMenuW--button'));
        },
        getEditButton: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('.ySEPageInfoStaticInfoContainer button'));
        },
        getPageNameField: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('#name-shortstring'));
        },
        getPageLabelField: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('#label-shortstring'));
        },
        getPageUidField: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('#uid-shortstring'));
        },
        getPageTitleField: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('[data-tab-id="en"] #title-shortstring'));
        },
        getPageCreationTimeField: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('#creationtime input[disabled]'));
        },
        getPageModifiedTimeField: function() {
            return this.getPageInfoMenuToolbarItem().element(by.css('#modifiedtime input[disabled]'));
        },
        getPageEditorModal: function() {
            return browser.findElement('.modal-dialog', true);
        }
    };

    pageObject.actions = {

        openPageInfoMenu: function() {
            return browser.click(pageObject.elements.getPageInfoMenuButton());
        },
        clickEditButton: function() {
            return browser.click(pageObject.elements.getEditButton());
        }
    };

    return pageObject;

}();
