/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
module.exports = (function() {

    var genericEditorPage = {};

    genericEditorPage.constants = {
        // Component Types
        CAROUSEL_COMPONENT_TYPE: "ProductCarouselComponent",

        // Custom View Controllers 
        CUSTOM_VIEW_CONTROLLERS: {
            ProductCarouselComponent: "genericEditor/carouselComponent/customViewController.js"
        }
    };

    genericEditorPage.elements = {
        getOpenGenericEditorButton: function() {
            return element(by.id('openEditorBtn'));
        }
    };

    genericEditorPage.actions = {

        openAndBeReady: function(componentType) {
            var customViewModulePath = genericEditorPage.constants.CUSTOM_VIEW_CONTROLLERS[componentType];

            return browser.bootstrap([{
                value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/util/commonMockedModule/setApparelStagedUKExperience.js\"}",
                key: "applications.setApparelStagedUKExperience"
            }, {
                value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/util/commonMockedModule/goToCustomView.js\"}",
                key: "applications.goToCustomView"
            }, {
                value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/" + customViewModulePath + "\"}",
                key: "applications.customViewModule"
            }]);
        },
        openGenericEditor: function() {
            return browser.click(genericEditorPage.elements.getOpenGenericEditorButton());
        }
    };

    genericEditorPage.assertions = {};

    return genericEditorPage;
}());
