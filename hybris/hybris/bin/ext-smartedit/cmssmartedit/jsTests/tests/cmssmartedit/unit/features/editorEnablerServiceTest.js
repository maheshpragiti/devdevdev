/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
describe("Editor Enabler Service: ", function() {

    // Spy Objects
    var featureService;
    var editorModalService;
    var componentHandlerService;
    var slotRestrictionsService;

    // Service Under Test
    var editorEnablerService;

    var gigo = {
        ctr: 0,
        generate: function() {
            return {
                gigoCount: gigo.ctr++
            };
        }
    };


    // Set-up Module Under Test
    beforeEach(function() {

        // is/has Smartedit module dependencies
        angular.module('featureServiceModule', []);
        angular.module('editorModalServiceModule', []);

        // is cmssmartedit module
        module('slotRestrictionsServiceModule');
        module('componentHandlerServiceModule');

        // mocks/spies
        module(function($provide) {
            featureService = jasmine.createSpyObj("featureService", ["addContextualMenuButton"]);
            $provide.value("featureService", featureService);

            editorModalService = jasmine.createSpyObj("editorModalService", ["open"]);
            $provide.value('editorModalService', editorModalService);

            componentHandlerService = jasmine.createSpyObj("componentHandlerService", ["getOriginalComponent", "getParentSlotForComponent"]);
            $provide.value("componentHandlerService", componentHandlerService);

            slotRestrictionsService = jasmine.createSpyObj("slotRestrictionsService", ["isSlotEditable"]);
            $provide.value("slotRestrictionsService", slotRestrictionsService);
        });

        // module under test
        module('editorEnablerServiceModule');
    });

    beforeEach(inject(function(_editorEnablerService_) {
        editorEnablerService = _editorEnablerService_;
    }));

    // Tests
    it("enableForComponent will add the Edit button to the contextual menu for specified component types", function() {

        var TYPE = "SimpleResponsiveBannerComponent";

        // Act
        editorEnablerService.enableForComponents([TYPE]);

        // Assert
        expect(featureService.addContextualMenuButton).toHaveBeenCalledWith({
            key: 'se.cms.edit',
            nameI18nKey: 'contextmenu.nameI18nKey.edit',
            descriptionI18nKey: 'contextmenu.descriptionI18n.edit',
            regexpKeys: [TYPE],
            displayClass: 'editbutton',
            displayIconClass: 'hyicon hyicon-edit cmsx-ctx__icon-edit',
            displaySmallIconClass: 'hyicon hyicon-edit cmsx-ctx__icon-edit--small',
            i18nKey: 'contextmenu.title.edit',
            callback: editorEnablerService._editButtonCallback,
            condition: editorEnablerService._condition,
            permissions: ['se.context.menu.edit.component']
        });
    });

    it('_editButtonCallback delegates to the editor modal service', function() {

        // Given
        var gigoForComponentAttributes = gigo.generate();

        // Act
        editorEnablerService._editButtonCallback({
            componentAttributes: gigoForComponentAttributes
        });

        // Assert
        expect(editorModalService.open).toHaveBeenCalledWith(gigoForComponentAttributes);
    });

    it('_condition delegates to the componentHandlerService and slotRestrictionsService', function() {

        // Given
        var gigoForElementHandle = gigo.generate();

        var contextualMenuParams = {
            componentId: "someId",
            componentType: "someType",
            element: gigoForElementHandle
        };
        var gigoForisSlotEditable = gigo.generate();
        var mockSlotId = "dummySlotId";
        componentHandlerService.getParentSlotForComponent.and.returnValue(mockSlotId);
        slotRestrictionsService.isSlotEditable.and.returnValue(gigoForisSlotEditable);

        // Act
        var result = editorEnablerService._condition(contextualMenuParams);

        // Assert
        expect(componentHandlerService.getParentSlotForComponent).toHaveBeenCalledWith(gigoForElementHandle);
        expect(slotRestrictionsService.isSlotEditable).toHaveBeenCalledWith(mockSlotId);
        expect(result).toEqual(gigoForisSlotEditable);
    });

});
