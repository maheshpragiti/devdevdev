/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
describe('slotVisibilityService', function() {


    // Service under test
    var slotVisibilityServiceTestInstance;


    // Mock dependencies
    var cmsitemsRestService;
    var pagesContentSlotsComponentsResource;
    var componentHandlerService;
    var renderService;


    // Util
    var $q, $rootScope;

    beforeEach(customMatchers);


    beforeEach(function() {

        angular.module('cmsitemsRestServiceModule', []);
        angular.module('resourceModule', []);
        angular.module('componentHandlerServiceModule', []);
        angular.module('renderServiceModule', []);

        module(function($provide) {

            cmsitemsRestService = jasmine.createSpyObj('cmsitemsRestService', ['get']);
            $provide.value('cmsitemsRestService', cmsitemsRestService);

            pagesContentSlotsComponentsResource = jasmine.createSpyObj('pagesContentSlotsComponentsResource', ['get']);
            $provide.value('pagesContentSlotsComponentsResource', pagesContentSlotsComponentsResource);

            componentHandlerService = jasmine.createSpyObj('componentHandlerService', ['getPageUID']);
            componentHandlerService.getPageUID.and.returnValue('homepage');
            $provide.value('componentHandlerService', componentHandlerService);

            renderService = jasmine.createSpyObj('renderService', ["renderSlots"]);
            renderService.renderSlots.and.returnValue();
            $provide.value("renderService", renderService);

        });
    });

    beforeEach(module('slotVisibilityServiceModule'));

    beforeEach(inject(function(_SlotVisibilityService_, _$q_, _$rootScope_) {
        $q = _$q_;
        slotVisibilityServiceTestInstance = new _SlotVisibilityService_();
        $rootScope = _$rootScope_;
    }));

    afterEach(function() {
        expect(pagesContentSlotsComponentsResource.get).toHaveBeenCalledWith({
            pageId: 'homepage'
        });
    });

    describe('content slots per page is empty', function() {
        var SLOT = 'some-slot-id';
        beforeEach(function() {
            // itemsResource.get.and.returnValue($q.when([]));
            pagesContentSlotsComponentsResource.get.and.returnValue($q.when([]));
        });

        it('should have an empty hidden component list.', function() {
            expect(slotVisibilityServiceTestInstance.getHiddenComponents(SLOT)).toBeResolvedWithData([]);
        });

        it('should have zero hidden components.', function() {
            expect(slotVisibilityServiceTestInstance.getHiddenComponentCount(SLOT)).toBeResolvedWithData(0);
        });
    });

    describe('content slots per page is not empty', function() {
        var SLOT1 = 'some-slot-id-1';
        var SLOT2 = 'some-slot-id-2';

        beforeEach(function() {
            cmsitemsRestService.get.and.returnValue($q.when({
                response: [{
                    visible: false,
                    uuid: 10
                }, {
                    visible: false,
                    uuid: 20
                }, {
                    visible: false,
                    uuid: 30
                }]
            }));
            pagesContentSlotsComponentsResource.get.and.returnValue($q.when({
                pageContentSlotComponentList: [{
                    slotId: SLOT1,
                    componentUuid: 10
                }, {
                    slotId: SLOT1,
                    componentUuid: 20
                }, {
                    slotId: SLOT2,
                    componentUuid: 30
                }]
            }));
        });

        it('should return a non-empty the hidden component list', function() {
            expect(slotVisibilityServiceTestInstance.getHiddenComponents(SLOT1)).toBeResolvedWithData(
                [{
                    visible: false,
                    uuid: 10
                }, {
                    visible: false,
                    uuid: 20
                }]);
        });

        it('should have hidden components on the count.', function() {
            expect(slotVisibilityServiceTestInstance.getHiddenComponentCount(SLOT1)).toBeResolvedWithData(2);
        });
    });

    describe('content slots per page is not empty and the component visibility has changed', function() {
        var SLOT1 = 'some-slot-id-1';
        beforeEach(function() {
            pagesContentSlotsComponentsResource.get.and.returnValue($q.when({
                pageContentSlotComponentList: [{
                    slotId: SLOT1,
                    componentUuid: 10
                }, {
                    slotId: SLOT1,
                    componentUuid: 20
                }]
            }));
        });
        it('should re-render the content slot.', function() {
            cmsitemsRestService.get.and.returnValue($q.when({
                response: [{
                    visible: false,
                    uuid: 10
                }, {
                    visible: false,
                    uuid: 20
                }]
            }));

            slotVisibilityServiceTestInstance.loadHiddenComponents();
            $rootScope.$digest();
            expect(renderService.renderSlots).not.toHaveBeenCalled();

            cmsitemsRestService.get.and.returnValue($q.when({
                response: [{
                    visible: false,
                    uuid: 10
                }, {
                    visible: true,
                    uuid: 20
                }]
            }));

            $rootScope.$digest();
            slotVisibilityServiceTestInstance.loadHiddenComponents();
            $rootScope.$digest();
            expect(renderService.renderSlots).toHaveBeenCalledWith([SLOT1]);
        });
    });
});
