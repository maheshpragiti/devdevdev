/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
describe('componentEditingFacadeModule', function() {

    var fixture;
    var componentEditingFacade;
    var mockRestServiceFactory;
    var mockContentSlotComponentsRestService;
    var mockComponentHandlerService;
    var mockComponentService;
    var mockEditorModalService;
    var mockRenderService;
    var mockRemoveComponentService;
    var mockAlertService;
    var $q;

    var MOCK_COMPONENT_TYPE = "MOCK_COMPONENT_TYPE";
    var MOCK_TARGET_SLOT_UID = "MOCK_TARGET_SLOT_UID";
    var MOCK_TARGET_SLOT_UUID = "MOCK_TARGET_SLOT_UUID";
    var MOCK_EXISTING_COMPONENT_UID = "MOCK_EXISTING_COMPONENT_UID";
    var MOCK_EXISTING_COMPONENT_UUID = "MOCK_EXISTING_COMPONENT_UUID";
    var MOCK_CATALOG_VERSION_UUID = "MOCK_CATALOG_VERSION_UUID";
    var MOCK_PAGE_UID = 'SomePageUID';
    var MOCK_NEW_COMPONENT = {
        uid: 'SomeNewComponentUID'
    };
    var MOCK_ERROR = {
        data: {
            errors: [{
                message: 'Some detailed error message'
            }]
        }
    };
    var MOCK_COMPONENT_ITEM_VISIBLE = {
        visible: true
    };
    var MOCK_COMPONENT_ITEM_HIDDEN = {
        visible: false
    };

    beforeEach(function() {
        fixture = AngularUnitTestHelper.prepareModule('componentEditingFacadeModule')
            .mock('ComponentService', 'addNewComponent')
            .mock('ComponentService', 'removeComponent')
            .mock('ComponentService', 'loadComponentItem')
            .mock('ComponentService', 'addExistingComponent')
            .mock('componentHandlerService', 'getPageUID')
            .mock('restServiceFactory', 'get')
            .mock('editorModalService', 'open')
            .mock('removeComponentService', 'removeComponent')
            .mock('renderService', 'renderSlots')
            .mock('alertService', 'showSuccess')
            .mock('alertService', 'showDanger')
            .mockConstant('PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', 'PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI')
            .withTranslations({
                'cmsdraganddrop.error': 'Failed to move {{sourceComponentId}} to slot {{targetSlotId}}: {{detailedError}}',
                'cmsdraganddrop.move.failed': 'Failed to move {{componentID}} to slot {{slotID}}',
                'cmsdraganddrop.success': 'The component {{sourceComponentId}} has been successfully added to slot {{targetSlotId}}',
                'cmsdraganddrop.success.but.hidden': 'The component {{sourceComponentId}} has been successfully added to slot {{targetSlotId}} but is hidden'
            })
            .service('componentEditingFacade');

        componentEditingFacade = fixture.service;
        mockRestServiceFactory = fixture.mocks.restServiceFactory;
        mockComponentHandlerService = fixture.mocks.componentHandlerService;
        mockComponentService = fixture.mocks.ComponentService;
        mockEditorModalService = fixture.mocks.editorModalService;
        mockRenderService = fixture.mocks.renderService;
        mockRemoveComponentService = fixture.mocks.removeComponentService;
        mockAlertService = fixture.mocks.alertService;
        $q = fixture.injected.$q;
    });

    beforeEach(function() {
        mockContentSlotComponentsRestService = jasmine.createSpyObj('mockContentSlotComponentsRestService', ['update']);
        mockRestServiceFactory.get.and.returnValue(mockContentSlotComponentsRestService);
    });

    describe('addNewComponentToSlot', function() {
        it('should get the current page UID by delegating to the componentHandlerService', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockComponentService.addNewComponent.and.returnValue($q.when(MOCK_NEW_COMPONENT));
            mockEditorModalService.open.and.returnValue($q.when());
            mockRenderService.renderSlots.and.returnValue($q.when());
            var slotInfo = {
                targetSlotId: MOCK_TARGET_SLOT_UID,
                targetSlotUUId: MOCK_TARGET_SLOT_UUID
            };

            // Act
            componentEditingFacade.addNewComponentToSlot(slotInfo, MOCK_COMPONENT_TYPE, 1);
            fixture.detectChanges();

            // Assert
            expect(mockComponentHandlerService.getPageUID).toHaveBeenCalled();
        });


        it('should open the component editor in a modal', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockEditorModalService.open.and.returnValue($q.when());
            var slotInfo = {
                targetSlotId: MOCK_TARGET_SLOT_UID,
                targetSlotUUId: MOCK_TARGET_SLOT_UUID
            };
            var expectedData = {
                smarteditComponentType: MOCK_COMPONENT_TYPE,
                catalogVersionUuid: MOCK_CATALOG_VERSION_UUID,
            };

            // Act
            componentEditingFacade.addNewComponentToSlot(slotInfo, MOCK_CATALOG_VERSION_UUID, MOCK_COMPONENT_TYPE, 1);
            fixture.detectChanges();

            // Assert
            expect(mockEditorModalService.open).toHaveBeenCalledWith(expectedData, MOCK_TARGET_SLOT_UUID, 1);
        });

    });

    describe('addExistingComponentToSlot', function() {

        var dragInfo = {
            componentId: MOCK_EXISTING_COMPONENT_UID,
            componentUuid: MOCK_EXISTING_COMPONENT_UUID
        };

        it('should get the current page UID by delegating to the componentHandlerService', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockComponentService.addExistingComponent.and.returnValue($q.when());
            mockComponentService.loadComponentItem.and.returnValue($q.when(MOCK_COMPONENT_ITEM_VISIBLE));
            mockRenderService.renderSlots.and.returnValue($q.when());

            // Act
            componentEditingFacade.addExistingComponentToSlot(MOCK_TARGET_SLOT_UID, dragInfo, 1);
            fixture.detectChanges();

            // Assert
            expect(mockComponentHandlerService.getPageUID).toHaveBeenCalled();
        });

        it('should delegate to componentService to add the existing component to the slot and show a success message if the component is visible', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockComponentService.addExistingComponent.and.returnValue($q.when());
            mockComponentService.loadComponentItem.and.returnValue($q.when(MOCK_COMPONENT_ITEM_VISIBLE));
            mockRenderService.renderSlots.and.returnValue($q.when());

            // Act
            componentEditingFacade.addExistingComponentToSlot(MOCK_TARGET_SLOT_UID, dragInfo, 1);
            fixture.detectChanges();

            // Assert
            expect(mockComponentService.addExistingComponent).toHaveBeenCalledWith(MOCK_PAGE_UID, MOCK_EXISTING_COMPONENT_UID, MOCK_TARGET_SLOT_UID, 1);
            expect(mockAlertService.showSuccess).toHaveBeenCalledWith({
                message: 'The component MOCK_EXISTING_COMPONENT_UID has been successfully added to slot MOCK_TARGET_SLOT_UID'
            });
        });

        it('should delegate to componentService to add the existing component to the slot and show a success but hidden message if the component is hidden', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockComponentService.addExistingComponent.and.returnValue($q.when());
            mockComponentService.loadComponentItem.and.returnValue($q.when(MOCK_COMPONENT_ITEM_HIDDEN));
            mockRenderService.renderSlots.and.returnValue($q.when());

            // Act
            componentEditingFacade.addExistingComponentToSlot(MOCK_TARGET_SLOT_UID, dragInfo, 1);
            fixture.detectChanges();

            // Assert
            expect(mockComponentService.addExistingComponent).toHaveBeenCalledWith('SomePageUID', MOCK_EXISTING_COMPONENT_UID, MOCK_TARGET_SLOT_UID, 1);
            expect(mockAlertService.showSuccess).toHaveBeenCalledWith({
                message: 'The component MOCK_EXISTING_COMPONENT_UID has been successfully added to slot MOCK_TARGET_SLOT_UID but is hidden'
            });
        });

        it('should delegate to the renderService to re-render the slot the componentService has successfully added the existing component to the slot', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockComponentService.addExistingComponent.and.returnValue($q.when());
            mockComponentService.loadComponentItem.and.returnValue($q.when(MOCK_COMPONENT_ITEM_VISIBLE));
            mockRenderService.renderSlots.and.returnValue($q.when());

            // Act
            componentEditingFacade.addExistingComponentToSlot(MOCK_TARGET_SLOT_UID, dragInfo, 1);
            fixture.detectChanges();

            // Assert
            expect(mockRenderService.renderSlots).toHaveBeenCalledWith(MOCK_TARGET_SLOT_UID);
        });

        it('should push an alert if adding the existing component to the slot fails', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockComponentService.addExistingComponent.and.returnValue($q.reject(MOCK_ERROR));

            // Act
            componentEditingFacade.addExistingComponentToSlot(MOCK_TARGET_SLOT_UID, dragInfo, 1);
            fixture.detectChanges();

            // Assert
            expect(mockAlertService.showDanger).toHaveBeenCalledWith({
                message: 'Failed to move MOCK_EXISTING_COMPONENT_UID to slot MOCK_TARGET_SLOT_UID: Some detailed error message'
            });
        });
    });

    describe('moveComponent', function() {
        it('should delegate to the slot update REST service to update the slot', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockContentSlotComponentsRestService.update.and.returnValue($q.when());
            mockRenderService.renderSlots.and.returnValue($q.when());

            // Act
            componentEditingFacade.moveComponent('SomeSourceSlotUID', MOCK_TARGET_SLOT_UID, MOCK_EXISTING_COMPONENT_UID, 1);
            fixture.detectChanges();

            // Assert
            expect(mockContentSlotComponentsRestService.update).toHaveBeenCalledWith({
                pageId: MOCK_PAGE_UID,
                currentSlotId: 'SomeSourceSlotUID',
                componentId: MOCK_EXISTING_COMPONENT_UID,
                slotId: MOCK_TARGET_SLOT_UID,
                position: 1
            });
        });

        it('should delegate to the renderService to re-render both changed slots', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockContentSlotComponentsRestService.update.and.returnValue($q.when());
            mockRenderService.renderSlots.and.returnValue($q.when());

            // Act
            componentEditingFacade.moveComponent('SomeSourceSlotUID', MOCK_TARGET_SLOT_UID, MOCK_EXISTING_COMPONENT_UID, 1);
            fixture.detectChanges();

            // Assert
            expect(mockRenderService.renderSlots).toHaveBeenCalledWith(['SomeSourceSlotUID', MOCK_TARGET_SLOT_UID]);
        });

        it('should push an alert if updating the slot via the slot update REST service fails', function() {
            // Arrange
            mockComponentHandlerService.getPageUID.and.returnValue(MOCK_PAGE_UID);
            mockContentSlotComponentsRestService.update.and.returnValue($q.reject());

            // Act
            componentEditingFacade.moveComponent('SomeSourceSlotUID', MOCK_TARGET_SLOT_UID, MOCK_EXISTING_COMPONENT_UID, 1);
            fixture.detectChanges();

            // Assert
            expect(mockAlertService.showDanger).toHaveBeenCalledWith({
                message: 'Failed to move MOCK_EXISTING_COMPONENT_UID to slot MOCK_TARGET_SLOT_UID'
            });
        });
    });

});
