/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

describe('Clone Page Wizard', function() {

    var clonePageWizard = e2e.pageObjects.clonePageWizard;

    beforeEach(function(done) {
        browser.bootstrap([{
            value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/util/commonMockedModule/goToApparelStagedUK.js\"}",
            key: "applications.goToApparelStagedUK"
        }, {
            value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/clonePageWizard/customMocks.js\"}",
            key: "applications.customMocksModule"
        }]);
        clonePageWizard.actions.openAndBeReady(done);

    });

    describe('Clone Option Step', function() {

        it('GIVEN the wizard is opened to the Clone Page tab WHEN "Clone" is clicked on the toolbar THEN the Clone Page wizard is opened to the "Clone Options" step and "Variation" is selected by default', function() {
            expect(clonePageWizard.util.isWindowOpen()).toBeTruthy();
            clonePageWizard.assertions.currentStepTextIs('CLONE OPTIONS');

            expect(clonePageWizard.elements.getSelectedConditionOption().getText()).toBe("Variation");
        });

    });

    describe('Next Step Information page condition "Variation" is selected in "Clone Options" step', function() {

        beforeEach(function(done) {
            clonePageWizard.actions.clickNext().then(function() {
                done();
            });
        });

        it('GIVEN the user is on the Clone Options step selected Variation page condition WHEN he is on the Information step THEN the page label field is disabled', function() {
            clonePageWizard.assertions.currentStepTextIs('INFORMATION');

            clonePageWizard.assertions.contentPageLabelFieldEnabledToBeFalse();

            expect(clonePageWizard.elements.getNameFieldText()).toBe('Homepage');
            expect(clonePageWizard.elements.getLabelFieldText()).toBe('MyLittlePrimary');
            expect(clonePageWizard.elements.getUidFieldText()).toBe('');
        });
    });

    describe('Next Step Information page condition "Primary" is selected in "Clone Options" step', function() {

        beforeEach(function(done) {

            clonePageWizard.actions.selectPrimaryCondition().then(function() {})
                .then(function() {
                    clonePageWizard.actions.clickNext();
                })
                .then(function() {
                    done();
                });
        });

        it('GIVEN the user is on the Clone Options step selected Primary page condition WHEN he is on the Information step THEN the page label field is enabled', function() {
            clonePageWizard.assertions.currentStepTextIs('INFORMATION');

            clonePageWizard.assertions.contentPageLabelFieldEnabledToBeTrue();

            expect(clonePageWizard.elements.getNameFieldText()).toBe('Homepage');
            expect(clonePageWizard.elements.getLabelFieldText()).toBe('i-love-pandas_copy');
            expect(clonePageWizard.elements.getUidFieldText()).toBe('');
        });
    });

    describe('3rd Step Restrictions page condition "Variation" selected from initial step', function() {

        beforeEach(function(done) {
            clonePageWizard.actions.clickNext().then(function() {})
                .then(function() {
                    clonePageWizard.actions.clickNext();
                })
                .then(function() {
                    done();
                });
        });

        it('GIVEN the user is on the Clone Options step WHEN he clicks next THEN he is presented with the Page Restrictions step that has two restrictions coming from the page being cloned', function() {
            clonePageWizard.assertions.currentStepTextIs('RESTRICTIONS');

            var restrictionLabels = clonePageWizard.elements.getRestrictionsLabelsList();
            expect(restrictionLabels.get(0).getText()).toBe("SOME TIME RESTRICTION A");
            expect(restrictionLabels.get(1).getText()).toBe("ANOTHER TIME B");
        });

    });

    describe('Create Clone Page full save operation', function() {

        beforeEach(function(done) {
            clonePageWizard.actions.clickNext().then(function() {})
                .then(function() {
                    done();
                });
        });

        it('GIVEN the user is on the Information step and enter wrong uid WHEN he clicks done THEN then a validation error is throw and INFORMATION tab is shown', function() {

            clonePageWizard.actions.enterTextInUidField();

            clonePageWizard.actions.clickNext().then(function() {
                clonePageWizard.actions.submit().then(function() {
                    clonePageWizard.assertions.currentStepTextIs('INFORMATION');
                    expect(clonePageWizard.elements.getUidErrorsText()).toBe('No Trump jokes plz.');
                    clonePageWizard.assertions.assertWindowIsOpen();
                });
            });
        });

        it('GIVEN the user is on the Restrictions step WHEN he clicks done THEN the wizard is removed', function() {
            clonePageWizard.actions.clickNext().then(function() {
                clonePageWizard.actions.submit().then(function() {
                    clonePageWizard.assertions.assertWindowIsClosed();
                });
            });
        });
    });
});
