/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('customViewModule', ['sharedDataServiceModule', 'backendMocksUtilsModule', 'yLoDashModule'])
    .constant('PATH_TO_CUSTOM_VIEW', 'restrictionsEditor/customView.html')
    .controller('customViewController', function($filter, sharedDataService, backendMocksUtils, lodash) {
        this.pageType = sessionStorage.getItem("pageType");

        this.editable = true;
        this.page = {
            "type": "contentPageData",
            "uid": "add-edit-address",
            "uuid": "add-edit-address",
            "typeCode": this.pageType,
            "onlyOneRestrictionMustApply": false,
            "creationtime": "2016-07-15T23:35:21+0000",
            "defaultPage": true,
            "modifiedtime": "2016-07-15T23:38:01+0000",
            "name": "Add Edit Address Page",
            "pk": "8796095743024",
            "template": "AccountPageTemplate",
            "title": {
                "de": "Adresse hinzufügen/bearbeiten"
            },
            "label": "add-edit-address",
            "restrictions": []
        };

        this.includeRestrictions = sessionStorage.getItem("existingRestrictions");
        if (this.includeRestrictions === null || this.includeRestrictions === 'false') {

            var items = JSON.parse(sessionStorage.getItem("componentMocks"));

            items.componentItems.forEach(function(item) {
                if (item.uuid === "add-edit-address") {
                    item.restrictions = [];
                }
            });

            sessionStorage.setItem("componentMocks", JSON.stringify(items));
        }

        backendMocksUtils.getBackendMock('componentPOSTMock').respond(function(method, url, data) {
            var parsedData = JSON.parse(data);

            switch (parsedData.itemtype) {
                case "CMSTimeRestriction":
                    var dateFrom = $filter('date')(parsedData.activeFrom, 'MM/dd/yy HH:mm a');
                    var dateUntil = $filter('date')(parsedData.activeUntil, 'MM/dd/yy HH:mm a');
                    parsedData.uid = 'restriction-time-1';
                    parsedData.uuid = 'restriction-time-1';
                    parsedData.description = "Page only applies from " + dateFrom + " to " + dateUntil;
                    break;
                case "CMSCategoryRestriction":
                    parsedData.uid = 'restriction-category-1';
                    parsedData.uuid = 'restriction-category-1';
                    parsedData.description = getCategoryRestrictionDescription(parsedData.categories);
                    break;
                case "CMSUserGroupRestriction":
                    parsedData.uid = 'restriction-usergroup-1';
                    parsedData.uuid = 'restriction-usergroup-1';
                    parsedData.description = getUserGroupRestrictionDescription(parsedData.userGroups);
                    break;
            }

            addRestrictionToList(parsedData);

            return [201, parsedData];
        });

        backendMocksUtils.getBackendMock('componentPUTMock').respond(function(method, url, data) {
            var parsedData = JSON.parse(data);

            switch (parsedData.itemtype) {
                case "CMSTimeRestriction":
                    var dateFrom = $filter('date')(parsedData.activeFrom, 'MM/dd/yy HH:mm a');
                    var dateUntil = $filter('date')(parsedData.activeUntil, 'MM/dd/yy HH:mm a');
                    parsedData.description = "Page only applies from " + dateFrom + " to " + dateUntil;
                    break;
                case "CMSCategoryRestriction":
                    parsedData.description = getCategoryRestrictionDescription(parsedData.categories);
                    break;
                case "CMSUserGroupRestriction":
                    parsedData.description = getUserGroupRestrictionDescription(parsedData.userGroups);
                    break;
            }

            addRestrictionToList(parsedData);

            return [201, parsedData];
        });

        // Helpers
        function addRestrictionToList(newRestriction) {
            var items = JSON.parse(sessionStorage.getItem("componentMocks"));

            lodash.remove(items.componentItems, function(item) {
                return item.uuid === newRestriction.uuid;
            });

            items.componentItems.push(newRestriction);

            sessionStorage.setItem("componentMocks", JSON.stringify(items));
        }

        this.dialogSaveFn = function() {
            this.saveResult = (this.isDirtyFn()) ?
                'Save executed.' : 'Save cannot be executed. Editor not dirty.';
        };
    });

try {
    angular.module('smarteditcontainer').requires.push('customViewModule');
} catch (e) {}
