/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
module.exports = (function() {
    var pageObject = {};
    pageObject.actions = {
        openAndBeReady: function() {
            return browser.bootstrap([{
                value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/util/commonMockedModule/setApparelStagedUKExperience.js\"}",
                key: "applications.setApparelStagedUKExperience"
            }, {
                value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/util/commonMockedModule/goToCustomView.js\"}",
                key: "applications.goToCustomView"
            }, {
                value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/genericEditor/customViewController.js\"}",
                key: "applications.customViewModule"
            }, {
                value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/genericEditor/componentCMSLink/customViewHTML.js\"}",
                key: "applications.customViewHTML"
            }]);
        }
    };
    return pageObject;
})();
