/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module('customMocksModule', ['backendMocksUtilsModule'])
    .run(function($httpBackend, backendMocksUtils) {

        $httpBackend
            .whenGET(/\/cmswebservices\/v1\/sites\/apparel-uk\/catalogs\/apparel-ukContentCatalog\/versions\/Staged\/pages\?defaultPage=true&typeCode=ContentPage/)
            .respond({
                pages: [{
                    uid: 'primaryContentPage',
                    name: 'My Little Primary Content Page',
                    defaultPage: true,
                    template: "LandingPage2Template",
                    typeCode: "ContentPage",
                    label: "MyLittlePrimary"
                }, {
                    uid: 'someOtherPrimaryPageContent',
                    name: 'Some Other Primary Content Page',
                    defaultPage: true,
                    template: "LandingPage2Template",
                    typeCode: "ContentPage",
                    label: "SomeOtherPrimary"
                }, {
                    uid: 'anotherPrimaryPageContent',
                    name: 'Another Primary Content Page',
                    defaultPage: true,
                    template: "LandingPage2Template",
                    typeCode: "ContentPage",
                    label: "AnotherPrimary"
                }]
            });

        $httpBackend.whenGET(/cmswebservices\/v1\/sites\/apparel-uk\/catalogs\/apparel-ukContentCatalog\/versions\/Staged$/).respond({
            "name": {
                "en": "Apparel UK Content Catalog"
            },
            "pageDisplayConditions": [{
                "options": [{
                    "label": "page.displaycondition.variation",
                    "id": "VARIATION"
                }],
                "typecode": "ProductPage"
            }, {
                "options": [{
                    "label": "page.displaycondition.primary",
                    "id": "PRIMARY"
                }],
                "typecode": "CategoryPage"
            }, {
                "options": [{
                    "label": "page.displaycondition.primary",
                    "id": "PRIMARY"
                }, {
                    "label": "page.displaycondition.variation",
                    "id": "VARIATION"
                }],
                "typecode": "ContentPage"
            }],
            "uid": "apparel-ukContentCatalog",
            "version": "Online"
        });



        backendMocksUtils.getBackendMock('componentPOSTMock').respond(function(method, url, data) {

            var dataObject = angular.fromJson(data);
            if (dataObject.restrictions[0] === "timeRestrictionIdA" && dataObject.restrictions[1] === "timeRestrictionIdB") {

                if (dataObject.uid === 'trump') {
                    return [400, {
                        "errors": [{
                            "message": "No Trump jokes plz.",
                            "reason": "invalid",
                            "subject": "uid",
                            "subjectType": "parameter",
                            "type": "ValidationError"
                        }]
                    }];
                }

                return [200, {
                    uid: 'valid'
                }];
            } else if (dataObject.restrictions.length < 2 || dataObject.restrictions.length > 2) {

                return [400, {
                    "errors": [{
                        "message": "There were 2 restrictions onscreen that didn't make it into the REST payload"
                    }]
                }];
            } else {
                return [400, {
                    "errors": [{
                        "message": "Unknown error"
                    }]
                }];
            }
        });

        $httpBackend
            .whenGET(/\/cmswebservices\/v1\/sites\/apparel-uk\/catalogs\/apparel-ukContentCatalog\/versions\/Staged\/pagesrestrictions\?pageId=homepage/)
            .respond({
                pageRestrictionList: [{
                    pageId: "homepage",
                    restrictionId: "timeRestrictionIdA"
                }, {
                    pageId: "homepage",
                    restrictionId: "timeRestrictionIdB"
                }]
            });
    });
try {
    angular.module('smarteditloader').requires.push('customMocksModule');
} catch (e) {}
try {
    angular.module('smarteditcontainer').requires.push('customMocksModule');
} catch (e) {}
