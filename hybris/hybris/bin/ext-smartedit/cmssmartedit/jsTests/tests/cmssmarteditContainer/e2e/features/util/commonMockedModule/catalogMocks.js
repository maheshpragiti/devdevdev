/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('catalogMocks', ['ngMockE2E'])
    .run(function($httpBackend) {

        $httpBackend.whenGET(/cmssmarteditwebservices\/v1\/sites\/.*\/contentcatalogs/).respond({
            "catalogs": [{
                "catalogId": "apparel-ukContentCatalog",
                "name": {
                    "en": "Apparel UK Content Catalog"
                },
                "versions": [{
                    "active": false,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }, {
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "apparel-ukContentCatalog/Staged",
                    "version": "Staged"
                }, {
                    "active": true,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }, {
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "apparel-ukContentCatalog/Online",
                    "version": "Online"
                }]
            }, {
                "catalogId": "electronicsProductCatalog",
                "name": {
                    "en": "Electronics Product Catalog",
                },
                "versions": [{
                    "active": true,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "electronicsProductCatalog/Online",
                    "version": "Online"
                }, {
                    "active": false,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "electronicsProductCatalog/Staged",
                    "version": "Staged"
                }]
            }, {
                "catalogId": "apparel-deContentCatalog",
                "name": {
                    "en": "Apparel DE Content Catalog",
                    "de": "Deutscher Produktkatalog Kleidung"
                },
                "versions": [{
                    "active": false,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }, {
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "apparel-deContentCatalog/Staged",
                    "version": "Staged"
                }, {
                    "active": true,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }, {
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "apparel-deContentCatalog/Online",
                    "version": "Online"
                }]
            }, {
                "catalogId": "electronicsContentCatalog",
                "name": {
                    "en": "Electronics Content Catalog",
                },
                "versions": [{
                    "active": true,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }, {
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "electronicsContentCatalog/Online",
                    "version": "Online"
                }, {
                    "active": false,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }, {
                            "label": "page.displaycondition.variation",
                            "value": "VARIATION"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "electronicsContentCatalog/Staged",
                    "version": "Staged"
                }]
            }, {
                "catalogId": "Default",
                "name": {
                    "en": "default catalog",
                    "de": "Default-Katalog"
                },
                "versions": [{
                    "active": true,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "Default/Online",
                    "version": "Online"
                }, {
                    "active": false,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "Default/Staged",
                    "version": "Staged"
                }]
            }, {
                "catalogId": "apparelProductCatalog",
                "name": {
                    "en": "Apparel Product Catalog",
                    "de": "Produktkatalog Kleidung"
                },
                "versions": [{
                    "active": true,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "apparelProductCatalog/Online",
                    "version": "Online"
                }, {
                    "active": false,
                    "pageDisplayConditions": [{
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ProductPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "CategoryPage"
                    }, {
                        "options": [{
                            "label": "page.displaycondition.primary",
                            "value": "PRIMARY"
                        }],
                        "typecode": "ContentPage"
                    }],
                    "uuid": "apparelProductCatalog/Staged",
                    "version": "Staged"
                }]
            }]
        });
    });
try {
    angular.module('smarteditloader').requires.push('catalogMocks');
} catch (e) {}
try {
    angular.module('smarteditcontainer').requires.push('catalogMocks');
} catch (e) {}
