/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('siteAndCatalogsMocks', ['ngMockE2E', 'functionsModule'])
    .run(function($httpBackend, parseQuery) {


        $httpBackend.whenGET(/cmswebservices\/v1\/sites$/).respond({
            sites: [{
                previewUrl: '/jsTests/tests/cmssmarteditContainer/e2e/features/dummystorefront.html',
                name: {
                    en: "Apparels"
                },
                redirectUrl: 'redirecturlApparels',
                uid: 'apparel-uk'
            }, {
                previewUrl: '/jsTests/tests/cmssmarteditContainer/e2e/features/dummystorefront.html',
                name: {
                    en: "Apparels"
                },
                redirectUrl: 'redirecturlApparels',
                uid: 'apparel-de'
            }]
        });

        $httpBackend.whenGET(/cmssmarteditwebservices\/v1\/sites\/apparel-uk\/contentcatalogs/).respond({
            catalogs: [{
                catalogId: "apparel-ukContentCatalog",
                name: {
                    en: "Apparel UK Content Catalog"
                },
                versions: [{
                    version: "Online",
                    active: true
                }, {
                    version: "Staged",
                    active: false
                }]
            }]
        });

        $httpBackend.whenGET(/cmssmarteditwebservices\/v1\/sites\/apparel-de\/contentcatalogs/).respond({
            catalogs: [{
                catalogId: "apparel-deContentCatalog",
                name: {
                    en: "Apparel DE Content Catalog"
                },
                versions: [{
                    version: "Online",
                    active: true
                }, {
                    version: "Staged",
                    active: false
                }]
            }]
        });

        $httpBackend.whenPOST(/thepreviewTicketURI/)
            .respond({
                ticketId: 'dasdfasdfasdfa',
                resourcePath: document.location.origin + '/jsTests/tests/cmssmarteditContainer/e2e/features/dummystorefront.html'
            });


        $httpBackend.whenGET(/view/).passThrough(); //calls to storefront render API
        $httpBackend.whenPUT(/contentslots/).passThrough();
        $httpBackend.whenGET(/\.html/).passThrough();
    });
try {
    angular.module('smarteditloader').requires.push('siteAndCatalogsMocks');
} catch (e) {}
try {
    angular.module('smarteditcontainer').requires.push('siteAndCatalogsMocks');
} catch (e) {}
