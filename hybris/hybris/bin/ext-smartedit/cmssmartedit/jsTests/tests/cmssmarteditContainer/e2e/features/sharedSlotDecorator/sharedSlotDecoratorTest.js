/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
// /*
//  * [y] hybris Platform
//  *
//  * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
//  *
//  * This software is the confidential and proprietary information of SAP
//  * ("Confidential Information"). You shall not disclose such Confidential
//  * Information and shall use it only in accordance with the terms of the
//  * license agreement you entered into with SAP.
//  */

describe('sharedSlotDecorator', function() {
    var perspective = e2e.componentObjects.modeSelector;
    var storefront = e2e.componentObjects.storefront;
    var disabledSlotDecorator = e2e.componentObjects.disabledSlotDecorator;
    var contextualMenu = e2e.componentObjects.componentContextualMenu;

    var TOP_HEADER_SLOT_ID = storefront.constants.TOP_HEADER_SLOT_ID;
    var BOTTOM_HEADER_SLOT_ID = storefront.constants.BOTTOM_HEADER_SLOT_ID;
    var COMPONENT1_ID = storefront.constants.COMPONENT1_NAME;
    var COMPONENT4_ID = storefront.constants.COMPONENT4_NAME;

    beforeEach(function() {
        browser.bootstrap([{
            value: "{\"smartEditContainerLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/util/commonMockedModule/goToApparelStagedUK.js\"}",
            key: "applications.goToApparelStagedUK"
        }, {
            value: "{\"smartEditLocation\":\"/jsTests/tests/cmssmarteditContainer/e2e/features/sharedSlotDecorator/mockDataOverrides.js\"}",
            key: "applications.mockDataOverridesModule"
        }]);
    });

    beforeEach(function(done) {
        browser.waitForWholeAppToBeReady().then(function() {
            done();
        });
    });

    describe(' basic edit mode - ', function() {

        beforeEach(function(done) {
            perspective.select(perspective.BASIC_CMS_PERSPECTIVE).then(function() {
                browser.switchToIFrame().then(function() {
                    done();
                });
            });
        });

        xit('GIVEN basic edit mode is selected' +
            'THEN the shared slot decorator is only shown for shared slots',
            function() {
                // THEN
                expect(disabledSlotDecorator.elements.slotDisabledDecorator(TOP_HEADER_SLOT_ID).isPresent()).toBe(true, 'Expected shared slot to be disabled');
                expect(disabledSlotDecorator.elements.slotDisabledDecorator(BOTTOM_HEADER_SLOT_ID).isPresent()).toBe(false, 'Expected non-shared slot to be enabled');
            });

        it('GIVEN basic edit mode is selected ' +
            'WHEN a component in a shared slot is hovered ' +
            'THEN its contextual menu buttons are not displayed',
            function() {
                // WHEN
                storefront.actions.moveToComponent(COMPONENT1_ID);

                // THEN
                contextualMenu.elements.getRemoveButtonForComponentId(COMPONENT1_ID).then(function(removeButton) {
                    contextualMenu.elements.getMoveButtonForComponentId(COMPONENT1_ID).then(function(moveButton) {
                        contextualMenu.elements.getEditButtonForComponentId(COMPONENT1_ID).then(function(editButton) {
                            expect(removeButton.isPresent()).toBe(false, 'Expected remove button not to be available');
                            expect(moveButton.isPresent()).toBe(false, 'Expected move button not to be available');
                            expect(editButton.isPresent()).toBe(false, 'Expected edit button not to be available');
                        });
                    });
                });
            });

        it('GIVEN basic edit mode is selected ' +
            'WHEN a component in a non-shared slot is hovered ' +
            'THEN its contextual menu buttons are displayed',
            function() {
                // WHEN
                storefront.actions.moveToComponent(COMPONENT4_ID);

                // THEN
                contextualMenu.elements.getRemoveButtonForComponentId(COMPONENT4_ID).then(function(removeButton) {
                    contextualMenu.elements.getMoveButtonForComponentId(COMPONENT4_ID).then(function(moveButton) {
                        contextualMenu.elements.getEditButtonForComponentId(COMPONENT4_ID).then(function(editButton) {
                            expect(removeButton.isPresent()).toBe(true, 'Expected remove button to be available');
                            expect(moveButton.isPresent()).toBe(true, 'Expected move button to be available');
                        });
                    });
                });
            });

        it('GIVEN basic edit mode is selected ' +
            'WHEN a component in a shared slot is hovered ' +
            'THEN the decorator tooltip is displayed',
            function() {
                // GIVEN
                expect(disabledSlotDecorator.elements.slotDisabledTooltip(TOP_HEADER_SLOT_ID).isPresent()).toBe(false);

                // WHEN
                storefront.actions.moveToComponent(COMPONENT1_ID);

                // THEN
                expect(disabledSlotDecorator.elements.slotDisabledTooltip(TOP_HEADER_SLOT_ID).isDisplayed()).toBe(true, 'Expected tooltip to be displayed.');
            });

    });

    describe(' advanced edit mode - ', function() {

        beforeEach(function(done) {
            perspective.select(perspective.ADVANCED_CMS_PERSPECTIVE).then(function() {
                browser.switchToIFrame().then(function() {
                    done();
                });
            });
        });

        it('GIVEN advanced edit mode is selected' +
            'THEN the shared slot decorator is not shown for shared slots',
            function() {
                // THEN
                expect(disabledSlotDecorator.elements.slotDisabledDecorator(TOP_HEADER_SLOT_ID).isPresent()).toBe(false, 'Expected shared slot not to be disabled');
                expect(disabledSlotDecorator.elements.slotDisabledDecorator(BOTTOM_HEADER_SLOT_ID).isPresent()).toBe(false, 'Expected non-shared slot to be enabled');
            });
    });
});
