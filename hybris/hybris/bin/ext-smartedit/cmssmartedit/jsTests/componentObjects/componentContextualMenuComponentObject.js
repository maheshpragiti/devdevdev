/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
module.exports = (function() {

    var componentObject = {};

    componentObject.constants = {
        MENU_ITEM_MOVE: 'move',
        MENU_ITEM_DELETE: 'delete',
        MENU_ITEM_EDIT: 'edit'
    };

    componentObject.elements = {
        getRemoveButtonForComponentId: function(componentId) {
            return browser.switchToIFrame().then(function() {
                return element(by.css("#smarteditoverlay .smartEditComponentX[data-smartedit-component-id='" + componentId + "'] .removebutton"));
            });
        },
        getMoveButtonForComponentId: function(componentId) {
            return browser.switchToIFrame().then(function() {
                return element(by.css("#smarteditoverlay .smartEditComponentX[data-smartedit-component-id='" + componentId + "'] .movebutton"));
            });
        },
        getEditButtonForComponentId: function(componentId) {
            return browser.switchToIFrame().then(function() {
                return element(by.css("#smarteditoverlay .smartEditComponentX[data-smartedit-component-id='" + componentId + "'] .editbutton"));
            });
        },
        getNumContextualMenuItemsForComponentId: function(componentId) {
            return element.all(by.css('.smartEditComponentX[data-smartedit-component-id="' + componentId + '"] .cmsx-ctx-btns')).count();
        }
    };

    componentObject.actions = {
        clickRemoveButton: function(componentId) {
            return browser.switchToIFrame().then(function() {
                return componentObject.elements.getRemoveButtonForComponentId(componentId).then(function(removeButton) {
                    return browser.actions()
                        .mouseMove(removeButton)
                        .click()
                        .perform();
                });
            });
        }
    };

    componentObject.assertions = {
        removeMenuItemForComponentIdLoadedRightImg: function(componentID) {
            componentObject.elements.getRemoveButtonForComponentId(componentID).then(function(removeButton) {
                expect(removeButton.getAttribute('class')).toContain('hyicon-removelg');
            });
        },
        editMenuItemForComponentIdLoadedRightImg: function(componentID) {
            componentObject.elements.getEditButtonForComponentId(componentID).then(function(editButton) {
                // this button is still not using the assets service.
                expect(editButton.getAttribute('class')).toContain('hyicon-edit');
            });
        },
        moveMenuItemForComponentIdLoadedRightImg: function(componentID) {
            componentObject.elements.getMoveButtonForComponentId(componentID).then(function(moveButton) {
                expect(moveButton.getAttribute('src')).toContain('/web/webroot/images/contextualmenu_move_off.png');
            });
        }
    };

    return componentObject;
})();
