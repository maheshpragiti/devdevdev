/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
module.exports = (function() {

    var componentObject = {};

    componentObject.elements = {
        // Elements
        window: function() {
            return element(by.css('.modal-dialog'));
        },
        nextButton: function() {
            return element(by.css('.modal-footer button#ACTION_NEXT'));
        },
        backButton: function() {
            return element(by.css('.modal-footer button#ACTION_BACK'));
        },
        closeButton: function() {
            return element(by.css('.modal-header button.close'));
        },
        stepHeaderByIndex: function(index) {
            return element.all(by.css('.modal-body button')).get(index - 1);
        },
        submitButton: function() {
            return element(by.css('.modal-footer button#ACTION_DONE'));
        },
        getCurrentStep: function() {
            // This might break if the CSS changes. It'd be a good idea to define another class specific to the
            // tab headers.
            return element.all(by.css('.modal-wizard-template-step__action__enabled')).count();
        },
        getCurrentStepText: function() {
            return element(by.css('.modal-wizard-template-step__action__current')).getText().then(function(text) {
                return text.trim();
            });
        }
    };

    componentObject.actions = {
        isWindowsOpen: function() {
            return componentObject.elements.window().isPresent();
        },
        assertWindowIsOpen: function() {
            return browser.waitForPresence(componentObject.elements.window(), "modal window is not open");
        },
        assertWindowIsClosed: function() {
            return browser.waitForAbsence(componentObject.elements.window(), "modal window is still showing");
        },
        moveNext: function() {
            var button = componentObject.elements.nextButton();
            return browser.actions().mouseMove(button).click().perform();
        },
        moveBack: function() {
            var button = componentObject.elements.backButton();
            return browser.actions().mouseMove(button).click().perform();
        },
        moveToStepByIndex: function(index) {
            var tabHeader = componentObject.elements.stepHeaderByIndex(index);
            return browser.actions().mouseMove(tabHeader).click().perform();
        },
        scrollIntoView: function(item) {
            return browser.executeScript('arguments[0].scrollIntoView()', item.getWebElement());
        },
        submit: function() {
            var button = componentObject.elements.submitButton();
            return this.scrollIntoView(button).then(function() {
                return browser.actions().mouseMove(button).click().perform();
            });
        }
    };

    componentObject.assertions = {
        currentStepTextIs: function(expectedLabel) {
            browser.waitUntil((function() {
                return componentObject.elements.getCurrentStepText().then(function(label) {
                    return label == expectedLabel;
                });
            }), "current step was expected to be " + expectedLabel);
        }
    };

    return componentObject;
})();
