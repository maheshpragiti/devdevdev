/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
///*
// * [y] hybris Platform
// *
// * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
// *
// * This software is the confidential and proprietary information of SAP
// * ("Confidential Information"). You shall not disclose such Confidential
// * Information and shall use it only in accordance with the terms of the
// * license agreement you entered into with SAP.
// */
module.exports = (function() {

    var componentObject = {};

    componentObject.elements = {
        slotDisabledDecorator: function(slotId) {
            return element(by.css('[data-smartedit-component-id="' + slotId + '"] .disabled-shared-slot'));
        },
        slotDisabledTooltip: function() {
            return element(by.css('.disabled-shared-slot-tooltip__wrapper'));
        }
    };

    componentObject.assertions = {};
    componentObject.actions = {};

    return componentObject;
})();
