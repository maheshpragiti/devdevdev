angular.module('personalizationsmarteditContextMenu', ['gatewayProxyModule'])

.factory('personalizationsmarteditContextModal', function(gatewayProxy) {

        var PersonalizationsmarteditContextModal = function() { //NOSONAR
            this.gatewayId = "personalizationsmarteditContextModal";
            gatewayProxy.initForService(this);
        };

        PersonalizationsmarteditContextModal.prototype.openDeleteAction = function(configuration) {};

        PersonalizationsmarteditContextModal.prototype.openAddAction = function(configuration) {};

        PersonalizationsmarteditContextModal.prototype.openEditAction = function(configuration) {};

        PersonalizationsmarteditContextModal.prototype.openInfoAction = function() {};

        PersonalizationsmarteditContextModal.prototype.openEditComponentAction = function(configuration) {};

        return new PersonalizationsmarteditContextModal();
    })
    .factory('personalizationsmarteditContextModalHelper', function(personalizationsmarteditContextModal, personalizationsmarteditContextService) {
        var helper = {};

        helper.openDeleteAction = function(config) {
            var configProperties = angular.fromJson(config.properties);

            var configurationToPass = {};
            configurationToPass.containerId = config.containerId;
            configurationToPass.slotId = config.slotId;
            configurationToPass.actionId = configProperties.smarteditPersonalizationActionId || null;
            configurationToPass.selectedVariationCode = configProperties.smarteditPersonalizationVariationId || null;
            configurationToPass.selectedCustomizationCode = configProperties.smarteditPersonalizationCustomizationId || null;

            return personalizationsmarteditContextModal.openDeleteAction(configurationToPass);
        };

        helper.openAddAction = function(config) {
            var configProperties = angular.fromJson(config.properties);

            var configurationToPass = {};
            configurationToPass.componentType = config.componentType;
            configurationToPass.componentId = config.componentId;
            configurationToPass.containerId = config.containerId;
            configurationToPass.slotId = config.slotId;
            configurationToPass.actionId = configProperties.smarteditPersonalizationActionId || null;
            configurationToPass.selectedVariationCode = personalizationsmarteditContextService.selectedVariations.code;
            configurationToPass.selectedCustomizationCode = personalizationsmarteditContextService.selectedCustomizations.code;

            return personalizationsmarteditContextModal.openAddAction(configurationToPass);
        };

        helper.openEditAction = function(config) {
            var configProperties = angular.fromJson(config.properties);

            var configurationToPass = {};
            configurationToPass.componentType = config.componentType;
            configurationToPass.componentId = config.componentId;
            configurationToPass.containerId = config.containerId;
            configurationToPass.slotId = config.slotId;
            configurationToPass.actionId = configProperties.smarteditPersonalizationActionId || null;
            configurationToPass.selectedVariationCode = configProperties.smarteditPersonalizationVariationId || null;
            configurationToPass.selectedCustomizationCode = configProperties.smarteditPersonalizationCustomizationId || null;

            return personalizationsmarteditContextModal.openEditAction(configurationToPass);
        };

        helper.openInfoAction = function(config) {
            return personalizationsmarteditContextModal.openInfoAction();
        };

        helper.openEditComponentAction = function(config) {
            var configurationToPass = {};
            configurationToPass.componentType = config.componentType;
            configurationToPass.componentId = config.componentId;
            return personalizationsmarteditContextModal.openEditComponentAction(configurationToPass);
        };

        return helper;
    });
