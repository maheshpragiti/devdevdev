<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 [y] hybris Platform

 Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.

 This software is the confidential and proprietary information of SAP
 ("Confidential Information"). You shall not disclose such Confidential
 Information and shall use it only in accordance with the terms of the
 license agreement you entered into with SAP.
-->

<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="beans.xsd">

	<bean class="de.hybris.platform.cmsfacades.data.SyncJobData">
		<property name="syncStatus" type="String" />
		<property name="startDate" type="java.util.Date" />
		<property name="endDate" type="java.util.Date" />
		<property name="creationDate" type="java.util.Date" />
		<property name="lastModifiedDate" type="java.util.Date" />
		<property name="syncResult" type="String" />
		<property name="sourceCatalogVersion" type="String" />
		<property name="targetCatalogVersion" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SyncJobRequestData" extends="de.hybris.platform.cmsfacades.data.SyncRequestData">
	</bean>

	<bean class="de.hybris.platform.commercefacades.storesession.data.LanguageData">
		<property name="required" type="boolean" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.MediaData">
		<property name="uuid" type="String" />
		<property name="code" type="String" />
		<property name="catalogId" type="String" />
		<property name="catalogVersion" type="String" />
		<property name="mime" type="String" />
		<property name="altText" type="String" />
		<property name="description" type="String" />
		<property name="url" type="String" />
		<property name="downloadUrl" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.NamedQueryData">
		<property name="namedQuery" type="String" />
		<property name="params" type="String" />
		<property name="sort" type="String" />
		<property name="pageSize" type="String" />
		<property name="currentPage" type="String" />
		<property name="queryType" type="java.lang.Class&lt;?>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ComponentTypeData">
		<property name="code" type="String" />
		<property name="category" type="String" />
		<property name="name" type="String" />
		<property name="i18nKey" type="String" />
		<property name="type" type="String" />
		<property name="attributes" type="java.util.List&lt;de.hybris.platform.cmsfacades.data.ComponentTypeAttributeData>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ComponentTypeAttributeData">
		<property name="qualifier" type="String" />
		<property name="required" type="boolean" />
		<property name="localized" type="Boolean" />
		<property name="editable" type="boolean" />
		<property name="cmsStructureType" type="String" />
		<property name="cmsStructureEnumType" type="String" />
		<property name="i18nKey" type="String" />
		<property name="paged" type="boolean" />
		<property name="collection" type="boolean" />
		<property name="dependsOn" type="String" />
		<property name="options" type="java.util.List&lt;de.hybris.platform.cmsfacades.data.OptionData&gt;" />
		<property name="idAttribute" type="String" />
		<property name="labelAttributes" type="java.util.List&lt;String>" />
		<property name="params" type="java.util.Map&lt;String,String>" />
	</bean>

	<enum class="de.hybris.platform.cmsfacades.data.StructureTypeCategory">
		<value>COMPONENT</value>
		<value>PREVIEW</value>
		<value>PAGE</value>
		<value>RESTRICTION</value>
	</enum>

	<bean class="de.hybris.platform.cmsfacades.data.OptionData">
		<property name="value" type="String" deprecated="Deprecated since 6.4, please use the id attribute of OptionData instead." />
		<property name="id" type="String" />
		<property name="label" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.AbstractPageData">
		<property name="uuid" type="String" />
		<property name="pk" type="String" />
		<property name="creationtime" type="java.util.Date" />
		<property name="modifiedtime" type="java.util.Date" />
		<property name="uid" type="String" />
		<property name="name" type="String" />
		<property name="title" type="java.util.Map&lt;String,String>" />
		<property name="typeCode" type="String" />
		<property name="template" type="String" />
		<property name="defaultPage" type="Boolean" />
		<property name="onlyOneRestrictionMustApply" type="Boolean" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ContentPageData" extends="AbstractPageData">
		<property name="label" type="String" />
		<property name="path" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ProductPageData" extends="AbstractPageData">
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.EmailPageData" extends="AbstractPageData">
		<property name="fromEmail" type="java.util.Map&lt;String,String>" />
		<property name="fromName" type="java.util.Map&lt;String,String>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CategoryPageData" extends="AbstractPageData">
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CatalogPageData" extends="AbstractPageData">
	</bean>

	<bean class="de.hybris.platform.cmsfacades.dto.UpdatePageValidationDto">
		<property name="originalUid" type="String" />
		<property name="page" type="de.hybris.platform.cmsfacades.data.AbstractPageData" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.AbstractRestrictionData">
		<property name="uuid" type="String" />
		<property name="uid" type="String" />
		<property name="name" type="String" />
		<property name="description" type="String" />
		<property name="typeCode" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.TimeRestrictionData" extends="AbstractRestrictionData">
		<property name="activeFrom" type="java.util.Date" />
		<property name="activeUntil" type="java.util.Date" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CategoryRestrictionData" extends="AbstractRestrictionData">
		<property name="recursive" type="boolean" />
		<property name="categories" type="java.util.List&lt;String>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.UserGroupRestrictionData" extends="AbstractRestrictionData">
		<property name="includeSubgroups" type="boolean" />
		<property name="userGroups" type="java.util.List&lt;String>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.dto.UpdateRestrictionValidationDto">
		<property name="originalUid" type="String" />
		<property name="restriction" type="de.hybris.platform.cmsfacades.data.AbstractRestrictionData" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.NavigationEntryTypeData">
		<property name="itemType" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.NavigationEntryData">
		<property name="itemId" type="String" />
		<property name="itemType" type="String" />
		<property name="itemSuperType" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.NavigationNodeData">
		<property name="uid" type="String" />
		<property name="parentUid" type="String" />
		<property name="name" type="String" />
		<property name="title" type="java.util.Map&lt;String,String>" />
		<property name="hasChildren" type="Boolean" />
		<property name="position" type="Integer" />
		<property name="entries" type="java.util.List&lt;de.hybris.platform.cmsfacades.data.NavigationEntryData>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.AbstractCMSComponentData">
		<property name="pk" type="String" />
		<property name="creationtime" type="java.util.Date" />
		<property name="modifiedtime" type="java.util.Date" />
		<property name="uid" type="String" />
		<property name="name" type="String" />
		<property name="visible" type="Boolean" />
		<property name="typeCode" type="String" />
		<property name="slotId" type="String" />
		<property name="pageId" type="String" />
		<property name="position" type="Integer" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CMSItemSearchData">
		<property name="mask" type="String" />
		<property name="typeCode" type="String" />
		<property name="catalogId" type="String" />
		<property name="catalogVersion" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CMSParagraphComponentData" extends="AbstractCMSComponentData">
		<property name="content" type="java.util.Map&lt;String, String&gt;" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SimpleBannerComponentData" extends="AbstractCMSComponentData">
		<property name="media" type="java.util.Map&lt;String, String&gt;" />
		<property name="urlLink" type="String" />
		<property name="external" type="Boolean" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.BannerComponentData" extends="AbstractCMSComponentData">
		<property name="content" type="java.util.Map&lt;String, String&gt;" />
		<property name="headline" type="java.util.Map&lt;String, String&gt;" />
		<property name="media" type="java.util.Map&lt;String, String&gt;" />
		<property name="urlLink" type="String" />
		<property name="external" type="Boolean" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CMSLinkComponentData" extends="AbstractCMSComponentData">
		<property name="linkName" type="java.util.Map&lt;String, String>" />
		<property name="url" type="String" />
		<property name="contentPage" type="String" />
		<property name="product" type="String" />
		<property name="category" type="String" />
		<property name="external" type="Boolean" />
		<property name="target" type="boolean" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SimpleResponsiveBannerComponentData" extends="AbstractCMSComponentData">
		<property name="media" type="java.util.Map&lt;String, java.util.Map&lt;String, String>>" />
		<property name="urlLink" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.NavigationComponentData" extends="AbstractCMSComponentData">
		<property name="navigationNode" type="String" />
		<property name="wrapAfter" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ProductCarouselComponentData" extends="AbstractCMSComponentData">
		<property name="title" type="java.util.Map&lt;String, String>" />
		<property name="products" type="java.util.List&lt;String>" />
		<property name="categories" type="java.util.List&lt;String>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.MediaContainerData">
		<property name="formatMediaCodeMap" type="java.util.Map&lt;String, String>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ContentSlotTypeRestrictionsData">
		<property name="contentSlotUid" type="String" />
		<property name="validComponentTypes" type="java.util.List&lt;String>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.PageContentSlotComponentData">
		<property name="pageId" type="String" />
		<property name="componentId" type="String" />
		<property name="componentUuid" type="String" />
		<property name="slotId" type="String" />
		<property name="position" type="Integer" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.PageContentSlotData">
		<property name="pageId" type="String" />
		<property name="slotId" type="String" />
		<property name="position" type="String" />
		<property name="slotShared" type="boolean" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.PageRestrictionData">
		<property name="pageId" type="String" />
		<property name="restrictionId" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.PageTypeRestrictionTypeData">
		<property name="pageType" type="String" />
		<property name="restrictionType" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CatalogVersionDetailData"
			deprecated="Deprecated since 6.4, please use de.hybris.platform.cmssmarteditwebservices.data.CatalogData instead.">
		<property name="catalogId" type="String" />
		<property name="redirectUrl" type="String" />
		<property name="name" type="java.util.Map&lt;String, String&gt;" />
		<property name="version" type="String" />
		<property name="active" type="Boolean" />
		<property name="thumbnailUrl" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CatalogData">
		<property name="catalogId" type="String" />
		<property name="name" type="java.util.Map&lt;String, String&gt;" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CatalogVersionData">
		<property name="uid" type="String" 
					 deprecated="Deprecated since 6.4, attribute releated to CatalogModel, please use de.hybris.platform.cmssmarteditwebservices.data.CatalogData instead." />
		<property name="name" type="java.util.Map&lt;String, String&gt;"
					 deprecated="Deprecated since 6.4, attribute releated to CatalogModel, please use de.hybris.platform.cmssmarteditwebservices.data.CatalogData instead." />
		<property name="catalogVersionDetails" type="java.util.Collection&lt;CatalogVersionDetailData>"
					 deprecated="Deprecated since 6.4, attribute releated to CatalogModel, please use de.hybris.platform.cmssmarteditwebservices.data.CatalogData instead." />
		<property name="active" type="Boolean" />
		<property name="pageDisplayConditions" type="java.util.List&lt;DisplayConditionData>" />
		<property name="version" type="String" />
		<property name="thumbnailUrl" type="String" />
		<property name="uuid" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.DisplayConditionData">
		<property name="typecode" type="String" />
		<property name="options" type="java.util.List&lt;de.hybris.platform.cmsfacades.data.OptionData>" />
	</bean>

	<enum class="de.hybris.platform.cmsfacades.page.DisplayCondition">
		<value>PRIMARY</value>
		<value>VARIATION</value>
	</enum>

	<bean class="de.hybris.platform.cmsfacades.data.SiteData">
		<property name="uid" type="String" />
		<property name="previewUrl" type="String" />
		<property name="redirectUrl" type="String" deprecated="Deprecated since 6.4"/>
		<property name="name" type="java.util.Map&lt;String, String>" />
		<property name="thumbnailUrl" type="String" deprecated="Deprecated since 6.4, please use de.hybris.platform.cmssmarteditwebservices.data.CatalogData instead."/>
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.EnumData">
		<description>Deprecated as of version 6.2</description>
		<property name="code" type="String" />
		<property name="label" type="String" />
	</bean>

	<!-- DTO beans -->
	<bean class="de.hybris.platform.cmsfacades.dto.ComponentAndContentSlotValidationDto">
		<property name="component" type="de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel" />
		<property name="contentSlot" type="de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.dto.ComponentTypeAndContentSlotValidationDto">
		<property name="componentType" type="String" />
		<property name="contentSlot" type="de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel" />
		<property name="page" type="de.hybris.platform.cms2.model.pages.AbstractPageModel" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.dto.UpdateComponentValidationDto">
		<property name="originalUid" type="String" />
		<property name="component" type="de.hybris.platform.cmsfacades.data.AbstractCMSComponentData" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.dto.MediaFileDto">
		<property name="inputStream" type="java.io.InputStream" />
		<property name="mime" type="String" />
		<property name="name" type="String" />
		<property name="size" type="java.lang.Long" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.PageTemplateData">
		<property name="uid" type="String" />
		<property name="uuid" type="String" />
		<property name="name" type="String" />
		<property name="frontEndName" type="String" />
		<property name="previewIcon" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.PageTemplateDTO">
		<property name="pageTypeCode" type="String" />
		<property name="active" type="Boolean" />
	</bean>

	<!-- Composed Type beans -->

	<bean class="de.hybris.platform.cmsfacades.data.ComposedTypeData">
		<property name="code" type="String" />
		<property name="name" type="java.util.Map&lt;String,String>" />
		<property name="description" type="java.util.Map&lt;String,String>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.RestrictionTypeData" extends="de.hybris.platform.cmsfacades.data.ComposedTypeData" />

	<bean class="de.hybris.platform.cmsfacades.data.PageTypeData" extends="de.hybris.platform.cmsfacades.data.ComposedTypeData">
		<property name="type" type="String" />
	</bean>

	<!-- beans for NamedQuery Service -->

	<bean class="de.hybris.platform.cmswebservices.namedquery.NamedQuery" template="resources/template/beans/simpleJavaBeanUsingWithMethods-template.vm"
			extends="de.hybris.platform.cms2.namedquery.NamedQuery">
		<description>Deprecated since 6.4, please use de.hybris.platform.cms2.namedquery.NamedQuery instead.</description>
	</bean>

	<bean class="de.hybris.platform.cmswebservices.namedquery.NamedQueryConversionDto" template="resources/template/beans/simpleJavaBeanUsingWithMethods-template.vm"
			extends="de.hybris.platform.cms2.namedquery.NamedQueryConversionDto">
		<description>Deprecated since 6.4, please use de.hybris.platform.cms2.namedquery.NamedQueryConversionDto instead.</description>
	</bean>

	<bean class="de.hybris.platform.cmswebservices.namedquery.Sort" template="resources/template/beans/simpleJavaBeanUsingWithMethods-template.vm"
			extends="de.hybris.platform.cms2.namedquery.Sort">
		<description>Deprecated since 6.4, please use de.hybris.platform.cms2.namedquery.Sort instead.</description>
	</bean>

	<enum class="de.hybris.platform.cmswebservices.namedquery.SortDirection">
		<description>Deprecated since 6.4, please use de.hybris.platform.cms2.enums.SortDirection instead.</description>
		<value>ASC</value>
		<value>DESC</value>
	</enum>

	<bean class="de.hybris.platform.cmsfacades.data.ProductData">
		<property name="code" type="String" />
		<property name="name" type="java.util.Map&lt;String,String>" />
		<property name="description" type="java.util.Map&lt;String,String>" />
		<property name="thumbnailMediaCode" type="String" />
		<property name="catalogId" type="String" />
		<property name="catalogVersion" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.CategoryData">
		<property name="code" type="String" />
		<property name="name" type="java.util.Map&lt;String,String>" />
		<property name="description" type="java.util.Map&lt;String,String>" />
		<property name="thumbnailMediaCode" type="String" />
		<property name="catalogId" type="String" />
		<property name="catalogVersion" type="String" />
	</bean>


	<!-- Synchronization Data Types -->
	<bean class="de.hybris.platform.cmsfacades.data.SyncRequestData">
		<property name="catalogId" type="String" />
		<property name="sourceVersionId" type="String" />
		<property name="targetVersionId" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SyncItemStatusData">
		<property name="itemId" type="String" />
		<property name="itemType" type="String" />
		<property name="name" type="String" />
		<property name="status" type="String" />
		<property name="lastSyncStatus" type="Long"/>
		<property name="dependentItemTypesOutOfSync" type="java.util.List&lt;ItemTypeData>" />
		<property name="selectedDependencies" type="java.util.List&lt;SyncItemStatusData>" />
		<property name="sharedDependencies" type="java.util.List&lt;SyncItemStatusData>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ItemTypeData">
		<property name="itemType" type="String" />
		<property name="i18nKey" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SynchronizationItemDetailsData">
		<property name="item" type="de.hybris.platform.core.model.ItemModel" />
		<property name="catalogId" type="String" />
		<property name="sourceVersionId" type="String" />
		<property name="targetVersionId" type="String" />
		<property name="syncStatus" type="String"/>
		<property name="lastSyncStatusDate" type="java.util.Date"/>
		<property name="relatedItemStatuses" type="java.util.List&lt;SyncItemInfoJobStatusData>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SyncItemInfoJobStatusData">
		<property name="item" type="de.hybris.platform.core.model.ItemModel"/>
		<property name="syncStatus" type="String"/>
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SyncItemStatusConfig">
		<property name="maxDepth" type="Integer" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ItemData">
		<property name="itemId" type="String" />
		<property name="name" type="String" />
		<property name="itemType" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ItemSynchronizationData">
		<property name="itemId" type="String" />
		<property name="itemType" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.SynchronizationData">
		<property name="items" type="java.util.List&lt;ItemSynchronizationData>" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.uniqueidentifier.ItemComposedKey">
		<property name="itemId" type="String" />
		<property name="catalogId" type="String" />
		<property name="catalogVersion" type="String" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.UserGroupData">
		<property name="uid" type="String" />
		<property name="name" type="java.util.Map&lt;String, String&gt;" />
	</bean>

	<bean class="de.hybris.platform.cmsfacades.data.ClonePageData">
		<property name="pageData" type="de.hybris.platform.cmsfacades.data.AbstractPageData" />
		<property name="cloneComponents" type="boolean" />
		<property name="restrictions" type="java.util.List&lt;String&gt;" />
	</bean>
	
	<!-- CMS Facade Validation Bean -->
	<bean class="de.hybris.platform.cmsfacades.validator.data.ValidationError">
		<property name="field" type="String" />
		<property name="rejectedValue" type="Object" />
		<property name="language" type="String" />
		<property name="errorCode" type="String" />
		<property name="errorArgs" type="Object[]" />
		<property name="exceptionMessage" type="String" />
		<property name="defaultMessage" type="String" />
		<property name="position" type="Integer"/>
	</bean>

</beans>
