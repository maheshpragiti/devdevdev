/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.sites.impl;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.cms2.catalogversion.service.CMSCatalogVersionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cmsfacades.data.SiteData;
import de.hybris.platform.cmsfacades.sites.SiteFacade;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;


/**
 * Default implementation of {@link SiteFacade}.
 */
public class DefaultSiteFacade implements SiteFacade
{
	private boolean writePermissionRequired;
	private boolean readPermissionRequired;
	private CMSCatalogVersionService cmsCatalogVersionService;
	private CMSAdminSiteService cmsAdminSiteService;
	private Converter<CMSSiteModel, SiteData> cmsSiteModelConverter;
	private Comparator<SiteData> siteDataComparator;

	@Override
	public List<SiteData> getAllSiteData()
	{
		return getCmsAdminSiteService().getSites().stream()
				.filter(hasPermittedCatalogs())
				.map(site -> getCmsSiteModelConverter().convert(site)) //
				.sorted(getSiteDataComparator()).collect(Collectors.toList());
	}

	/**
	 * Predicate to determine if a site contains any content catalog which the current user has read and/or write access
	 * to.
	 * 
	 * @return {@code true} when a site contains at least one content catalog accessible by the user; {@code false}
	 *         otherwise.
	 */
	protected Predicate<CMSSiteModel> hasPermittedCatalogs()
	{
		return cmsSite -> {
			final Set<CatalogModel> permittedCatalogs = getCmsCatalogVersionService()
					.getContentCatalogsAndVersions(isReadPermissionRequired(), isWritePermissionRequired(), cmsSite).keySet();

			return cmsSite.getContentCatalogs().stream().anyMatch(contentCatalog -> permittedCatalogs.contains(contentCatalog));
		};
	}

	@Required
	public void setWritePermissionRequired(final boolean writePermissionRequired)
	{
		this.writePermissionRequired = writePermissionRequired;
	}

	/**
	 * This method is used to identify whether users need write permissions to be retrieved by this facade.
	 *
	 * @return if permission is needed or not
	 */
	protected boolean isWritePermissionRequired()
	{
		return writePermissionRequired;
	}

	@Required
	public void setReadPermissionRequired(final boolean readPermissionRequired)
	{
		this.readPermissionRequired = readPermissionRequired;
	}

	/**
	 * This method is used to identify whether users need read permissions to be retrieved by this facade.
	 *
	 * @return if permission is needed or not
	 */
	protected boolean isReadPermissionRequired()
	{
		return readPermissionRequired;
	}

	protected CMSCatalogVersionService getCmsCatalogVersionService()
	{
		return cmsCatalogVersionService;
	}

	@Required
	public void setCmsCatalogVersionService(final CMSCatalogVersionService cmsCatalogVersionService)
	{
		this.cmsCatalogVersionService = cmsCatalogVersionService;
	}

	@Required
	public void setSiteDataComparator(final Comparator<SiteData> siteDataComparator)
	{
		this.siteDataComparator = siteDataComparator;
	}

	protected Comparator<SiteData> getSiteDataComparator()
	{
		return siteDataComparator;
	}

	@Required
	public void setCmsAdminSiteService(final CMSAdminSiteService cmsAdminSiteService)
	{
		this.cmsAdminSiteService = cmsAdminSiteService;
	}

	protected CMSAdminSiteService getCmsAdminSiteService()
	{
		return cmsAdminSiteService;
	}

	@Required
	public void setCmsSiteModelConverter(final Converter<CMSSiteModel, SiteData> cmsSiteModelConverter)
	{
		this.cmsSiteModelConverter = cmsSiteModelConverter;
	}

	protected Converter<CMSSiteModel, SiteData> getCmsSiteModelConverter()
	{
		return cmsSiteModelConverter;
	}
}
