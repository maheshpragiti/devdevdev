/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.catalogversions.populator;


import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cmsfacades.data.CatalogVersionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Populates a {@Link CatalogVersionData} dto from a {@Link CatalogVersionModel} item
 */
public class CatalogVersionModelPopulator implements Populator<CatalogVersionModel, CatalogVersionData>
{
	@Override
	public void populate(final CatalogVersionModel source, final CatalogVersionData target) throws ConversionException
	{
		target.setVersion(source.getVersion());
		target.setActive(source.getActive());
	}
}
