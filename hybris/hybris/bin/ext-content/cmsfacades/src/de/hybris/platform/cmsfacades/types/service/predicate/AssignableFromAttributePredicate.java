/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.types.service.predicate;

import de.hybris.platform.cms2.servicelayer.services.AttributeDescriptorModelHelperService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Required;


/**
 * Predicate to test if an attribute type is assignable from the input <code>typeCode</code> passed from
 * the configuration. If matches, will return true else false.
 */
public class AssignableFromAttributePredicate implements Predicate<AttributeDescriptorModel>
{

	private String typeCode;

	private AttributeDescriptorModelHelperService attributeDescriptorModelHelperService;
	private TypeService typeService;

	@Override
	public boolean test(final AttributeDescriptorModel attributeDescriptor)
	{
		final Predicate<Class<?>> isTypeOf = modelClass ->
		{
			ComposedTypeModel composedTypeForCode = getTypeService().getComposedTypeForCode(getTypeCode());
			Class<ItemModel> parentClass = getTypeService().getModelClass(composedTypeForCode);
			return parentClass.isAssignableFrom(modelClass);
		};

		return isTypeOf.test(getAttributeDescriptorModelHelperService().getAttributeClass(attributeDescriptor));
	}

	protected String getTypeCode()
	{
		return typeCode;
	}

	@Required
	public void setTypeCode(final String typeCode)
	{
		this.typeCode = typeCode;
	}

	protected AttributeDescriptorModelHelperService getAttributeDescriptorModelHelperService()
	{
		return attributeDescriptorModelHelperService;
	}

	@Required
	public void setAttributeDescriptorModelHelperService(
			AttributeDescriptorModelHelperService attributeDescriptorModelHelperService)
	{
		this.attributeDescriptorModelHelperService = attributeDescriptorModelHelperService;

	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

}
