/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.validator;

import static de.hybris.platform.cmsfacades.common.validator.ValidationErrorBuilder.newValidationErrorBuilder;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cmsfacades.common.function.Validator;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrorsProvider;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Required;

/**
 * Default implementation of the validator for {@link CMSLinkComponentModel}
 */
public class DefaultCMSLinkComponentValidator implements Validator<CMSLinkComponentModel>
{
    private final String LINK_TO = "linkTo";
    private final String LINK_TO_ERROR_CODE = "The Link To field is required.";

    private ValidationErrorsProvider validationErrorsProvider;

    @Override
    public void validate(final CMSLinkComponentModel validatee)
    {
        if (Objects.isNull(validatee.getProduct())
                && Objects.isNull(validatee.getContentPage())
                && Objects.isNull(validatee.getCategory())
                && Objects.isNull(validatee.getUrl()))
        {
            getValidationErrorsProvider().getCurrentValidationErrors().add(
                    newValidationErrorBuilder() //
                            .field(LINK_TO)
                            .errorCode(LINK_TO_ERROR_CODE) //
                            .build()
            );
        }
    }

    protected ValidationErrorsProvider getValidationErrorsProvider()
    {
        return validationErrorsProvider;
    }

    @Required
    public void setValidationErrorsProvider(final ValidationErrorsProvider validationErrorsProvider)
    {
        this.validationErrorsProvider = validationErrorsProvider;
    }
}
