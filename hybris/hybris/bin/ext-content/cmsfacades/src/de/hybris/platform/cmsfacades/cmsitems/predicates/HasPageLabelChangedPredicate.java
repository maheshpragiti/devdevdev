/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.predicates;

import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.SESSION_ORIGINAL_ITEM_MODEL;
import static java.util.Objects.nonNull;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminPageService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Required;


/**
 * Predicate to test if the label has been changed by comparing the given label to the label in
 * {@ContentPageModel}.
 * <p>
 * Returns <tt>TRUE</tt> if the page label has been modified; <tt>FALSE</tt> otherwise.
 * </p>
 */
public class HasPageLabelChangedPredicate implements Predicate<String>
{
	private SessionService sessionService;

	@Override
	public boolean test(final String label)
	{
		final ContentPageModel contentPage = getSessionService().getAttribute(SESSION_ORIGINAL_ITEM_MODEL);

		if (nonNull(contentPage))
		{
			return !contentPage.getLabel().equals(label);
		}
		return false;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

}
