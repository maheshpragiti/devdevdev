/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.catalogversions.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cmsfacades.catalogversions.CatalogVersionFacade;
import de.hybris.platform.cmsfacades.catalogversions.service.PageDisplayConditionService;
import de.hybris.platform.cmsfacades.data.CatalogVersionData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Required;


/**
 * Facade interface which deals with methods related to catalog version operations.
 */
public class DefaultCatalogVersionFacade implements CatalogVersionFacade
{
	private CatalogVersionService catalogVersionService;
	private Converter<CatalogVersionModel, CatalogVersionData> cmsCatalogVersionConverter;
	private PageDisplayConditionService pageDisplayConditionService;

	@Override
	public CatalogVersionData getCatalogVersion(final String catalogId, final String versionId) throws CMSItemNotFoundException
	{
		final CatalogVersionModel catalogVersionModel = getCatalogVersionService().getCatalogVersion(catalogId, versionId);
		// populate basic information : catalog id, name and version
		final CatalogVersionData catalogVersion = getCmsCatalogVersionConverter().convert(catalogVersionModel);

		if (Objects.isNull(catalogVersion))
		{
			throw new CMSItemNotFoundException("Cannot find catalog version");
		}

		// find all page display options per page type
		catalogVersion.setPageDisplayConditions(getPageDisplayConditionService().getDisplayConditions());

		return catalogVersion;
	}


	protected Converter<CatalogVersionModel, CatalogVersionData> getCmsCatalogVersionConverter()
	{
		return cmsCatalogVersionConverter;
	}

	@Required
	public void setCmsCatalogVersionConverter(final Converter<CatalogVersionModel, CatalogVersionData> cmsCatalogVersionConverter)
	{
		this.cmsCatalogVersionConverter = cmsCatalogVersionConverter;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected PageDisplayConditionService getPageDisplayConditionService()
	{
		return pageDisplayConditionService;
	}

	@Required
	public void setPageDisplayConditionService(final PageDisplayConditionService pageDisplayConditionService)
	{
		this.pageDisplayConditionService = pageDisplayConditionService;
	}
}
