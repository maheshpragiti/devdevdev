/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.restrictions.populator.model;

import de.hybris.platform.cms2.model.restrictions.CMSCategoryRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.CMSUserGroupRestrictionModel;
import de.hybris.platform.cmsfacades.data.CategoryRestrictionData;
import de.hybris.platform.cmsfacades.data.UserGroupRestrictionData;
import de.hybris.platform.cmsfacades.uniqueidentifier.UniqueItemIdentifierService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Converts an {@link CMSUserGroupRestrictionModel} Restriction to a {@link UserGroupRestrictionData} dto
 */
public class UserGroupRestrictionModelToDataPopulator implements Populator<CMSUserGroupRestrictionModel, UserGroupRestrictionData>
{

	private UniqueItemIdentifierService uniqueItemIdentifierService;

	@Override
	public void populate(CMSUserGroupRestrictionModel source, UserGroupRestrictionData target)
			throws ConversionException
	{
		target.setIncludeSubgroups(source.isIncludeSubgroups());
		final List<String> userGroups = source.getUserGroups().stream().map(userGroupModel ->
		{
			return getUniqueItemIdentifierService().getItemData(userGroupModel).map(itemData ->
			{
				return itemData.getItemId();
			}).get();
		}).collect(Collectors.toList());
		target.setUserGroups(userGroups);
	}

	protected UniqueItemIdentifierService getUniqueItemIdentifierService()
	{
		return uniqueItemIdentifierService;
	}

	@Required
	public void setUniqueItemIdentifierService(UniqueItemIdentifierService uniqueItemIdentifierService)
	{
		this.uniqueItemIdentifierService = uniqueItemIdentifierService;
	}
}
