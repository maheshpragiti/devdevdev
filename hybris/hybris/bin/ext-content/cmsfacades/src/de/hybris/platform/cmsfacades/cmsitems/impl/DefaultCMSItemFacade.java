/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.impl;

import static de.hybris.platform.cmsfacades.common.validator.ValidationErrorBuilder.newValidationErrorBuilder;
import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_REQUIRED;
import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_UUID;
import static java.util.stream.Collectors.toList;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.cmsitems.service.CMSItemSearchService;
import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cmsfacades.cmsitems.CMSItemConverter;
import de.hybris.platform.cmsfacades.cmsitems.CMSItemFacade;
import de.hybris.platform.cmsfacades.cmsitems.CMSItemValidator;
import de.hybris.platform.cmsfacades.cmsitems.ItemTypePopulatorProvider;
import de.hybris.platform.cmsfacades.common.function.Converter;
import de.hybris.platform.cmsfacades.common.validator.FacadeValidationService;
import de.hybris.platform.cmsfacades.common.validator.ValidatableService;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrorsProvider;
import de.hybris.platform.cmsfacades.constants.CmsfacadesConstants;
import de.hybris.platform.cmsfacades.data.CMSItemSearchData;
import de.hybris.platform.cmsfacades.data.ItemData;
import de.hybris.platform.cmsfacades.exception.ValidationException;
import de.hybris.platform.cmsfacades.uniqueidentifier.UniqueItemIdentifierService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.validation.Validator;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Default implementation of the {@link CMSItemFacade}.
 */
public class DefaultCMSItemFacade implements CMSItemFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultCMSItemFacade.class);
	private static final String MODEL_SAVING_EXCEPTION_REGEX = "\\[\\w*\\]";

	private CMSItemConverter cmsItemConverter;

	private ModelService modelService;

	private ObjectFactory<ItemData> itemDataDataFactory;

	private ItemTypePopulatorProvider itemTypePopulatorProvider;

	private CMSItemSearchService cmsItemSearchService;

	private Validator cmsItemSearchDataValidator;

	private FacadeValidationService facadeValidationService;

	private UniqueItemIdentifierService uniqueItemIdentifierService;

	private CMSAdminSiteService cmsAdminSiteService;

	private CatalogVersionService catalogVersionService;

	private Converter<CMSItemSearchData, de.hybris.platform.cms2.data.CMSItemSearchData> cmsItemSearchDataConverter;

	private ValidatableService validatableService;

	private CMSItemValidator<ItemModel> cmsItemValidatorCreate;

	private CMSItemValidator<ItemModel> cmsItemValidatorUpdate;

	private ValidationErrorsProvider validationErrorsProvider;

	private PlatformTransactionManager transactionManager;

	private TypeService typeService;
	
	private SessionService sessionService;
	
	@Override
	public SearchResult<Map<String, Object>> findCMSItems(final CMSItemSearchData cmsItemSearchData,
			final PageableData pageableData)
	{
		getFacadeValidationService().validate(getCmsItemSearchDataValidator(), cmsItemSearchData);

		if(StringUtils.isNotBlank(cmsItemSearchData.getTypeCode())) {
			try
			{
				getTypeService().getComposedTypeForCode(cmsItemSearchData.getTypeCode());
			}
			catch (final UnknownIdentifierException e)
			{

				final List<Map<String, Object>> convertedResults = new ArrayList<>();
				final SearchResult<Map<String, Object>> searchResult = new SearchResultImpl<>(
						convertedResults, 0, pageableData.getPageSize(), pageableData.getCurrentPage());

				return searchResult;
			}
		}

		final SearchResult<CMSItemModel> searchResults = getCmsItemSearchService()
				.findCMSItems(getCmsItemSearchDataConverter().convert(cmsItemSearchData), pageableData);

		final List<Map<String, Object>> convertedResults = searchResults.getResult().stream().map(getCmsItemConverter()::convert)
				.collect(Collectors.toList());
		final SearchResult<Map<String, Object>> searchResult = new SearchResultImpl<>(
				convertedResults, searchResults.getTotalCount(), searchResults.getRequestedCount(),
				searchResults.getRequestedStart());

		return searchResult;
	}

	@Override
	public List<Map<String, Object>> findCMSItems(final List<String> uuids) throws CMSItemNotFoundException
	{
		return uuids.stream().map(uuid -> {
			try{
				return getCMSItemByUuid(uuid);
			}catch(final CMSItemNotFoundException e){
				throw new RuntimeException(e);
			}

		}).collect(toList());
	}

	@Override
	public Map<String, Object> getCMSItemByUuid(final String uuid) throws CMSItemNotFoundException
	{
		final ItemModel cmsItem = getUniqueItemIdentifierService() //
				.getItemModel(createCmsItemData(uuid)) //
				.orElseThrow(() -> new CMSItemNotFoundException("CMS Item [" + uuid + "] does not exist."));

		return getCmsItemConverter().convert(cmsItem);
	}

	@Override
	@Transactional
	public Map<String, Object> createItem(final Map<String, Object> itemMap) throws CMSItemNotFoundException
	{
		return saveItem(itemMap, getCmsItemValidatorCreate());
	}


	@Override
	@Transactional
	public Map<String, Object> updateItem(final String uuid, final Map<String, Object> itemMap) throws CMSItemNotFoundException
	{
		// checks if the item exists
		final ItemModel originalItemModel = getUniqueItemIdentifierService() //
				.getItemModel(createCmsItemData(uuid)) //
				.orElseThrow(() -> new CMSItemNotFoundException("CMS Item [" + uuid + "] does not exist."));
		cloneAndStoreInLocalSession(originalItemModel);

		if (!StringUtils.equals(uuid, (String) itemMap.get(FIELD_UUID)))
		{
			throw new CMSItemNotFoundException("Inconsistent CMS Item [" + uuid + "] - ["
					+ itemMap.get(FIELD_UUID) + "].");
		}

		return saveItem(itemMap, getCmsItemValidatorUpdate());
	}

	/**
	 * Clones the original item model and stores it in session after detaching it from the data model. 
	 * @param originalItemModel the original item model 
	 */
	protected void cloneAndStoreInLocalSession(final ItemModel originalItemModel)
	{
		final ItemModel itemModel = getModelService().clone(originalItemModel);
		getModelService().detachAll();
		getSessionService().setAttribute(CmsfacadesConstants.SESSION_ORIGINAL_ITEM_MODEL, itemModel);
	}


	/**
	 * Saves Item performing task in local transaction.
	 * @param itemMap the itemMap to be saved
	 * @param validator the type validator to be used (create/update)
	 * @return the item Map representation after saving.
	 * @throws CMSItemNotFoundException
	 */
	protected Map<String, Object> saveItem(final Map<String, Object> itemMap, final CMSItemValidator<ItemModel> validator) throws CMSItemNotFoundException
	{
		setCatalogInSession(itemMap);
		try
		{
			return new TransactionTemplate(getTransactionManager())
					.execute(status -> {
						final Map<String, Object> responseMap = validateAndConvert(itemMap, validator);

						getModelService().saveAll();
						return responseMap;
					});
		}
		catch (final ModelSavingException e)
		{
			LOG.info("Failed to save the item model", e);
			if (e.getMessage() != null)
			{
				final Pattern pattern = Pattern.compile(MODEL_SAVING_EXCEPTION_REGEX);
				final Matcher matcher = pattern.matcher(e.getMessage());
				final String qualifier = matcher.find() ? matcher.group().replaceAll("\\[", "").replaceAll("\\]", "") : null;
				getValidationErrorsProvider().getCurrentValidationErrors().add(
						newValidationErrorBuilder() //
						.field(qualifier) //
						.errorCode(FIELD_REQUIRED) //
						.exceptionMessage(e.getMessage()) //
						.build()
						);
			}
			throw new ValidationException(getValidationErrorsProvider().getCurrentValidationErrors());
		}
	}


	/**
	 * Validates and process Conversion to save the Item Model.
	 * @param itemMap the Map representing the ItemModel to be validated and saved
	 * @return A Map that represents the Item Model.
	 */
	protected Map<String, Object> validateAndConvert(final Map<String, Object> itemMap, final CMSItemValidator<ItemModel> cmsItemValidator)
	{
		final ItemModel itemModel = getValidatableService().execute(() ->
		{
			final ItemModel itemModelConverted = getCmsItemConverter().convert(itemMap);
			// type validation
			cmsItemValidator.validate(itemModelConverted);
			return itemModelConverted;
		});

		getItemTypePopulatorProvider().getItemTypePopulator(itemModel.getItemtype()) //
		.ifPresent(populator -> populator.populate(itemMap, itemModel));

		return getCmsItemConverter().convert(itemModel);
	}


	@Override
	public void deleteCMSItemByUuid(final String uuid) throws CMSItemNotFoundException
	{
		final ItemModel cmsItem = getUniqueItemIdentifierService() //
				.getItemModel(createCmsItemData(uuid)) //
				.orElseThrow(() -> new CMSItemNotFoundException("CMS Item [" + uuid + "] does not exist."));

		getModelService().remove(cmsItem);
	}

	protected CMSItemConverter getCmsItemConverter()
	{
		return cmsItemConverter;
	}

	@Required
	public void setCmsItemConverter(final CMSItemConverter cmsItemConverter)
	{
		this.cmsItemConverter = cmsItemConverter;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	protected UniqueItemIdentifierService getUniqueItemIdentifierService()
	{
		return uniqueItemIdentifierService;
	}

	@Required
	public void setUniqueItemIdentifierService(final UniqueItemIdentifierService uniqueItemIdentifierService)
	{
		this.uniqueItemIdentifierService = uniqueItemIdentifierService;
	}

	protected ItemData createCmsItemData(final String uuid)
	{
		final ItemData itemData = getItemDataDataFactory().getObject();
		itemData.setItemId(uuid);
		itemData.setItemType(CMSItemModel._TYPECODE);
		return itemData;
	}

	/**
	 * Sets the catalogVersion in the current session.
	 * @param source
	 * @throws CMSItemNotFoundException
	 */
	protected void setCatalogInSession(final Map<String, Object> source) throws CMSItemNotFoundException
	{
		if (source == null)
		{
			return;
		}
		final String catalogVersionUUID = (String) source.get("catalogVersion");
		final Optional<CatalogVersionModel> catalogVersionOpt = getUniqueItemIdentifierService().getItemModel(catalogVersionUUID, CatalogVersionModel.class);
		if (catalogVersionOpt.isPresent())
		{
			final CatalogVersionModel catalogVersion = catalogVersionOpt.get();
			getCmsAdminSiteService().setActiveCatalogVersion(catalogVersion.getCatalog().getId(), catalogVersion.getVersion());
			getCatalogVersionService().setSessionCatalogVersion(catalogVersion.getCatalog().getId(), catalogVersion.getVersion());
		}
	}

	protected ObjectFactory<ItemData> getItemDataDataFactory()
	{
		return itemDataDataFactory;
	}

	@Required
	public void setItemDataDataFactory(final ObjectFactory<ItemData> itemDataDataFactory)
	{
		this.itemDataDataFactory = itemDataDataFactory;
	}

	protected ItemTypePopulatorProvider getItemTypePopulatorProvider()
	{
		return itemTypePopulatorProvider;
	}

	@Required
	public void setItemTypePopulatorProvider(final ItemTypePopulatorProvider itemTypePopulatorProvider)
	{
		this.itemTypePopulatorProvider = itemTypePopulatorProvider;
	}

	protected CMSItemSearchService getCmsItemSearchService()
	{
		return cmsItemSearchService;
	}

	@Required
	public void setCmsItemSearchService(final CMSItemSearchService cmsItemSearchService)
	{
		this.cmsItemSearchService = cmsItemSearchService;
	}

	protected Validator getCmsItemSearchDataValidator()
	{
		return cmsItemSearchDataValidator;
	}

	@Required
	public void setCmsItemSearchDataValidator(final Validator cmsItemSearchDataValidator)
	{
		this.cmsItemSearchDataValidator = cmsItemSearchDataValidator;
	}

	protected FacadeValidationService getFacadeValidationService()
	{
		return facadeValidationService;
	}

	@Required
	public void setFacadeValidationService(final FacadeValidationService facadeValidationService)
	{
		this.facadeValidationService = facadeValidationService;
	}

	protected Converter<CMSItemSearchData, de.hybris.platform.cms2.data.CMSItemSearchData> getCmsItemSearchDataConverter()
	{
		return cmsItemSearchDataConverter;
	}

	@Required
	public void setCmsItemSearchDataConverter(
			final Converter<CMSItemSearchData, de.hybris.platform.cms2.data.CMSItemSearchData> cmsItemSearchDataConverter)
	{
		this.cmsItemSearchDataConverter = cmsItemSearchDataConverter;
	}

	protected ValidatableService getValidatableService()
	{
		return validatableService;
	}

	@Required
	public void setValidatableService(final ValidatableService validatableService)
	{
		this.validatableService = validatableService;
	}

	protected CMSItemValidator<ItemModel> getCmsItemValidatorCreate()
	{
		return cmsItemValidatorCreate;
	}

	@Required
	public void setCmsItemValidatorCreate(final CMSItemValidator<ItemModel> cmsItemValidatorCreate)
	{
		this.cmsItemValidatorCreate = cmsItemValidatorCreate;
	}

	protected CMSItemValidator<ItemModel> getCmsItemValidatorUpdate()
	{
		return cmsItemValidatorUpdate;
	}

	@Required
	public void setCmsItemValidatorUpdate(final CMSItemValidator<ItemModel> cmsItemValidatorUpdate)
	{
		this.cmsItemValidatorUpdate = cmsItemValidatorUpdate;
	}

	protected ValidationErrorsProvider getValidationErrorsProvider()
	{
		return validationErrorsProvider;
	}

	@Required
	public void setValidationErrorsProvider(final ValidationErrorsProvider validationErrorsProvider)
	{
		this.validationErrorsProvider = validationErrorsProvider;
	}

	protected PlatformTransactionManager getTransactionManager()
	{
		return transactionManager;
	}

	@Required
	public void setTransactionManager(final PlatformTransactionManager transactionManager)
	{
		this.transactionManager = transactionManager;
	}
	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
		this.catalogVersionService = catalogVersionService;
	}

	protected CatalogVersionService getCatalogVersionService() { return catalogVersionService; }

	@Required
	public void setCmsAdminSiteService(final CMSAdminSiteService cmsAdminSiteService) {
		this.cmsAdminSiteService = cmsAdminSiteService;
	}

	protected CMSAdminSiteService getCmsAdminSiteService() { return cmsAdminSiteService; }

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
