/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.common.validator.impl;

import de.hybris.platform.cmsfacades.common.validator.ValidationErrors;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrorsProvider;
import de.hybris.platform.cmsfacades.constants.CmsfacadesConstants;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default implementation of {@link ValidationErrorsProvider}. 
 * Stores the {@link ValidationErrors} instance on the current Session. 
 */
public class DefaultValidationErrorsProvider implements ValidationErrorsProvider
{
	private ObjectFactory<ValidationErrors> validationErrorsObjectFactory;
	
	private SessionService sessionService;
	
	private final ReentrantLock lock = new ReentrantLock();
	
	@Override
	public ValidationErrors getCurrentValidationErrors()
	{
		final ValidationErrors validationErrors;
		lock.lock();
		try 
		{
			final Object value = getSessionService().getAttribute(CmsfacadesConstants.SESSION_VALIDATION_ERRORS_OBJ);
			if (value == null)
			{
				validationErrors = getValidationErrorsObjectFactory().getObject();
				getSessionService().setAttribute(CmsfacadesConstants.SESSION_VALIDATION_ERRORS_OBJ, validationErrors);
			}
			else
			{
				validationErrors = (ValidationErrors) value;
			}
		} 
		finally
		{
			lock.unlock();
		}
		return validationErrors;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected ObjectFactory<ValidationErrors> getValidationErrorsObjectFactory()
	{
		return validationErrorsObjectFactory;
	}

	@Required
	public void setValidationErrorsObjectFactory(final ObjectFactory<ValidationErrors> validationErrorsObjectFactory)
	{
		this.validationErrorsObjectFactory = validationErrorsObjectFactory;
	}
}
