/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.populators;

import de.hybris.platform.cmsfacades.data.CMSItemSearchData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * CMSItemSearchData populator for cmsfacades to integrate with cms2's version of the Search Data.  
 */
public class CMSItemSearchDataPopulator implements Populator<CMSItemSearchData, de.hybris.platform.cms2.data.CMSItemSearchData>
{
	@Override
	public void populate(final CMSItemSearchData source, final de.hybris.platform.cms2.data.CMSItemSearchData target) throws ConversionException
	{
		target.setMask(source.getMask());
		target.setTypeCode(source.getTypeCode());
		target.setCatalogId(source.getCatalogId());
		target.setCatalogVersion(source.getCatalogVersion());
	}
}
