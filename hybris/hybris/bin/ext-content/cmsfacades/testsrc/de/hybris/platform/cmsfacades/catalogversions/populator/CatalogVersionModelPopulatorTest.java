/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.catalogversions.populator;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cmsfacades.data.CatalogVersionData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatalogVersionModelPopulatorTest
{
	private static final String CATALOG_VERSION = "test-version";
	private static final Boolean CATALOG_ACTIVE = true;

	private final CatalogVersionModelPopulator populator = new CatalogVersionModelPopulator();

	@Mock
	private CatalogVersionModel catalogVersionModel;
	@Mock
	private CatalogVersionData versionDto;

	@Before
	public void setup()
	{
		when(catalogVersionModel.getVersion()).thenReturn(CATALOG_VERSION);
		when(catalogVersionModel.getActive()).thenReturn(CATALOG_ACTIVE);
	}

	@Test
	public void shouldPopulateAllFields() throws Exception
	{
		populator.populate(catalogVersionModel, versionDto);

		verify(versionDto).setActive(CATALOG_ACTIVE);
		verify(versionDto).setVersion(CATALOG_VERSION);
	}
}