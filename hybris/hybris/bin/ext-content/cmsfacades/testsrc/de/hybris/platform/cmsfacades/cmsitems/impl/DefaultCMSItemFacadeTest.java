/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.impl;

import com.google.common.collect.Lists;

import static java.util.Arrays.asList;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cms2.cmsitems.service.CMSItemSearchService;
import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cmsfacades.cmsitems.CMSItemConverter;
import de.hybris.platform.cmsfacades.cmsitems.CMSItemValidator;
import de.hybris.platform.cmsfacades.cmsitems.ItemTypePopulatorProvider;
import de.hybris.platform.cmsfacades.common.function.Converter;
import de.hybris.platform.cmsfacades.common.validator.ValidatableService;
import de.hybris.platform.cmsfacades.common.validator.FacadeValidationService;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrors;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrorsProvider;
import de.hybris.platform.cmsfacades.data.CMSItemSearchData;
import de.hybris.platform.cmsfacades.data.ItemData;
import de.hybris.platform.cmsfacades.uniqueidentifier.UniqueItemIdentifierService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import static de.hybris.platform.cmsfacades.constants.CmsfacadesConstants.FIELD_UUID;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCMSItemFacadeTest
{
	private static final String VALID_UID = "valid-item-uid";
	private static final String INVALID_UID = "invalid-item-uid";

	@InjectMocks
	private DefaultCMSItemFacade facade;

	@Mock
	private PlatformTransactionManager transactionManager;
	
	@Mock
	private ValidationErrorsProvider validationErrorsProvider;

	@Mock
	private CMSAdminSiteService cmsAdminSiteService;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private CMSItemConverter CMSItemConverter;

	@Mock
	private ModelService modelService;

	@Mock
	private UniqueItemIdentifierService uniqueItemIdentifierService;

	@Mock
	private ObjectFactory<ItemData> itemDataDataFactory;
	
	@Mock
	private ItemModel itemModel;
	
	@Mock
	private ItemData invalidItemData;

	@Mock
	private ItemData validItemData;
	
	@Mock
	private ItemTypePopulatorProvider itemTypePopulatorProvider;

	@Mock
	private Populator<Map<String, Object>, ItemModel> populator;

	@Mock
	private CMSItemSearchService cmsItemSearchService;

	@Mock
	private Validator cmsItemSearchParamsDataValidator;

	@Mock
	private FacadeValidationService facadeValidationService;

	@Mock
	private SessionService sessionService;

	@Mock
	private Converter<CMSItemSearchData, de.hybris.platform.cms2.data.CMSItemSearchData> cmsItemSearchDataConverter;

	private ValidatableService validatableService = new ValidatableService()
	{
		@Override
		public <T> T execute(final Supplier<T> validatable)
		{
			return validatable.get();
		}
	};

	@Mock
	private CMSItemValidator<ItemModel> cmsItemValidatorCreate;

	@Mock
	private CMSItemValidator<ItemModel> cmsItemValidatorUpdate;
	@Mock
	private ValidationErrors validationErrors;
	
	@Before
	public void setup() throws CMSItemNotFoundException
	{
		when(validationErrors.getValidationErrors()).thenReturn(asList());
		when(validationErrorsProvider.getCurrentValidationErrors()).thenReturn(validationErrors);
		when(uniqueItemIdentifierService.getItemModel(invalidItemData)).thenReturn(Optional.empty());
		when(uniqueItemIdentifierService.getItemModel(validItemData)).thenReturn(Optional.of(itemModel));
		when(uniqueItemIdentifierService.getItemModel(any(), any())).thenReturn(Optional.empty());
		when(itemTypePopulatorProvider.getItemTypePopulator(any())).thenReturn(Optional.of(populator));
		when(CMSItemConverter.convert(any(Map.class))).thenReturn(itemModel);
		facade.setValidatableService(validatableService);
	}
	
	@Test(expected = CMSItemNotFoundException.class)
	public void whenFindByIdWithInvalidUidThenShouldThrowException() throws CMSItemNotFoundException
	{
		when(itemDataDataFactory.getObject()).thenReturn(invalidItemData);
		facade.getCMSItemByUuid(INVALID_UID);
	}
	
	@Test
	public void whenFindByIdWithValidUidShouldPerformConversion() throws CMSItemNotFoundException
	{
		when(itemDataDataFactory.getObject()).thenReturn(validItemData);
		facade.getCMSItemByUuid(VALID_UID);
		
		verify(CMSItemConverter).convert(itemModel);
	}

	@Test
	public void whenDeleteByIdWithValidUidShouldRemoveItem() throws CMSItemNotFoundException
	{
		when(itemDataDataFactory.getObject()).thenReturn(validItemData);
		facade.deleteCMSItemByUuid(VALID_UID);
		verify(modelService).remove(itemModel);
	}

	@Test
	public void shouldCreateItem() throws CMSItemNotFoundException
	{
		final Map<String, Object> map = new HashMap<>();
		facade.createItem(map);
		verify(CMSItemConverter).convert(map);
		verify(populator).populate(map, itemModel);
		verify(CMSItemConverter).convert(itemModel);
		verify(cmsItemValidatorCreate).validate(itemModel);
	}

	@Test(expected = CMSItemNotFoundException.class)
	public void updateShouldThrowExceptionWhenUuidIsInvalid() throws CMSItemNotFoundException
	{
		when(itemDataDataFactory.getObject()).thenReturn(invalidItemData);
		facade.updateItem(INVALID_UID, new HashMap<>());
	}

	@Test(expected = CMSItemNotFoundException.class)
	public void shouldThrowExceptionWhenUpdateItemHasInconsistentValue() throws CMSItemNotFoundException
	{
		when(itemDataDataFactory.getObject()).thenReturn(validItemData);
		final Map<String, Object> map = new HashMap<>();
		facade.updateItem(VALID_UID, map);
		verify(CMSItemConverter).convert(map);
		verify(populator).populate(map, itemModel);
		verify(CMSItemConverter).convert(itemModel);
	}

	@Test
	public void shouldUpdateItem() throws CMSItemNotFoundException
	{
		when(itemDataDataFactory.getObject()).thenReturn(validItemData);
		final Map<String, Object> map = new HashMap<>();
		map.put(FIELD_UUID, VALID_UID);
		facade.updateItem(VALID_UID, map);
		verify(CMSItemConverter).convert(map);
		verify(populator).populate(map, itemModel);
		verify(CMSItemConverter).convert(itemModel);
		verify(cmsItemValidatorUpdate).validate(itemModel);
	}
	
	@Test
	public void shouldSearchForItems() {

		// Input
		final CMSItemSearchData cmsItemSearchData = new CMSItemSearchData();
		final PageableData pageableData = new PageableData();

		// Intermediary data
		final SearchResult<CMSItemModel> modelResults = mock(SearchResult.class);
		final CMSItemModel mockItem1 = mock(CMSItemModel.class);
		final CMSItemModel mockItem2 = mock(CMSItemModel.class);
		final List<CMSItemModel> mockedResuls = Lists.newArrayList(mockItem1, mockItem2);

		final de.hybris.platform.cms2.data.CMSItemSearchData cms2ItemSearchData = mock(de.hybris.platform.cms2.data.CMSItemSearchData.class);
		when(cmsItemSearchDataConverter.convert(cmsItemSearchData)).thenReturn(cms2ItemSearchData);

		when(cmsItemSearchService.findCMSItems(cms2ItemSearchData, pageableData)).thenReturn(modelResults);
		when(modelResults.getResult()).thenReturn(mockedResuls);

		facade.findCMSItems(cmsItemSearchData, pageableData);

		verify(facadeValidationService).validate(cmsItemSearchParamsDataValidator, cmsItemSearchData);
		verify(cmsItemSearchService).findCMSItems(cms2ItemSearchData, pageableData);
		verify(CMSItemConverter).convert(mockItem1);
		verify(CMSItemConverter).convert(mockItem2);
	}


}
