/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrors;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrorsProvider;
import de.hybris.platform.cmsfacades.common.validator.impl.DefaultValidationErrors;
import de.hybris.platform.cmsfacades.validator.data.ValidationError;

import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.Matchers.is;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCMSLinkComponentValidatorTest
{
    private final String LINK_TO = "linkTo";
    private final String LINK_TO_ERROR_CODE = "The Link To field is required.";

    @InjectMocks
    private DefaultCMSLinkComponentValidator validator;

    @Mock
    private ValidationErrorsProvider validationErrorsProvider;

    private ValidationErrors validationErrors = new DefaultValidationErrors();

    @Before
    public void setup()
    {
        when(validationErrorsProvider.getCurrentValidationErrors()).thenReturn(validationErrors);
    }

    @Test
    public void testValidateWithoutRequiredAttributeAddOneError()
    {
        final CMSLinkComponentModel itemModel = new CMSLinkComponentModel();
        validator.validate(itemModel);

        final List<ValidationError> errors = validationErrorsProvider.getCurrentValidationErrors().getValidationErrors();

        assertEquals(1, errors.size());
        assertThat(errors.get(0).getField(), is(LINK_TO));
        assertThat(errors.get(0).getErrorCode(), is(LINK_TO_ERROR_CODE));
    }

    @Test
    public void testValidateWithProductModelAddNoError()
    {
        final CMSLinkComponentModel itemModel = new CMSLinkComponentModel();
        itemModel.setProduct(new ProductModel());
        validator.validate(itemModel);

        final List<ValidationError> errors = validationErrorsProvider.getCurrentValidationErrors().getValidationErrors();

        assertTrue(errors.isEmpty());
    }
}
