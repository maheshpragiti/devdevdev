/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.common.validator.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrors;
import de.hybris.platform.cmsfacades.constants.CmsfacadesConstants;
import de.hybris.platform.servicelayer.session.SessionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.ObjectFactory;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultValidationErrorsProviderTest
{
	@Mock
	private SessionService sessionService;

	@Mock
	private ObjectFactory<ValidationErrors> validationErrorsObjectFactory;
	
	@InjectMocks
	private DefaultValidationErrorsProvider validationErrorsProvider;
	
	@Before
	public void setup()
	{
		when(validationErrorsObjectFactory.getObject()).thenReturn(new DefaultValidationErrors());
	}
	
	@Test
	public void testWhenSessionDoesNotHaveValidationErrorsStored_shouldCreateValidationErrorsAndStoreInSesion()
	{
		final ValidationErrors validationErrors = validationErrorsProvider.getCurrentValidationErrors();
		verify(sessionService).setAttribute(CmsfacadesConstants.SESSION_VALIDATION_ERRORS_OBJ, validationErrors);
	}


	@Test
	public void testWhenSessionHasValidationErrorsStored_shouldNotCreateNewValidationError()
	{
		final ValidationErrors validationErrors = mock(ValidationErrors.class);
		when(sessionService.getAttribute(CmsfacadesConstants.SESSION_VALIDATION_ERRORS_OBJ)).thenReturn(validationErrors);
		final ValidationErrors validationErrorsReturned = validationErrorsProvider.getCurrentValidationErrors();
		verify(sessionService).getAttribute(CmsfacadesConstants.SESSION_VALIDATION_ERRORS_OBJ);
		verifyNoMoreInteractions(sessionService);
		assertThat(validationErrors, is(validationErrorsReturned));
	}
	
}
