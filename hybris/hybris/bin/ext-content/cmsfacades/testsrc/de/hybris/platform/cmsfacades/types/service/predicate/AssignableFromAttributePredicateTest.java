/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.types.service.predicate;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.servicelayer.services.AttributeDescriptorModelHelperService;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.type.TypeService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AssignableFromAttributePredicateTest
{
	@Mock
	private TypeService typeService;
	@Mock
	private AttributeDescriptorModelHelperService attributeDescriptorModelHelperService;

	@InjectMocks
	private AssignableFromAttributePredicate assignableFromAttributePredicate;

	@Mock
	private AttributeDescriptorModel attributeDescriptor;
	@Mock
	private ComposedTypeModel composedTypeModel;

	public static class ParentType
	{

	};

	public static class SubTypeOfParentType extends ParentType
	{

	};

	public static class SomeType
	{

	};

	@Before
	public void setUp()
	{

		assignableFromAttributePredicate.setTypeCode("ParentType");
		when(typeService.getComposedTypeForCode("ParentType")).thenReturn(composedTypeModel);
		doReturn(ParentType.class).when(typeService).getModelClass(composedTypeModel);
	}

	@Test
	public void givenAttributeDescriptorWhenTypeIsAssignableToProvidedTypeCodeWillReturnTrue()
	{

		doReturn(SubTypeOfParentType.class).when(attributeDescriptorModelHelperService).getAttributeClass(attributeDescriptor);

		assertThat(assignableFromAttributePredicate.test(attributeDescriptor), is(true));
	}

	@Test
	public void givenAttributeDescriptorWhenTypeIsNotAssignableToProvidedTypeCodeWillReturnFalse()
	{

		doReturn(SomeType.class).when(attributeDescriptorModelHelperService).getAttributeClass(attributeDescriptor);

		assertThat(assignableFromAttributePredicate.test(attributeDescriptor), is(false));
	}



}
