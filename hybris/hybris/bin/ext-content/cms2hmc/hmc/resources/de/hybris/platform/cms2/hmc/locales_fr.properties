# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
# All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
cms2=WCMS
type_tree_componenttypegroup=Groupe de types de composants
type_tree_abstractcmscomponentcontainer=Conteneurs de composants
type_tree_abstractpage=Pages
type_tree_abstractrestriction=Restrictions
type_tree_cmssite=Sites Web
type_tree_contentslot=Emplacements de contenu
type_tree_contentslotforpage=Emplacements de contenu pour pages
type_tree_contentslotfortemplate=Emplacements de contenu pour modèles de pages
type_tree_pagetemplate=Modèles de pages
type_tree_simplecmscomponent=Composants
type_tree_cmsnavigationnode=Nœuds de navigation
type_tree_cmspagetype=Types de page
type_tree_restrictiontype=Types de restriction
typeref.cms.description=hybris WCMS
typeref.cmssite.description=L’objet site est le point d’entrée du système frontal d’une boutique. Il collecte les catalogues (produits et contenu) qui seront disponibles au niveau du système frontal et les schémas d'URL pour activer le site.
typeref.cmspagetype.description=Types de page
typeref.pagetemplate.description=Un modèle de page est un document maître pour les pages. Il définit les emplacements de contenu disponibles pour une page et peut également comporter des emplacements de contenu préconfigurés.
typeref.contentslotfortemplate.description=Modélise une relation entre un emplacement de contenu et un modèle de page avec une position donnée (par exemple, l’emplacement de contenu "pied de page" pour le modèle de page "principal" à la position "pied de page").
typeref.abstractpage.description=Page. Il peut s'agir d’une page de contenu, d’une page de catégorie, d’une page produit, etc.
typeref.contentslot.description=Les emplacements de contenu sont placés sur les pages et contiennent des composants.
typeref.contentslotforpage.description=Modélise une relation entre un emplacement de contenu et une page avec une position donnée (par exemple, l’emplacement de contenu "home_main" pour la page "homepage" à la position "main").
typeref.simplecmscomponent.description=Un composant, positionné dans les emplacements de contenu, comprend des informations éditoriales, comme un titre, du texte, une image, etc.
typeref.abstractcmscomponentcontainer.description=Conteneur de composants.
typeref.restrictiontype.description=Types de restriction
typeref.abstractrestriction.description=Les restrictions permettent de gérer la visibilité des pages. Elles déterminent le contenu/le produit/la catégorie/la page de catalogue présentés à un utilisateur.
typeref.cmsnavigationnode.description=Nœuds de navigation pour le WCMS. Un nœud de navigation peut comporter des nœuds de navigation enfants, une liste de liens et/ou une liste de pages de contenu. Ce type permet de modéliser les navigations classiques.
typeref.componenttypegroup.description=Un groupe de types de composants permet de définir des groupes personnalisés de types de composants CMS. Ce type de groupe permet de définir les types de composants valides pour l’emplacement de contenu.
section.cmssite.contentcatalogs=Catalogues de contenu
txt.cmssite.contentcatalogs=Les catalogues de contenu suivants sont disponibles pour ce site.
txt.cmssite.catalog.default=Si ce site contient plusieurs catalogues de produits, sélectionnez le catalogue de produits qui sera activé initialement.
section.cmssite.config=Configuration
txt.cmssite.startingpage=La page d’ouverture sélectionnée sera la première page présentée au client lorsque celui-ci accédera à la boutique.
txt.cmssite.redirect=Si vous voulez rediriger le client vers un autre emplacement, vous pouvez utiliser l’URL de redirection (dans ce cas, laissez la page d’ouverture vide).
txt.cmssite.urlpatterns=Les schémas d’URL correspondent à une liste d’expressions régulières. Si une URL donnée correspond à l’un de ces schémas, ce site est activé.
section.cmssite.active=Actif
txt.cmssite.active=Si vous définissez une date de début d'activation et/ou une date de fin d'activation, le site sera uniquement disponible au cours de cette période. Vous ne devez pas indiquer les deux valeurs.
section.cmssite.cockpit=Cockpit WCMS
section.cms.admin=Administration WCMS
section.cms.restrictions=Restrictions
txt.contentslotname.general=Le nom d’un emplacement de contenu définit une section/un emplacement dans un modèle de page. Un emplacement de contenu peut être affecté à chaque section.
section.pagetemplate.contentslots=Emplacements de contenu
text.pagetemplate.availableslots=Il s'agit de la liste de tous les noms de sections qui seront disponibles pour ce modèle de page. Chaque section peut se voir affecter un emplacement de contenu.
text.pagetemplate.contentslots=Les emplacements de contenu suivants peuvent être définis pour ce modèle de page. Par conséquent, si une page est créée à l’aide de ce modèle de page, ces emplacements de contenu seront automatiquement affectés à la nouvelle page.
text.slotfortemplate.allowoverwrite=Si le paramètre est défini sur true, l’emplacement de contenu peut être écrasé au niveau de la page. S'il est défini sur false, l’emplacement de contenu affecté ne peut pas être écrasé au niveau de la page.
text.contentpage.label=Une page de contenu peut également être adressée via son étiquette. Vous pouvez regrouper des pages restreintes en définissant la même étiquette pour toutes ces pages.
section.contentslot.active=Actif
txt.contentslot.active=Vous pouvez indiquer ici si cet emplacement de contenu doit être actif ou non, ou s’il doit uniquement être actif pour une période donnée. Vous pouvez également n’indiquer qu’une date d'activation.
section.contentslot.components=Composants
txt.contentslot.components=Les composants suivants sont affectés à cet emplacement de contenu
section.cmscomponent.visibility=Visibilité
tab.components.contentslots=Emplacements de contenu
section.components.contentslots=Emplacements de contenu
txt.components.contentslots=Ce composant est affecté aux emplacements de contenu suivants
section.componentcontainer.components=Composants
txt.componentcontainer.components=Les composants suivants dépendent de ce conteneur de composants
section.abtestcontainer.scope=Étendue de test A/B
text.page.title=Titre de la page dans le système frontal. Cette valeur est localisée.
text.page.defaultpage=S’il existe plusieurs pages pour une ressource, une page au moins doit être définie comme page par défaut en cas de reprise.
tab.page.contentslots=Emplacements de contenu
section.page.contentslots.configuration=Configuration des emplacements de contenu
text.page.availablecontentslots=Emplacements de contenu disponibles pour cette page. Cette liste provient du modèle de page.
text.page.missingcontentslots=Emplacements de contenu qui ne sont pas définis pour cette page et ne sont pas non plus définis dans le modèle de page.
section.page.contentslots=Emplacements de contenu
text.page.contentslots=Les emplacements de contenu suivants sont affectés à cette page.
tab.cms.restrictions=Restrictions
text.page.restrictions=Les restrictions suivantes sont affectées à cette page :
restriction.pages=Pages restreintes
restriction.components=Composants restreints
text.restriction.pages=Les pages suivantes sont affectées à cette restriction :
section.restriction.objects=Valeurs de restriction
section.cmsnavigationnode.nodes=Nœuds de navigation parent et enfants
text.cmsnavigationnode.parent=Nœud de navigation qui représente le parent pour le nœud de navigation concerné.
text.cmsnavigationnode.children=Nœud de navigation qui représente les enfants pour le nœud de navigation concerné.
section.cmsnavigationnode.links=Composants de lien
section.cmsnavigationnode.pages=Pages de contenu
tab.cms.navigationnodes=Nœuds de navigation
section.navigationnodes=Nœuds de navigation
text.pages.navigationnodes=Cette page est affectée avec les nœuds de navigation suivants :
text.cmsnavigationnode.pages=Ce nœud de navigation est lié aux pages suivantes :
text.cmsnavigationnode.links=Liens qui sont affectés à ce nœud de navigation
section.basecommerce.cmssites=Sites Web
template.cmslinkcomponent.internallink.name=Lien interne
template.cmslinkcomponent.internallink.description=
template.cmslinkcomponent.externallink.name=Lien externe
template.cmslinkcomponent.externallink.description=
txt.cmssite.validcomponenttypes=Liste des types de composants pouvant être utilisés dans ce site.
section.cmssite.basestores=Magasins de base
tab.wcms.cockpit.properties=Propriétés du cockpit WCMS
section.cms.component.configuration=Configuration des composants CMS
section.wcms.cockpit.preview.information=Informations d'aperçu du cockpit WCMS
txt.componenttypegroup.general=Un groupe de types de composants permet de définir des groupes personnalisés de types de composants CMS. Ce type de groupe permet de définir les types de composants valides pour l’emplacement de contenu.
tab.pagetypes.applicablerestrictiontypes=Types de restriction applicables
section.pagetypes.applicablerestricitiontypes=Types de restriction applicables
tab.restrictiontype.assignedpagetypes=Types de page affectés
section.restrictiontype.assignedpagetypes=Types de page affectés
section.cms.pagetemplate.previewicon=Icône d'aperçu
