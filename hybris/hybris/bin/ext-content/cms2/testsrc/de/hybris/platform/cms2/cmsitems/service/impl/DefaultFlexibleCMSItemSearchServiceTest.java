/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cms2.cmsitems.service.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.cms2.data.CMSItemSearchData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultFlexibleCMSItemSearchServiceTest
{

    private static final String CATALOG_ID = "CATALOG_ID";
    private static final String CATALOG_VERSION = "CATALOG_VERSION";

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private CatalogVersionModel catalogVersionModel;

    @InjectMocks
    private DefaultFlexibleCMSItemSearchService flexibleCMSItemsSearchService;

    @Before
    public void setup() {
        when(catalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION)).thenReturn(catalogVersionModel);
    }

    @Test
    public void testSearchQueryContainsNOMaskAndNOType() {

        final String mask = null;
        final String type = null;
        final FlexibleSearchQuery capturedQuery = triggerQuery(mask, type);

        assertFalse(isValidMaskQuery(capturedQuery, mask));
        assertFalse(isValidTypeQuery(capturedQuery, type));
    }

    @Test
    public void testSearchQueryContainsMaskAndNOType() {

        final String mask = "someMask";
        final String type = null;
        final FlexibleSearchQuery capturedQuery = triggerQuery(mask, type);

        assertTrue(isValidMaskQuery(capturedQuery, mask));
        assertFalse(isValidTypeQuery(capturedQuery, type));
    }

    @Test
    public void testSearchQueryContainsNOMaskAndType() {

        final String mask = null;
        final String type = "someType";
        final FlexibleSearchQuery capturedQuery = triggerQuery(mask, type);

        assertFalse(isValidMaskQuery(capturedQuery, mask));
        assertTrue(isValidTypeQuery(capturedQuery, type));
    }

    @Test
    public void testSearchContainsMaskAndType() {

        final String mask = "someMask";
        final String type = "someType";
        final FlexibleSearchQuery capturedQuery = triggerQuery(mask, type);

        assertTrue(isValidMaskQuery(capturedQuery, mask));
        assertTrue(isValidTypeQuery(capturedQuery, type));
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionIsThrownForMissingSearchParamData() {
        flexibleCMSItemsSearchService.findCMSItems(null, new PageableData());
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionIsThrownForMissingPagingData() {
        flexibleCMSItemsSearchService.findCMSItems(new CMSItemSearchData(), null);
    }


    protected FlexibleSearchQuery triggerQuery(final String mask, final String typeCode) {

        final CMSItemSearchData cmsItemSearchData = new CMSItemSearchData();
        cmsItemSearchData.setCatalogId(CATALOG_ID);
        cmsItemSearchData.setCatalogVersion(CATALOG_VERSION);
        cmsItemSearchData.setMask(mask);
        cmsItemSearchData.setTypeCode(typeCode);

        final PageableData pageableData = new PageableData();

        flexibleCMSItemsSearchService.findCMSItems(cmsItemSearchData, pageableData);

        // capture query
        final ArgumentCaptor<FlexibleSearchQuery> args = ArgumentCaptor.forClass(FlexibleSearchQuery.class);
        verify(flexibleSearchService).search(args.capture());

        return args.getValue();
    }


    protected boolean isValidMaskQuery(final FlexibleSearchQuery flexibleSearchQuery, final String expectedMask) {
        return (flexibleSearchQuery.getQueryParameters().containsKey(DefaultFlexibleCMSItemSearchService.QUERY_PARAM_MASK)
                && flexibleSearchQuery.getQueryParameters().get(DefaultFlexibleCMSItemSearchService.QUERY_PARAM_MASK).equals("%" + expectedMask + "%")
                && flexibleSearchQuery.getQuery().contains(DefaultFlexibleCMSItemSearchService.MASK_CHECK));
    }

	protected boolean isValidTypeQuery(final FlexibleSearchQuery flexibleSearchQuery, final String expectedType)
	{
		return (expectedType != null && flexibleSearchQuery.getQuery().contains(expectedType));
	}

}
