/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cms2.servicelayer.services.admin.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForPageModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForTemplateModel;
import de.hybris.platform.cms2.servicelayer.daos.CMSContentSlotDao;
import de.hybris.platform.cms2.servicelayer.data.CMSDataFactory;
import de.hybris.platform.cms2.servicelayer.data.ContentSlotData;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCMSAdminContentSlotServiceTest
{

	private static final int MIN_CONTENT_SLOT_RELATIONS = 1;
	private static final int MAX_CONTENT_SLOT_RELATIONS = 5;

	private static final String CONTENT_SLOT_POSITION = "contentSlotPosition";

	@Captor
	private ArgumentCaptor<ContentSlotModel> savedContentSlotCaptor;

	@Captor
	private ArgumentCaptor<ContentSlotForPageModel> savedContentSlotForPageCaptor;

	@Mock
	private KeyGenerator keyGenerator;

	@Mock
	private ModelService modelService;

	@InjectMocks
	private DefaultCMSAdminContentSlotService cmsAdminContentSlotService;

	@Mock
	private DefaultCMSAdminComponentService cmsAdminComponentService;

	@Mock
	private CMSContentSlotDao cmsContentSlotDao;

	@Mock
	private CMSDataFactory cmsDataFactory;

	@Mock
	private AbstractPageModel page;

	@Mock
	private PageTemplateModel template;

	@Mock
	private CatalogVersionModel catalogVersion;

	@Mock
	private SessionService sessionService;

	@Mock
	private Date from;

	@Mock
	private Date to;

	/**
	 * Generates a list of {@value MIN_CONTENT_SLOT_RELATIONS} to
	 * {@value MAX_CONTENT_SLOT_RELATIONS} content slot relation model mocks for the page.
	 *
	 * @return the list content slot relation model mocks for the page
	 */
	protected List<ContentSlotForPageModel> generateContentSlotForPageModelMocks()
	{
		List<ContentSlotForPageModel> contentSlotForPageModels = new ArrayList<>();

		int contentSlotRelationCount = ThreadLocalRandom.current().nextInt(
				MIN_CONTENT_SLOT_RELATIONS, MAX_CONTENT_SLOT_RELATIONS);

		for (int i = 0; i < contentSlotRelationCount; i++)
		{
			ContentSlotForPageModel contentSlotForPageModel = mock(ContentSlotForPageModel.class);
			when(contentSlotForPageModel.getPosition()).thenReturn(CONTENT_SLOT_POSITION + i);

			contentSlotForPageModels.add(contentSlotForPageModel);
		}

		return contentSlotForPageModels;
	}

	/**
	 * Generates a list of {@value MIN_CONTENT_SLOT_RELATIONS} to
	 * {@value MAX_CONTENT_SLOT_RELATIONS} content slot relation model mocks for the page
	 * template.
	 *
	 * @param withPositions
	 *           true if the template slots should have a position parameter to test the
	 *           filtering out of custom content slots on the page
	 * @return the list content slot relation model mocks for the page template
	 */
	protected List<ContentSlotForTemplateModel> generateContentSlotForTemplateModelMocks(boolean withPositions)
	{
		List<ContentSlotForTemplateModel> contentSlotForTemplateModels = new ArrayList<>();

		int contentSlotRelationCount = ThreadLocalRandom.current().nextInt(
				MIN_CONTENT_SLOT_RELATIONS, MAX_CONTENT_SLOT_RELATIONS);

		for (int i = 0; i < contentSlotRelationCount; i++)
		{
			ContentSlotForTemplateModel contentSlotForTemplateModel = mock(ContentSlotForTemplateModel.class);

			if (withPositions)
			{
				when(contentSlotForTemplateModel.getPosition()).thenReturn(CONTENT_SLOT_POSITION + i);
			}

			contentSlotForTemplateModels.add(contentSlotForTemplateModel);
		}

		return contentSlotForTemplateModels;
	}

	/**
	 * Calculates how many content slots should be returned, based on the number of content slots on the page,
	 * the template and if an overlap of slots (custom page slots replacing template slots) is expected.
	 * @param contentSlotsForPageCount
	 *           the number of content slots on the page
	 * @param contentSlotsForTemplateCount
	 *           the number of content slots on the template
	 * @param isOverlapExpected
	 *           true if there are custom content slots on the page that override the slot on the template,
	 *           false otherwise
	 * @return
	 */
	protected int calculateNumberExpectedContentSlotsForPage(int contentSlotsForPageCount, int contentSlotsForTemplateCount,
			boolean isOverlapExpected)
	{
		int expectedContentSlotsForPageCount;

		if (isOverlapExpected)
		{
			expectedContentSlotsForPageCount = contentSlotsForTemplateCount > contentSlotsForPageCount
					? contentSlotsForPageCount + (contentSlotsForTemplateCount - contentSlotsForPageCount) : contentSlotsForPageCount;
		}
		else
		{
			expectedContentSlotsForPageCount = contentSlotsForPageCount + contentSlotsForTemplateCount;
		}

		return expectedContentSlotsForPageCount;
	}

	@Before
	public void setUp()
	{
		when(keyGenerator.generate()).thenReturn("generatedKey");

		when(page.getUid()).thenReturn("mypage$uid");
		when(page.getCatalogVersion()).thenReturn(catalogVersion);

		when(cmsDataFactory.createContentSlotData(any(ContentSlotForPageModel.class)))
				.thenReturn(mock(ContentSlotData.class));
		when(cmsDataFactory.createContentSlotData(any(AbstractPageModel.class), any(ContentSlotForTemplateModel.class)))
				.thenReturn(mock(ContentSlotData.class));
	}

	@Test
	public void willCreateAndAssignSlotsToThePositionsNotBusyWithSharedSlots()
	{
		ContentSlotModel returnValue = cmsAdminContentSlotService.createContentSlot(page, null, "name1", "position1", true, from,
				to);

		verify(modelService, times(1)).saveAll(savedContentSlotCaptor.capture(), savedContentSlotForPageCaptor.capture());

		ContentSlotModel contentSlotModel = savedContentSlotCaptor.getValue();
		ContentSlotForPageModel contentSlotForPageModel = savedContentSlotForPageCaptor.getValue();

		assertThat(returnValue, is(contentSlotModel));

		assertThat(contentSlotModel, allOf(
				hasProperty("uid", is("position1Slot-mypage-uid")),
				hasProperty("name", is("name1")),
				hasProperty("active", is(true)),
				hasProperty("activeFrom", is(from)),
				hasProperty("activeUntil", is(to)),
				hasProperty("catalogVersion", is(catalogVersion))));

		assertThat(contentSlotForPageModel, allOf(
				hasProperty("uid", is("contentSlotForPage-generatedKey")),
				hasProperty("catalogVersion", is(catalogVersion)),
				hasProperty("position", is("position1")),
				hasProperty("page", is(page)),
				hasProperty("contentSlot", is(contentSlotModel))));

	}

	@Test
	public void willReturnOnlyContentSlotsForPage()
	{
		// Setup
		List<ContentSlotForPageModel> expectedContentSlotsForPage = generateContentSlotForPageModelMocks();
		when(cmsContentSlotDao.findAllContentSlotRelationsByPage(any(AbstractPageModel.class), any(CatalogVersionModel.class)))
				.thenReturn(expectedContentSlotsForPage);

		// Act
		Collection<ContentSlotData> actualContentSlotsForPage = cmsAdminContentSlotService.getContentSlotsForPage(page, false);

		// Assert
		assertEquals(expectedContentSlotsForPage.size(), actualContentSlotsForPage.size());

		verify(cmsContentSlotDao, never()).findAllContentSlotRelationsByPageTemplate(template, catalogVersion);
	}

	@Test
	public void willReturnContentSlotsForPageAndForMasterTemplate()
	{
		// Setup
		List<ContentSlotForPageModel> expectedContentSlotsForPage = generateContentSlotForPageModelMocks();
		when(cmsContentSlotDao.findAllContentSlotRelationsByPage(any(AbstractPageModel.class), any(CatalogVersionModel.class)))
				.thenReturn(expectedContentSlotsForPage);

		List<ContentSlotForTemplateModel> expectedContentSlotsForTemplate = generateContentSlotForTemplateModelMocks(false);
		when(cmsContentSlotDao.findAllContentSlotRelationsByPageTemplate(any(PageTemplateModel.class),
				any(CatalogVersionModel.class)))
						.thenReturn(expectedContentSlotsForTemplate);

		int expectedContentSlotsForPageCount = calculateNumberExpectedContentSlotsForPage(expectedContentSlotsForPage.size(),
				expectedContentSlotsForTemplate.size(), false);

		// Act
		Collection<ContentSlotData> actualContentSlotsForPage = cmsAdminContentSlotService.getContentSlotsForPage(page, true);

		// Assert
		assertEquals(expectedContentSlotsForPageCount, actualContentSlotsForPage.size());
	}

	@Test
	public void willReturnCustomContentSlotsInTemplateSlotPositions()
	{
		// Setup
		List<ContentSlotForPageModel> expectedContentSlotsForPage = generateContentSlotForPageModelMocks();
		when(cmsContentSlotDao.findAllContentSlotRelationsByPage(any(AbstractPageModel.class), any(CatalogVersionModel.class)))
				.thenReturn(expectedContentSlotsForPage);

		List<ContentSlotForTemplateModel> expectedContentSlotsForTemplate = generateContentSlotForTemplateModelMocks(true);
		when(cmsContentSlotDao.findAllContentSlotRelationsByPageTemplate(any(PageTemplateModel.class),
				any(CatalogVersionModel.class)))
						.thenReturn(expectedContentSlotsForTemplate);

		int expectedContentSlotsForPageCount = calculateNumberExpectedContentSlotsForPage(expectedContentSlotsForPage.size(),
				expectedContentSlotsForTemplate.size(), true);

		// Act
		Collection<ContentSlotData> actualContentSlotsForPage = cmsAdminContentSlotService.getContentSlotsForPage(page, true);

		// Assert
		assertEquals(expectedContentSlotsForPageCount, actualContentSlotsForPage.size());
	}

}
