/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    var perspectives = require("../utils/components/Perspectives.js");
    var storefront = require("../utils/components/Storefront.js");
    var hotKeys = require("../utils/components/HotKeys.js");
    var toolbar = require("../utils/components/WhiteToolbarComponentObject.js");
    var somePerspective = "somenameI18nKey";
    var permissionsPerspective = "permissionsI18nKey";
    var defaultPerspectiveName = "PERSPECTIVE.NONE.NAME";

    describe('Perspectives', function() {

        beforeEach(function() {
            perspectives.actions.openAndBeReady();
        });

        it('WHEN application starts THEN default perspective is selected.', function() {

            perspectives.assertions.assertPerspectiveActive(defaultPerspectiveName);
        });

        it('GIVEN application started in default perspective, features are off; WHEN new perspective is selected THEN features are activated and decorators are present in overlay', function() {

            storefront.assertions.assertComponentContains(
                storefront.constants.COMPONENT_1_ID, 'test component 1');

            storefront.assertions.assertComponentInOverlayPresent(
                storefront.constants.COMPONENT_1_ID, storefront.constants.COMPONENT_1_TYPE, false);

            // Act
            perspectives.actions.selectPerspective(somePerspective);

            // Assert
            perspectives.assertions.assertPerspectiveActive(somePerspective);

            storefront.assertions.assertComponentInOverlayPresent(
                storefront.constants.COMPONENT_1_ID, storefront.constants.COMPONENT_1_TYPE, true);

            storefront.assertions.assertComponentContains(
                storefront.constants.COMPONENT_1_ID, 'test component 1');

            storefront.assertions.assertComponentInOverlayContains(
                storefront.constants.COMPONENT_1_ID, storefront.constants.COMPONENT_1_TYPE,
                'Text_is_been_displayed_TextDisplayDecorator');
        });

        it('IF application has not changed default perspective WHEN deep linked THEN default perspective is still selected and component is not present in the overlay', function() {

            perspectives.assertions.assertPerspectiveActive(defaultPerspectiveName);

            // Act
            storefront.actions.deepLink();

            // Assert
            perspectives.assertions.assertPerspectiveActive(defaultPerspectiveName);

            storefront.assertions.assertComponentInOverlayPresent(
                storefront.constants.COMPONENT_2_ID, storefront.constants.COMPONENT_2_TYPE, false);

            storefront.assertions.assertComponentHtmlContains(
                storefront.constants.COMPONENT_2_ID, storefront.constants.COMPONENT_2_ID);

        });

        it('IF application is in preview mode THEN hotkey tooltip icon is not present', function() {

            // Act
            perspectives.actions.selectPerspective(defaultPerspectiveName);

            // Assert
            perspectives.assertions.assertPerspectiveActive(defaultPerspectiveName);
            hotKeys.assertions.assertHotkeyTooltipIconPresent(false);
        });

        it('IF application is not in preview mode THEN hotkey tooltip icon is present', function() {

            // Act
            perspectives.actions.selectPerspective(somePerspective);

            // Assert
            perspectives.assertions.assertPerspectiveActive(somePerspective);
            hotKeys.assertions.assertHotkeyTooltipIconPresent(true);
        });

        it('GIVEN application started in default perspective, features are off; WHEN new perspective is selected AND permission service returns true for inner feature THEN feature is present in overlay', function() {
            // WHEN
            permissionServiceProvidesPositiveResult();
            perspectives.actions.selectPerspective(permissionsPerspective);

            //THEN
            storefront.assertions.assertComponentInOverlayContains(
                storefront.constants.COMPONENT_3_ID, storefront.constants.COMPONENT_3_TYPE,
                'Test permission component');
        });

        it('GIVEN application started in default perspective, features are off; WHEN new perspective is selected AND permission service returns true for inner feature THEN feature whose permissions were registered from the inner app is present in overlay', function() {
            // WHEN
            permissionServiceProvidesPositiveResult();
            perspectives.actions.selectPerspective(permissionsPerspective);

            //THEN
            storefront.assertions.assertComponentInOverlayContains(
                storefront.constants.COMPONENT_4_ID, storefront.constants.COMPONENT_4_TYPE,
                'Test permission decorator registered inner');
        });

        it('GIVEN application started in default perspective, features are off; WHEN new perspective is selected AND permission service returns false for inner feature THEN feature whose permissions were registered from the inner app is not present in overlay', function() {
            // WHEN
            permissionServiceProvidesNegativeResult();
            perspectives.actions.selectPerspective(permissionsPerspective);

            //THEN
            storefront.assertions.assertComponentInOverlayNotContains(
                storefront.constants.COMPONENT_4_ID, storefront.constants.COMPONENT_4_TYPE,
                'Test permission decorator registered inner');
        });

        it('GIVEN application started in default perspective, features are off; WHEN new perspective is selected AND permission service returns false for inner feature THEN feature is not present in overlay', function() {
            // WHEN
            permissionServiceProvidesNegativeResult();
            perspectives.actions.selectPerspective(permissionsPerspective);

            // THEN
            storefront.assertions.assertComponentInOverlayNotContains(
                storefront.constants.COMPONENT_3_ID, storefront.constants.COMPONENT_3_TYPE,
                'Test permission component');
        });

        it('GIVEN application started in default perspective, features are off; WHEN new perspective is selected AND permission service returns false for outer feature THEN feature is not present in outer application', function() {
            // WHEN
            permissionServiceProvidesNegativeResult();
            perspectives.actions.selectPerspective(permissionsPerspective);

            // THEN
            toolbar.assertions.assertButtonNotPresent('Some Item');
        });

        it('GIVEN application started in default perspective, features are off; WHEN new perspective is selected AND permission service returns true for outer feature THEN feature is present in outer application', function() {
            // WHEN
            permissionServiceProvidesPositiveResult();
            perspectives.actions.selectPerspective(permissionsPerspective);

            // THEN
            toolbar.assertions.assertButtonPresent('Some Item');
        });

        it('GIVEN application started in some perspective, features are on; WHEN new perspective is selected and permission service returns true THEN feature from some perspective is not present and feature new perspective is present', function() {
            // GIVEN
            perspectives.actions.selectPerspective(somePerspective);
            perspectives.assertions.assertPerspectiveActive(somePerspective);
            storefront.assertions.assertComponentInOverlayPresent(storefront.constants.COMPONENT_1_ID, storefront.constants.COMPONENT_1_TYPE, true);
            permissionServiceProvidesPositiveResult();

            // WHEN
            perspectives.actions.selectPerspective(permissionsPerspective);

            // THEN
            storefront.assertions.assertComponentInOverlayNotContains(storefront.constants.COMPONENT_1_ID, storefront.constants.COMPONENT_1_TYPE, true);
            toolbar.assertions.assertButtonPresent('Some Item');
        });


        function permissionServiceProvidesPositiveResult() {
            browser.executeScript('window.sessionStorage.setItem("PERSPECTIVE_SERVICE_RESULT", arguments[0])', true);
        }

        function permissionServiceProvidesNegativeResult() {
            browser.executeScript('window.sessionStorage.setItem("PERSPECTIVE_SERVICE_RESULT", arguments[0])', false);
        }

    });

})();
