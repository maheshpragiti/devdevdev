/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    angular.module('e2eOnLoadingSetup', ['ngMockE2E', 'resourceLocationsModule'])
        .run(function(STOREFRONT_PATH, $location, $timeout, $httpBackend) {

            $httpBackend.whenGET(/smarteditcontainerJSTests/).passThrough();
            $httpBackend.whenGET(/static-resources/).passThrough();

            $httpBackend.whenGET(/cmssmarteditwebservices\/v1\/sites\/electronics\/contentcatalogs/).respond({
                catalogs: [{
                    catalogId: "electronicsContentCatalog",
                    name: {
                        en: "Electronics Content Catalog"
                    },
                    versions: [{
                        version: "Online",
                        active: true
                    }, {
                        version: "Staged",
                        active: false
                    }]
                }]
            });

            $httpBackend.whenGET(/cmssmarteditwebservices\/v1\/sites\/apparel-uk\/contentcatalogs/).respond({
                catalogs: [{
                    catalogId: "apparel-ukContentCatalog",
                    name: {
                        en: "Apparel UK Content Catalog"
                    },
                    versions: [{
                        version: "Online",
                        active: true
                    }, {
                        version: "Staged",
                        active: false
                    }]
                }]
            });

            $httpBackend.whenGET(/cmswebservices\/v1\/sites$/).respond({
                sites: [{
                    previewUrl: '/smarteditcontainerJSTests/e2e/dummystorefront.html',
                    name: {
                        en: "Electronics"
                    },
                    redirectUrl: 'redirecturlElectronics',
                    uid: 'electronics'
                }, {
                    previewUrl: '/smarteditcontainerJSTests/e2e/dummystorefront.html',
                    name: {
                        en: "Apparels"
                    },
                    redirectUrl: 'redirecturlApparels',
                    uid: 'apparel-uk'
                }]
            });

            $httpBackend.whenGET(/cmswebservices\/v1\/sites\/electronics\/languages/).respond({
                languages: [{
                    nativeName: 'English',
                    isocode: 'en',
                    required: true
                }]
            });

            $httpBackend.whenGET(/cmssmarteditwebservices\/v1\/structures\/PreviewData\?mode=DEFAULT/).respond({
                attributes: [{
                    cmsStructureType: 'EditableDropdown',
                    qualifier: 'previewCatalog',
                    i18nKey: 'experience.selector.catalog',
                }, {
                    cmsStructureType: 'EditableDropdown',
                    qualifier: 'language',
                    i18nKey: 'experience.selector.language',
                    dependsOn: 'previewCatalog'
                }, {
                    cmsStructureType: 'DateTime',
                    qualifier: 'time',
                    i18nKey: 'experience.selector.date.and.time'
                }, {
                    cmsStructureType: 'ShortString',
                    qualifier: 'newField',
                    i18nKey: 'experience.selector.newfield'
                }]
            });

            $httpBackend.whenPOST(/thepreviewTicketURI/).respond(function(method, url, data, headers) {

                var returnedPayload = angular.extend({}, data, {
                    ticketId: 'dasdfasdfasdfa',
                    resourcePath: document.location.origin + '/smarteditcontainerJSTests/e2e/dummystorefront.html'
                });

                return [200, returnedPayload];
            });

            var pathWithExperience = STOREFRONT_PATH
                .replace(":siteId", "electronics")
                .replace(":catalogId", "electronicsContentCatalog")
                .replace(":catalogVersion", "Online");
            $location.path(pathWithExperience);
        });

    angular.module('smarteditcontainer').constant('isE2eTestingActive', true);
    angular.module('smarteditcontainer').requires.push('e2eOnLoadingSetup');

})();
