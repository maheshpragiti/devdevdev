/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
describe('HTTP Error Interceptor - ', function() {
    var alerts = require("../utils/components/systemAlertsComponentObject");
    var configurations = require("../utils/components/Configurations.js");
    var login = require("../utils/components/Login.js");
    var page = require("../utils/components/Page.js");
    var perspectives = require("../utils/components/Perspectives.js");
    var storefront = require("../utils/components/Storefront.js");

    beforeEach(function() {
        page.actions.getAndWaitForLogin('smarteditcontainerJSTests/e2e/a_httpErrorInterceptor/httpErrorInterceptorTest.html').then(function() {
            return login.loginAsAdmin().then(function() {
                return perspectives.actions.selectPerspective(perspectives.constants.DEFAULT_PERSPECTIVES.ALL).then(function() {
                    return page.actions.setWaitForPresence(0);
                });
            });
        });
    });

    it("GIVEN I'm in the SmartEdit container WHEN I trigger a GET AJAX request to an API AND I receive a failure response status that is not an HTML failure THEN I expect to see a timed alert with the message ", function() {
        configurations.openConfigurationEditor();
        configurations.setConfigurationValue(0, '\"new\"');
        configurations.clickSave();

        //TODO - refactor somehow... this is flaky, config is saved 1 property at a time so there could be many alerts...
        alerts.assertions.assertAlertTextByIndex(0, 'Your request could not be processed! Please try again later!');
        alerts.assertions.assertAlertIsOfTypeByIndex(0, 'danger');
    });

    it("GIVEN I'm in the SmartEdit application WHEN I trigger a GET AJAX request to an API AND I receive a failure response status that is not an HTML failure THEN I expect to see a timed alert with the message", function() {
        browser.switchToIFrame();
        browser.click(storefront.elements.componentButton());
        browser.switchToParent();

        alerts.assertions.assertAlertTextByIndex(0, 'Your request could not be processed! Please try again later!');
        alerts.assertions.assertAlertIsOfTypeByIndex(0, 'danger');
    });

    it("GIVEN I'm in the SmartEdit application WHEN I trigger a GET AJAX request to an API AND I receive an HTML failure THEN a timed alert won't be displayed", function() {
        browser.switchToIFrame();
        browser.click(storefront.elements.secondComponentButton(), "could not find secondary button");
        browser.switchToParent();

        alerts.assertions.assertNoAlertsDisplayed();
    });


});
