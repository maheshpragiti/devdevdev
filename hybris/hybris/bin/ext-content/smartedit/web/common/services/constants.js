/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seConstantsModule', [])
    .constant('DATE_CONSTANTS', {
        ANGULAR_FORMAT: 'short',
        MOMENT_FORMAT: 'M/D/YY h:mm A',
        ISO: 'yyyy-MM-ddTHH:mm:ssZ'
    })
    /**
     * @ngdoc object
     * @name seConstantsModule.object:EVENT_PERSPECTIVE_CHANGED
     * @description
     * The ID of the event that is triggered when the perspective (known as mode for users) is changed.
     */
    .constant('EVENT_PERSPECTIVE_CHANGED', 'EVENT_PERSPECTIVE_CHANGED')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:EVENT_PERSPECTIVE_ADDED
     * @description
     * The ID of the event that is triggered when a new perspective (known as mode for users) is registered.
     */
    .constant('EVENT_PERSPECTIVE_ADDED', 'EVENT_PERSPECTIVE_ADDED')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:ALL_PERSPECTIVE
     * @description
     * The key of the default All Perspective.
     */
    .constant('ALL_PERSPECTIVE', 'se.all')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:NONE_PERSPECTIVE
     * @description
     * The key of the default None Perspective.
     */
    .constant('NONE_PERSPECTIVE', 'se.none');
