/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('linkToggleModule', [])
    .controller('linkToggleController', function() {
        this.emptyUrlLink = function() {
            this.model.urlLink = '';
        };
        this.$onInit = function() {
            if (this.model.external === undefined) {
                this.model.external = true;
            }
        };
    })
    .component('linkToggle', {
        templateUrl: 'linkToggleTemplate.html',
        transclude: true,
        controller: 'linkToggleController',
        bindings: {
            field: '<',
            model: '<',
        }
    });
