/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('experienceSelectorButtonModule', ['eventServiceModule', 'sharedDataServiceModule', 'sharedDataServiceModule', 'l10nModule'])
    .controller('experienceSelectorButtonController', function(systemEventService, sharedDataService, l10nFilter, EVENTS) {

        this.buildExperienceText = function() {
            if (!this.experience) {
                return '';
            }

            return l10nFilter(this.experience.catalogDescriptor.name) + ' - ' +
                this.experience.catalogDescriptor.catalogVersion + '  |  ' +
                this.experience.languageDescriptor.nativeName +
                (this.experience.time ? '  |  ' + this.experience.time : '');
        };

        this.updateExperience = function() {
            return sharedDataService.get('experience').then(function(experience) {
                this.experience = experience;
            }.bind(this));
        };

        this.$onInit = function() {

            this.updateExperience();
            this.unregFn = systemEventService.registerEventHandler(EVENTS.EXPERIENCE_UPDATE, this.updateExperience.bind(this));
        };

        this.$onDestroy = function() {
            this.unregFn();
        };

    })
    .component('experienceSelectorButton', {
        templateUrl: 'experienceSelectorButtonTemplate.html',
        transclude: true,
        controller: 'experienceSelectorButtonController'
    });
