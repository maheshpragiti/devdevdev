/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
window.smartEditBootstrapped = {}; //storefront actually loads twice all the JS files, including webApplicationInjector.js, smartEdit must be protected against receiving twice a smartEditBootstrap event
angular.module('smarteditcontainer', [
        'configModule',
        'landingPageModule',
        'templateCacheDecoratorModule',
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'coretemplates',
        'loadConfigModule',
        'iFrameManagerModule',
        'alertsBoxModule',
        'httpAuthInterceptorModule',
        'httpErrorInterceptorModule',
        'experienceInterceptorModule',
        'bootstrapServiceModule',
        'toolbarModule',
        'leftToolbarModule',
        'modalServiceModule',
        'previewTicketInterceptorModule',
        'catalogServiceModule',
        'catalogDetailsModule',
        'experienceSelectorButtonModule',
        'experienceSelectorModule',
        'sharedDataServiceModule',
        'inflectionPointSelectorModule',
        'paginationFilterModule',
        'resourceLocationsModule',
        'experienceServiceModule',
        'eventServiceModule',
        'urlServiceModule',
        'perspectiveServiceModule',
        'perspectiveSelectorModule',
        'authorizationModule',
        'hasOperationPermissionModule',
        'l10nModule',
        'treeModule',
        'yInfiniteScrollingModule',
        'ySelectModule',
        'yHelpModule',
        'crossFrameEventServiceModule',
        'renderServiceModule',
        'systemAlertsModule',
        'yCollapsibleContainerModule',
        'seDropdownModule',
        'permissionServiceModule',
        'yNotificationPanelModule',
        'catalogAwareRouteResolverModule',
        'catalogVersionPermissionModule'
    ])
    .config(function(LANDING_PAGE_PATH, STOREFRONT_PATH, STOREFRONT_PATH_WITH_PAGE_ID, $routeProvider, $logProvider, catalogAwareRouteResolverFunctions) {
        $routeProvider.when(LANDING_PAGE_PATH, {
                template: '<landing-page></landing-page>'
            })
            .when(STOREFRONT_PATH, {
                templateUrl: 'mainview.html',
                controller: 'defaultController',
                resolve: {
                    setExperience: catalogAwareRouteResolverFunctions.setExperience
                }
            })
            .when(STOREFRONT_PATH_WITH_PAGE_ID, {
                templateUrl: 'mainview.html',
                controller: 'defaultController',
                resolve: {
                    setExperience: catalogAwareRouteResolverFunctions.setExperience
                }
            })
            .otherwise({
                redirectTo: LANDING_PAGE_PATH
            });

        $logProvider.debugEnabled(false);
    })
    .service('smartEditBootstrapGateway', function(gatewayFactory) {
        return gatewayFactory.createGateway('smartEditBootstrap');
    })
    .run(
        function($rootScope, $log, $q, DEFAULT_RULE_NAME, smartEditBootstrapGateway, toolbarServiceFactory, perspectiveService, gatewayFactory, loadConfigManagerService, bootstrapService, iFrameManager, restServiceFactory, sharedDataService, urlService, featureService, storageService, renderService, closeOpenModalsOnBrowserBack, authorizationService, permissionService) {
            gatewayFactory.initListener();

            loadConfigManagerService.loadAsObject().then(function(configurations) {
                sharedDataService.set('defaultToolingLanguage', configurations.defaultToolingLanguage);
            });

            var smartEditTitleToolbarService = toolbarServiceFactory.getToolbarService("smartEditTitleToolbar");

            smartEditTitleToolbarService.addItems([{
                key: 'topToolbar.leftToolbarTemplate',
                type: 'TEMPLATE',
                include: 'leftToolbarWrapperTemplate.html',
                priority: 1,
                section: 'left'
            }, {
                key: 'topToolbar.logoTemplate',
                type: 'TEMPLATE',
                include: 'logoTemplate.html',
                priority: 2,
                section: 'left'
            }, {
                key: 'topToolbar.deviceSupportTemplate',
                type: 'HYBRID_ACTION',
                include: 'deviceSupportTemplate.html',
                priority: 1,
                section: 'right'
            }, {
                type: 'TEMPLATE',
                key: 'topToolbar.experienceSelectorTemplate',
                className: 'ySEPreviewSelector',
                include: 'experienceSelectorWrapperTemplate.html',
                priority: 1, //first in the middle
                section: 'middle'
            }]);

            var experienceSelectorToolbarService = toolbarServiceFactory.getToolbarService("experienceSelectorToolbar");

            experienceSelectorToolbarService.addItems([{
                key: "bottomToolbar.perspectiveSelectorTemplate",
                type: 'TEMPLATE',
                section: 'right',
                priority: 1,
                include: 'perspectiveSelectorWrapperTemplate.html'
            }]);

            function offSetStorefront() {
                // Set the storefront offset
                $('#js_iFrameWrapper').css('padding-top', $('.ySmartEditToolbars').height() + 'px');
            }

            smartEditBootstrapGateway.subscribe("reloadFormerPreviewContext", function(eventId, data) {
                offSetStorefront();
                var deferred = $q.defer();
                iFrameManager.initializeCatalogPreview();
                deferred.resolve();
                return deferred.promise;
            });
            smartEditBootstrapGateway.subscribe("loading", function(eventId, data) {
                var deferred = $q.defer();

                iFrameManager.setCurrentLocation(data.location);
                iFrameManager.showWaitModal();

                delete window.smartEditBootstrapped[data.location];

                perspectiveService.clearActivePerspective();

                return deferred.promise;
            });
            smartEditBootstrapGateway.subscribe("bootstrapSmartEdit", function(eventId, data) {
                offSetStorefront();
                var deferred = $q.defer();
                if (!window.smartEditBootstrapped[data.location]) {
                    window.smartEditBootstrapped[data.location] = true;
                    loadConfigManagerService.loadAsObject().then(function(configurations) {
                        bootstrapService.bootstrapSEApp(configurations);
                        deferred.resolve();
                    });
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            });
            smartEditBootstrapGateway.subscribe("smartEditReady", function(eventId, data) {
                var deferred = $q.defer();
                deferred.resolve();

                iFrameManager.hideWaitModal();
                return deferred.promise;
            });

            gatewayFactory.createGateway('accessTokens').subscribe("get", function(eventId, data) {
                return $q.when(storageService.getAuthTokens());
            });

            $rootScope.$on('$routeChangeSuccess', function() {
                closeOpenModalsOnBrowserBack();
            });

            permissionService.registerDefaultRule({
                names: [DEFAULT_RULE_NAME],
                verify: function(permissionNameObjs) {
                    var permisssionNames = permissionNameObjs.map(function(permissionName) {
                        return permissionName.name;
                    });
                    return authorizationService.hasGlobalPermissions(permisssionNames);
                }
            });
        })
    .controller(
        'defaultController',
        function($log, $routeParams, $location, iFrameManager, experienceService, sharedDataService) {
            sharedDataService.get('experience').then(function(experience) {
                iFrameManager.applyDefault();
                iFrameManager.initializeCatalogPreview(experience);
            });

            var bodyTag = angular.element(document.querySelector('body'));
            bodyTag.addClass('is-storefront');
        });
