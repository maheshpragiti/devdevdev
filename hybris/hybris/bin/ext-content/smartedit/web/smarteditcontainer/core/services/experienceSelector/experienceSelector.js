/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('experienceSelectorModule', ['eventServiceModule', 'genericEditorModule', 'sharedDataServiceModule', 'iframeClickDetectionServiceModule', 'iFrameManagerModule', 'siteServiceModule', 'genericEditorModule', 'resourceLocationsModule', 'previewDataDropdownPopulatorModule', 'experienceServiceModule', 'yLoDashModule'])
    .controller('ExperienceSelectorController', function($scope, $filter, $timeout, $document, systemEventService, siteService, sharedDataService, iframeClickDetectionService, iFrameManager, GenericEditor, experienceService, EVENTS, STRUCTURES_RESOURCE_URI, PREVIEW_RESOURCE_URI, DATE_CONSTANTS, lodash) {
        var selectedExperience = {};
        var siteCatalogs = {};

        this.$onInit = function() {
            this.resetExperienceSelector = function() {
                sharedDataService.get('experience').then(function(experience) {

                    selectedExperience = lodash.cloneDeep(experience);
                    delete selectedExperience.siteDescriptor;
                    delete selectedExperience.languageDescriptor;

                    selectedExperience.previewCatalog = experience.siteDescriptor.uid + '_' + experience.catalogDescriptor.catalogId + '_' + experience.catalogDescriptor.catalogVersion;
                    selectedExperience.language = experience.languageDescriptor.isocode;
                    this.setFields();
                }.bind(this));
            }.bind(this);

            this.reset = function() {
                this.editor.reset($scope.componentForm);
                this.dropdownStatus.isopen = false;
            }.bind(this);

            this.submit = function() {
                console.info("submit", this.editor);
                return this.editor.submit($scope.componentForm);
            }.bind(this);

            this.isSubmitDisabled = function(componentForm) {
                return !(this.editor.isDirty() && componentForm.$valid);
            }.bind(this);

        };

        this.setFields = function() {
            sharedDataService.get('configuration').then(function(configuration) {
                this.editor = new GenericEditor({
                    smarteditComponentType: 'PreviewData',
                    smarteditComponentId: null,
                    structureApi: STRUCTURES_RESOURCE_URI + '/:smarteditComponentType?mode=DEFAULT',
                    contentApi: configuration && configuration.previewTicketURI || PREVIEW_RESOURCE_URI,
                    updateCallback: null,
                    content: selectedExperience
                });

                this.editor.alwaysShowReset = true;
                this.editor.alwaysShowSubmit = true;

                this.editor.init();

                this.editor.preparePayload = function(originalPayload) {
                    siteCatalogs.siteId = originalPayload.previewCatalog.split('_')[0];
                    siteCatalogs.catalogId = originalPayload.previewCatalog.split('_')[1];
                    siteCatalogs.catalogVersion = originalPayload.previewCatalog.split('_')[2];

                    return sharedDataService.get('configuration').then(function(configuration) {

                        return sharedDataService.get('experience').then(function(experience) {

                            return siteService.getSiteById(siteCatalogs.siteId).then(function(siteDescriptor) {

                                var transformedPayload = lodash.cloneDeep(originalPayload);
                                delete transformedPayload.previewCatalog;
                                delete transformedPayload.time;

                                transformedPayload.catalog = siteCatalogs.catalogId;
                                transformedPayload.catalogVersion = siteCatalogs.catalogVersion;
                                transformedPayload.resourcePath = configuration.domain + siteDescriptor.previewUrl;
                                transformedPayload.pageId = experience.pageId;
                                transformedPayload.time = originalPayload.time;

                                return transformedPayload;

                            }.bind(this));

                        }.bind(this));

                    }.bind(this));
                };

                this.editor.updateCallback = function(payload, response) {
                    delete this.smarteditComponentId; //to force a permanent POST
                    this.dropdownStatus.isopen = false;
                    sharedDataService.get('configuration').then(function(configuration) {
                        // Then perform the actual update.
                        var experienceParams = lodash.cloneDeep(response);
                        delete experienceParams.catalog;
                        delete experienceParams.time;
                        experienceParams.siteId = siteCatalogs.siteId;
                        experienceParams.catalogId = siteCatalogs.catalogId;
                        experienceParams.time = $filter('date')(payload.time, DATE_CONSTANTS.ANGULAR_FORMAT);
                        experienceParams.pageId = response.pageId;
                        experienceService.buildDefaultExperience(experienceParams).then(function(experience) {
                            sharedDataService.set('experience', experience).then(function() {
                                systemEventService.sendAsynchEvent(EVENTS.EXPERIENCE_UPDATE);
                                var fullPreviewUrl = configuration.domain + experience.siteDescriptor.previewUrl;
                                iFrameManager.loadPreview(fullPreviewUrl, response.ticketId);
                                var preview = {
                                    previewTicketId: response.ticketId,
                                    resourcePath: fullPreviewUrl
                                };
                                sharedDataService.set('preview', preview);
                            });
                        });

                    }.bind(this));
                }.bind(this);
            }.bind(this));

        };

        this.submitButtonText = 'componentform.actions.apply';
        this.cancelButtonText = 'componentform.actions.cancel';
        this.modalHeaderTitle = 'experience.selector.header';

        this.$postLink = function() {
            ///////////////////////////////////////////////////////////////////////
            // Close on clicking away from the experience selector
            ///////////////////////////////////////////////////////////////////////

            $document.on('click', function(event) {
                $timeout(function() {
                    if ($(event.target).parents('.ySEPreviewSelector').length <= 0) {
                        if ($(event.target).parents('.ui-select-choices-row').length > 0) {
                            return;
                        }

                        if (this.dropdownStatus && this.dropdownStatus.isopen) {
                            this.editor.reset($scope.componentForm);
                            this.dropdownStatus.isopen = false;
                        }
                    }
                }.bind(this), 0);
            }.bind(this));

            //FIXME: unregister
            iframeClickDetectionService.registerCallback('closeExperienceSelector', function() {
                if (this.dropdownStatus && this.dropdownStatus.isopen) {
                    this.editor.reset($scope.componentForm);
                    this.dropdownStatus.isopen = false;
                }
            }.bind(this));

            this.unRegFn = systemEventService.registerEventHandler('OVERLAY_DISABLED', function() {
                if (this.dropdownStatus && this.dropdownStatus.isopen) {
                    this.editor.reset($scope.componentForm);
                    this.dropdownStatus.isopen = false;
                }
            }.bind(this));
        };

        this.$onDestroy = function() {
            this.unRegFn();
        };

    })
    .component('experienceSelector', {
        templateUrl: 'genericEditorTemplate.html',
        transclude: true,
        controller: 'ExperienceSelectorController',
        controllerAs: 'ge',
        bindings: {
            experience: '=',
            dropdownStatus: '=',
            resetExperienceSelector: '='
        }
    });
