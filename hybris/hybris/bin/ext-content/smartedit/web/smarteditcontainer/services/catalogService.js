/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('catalogServiceModule', ['gatewayProxyModule', 'sharedDataServiceModule', 'urlServiceModule', 'restServiceFactoryModule', 'siteServiceModule', 'resourceLocationsModule', 'functionsModule'])
    /**
     * @deprecated since 6.4
     */
    .constant('CATALOG_VERSION_DETAILS_RESOURCE_URI', '/cmswebservices/v1/sites/:siteUID/catalogversiondetails')
    .constant('CONTENT_CATALOG_VERSION_DETAILS_RESOURCE_API', '/cmssmarteditwebservices/v1/sites/:siteUID/contentcatalogs')
    .constant('PRODUCT_CATALOG_VERSION_DETAILS_RESOURCE_API', '/cmssmarteditwebservices/v1/sites/:siteUID/productcatalogs')
    .service('catalogService', function($q, gatewayProxy, sharedDataService, restServiceFactory, siteService, urlService, CATALOG_VERSION_DETAILS_RESOURCE_URI, CONTENT_CATALOG_VERSION_DETAILS_RESOURCE_API, PRODUCT_CATALOG_VERSION_DETAILS_RESOURCE_API, CONTEXT_SITE_ID, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION, URIBuilder) {

        // ------------------------------------------------------------------------------------------------------------------------
        //  Deprecated
        // ------------------------------------------------------------------------------------------------------------------------
        /**
         * @deprecated since 6.4
         */
        var cache = {};

        /**
         * @deprecated since 6.4
         */
        var catalogRestService = restServiceFactory.get(CATALOG_VERSION_DETAILS_RESOURCE_URI);

        /**
         * @deprecated since 6.4
         */
        this.getCatalogsForSite = function(siteUID) {
            return cache[siteUID] ? $q.when(cache[siteUID]) : catalogRestService.get({
                siteUID: siteUID
            }).then(function(catalogsDTO) {
                cache[siteUID] = catalogsDTO.catalogVersionDetails.reduce(function(acc, catalogVersionDescriptor) {
                    if (catalogVersionDescriptor.name && catalogVersionDescriptor.catalogId && catalogVersionDescriptor.version) {
                        acc.push({
                            name: catalogVersionDescriptor.name,
                            catalogId: catalogVersionDescriptor.catalogId,
                            catalogVersion: catalogVersionDescriptor.version,
                            active: catalogVersionDescriptor.active,
                            thumbnailUrl: catalogVersionDescriptor.thumbnailUrl
                        });
                    }
                    return acc;
                }, []);
                return cache[siteUID];
            });
        };

        /**
         * @deprecated since 6.4
         */
        this.getAllCatalogsGroupedById = function() {
            return this.getAllContentCatalogsGroupedById();
        };

        // ------------------------------------------------------------------------------------------------------------------------
        //  Active
        // ------------------------------------------------------------------------------------------------------------------------
        var contentCatalogsCache = {};
        var productCatalogsCache = {};

        var contentCatalogRestService = restServiceFactory.get(CONTENT_CATALOG_VERSION_DETAILS_RESOURCE_API);
        var productCatalogRestService = restServiceFactory.get(PRODUCT_CATALOG_VERSION_DETAILS_RESOURCE_API);

        this.getContentCatalogsForSite = function(siteUID) {
            return (contentCatalogsCache[siteUID]) ? $q.when(contentCatalogsCache[siteUID]) : contentCatalogRestService.get({
                siteUID: siteUID
            }).then(function(catalogsDTO) {
                contentCatalogsCache[siteUID] = catalogsDTO.catalogs;
                return contentCatalogsCache[siteUID];
            });
        };

        this.getAllContentCatalogsGroupedById = function() {
            return siteService.getSites().then(function(sites) {
                var promisesToResolve = sites.map(function(site) {
                    return this.getContentCatalogsForSite(site.uid).then(function(catalogs) {
                        catalogs.forEach(function(catalog) {
                            catalog.versions = catalog.versions.map(function(catalogVersion) {
                                catalogVersion.siteDescriptor = site;
                                return catalogVersion;
                            });
                        });

                        return catalogs;
                    });
                }.bind(this));

                return $q.all(promisesToResolve);
            }.bind(this));
        };

        this.getCatalogByVersion = function(siteUID, catalogVersionName) {
            return this.getContentCatalogsForSite(siteUID).then(function(catalogs) {
                return catalogs.filter(function(catalog) {
                    return catalog.versions.some(function(currentCatalogVersion) {
                        return currentCatalogVersion.version === catalogVersionName;
                    });
                });
            });
        };

        this.isContentCatalogVersionNonActive = function(_uriContext) {
            return this._getContext(_uriContext).then(function(uriContext) {
                return this.getContentCatalogsForSite(uriContext[CONTEXT_SITE_ID]).then(function(catalogs) {
                    var currentCatalog = catalogs.find(function(catalog) {
                        return catalog.catalogId === uriContext[CONTEXT_CATALOG];
                    });
                    var currentCatalogVersion = (currentCatalog) ? currentCatalog.versions.find(function(catalogVersion) {
                        return catalogVersion.version === uriContext[CONTEXT_CATALOG_VERSION];
                    }) : null;

                    if (!currentCatalogVersion) {
                        throw Error('Invalid URI ', uriContext, ". Cannot find catalog version.");
                    }

                    return !currentCatalogVersion.active;
                });
            }.bind(this));
        };

        this.getContentCatalogActiveVersion = function(_uriContext) {
            return this._getContext(_uriContext).then(function(uriContext) {
                return this.getContentCatalogsForSite(uriContext[CONTEXT_SITE_ID]).then(function(catalogs) {
                    var currentCatalog = catalogs.find(function(catalog) {
                        return catalog.catalogId === uriContext[CONTEXT_CATALOG];
                    });

                    var activeCatalogVersion = currentCatalog ? currentCatalog.versions.find(function(catalogVersion) {
                        return catalogVersion.active;
                    }) : null;

                    if (!activeCatalogVersion) {
                        throw Error('Invalid URI ', uriContext, ". Cannot find catalog version.");
                    }

                    return activeCatalogVersion.version;
                });
            }.bind(this));
        };

        this.getActiveContentCatalogVersionByCatalogId = function(contentCatalogId) {
            return this._getContext().then(function(uriContext) {
                return this.getContentCatalogsForSite(uriContext[CONTEXT_SITE_ID]).then(function(catalogs) {
                    var currentCatalog = catalogs.find(function(catalog) {
                        return catalog.catalogId === contentCatalogId;
                    });

                    var currentCatalogVersion = (currentCatalog) ? currentCatalog.versions.find(function(catalogVersion) {
                        return catalogVersion.active;
                    }) : null;

                    if (!currentCatalogVersion) {
                        throw Error('Invalid content catalog ', contentCatalogId, ". Cannot find any active catalog version.");
                    }

                    return currentCatalogVersion.version;
                });
            }.bind(this));
        };

        this.retrieveUriContext = function(_uriContext) {
            return this._getContext(_uriContext);
        };

        this._getContext = function(_uriContext) {
            return _uriContext ? $q.when(_uriContext) : sharedDataService.get('experience').then(function(experience) {
                if (!experience) {
                    throw "catalogService was not provided with a uriContext and could not retrive an experience from sharedDataService";
                }
                return urlService.buildUriContext(experience.siteDescriptor.uid, experience.catalogDescriptor.catalogId, experience.catalogDescriptor.catalogVersion);
            });
        };

        this.getProductCatalogsForSite = function(siteUID) {
            return productCatalogsCache[siteUID] ? $q.when(productCatalogsCache[siteUID]) : productCatalogRestService.get({
                siteUID: siteUID
            }).then(function(catalogsDTO) {
                productCatalogsCache[siteUID] = catalogsDTO.catalogs;
                return productCatalogsCache[siteUID];
            }.bind(this));
        };

        this.getActiveProductCatalogVersionByCatalogId = function(productCatalogId) {
            return this.getProductCatalogsForSite(CONTEXT_SITE_ID).then(function(catalogs) {
                var currentCatalog = catalogs.find(function(catalog) {
                    return catalog.catalogId === productCatalogId;
                });

                var currentCatalogVersion = (currentCatalog) ? currentCatalog.versions.find(function(catalogVersion) {
                    return catalogVersion.active;
                }) : null;

                if (!currentCatalogVersion) {
                    throw Error('Invalid product catalog ', productCatalogId, ". Cannot find any active catalog version.");
                }

                return currentCatalogVersion.version;
            });
        };

        this.getCatalogVersionUUid = function(_uriContext) {
            return this._getContext(_uriContext).then(function(uriContext) {
                return this.getContentCatalogsForSite(uriContext[CONTEXT_SITE_ID]).then(function(catalogs) {
                    return catalogs.filter(function(catalog) {
                        return catalog.catalogId === uriContext[CONTEXT_CATALOG];
                    })[0].versions.filter(function(version) {
                        return version.version === uriContext[CONTEXT_CATALOG_VERSION];
                    })[0].uuid;
                }, function(error) {
                    console.error(error);
                });
            }.bind(this));
        };


        this.clearCache = function() {
            cache = {};
            contentCatalogsCache = {};
            productCatalogsCache = {};
        };

        this.gatewayId = "catalogService";
        gatewayProxy.initForService(this);

    });
