/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('perspectiveServiceModule', [
        'featureServiceModule',
        'functionsModule',
        'perspectiveServiceInterfaceModule',
        'gatewayProxyModule',
        'iFrameManagerModule',
        'storageServiceModule',
        'eventServiceModule',
        'crossFrameEventServiceModule',
        'permissionServiceModule',
        'seConstantsModule'
    ])
    .factory('perspectiveService', function($log, $rootScope, $q, extend, isBlank, uniqueArray, hitch, systemEventService, Perspective, PerspectiveServiceInterface, featureService, gatewayProxy, iFrameManager, storageService, crossFrameEventService, NONE_PERSPECTIVE, ALL_PERSPECTIVE, EVENTS, EVENT_PERSPECTIVE_CHANGED, EVENT_PERSPECTIVE_ADDED, permissionService) {

        // Constants
        var PERSPECTIVE_COOKIE_NAME = "smartedit-perspectives";

        var data = {
            activePerspective: undefined,
            previousPerspective: undefined
        };

        var perspectives = [];

        var PerspectiveService = function() {
            this.gatewayId = "perspectiveService";
            gatewayProxy.initForService(this, ['register', 'switchTo', 'hasActivePerspective', 'isEmptyPerspectiveActive', 'selectDefault']);

            this._addDefaultPerspectives();
            this._registerEventHandlers();
        };

        PerspectiveService = extend(PerspectiveServiceInterface, PerspectiveService);

        PerspectiveService.prototype._addDefaultPerspectives = function() {
            this.register({
                key: NONE_PERSPECTIVE,
                nameI18nKey: 'perspective.none.name',
                descriptionI18nKey: 'perspective.none.description'
            });

            this.register({
                key: ALL_PERSPECTIVE,
                nameI18nKey: 'perspective.all.name',
                descriptionI18nKey: 'perspective.all.description'
            });
        };

        PerspectiveService.prototype._validate = function(configuration) {
            if (isBlank(configuration.key)) {
                throw new Error("perspectiveService.configuration.key.error.required");
            }
            if (isBlank(configuration.nameI18nKey)) {
                throw new Error("perspectiveService.configuration.nameI18nKey.error.required");
            }
            if ([NONE_PERSPECTIVE, ALL_PERSPECTIVE].indexOf(configuration.key) === -1 && (isBlank(configuration.features) || configuration.features.length === 0)) {
                throw new Error("perspectiveService.configuration.features.error.required");
            }
        };

        PerspectiveService.prototype._findByKey = function(key) {
            var perspective = perspectives.filter(function(persp) {
                return persp.key === key;
            })[0];
            return perspective;
        };

        PerspectiveService.prototype._fetchAllFeatures = function(perspective, holder) {
            if (!holder) {
                holder = [];
            }

            if (perspective.key === ALL_PERSPECTIVE) {
                uniqueArray(holder, (featureService.getFeatureKeys() || []));
            } else {
                uniqueArray(holder, (perspective.features || []));

                (perspective.perspectives || []).forEach(hitch(this, function(perspectiveKey) {
                    var nestedPerspective = this._findByKey(perspectiveKey);
                    if (nestedPerspective) {
                        this._fetchAllFeatures(nestedPerspective, holder);
                    } else {
                        $log.debug("nested perspective " + perspectiveKey + " was not found in the registry");
                    }
                }));
            }
        };

        PerspectiveService.prototype.register = function(configuration) {

            this._validate(configuration);

            var perspective = this._findByKey(configuration.key);

            if (!perspective) {
                perspective = new Perspective(configuration.nameI18nKey, [], true); //constructor will be modified once dependant code is refactored
                perspective.key = configuration.key;
                perspective.descriptionI18nKey = configuration.descriptionI18nKey;
                perspectives.push(perspective);
            }

            perspective.features = uniqueArray(perspective.features || [], configuration.features || []);
            perspective.perspectives = uniqueArray(perspective.perspectives || [], configuration.perspectives || []);
            perspective.permissions = uniqueArray(perspective.permissions || [], configuration.permissions || []);

            systemEventService.sendAsynchEvent(EVENT_PERSPECTIVE_ADDED);
        };

        PerspectiveService.prototype.getPerspectives = function() {
            var promises = [];

            perspectives.forEach(function(perspective) {
                var promise;

                if (perspective.permissions.length > 0) {
                    promise = permissionService.isPermitted([{
                        names: perspective.permissions
                    }]);
                } else {
                    promise = $q.when(true);
                }

                promises.push(promise);
            });

            return $q.all(promises).then(function(results) {
                return perspectives.filter(function(perspective, index) {
                    return results[index];
                });
            }.bind(this));
        };

        PerspectiveService.prototype.hasActivePerspective = function() {
            return Boolean(data.activePerspective);
        };

        PerspectiveService.prototype.getActivePerspective = function() {
            return data.activePerspective ? this._findByKey(data.activePerspective.key) : null;
        };

        PerspectiveService.prototype.isEmptyPerspectiveActive = function() {
            return (!!data.activePerspective && data.activePerspective.key === NONE_PERSPECTIVE);
        };

        PerspectiveService.prototype._enableFeature = function(featureKey) {
            return featureService.getFeatureProperty(featureKey, "permissions").then(function(permissionNames) {
                if (!Array.isArray(permissionNames)) {
                    permissionNames = [];
                }
                return permissionService.isPermitted([{
                    names: permissionNames
                }]).then(function(allowCallback) {
                    if (allowCallback) {
                        featureService.enable(featureKey);
                    }
                });
            });
        };

        PerspectiveService.prototype.switchTo = function(key) {
            if (!this._changeActivePerspective(key)) {
                return;
            }

            iFrameManager.showWaitModal();
            var featuresFromPreviousPerspective = [];
            if (data.previousPerspective) {
                this._fetchAllFeatures(data.previousPerspective, featuresFromPreviousPerspective);
            }
            var featuresFromNewPerspective = [];
            this._fetchAllFeatures(data.activePerspective, featuresFromNewPerspective);

            //deactivating any active feature not belonging to either the perspective or one of its nested pespectives
            featuresFromPreviousPerspective.filter(function(featureKey) {
                return !featuresFromNewPerspective.some(function(f) {
                    return featureKey == f;
                });
            }).forEach(function(featureKey) {
                featureService.disable(featureKey);
            });

            //activating any feature belonging to either the perspective or one of its nested pespectives
            var permissionPromises = [];
            featuresFromNewPerspective.filter(function(feature) {
                return !featuresFromPreviousPerspective.some(function(f) {
                    return feature == f;
                });
            }).forEach(function(featureKey) {
                permissionPromises.push(this._enableFeature(featureKey));
            }.bind(this));

            $q.all(permissionPromises).then(function(result) {
                if (data.activePerspective.key == NONE_PERSPECTIVE) {
                    iFrameManager.hideWaitModal();
                }

                this.unregisterEvent = crossFrameEventService.publish(EVENT_PERSPECTIVE_CHANGED, data.activePerspective.key != NONE_PERSPECTIVE);
            }.bind(this), function(e) {
                throw e;
            });
        };

        PerspectiveService.prototype._retrievePerspective = function(key) {
            // Validation
            // Change the perspective only if it makes sense.
            if (data.activePerspective && data.activePerspective.key === key) {
                return null;
            }

            var newPerspective = this._findByKey(key);
            if (!newPerspective) {
                throw new Error("switchTo() - Couldn't find perspective with key " + key);
            }

            return newPerspective;
        };

        PerspectiveService.prototype._changeActivePerspective = function(newPerspectiveKey) {
            var newPerspective = this._retrievePerspective(newPerspectiveKey);
            if (newPerspective) {
                data.previousPerspective = data.activePerspective;
                data.activePerspective = newPerspective;
                storageService.putValueInCookie(PERSPECTIVE_COOKIE_NAME, newPerspective.key, true);
            }
            return newPerspective;
        };

        PerspectiveService.prototype.selectDefault = function() {
            return storageService.getValueFromCookie(PERSPECTIVE_COOKIE_NAME, true).then(hitch(this, function(cookieValue) {
                var defaultPerspective = (cookieValue && this._findByKey(cookieValue)) ? cookieValue : NONE_PERSPECTIVE;
                var perspective = (data.previousPerspective) ? data.previousPerspective.key : defaultPerspective;

                this.switchTo(perspective);
            }));
        };

        PerspectiveService.prototype.clearActivePerspective = function() {
            data.previousPerspective = data.activePerspective;
            delete data.activePerspective;
        };

        PerspectiveService.prototype._registerEventHandlers = function() {
            systemEventService.registerEventHandler(EVENTS.EXPERIENCE_UPDATE, this._clearPerspectiveFeatures.bind(this));
            systemEventService.registerEventHandler(EVENTS.LOGOUT, this._onLogoutPerspectiveCleanup.bind(this));
            systemEventService.registerEventHandler(EVENTS.AUTHORIZATION_SUCCESS, this._clearPerspectiveFeatures.bind(this));
        };

        PerspectiveService.prototype._clearPerspectiveFeatures = function(eventType, authenticationPayload) {
            var needToClearFeatures = isBlank(authenticationPayload) || isBlank(authenticationPayload.userHasChanged) || !!authenticationPayload.userHasChanged;
            if (needToClearFeatures) {
                // De-activates all current perspective's features (Still leaves the cookie in the system).
                var perspectiveFeatures = [];
                if (data && data.activePerspective) {
                    this._fetchAllFeatures(data.activePerspective, perspectiveFeatures);
                }

                perspectiveFeatures.forEach(function(feature) {
                    featureService.disable(feature);
                });
            }
            return $q.when();
        };

        PerspectiveService.prototype._onLogoutPerspectiveCleanup = function() {
            return this._clearPerspectiveFeatures().then(function() {
                this.clearActivePerspective();
                this.unregisterEvent();

                return $q.when();
            }.bind(this));
        };

        return new PerspectiveService();

    });
