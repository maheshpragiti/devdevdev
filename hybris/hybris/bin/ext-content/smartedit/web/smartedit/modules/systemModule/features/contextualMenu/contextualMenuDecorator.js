/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('contextualMenuDecoratorModule', ['componentHandlerServiceModule', 'contextualMenuServiceModule', 'eventServiceModule', 'ui.bootstrap'])
    .controller('baseContextualMenuController', function($scope, smarteditroot) {
        this.status = {
            isopen: false
        };

        this.isHybrisIcon = function(icon) {
            return icon && icon.indexOf("hyicon") >= 0;
        };

        this.remainOpenMap = {};

        /*
         setRemainOpen receives a button name and a boolean value
         the button name needs to be unique across all buttons so it won' t collide with other button actions.
         */
        this.setRemainOpen = function(button, remainOpen) {
            this.remainOpenMap[button] = remainOpen;
        };

        this.showOverlay = function() {
            var remainOpen = Object.keys(this.remainOpenMap)
                .reduce(function(isOpen, button) {
                    return isOpen || this.remainOpenMap[button];
                }.bind(this), false);

            return this.active || remainOpen;
        };

        this.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.status.isopen = !this.status.isopen;

            var moreMenuIconImg = $($event.target).find('img');
            var moreMenuIconFont = $($event.target).find('span');

            if (this.status.isopen) {
                if (moreMenuIconImg) {
                    moreMenuIconImg.attr('src', this.moreButton.iconOpen);
                    moreMenuIconImg.addClass('bottom-shadow');
                }
                if (moreMenuIconFont) {
                    moreMenuIconFont.addClass('bottom-shadow');
                }
            } else {
                if (moreMenuIconImg) {
                    moreMenuIconImg.attr('src', this.moreButton.iconIdle);
                    moreMenuIconImg.addClass('bottom-shadow');
                }
                if (moreMenuIconFont) {
                    moreMenuIconFont.addClass('bottom-shadow');
                }
            }
        };

        this.moreButton = {
            key: 'context.menu.title.more',
            displayClass: 'hyicon hyicon-more cmsx-ctx__icon-more',
            i18nKey: 'context.menu.title.more',
            condition: function() {
                return true;
            },

            callback: function(configuration, $event) {
                this.remainOpenMap.moreButton = !this.remainOpenMap.moreButton;
                this.toggleDropdown($event);
                $('[uib-dropdown-toggle]').dropdown();
                this.icon = smarteditroot + '/static-resources/images/more_white.png';
            }.bind(this)
        };

        this.triggerItemCallback = function(item, $event) {
            item.callback({
                componentType: this.smarteditComponentType,
                componentId: this.smarteditComponentId,
                containerType: this.smarteditContainerType,
                containerId: this.smarteditContainerId,
                componentAttributes: this.componentAttributes,
                slotId: this.smarteditSlotId,
                slotUuid: this.smarteditSlotUuid,
                //@deprecated since 6.4
                properties: JSON.stringify(this.componentAttributes)
            }, $event);
        };

    })
    .controller('contextualMenuController', function($scope, $element, $controller, $timeout, REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT, smarteditroot, contextualMenuService, systemEventService) {
        angular.extend(this, $controller('baseContextualMenuController', {
            $scope: $scope
        }));

        this.positionMoreMenu = function() {
            $timeout(function() {
                var moreButtonElement = $element.find('.cmsx-ctx-more');
                var moreMenuElement = $('#' + this.smarteditComponentId + '-' + this.smarteditComponentType + '-more-menu');

                var moreButtonWidth = moreButtonElement.width();
                var moreMenuWidth = moreMenuElement.width();

                var offset = moreMenuWidth - moreButtonWidth;
                moreMenuElement.offset({
                    top: moreMenuElement.offset().top,
                    left: moreMenuElement.offset().left - offset
                });
            }.bind(this));
        };

        this.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            this.remainOpenMap.moreButton = !this.remainOpenMap.moreButton;

            var moreMenuFontIcon = $($event.target).find('span');

            if (this.status.isopen) {
                moreMenuFontIcon.addClass('bottom-shadow');
            } else {
                moreMenuFontIcon.addClass('bottom-shadow');
            }

            this.positionMoreMenu();
        };

        this.maxContextualMenuItems = (function() {
            var ctxSize = 50;
            var buttonMaxCapacity = Math.round($element.width() / ctxSize) - 1;
            var leftButtons = buttonMaxCapacity >= 4 ? 3 : buttonMaxCapacity - 1;
            leftButtons = (leftButtons < 0 ? 0 : leftButtons);
            return leftButtons;
        }());

        this.$onInit = function() {
            this.updateItems();
        };

        this.updateItems = function() {
            contextualMenuService.getContextualMenuItems({
                componentType: this.smarteditComponentType,
                componentId: this.smarteditComponentId,
                containerType: this.smarteditContainerType,
                containerId: this.smarteditContainerId,
                componentAttributes: this.componentAttributes,
                iLeftBtns: this.maxContextualMenuItems,
                element: $element,
                //@deprecated since 6.4
                properties: JSON.stringify(this.componentAttributes)
            }).then(function(newItems) {
                this.items = newItems;
            }.bind(this));
        }.bind(this);

        this.getItems = function() {
            return this.items;
        };

        systemEventService.registerEventHandler(REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT, this.updateItems);

        $scope.$on("$destroy", function() {
            systemEventService.unRegisterEventHandler(REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT, this.updateItems);
        }.bind(this));
    })
    .directive('contextualMenu', function($q, $timeout, contextualMenuService, componentHandlerService) {
        return {
            templateUrl: 'contextualMenuDecoratorTemplate.html',
            restrict: 'C',
            transclude: true,
            replace: false,
            controller: 'contextualMenuController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                smarteditComponentId: '@',
                smarteditComponentType: '@',
                smarteditContainerId: '@',
                smarteditContainerType: '@',
                componentAttributes: '<',
                active: '='
            },
            link: function($scope, $element) {
                $scope.ctrl.smarteditSlotId = componentHandlerService.getParentSlotForComponent($element);
                $scope.ctrl.smarteditSlotUuid = componentHandlerService.getParentSlotUuidForComponent($element);
            }
        };
    })
    .directive('contextualMenuItem', function(componentHandlerService) {
        return {
            restrict: 'A',
            scope: {
                item: '=',
                componentType: '@',
                componentId: '@',
                containerType: '@',
                containerId: '@',
                componentAttributes: '<',
            },
            link: function($scope, $element) {
                $element.on('load', function() {
                    if ($scope.item.callbacks) {
                        angular.forEach($scope.item.callbacks, function(value, key) {
                            $scope.smarteditSlotId = componentHandlerService.getParentSlotForComponent($element);
                            $scope.smarteditSlotUuid = componentHandlerService.getParentSlotUuidForComponent($element);
                            $element.on(key, value.bind(undefined, {
                                componentType: $scope.componentType,
                                componentId: $scope.componentId,
                                containerType: $scope.containerType,
                                containerId: $scope.containerId,
                                componentAttributes: $scope.componentAttributes,
                                slotId: $scope.smarteditSlotId,
                                slotUuid: $scope.smarteditSlotUuid
                            }));
                        });
                    }

                    $element.unbind("load");
                });
            }
        };
    });
angular.module('sakExecutorDecorator').requires.push('contextualMenuDecoratorModule');
