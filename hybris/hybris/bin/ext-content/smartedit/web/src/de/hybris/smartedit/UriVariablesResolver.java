/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.smartedit;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Helper class to resolve URI variables to be used with spring integration configuration
 */
public class UriVariablesResolver
{
	private static final Logger LOG = Logger.getLogger(UriVariablesResolver.class);

	private static final String HTTP = "http";

	private static String unsecuredPort;
	private static String securedPort;

	/**
	 * Find the host address of the server running smartedit. If the host name in the request does not match the current
	 * host, then return the current host address instead.
	 * @return the host address
	 * @throws UnknownHostException
	 *            when no IP address for the host could be found
	 */
	public static String resolveHost() throws UnknownHostException
	{
		final ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		String host = requestAttributes.getRequest().getServerName();

		try
		{
			final String requestHostAddress = InetAddress.getByName(host).getHostAddress();
			final String currentHostAddress = InetAddress.getLocalHost().getHostAddress();

			if (!requestHostAddress.equals(currentHostAddress))
			{
				host = currentHostAddress;
			}
		}
		catch (final UnknownHostException e)
		{
			LOG.info("Failed to resolve host address", e);
		}
		return host;
	}

	/**
	 * Find the port of the server running smartedit. If the port in the request does not match the port of the current
	 * host, then return the current host port instead.
	 *
	 * @return the host port
	 */
	public static String resolvePort()
	{
		final ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		final String scheme = requestAttributes.getRequest().getScheme();
		String port = String.valueOf(requestAttributes.getRequest().getServerPort());

		if (!port.equals(unsecuredPort) && !port.equals(securedPort))
		{
			if (Objects.nonNull(scheme) && scheme.equalsIgnoreCase(HTTP))
			{
				port = unsecuredPort;
			}
			else
			{
				port = securedPort;
			}
		}
		return port;
	}

	/**
	 * Set the port values used by the host
	 *
	 * @param securedPort
	 *           the value of the secured (ssl) port
	 * @param unsecuredPort
	 *           the value of the unsecured (http) port
	 */
	public static void setHostPorts(final String securedPort, final String unsecuredPort)
	{
		UriVariablesResolver.securedPort = securedPort;
		UriVariablesResolver.unsecuredPort = unsecuredPort;
	}

}
