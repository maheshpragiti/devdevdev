/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
describe('test sakExecutor', function() {

    var $q, $rootScope;
    var sakExecutor, decorators, compiler, compiled;

    beforeEach(customMatchers);

    beforeEach(module('sakExecutorDecorator', function($provide) {
        $provide.constant("NUM_SE_SLOTS", 5);
        smarteditComponentType = "smarteditComponentType";
        smarteditComponentId = "smarteditComponentId";

        decoratorService = jasmine.createSpyObj('decoratorService', ['getDecoratorsForComponent']);
        decorators = ['decorator1', 'decorator2'];

        $provide.value('decoratorService', decoratorService);

        compiler = jasmine.createSpyObj('compiler', ['compile']);
        compiled = angular.element("<div>compiled</div>").html();
        compiler.compile.and.returnValue(compiled);
        $provide.value('$compile', function(template, transcludeFn) {
            return compiler.compile(template, transcludeFn);
        });
    }));

    beforeEach(inject(function(_sakExecutor_, _$q_, _$rootScope_) {
        sakExecutor = _sakExecutor_;
        $q = _$q_;
        $rootScope = _$rootScope_;
    }));

    beforeEach(function() {
        var deferred = $q.defer();
        deferred.resolve(decorators);
        decoratorService.getDecoratorsForComponent.and.returnValue(deferred.promise);
    });

    it('sakExecutor.wrapDecorators fetches eligible decorators for given component type and compiles a stack of those decorators around the clone + the sakDecorator', function() {

        //GIVEN
        var transcludeFn = function() {};

        //WHEN
        spyOn(sakExecutor, 'wrapDecorators').and.callThrough();
        var promise = sakExecutor.wrapDecorators(transcludeFn, smarteditComponentId, smarteditComponentType, {});

        //THEN
        promise.then(function(value) {
            expect(value).toBe(compiled);
        });

        $rootScope.$digest();

        expect(compiler.compile).toHaveBeenCalledWith("<div class='decorator2' " +
            "data-active='active' " +
            "data-smartedit-component-id='{{smarteditComponentId}}' " +
            "data-smartedit-component-type='{{smarteditComponentType}}' " +
            "data-smartedit-container-id='{{smarteditContainerId}}' " +
            "data-smartedit-container-type='{{smarteditContainerType}}' " +
            "data-component-attributes='componentAttributes'>" +
            "<div class='decorator1' " +
            "data-active='active' " +
            "data-smartedit-component-id='{{smarteditComponentId}}' " +
            "data-smartedit-component-type='{{smarteditComponentType}}' " +
            "data-smartedit-container-id='{{smarteditContainerId}}' " +
            "data-smartedit-container-type='{{smarteditContainerType}}' " +
            "data-component-attributes='componentAttributes'>" +
            "<div data-ng-transclude></div></div></div>", transcludeFn);
    });

    xit('areAllDecoratorsProcessed will only return true when the number of processed decorators is greater or equal to the reset value', function() {

        sakExecutor.resetCounters(3);
        expect(sakExecutor.areAllDecoratorsProcessed()).toBe(false);

        sakExecutor.markDecoratorProcessed("type1", "id1");
        expect(sakExecutor.areAllDecoratorsProcessed()).toBe(false);

        sakExecutor.markDecoratorProcessed("type1", "id2");
        expect(sakExecutor.areAllDecoratorsProcessed()).toBe(false);

        //entering same value has no effect
        sakExecutor.markDecoratorProcessed("type1", "id2");
        expect(sakExecutor.areAllDecoratorsProcessed()).toBe(false);

        sakExecutor.markDecoratorProcessed("type2", "id3");
        expect(sakExecutor.areAllDecoratorsProcessed()).toBe(true);
    });

});
