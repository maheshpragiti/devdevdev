/*
* [y] hybris Platform
*
* Copyright (c) 2017 SAP SE or an SAP affiliate company.
* All rights reserved.
*
* This software is the confidential and proprietary information of SAP
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with SAP.
*
*/
package de.hybris.platform.yacceleratorfractusfulfilmentprocess.constants;

public class YAcceleratorfractusfulfilmentProcessConstants extends GeneratedYAcceleratorfractusfulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "yacceleratorfractusfulfilmentprocess";

	public static final String CONSIGNMENT_SUBPROCESS_END_EVENT_NAME = "ConsignmentSubprocessEnd";
	public static final String CONSIGNMENT_SUBPROCESS_NAME = "fractus-consignment-process";


}
