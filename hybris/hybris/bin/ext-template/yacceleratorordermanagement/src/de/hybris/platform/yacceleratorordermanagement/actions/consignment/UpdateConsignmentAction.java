/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package de.hybris.platform.yacceleratorordermanagement.actions.consignment;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;


/**
 * Updates a consignment status to a given status.
 */
public class UpdateConsignmentAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
	private static final Logger LOG = Logger.getLogger(UpdateConsignmentAction.class);

	protected static final String PICKUP_ACTION = "Pick up";
	protected static final String SHIPPING_ACTION = "Shipping";

	private ConsignmentStatus status;
	private WorkflowService workflowService;
	private WorkflowProcessingService workflowProcessingService;

	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());
		final ConsignmentModel consignment = process.getConsignment();
		consignment.setStatus(status);
		save(consignment);

		if (consignment.getTaskAssignmentWorkflow() != null)
		{
			try
			{
				// synchronize with the Task Assignment Workflow
				final WorkflowModel taskAssignmentWorkflow = getWorkflowService()
						.getWorkflowForCode(consignment.getTaskAssignmentWorkflow());
				final Optional<WorkflowActionModel> pickingActionOptional = taskAssignmentWorkflow.getActions().stream()
						.filter(action -> action.getName().equals("Picking")).findAny();
				if (pickingActionOptional.isPresent())
				{
					pickingActionOptional.get().setStatus(WorkflowActionStatus.COMPLETED);
					save(taskAssignmentWorkflow);
					save(pickingActionOptional.get());
				}

				// determine whether to activate the shipping or pick up actions
				Optional<WorkflowActionModel> actionModelOptional;
				if (consignment.getDeliveryPointOfService() == null)
				{
					actionModelOptional = taskAssignmentWorkflow.getActions().stream()
							.filter(action -> action.getName().equals(SHIPPING_ACTION)).findAny();
				}
				else
				{
					actionModelOptional = taskAssignmentWorkflow.getActions().stream()
							.filter(action -> action.getName().equals(PICKUP_ACTION)).findAny();
				}
				actionModelOptional.ifPresent(workflowActionModel -> getWorkflowProcessingService().activate(workflowActionModel));
			}
			catch (final UnknownIdentifierException e) // NOSONAR
			{
				LOG.debug("No synchronization with a task assignment workflow since the consignment " + consignment.getCode()
						+ " has no workflow assigned to it.");
			}
		}
	}

	protected ConsignmentStatus getStatus()
	{
		return status;
	}

	@Required
	public void setStatus(final ConsignmentStatus status)
	{
		this.status = status;
	}

	protected WorkflowService getWorkflowService()
	{
		return workflowService;
	}

	@Required
	public void setWorkflowService(final WorkflowService workflowService)
	{
		this.workflowService = workflowService;
	}

	protected WorkflowProcessingService getWorkflowProcessingService()
	{
		return workflowProcessingService;
	}

	@Required
	public void setWorkflowProcessingService(final WorkflowProcessingService workflowProcessingService)
	{
		this.workflowProcessingService = workflowProcessingService;
	}
}
