/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.marketplaceservices.dataimport.batch.util;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.springframework.util.Assert;

import de.hybris.platform.acceleratorservices.dataimport.batch.util.BatchDirectoryUtils;


/**
 * Utility methods used in Marketplace
 */
public class DataIntegrationUtils
{
	protected static final String LOG_DIRECTORY = "log";

	private DataIntegrationUtils()
	{
	}

	/**
	 * resolve vendor code from file parent directories
	 *
	 * @param file
	 *           the csv being imported
	 * @return the vendor code
	 */
	public static String resolveVendorCode(final File file)
	{
		Assert.notNull(file);
		final File processingFolder = file.getParentFile();
		Assert.notNull(processingFolder);
		final File vendorFolder = processingFolder.getParentFile();
		Assert.notNull(vendorFolder);
		return vendorFolder.getName();
	}

	/**
	 * get the log file for an imported csv file
	 * 
	 * @param file
	 *           the csv being imported
	 * @return the log file
	 */
	public static File getLogFile(final File file)
	{
		final String logDir = BatchDirectoryUtils
				.verify(BatchDirectoryUtils.getRelativeBaseDirectory(file) + File.separator + LOG_DIRECTORY);
		return new File(logDir, FilenameUtils.getBaseName(file.getName()) + ".log");
	}
}
