/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
*/
package de.hybris.platform.configurablebundleservices.order;


import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.jalo.BundleTemplateStatus;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.constants.ImpExConstants;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import javax.annotation.Resource;

import static de.hybris.platform.configurablebundleservices.constants.ConfigurableBundleServicesConstants.NEW_BUNDLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@IntegrationTest
public class BundledCommerceCartServiceTest extends ServicelayerTest
{
	private static final Logger LOG = Logger.getLogger(BundledCommerceCartServiceTest.class);

	@Resource
	protected CommerceCartService commerceCartService;
	@Resource
	protected ModelService modelService;
	@Resource
	protected UserService userService;
	@Resource
	protected ProductService productService;
	@Resource
	protected BundleTemplateService bundleTemplateService;

	protected CartModel cart;

	@Before
	public void setUp() throws Exception
	{
		LOG.debug("Preparing test data");
		final String legacyModeBackup = Config.getParameter(ImpExConstants.Params.LEGACY_MODE_KEY);
		Config.setParameter(ImpExConstants.Params.LEGACY_MODE_KEY, "true");
		try
		{
			importCsv("/configurablebundleservices/test/cartRegistration.impex", "utf-8");
			importCsv("/configurablebundleservices/test/nestedBundleTemplates.impex", "utf-8");
		}
		finally
		{
			Config.setParameter(ImpExConstants.Params.LEGACY_MODE_KEY, legacyModeBackup);
		}

		cart = userService.getUserForUID("bundle").getCarts().iterator().next();
	}

	@Test
	public void shouldAssignBundlePropertiesToEntry() throws CommerceCartModificationException
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setProduct(productService.getProductForCode("PRODUCT01"));
		parameter.setCart(cart);
		final BundleTemplateModel productComponent = bundleTemplateService.getBundleTemplateForCode("ProductComponent1");
		parameter.setBundleTemplate(productComponent);
		parameter.setQuantity(1);

		final CommerceCartModification result = commerceCartService.addToCart(parameter);

		assertNotNull(result);
		assertNotNull(result.getEntry());
		assertNotNull(result.getEntry().getBundleNo());
		assertEquals(productComponent, result.getEntry().getBundleTemplate());
	}
}
