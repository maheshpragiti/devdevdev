/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialservices.services.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.configurablebundleservices.bundle.impl.DefaultBundleCommerceCartService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Nonnull;
/*
 * Class DefaultFinancialBundleCommerceCartService overriding logic for adding bundles to the cart from DefaultBundleCommerceCartService
 */
public class DefaultFinancialBundleCommerceCartService extends DefaultBundleCommerceCartService
{

	/**
	 * @deprecated Since 6.4 - Use the generic
	 *             {@link de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartService#addToCart(CommerceCartParameter)}
	 */
	@Override
	@Nonnull
	@Deprecated
	// NO SONAR
	public List<CommerceCartModification> addToCart(@Nonnull final CartModel masterCartModel,
			@Nonnull final ProductModel productModel, final long quantityToAdd, final UnitModel unit, final boolean forceNewEntry,
			final int bundleNo, final BundleTemplateModel bundleTemplateModel, final boolean removeCurrentProducts,
			final String xmlProduct) throws CommerceCartModificationException
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(masterCartModel);
		parameter.setProduct(productModel);
		parameter.setBundleTemplate(bundleTemplateModel);
		parameter.setQuantity(quantityToAdd);
		parameter.setUnit(unit);
		parameter.setCreateNewEntry(forceNewEntry);
		parameter.setXmlProduct(xmlProduct);

		final Integer entryGroupNumber = getEntryGroupFor(masterCartModel, bundleNo, bundleTemplateModel);
		if (entryGroupNumber != null)
		{
			parameter.setEntryGroupNumbers(new HashSet<>(Collections.singletonList(entryGroupNumber)));
		}

		if (removeCurrentProducts)
		{
			final List<CartEntryModel> bundleEntries = getBundleCartEntryDao().findEntriesByMasterCartAndBundleNoAndTemplate(
					masterCartModel, bundleNo, bundleTemplateModel);
			checkAndRemoveDependentComponents(masterCartModel, bundleNo, bundleTemplateModel);
			removeEntriesWithChildren(bundleEntries, masterCartModel);
		}

		if (bundleNo != NO_BUNDLE)
		{
			checkAutoPickAddToCart(bundleTemplateModel, productModel);
		}

		final CommerceCartModification modification = addToCartWithoutCalculation(parameter);

		final List<CommerceCartModification> modificationList = new ArrayList<>();
		modificationList.add(modification);
		if (modification.getQuantityAdded() > 0 && modification.getEntry() != null)
		{
			// If starting a new bundle or replacing the contents of the existing bundle, check for auto-pick components
			if (bundleNo == NEW_BUNDLE || removeCurrentProducts)
			{
				final int newBundleNo = modification.getEntry().getBundleNo().intValue();
				final List<CommerceCartModification> autoPicks = addAutoPickProductsToCart(masterCartModel, newBundleNo,
						bundleTemplateModel, unit);
				modificationList.addAll(autoPicks);
			}

			calculateCart(masterCartModel);
		}

		updateLastModifiedEntriesList(masterCartModel, modificationList);

		return modificationList;
	}

}
