/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialservices.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.util.*;


public class BillingEventNameValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {

    public static final String PAYNOW_EVENT = "paynow";

    private CommonI18NService commonI18NService;
    private SessionService sessionService;
    private FieldNameProvider fieldNameProvider;
    private Converter<ProductModel, ProductData> productConverter;


    protected Converter<ProductModel, ProductData> getProductConverter() {
        return productConverter;
    }

    @Required
    public void setProductConverter(final Converter<ProductModel, ProductData> productConverter) {
        this.productConverter = productConverter;
    }

    protected CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    protected SessionService getSessionService() {
        return sessionService;
    }

    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    protected FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }


    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model) throws FieldValueProviderException {
        if (model == null) {
            throw new IllegalArgumentException("No model given");
        }

        final List<FieldValue> fieldValues = new ArrayList<FieldValue>();

        if (indexedProperty.isLocalized()) {
            final Collection<LanguageModel> languages = indexConfig.getLanguages();

            languages.stream().forEach(language -> {
                final Object value = getSessionService().executeInLocalView(new SessionExecutionBody() {
                    @Override
                    public Object execute() {
                        getCommonI18NService().setCurrentLanguage(language);
                        return getPropertyValue(model);
                    }
                });

                if (value != null) {
                    final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, language.getIsocode());
                    fieldNames.stream().forEach(fieldName -> fieldValues.add(new FieldValue(fieldName, value)));
                }
            });

        }

        return fieldValues;
    }

    /**
     * Gets billing events for main  product.
     *
     * @param model main product
     * @return billingEventList
     */
    protected Object getPropertyValue(final Object model) {
        if (model instanceof ProductModel) {
            final Set<String> billingEvents = new HashSet<>();

            collectProductBillingEvents(billingEvents, (SubscriptionProductModel) model);

            final Set<String> billingEventList = new HashSet<>();

            billingEvents.stream().forEach(billingEvent -> billingEventList.add(billingEvent));

            return billingEventList;
        }
        return null;
    }

    /**
     * Collect all billing events for main  productt.
     *
     * @param billingEvents list of billing events
     * @param product       main product
     */
    protected void collectProductBillingEvents(final Set<String> billingEvents, final SubscriptionProductModel product) {


        ProductData productData = new ProductData();

        getProductConverter().convert(product, productData);

        if (productData.getPrice() != null) {

            ((SubscriptionPricePlanData) productData.getPrice()).getOneTimeChargeEntries().stream()
                    .filter(billingEventCode -> !PAYNOW_EVENT.equals(billingEventCode.getBillingTime().getCode()))
                    .forEach(billingEvent -> billingEvents.add(billingEvent.getName()));
        }
    }
}

