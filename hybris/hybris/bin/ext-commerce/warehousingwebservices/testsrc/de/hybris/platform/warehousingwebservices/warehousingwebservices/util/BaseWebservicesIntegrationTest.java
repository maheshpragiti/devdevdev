/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.warehousingwebservices.warehousingwebservices.util;

import de.hybris.platform.basecommerce.util.SpringCustomContextLoader;
import de.hybris.platform.core.Registry;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.warehousingwebservices.constants.WarehousingwebservicesConstants;
import de.hybris.platform.webservicescommons.webservices.AbstractWebServicesTest;

import javax.annotation.Resource;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Ignore;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;


@Ignore("Just a testing base class.")
@ContextConfiguration(locations = { "classpath:/warehousingwebservices-spring-test.xml" })
public class BaseWebservicesIntegrationTest extends AbstractWebServicesTest
{
	public static final String YYYY_MM_DD_T_HH_MM_SS_Z = "yyyy-MM-dd'T'HH:mm:ssZ";
	protected static SpringCustomContextLoader springCustomContextLoader = null;

	@Resource
	private EnumerationService enumerationService;

	@Override
	@Before
	public void setUp() throws Exception
	{
		ensureEmbeddedServerIsRunning();
		final JacksonJsonProvider provider = new JacksonJaxbJsonProvider()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, false)
				.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

		final DateFormat isoDateFormat = new SimpleDateFormat(YYYY_MM_DD_T_HH_MM_SS_Z);
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setDateFormat(isoDateFormat);

		provider.setMapper(objectMapper);
		final ClientConfig config = new ClientConfig(provider);

		//Jersey needs its own logger to setup logging entity
		final java.util.logging.Logger loggerForJerseyLoggingFilter = java.util.logging.Logger
				.getLogger(AbstractWebServicesTest.class.getName());
		config.register(new LoggingFilter(loggerForJerseyLoggingFilter, true));

		jerseyClient = createClient(config);
		webResource = setupWebResource(getExtensionInfo());
		ensureWebappIsRunning();
	}

	public BaseWebservicesIntegrationTest()
	{
		if (springCustomContextLoader == null)
		{
			try
			{
				springCustomContextLoader = new SpringCustomContextLoader(getClass());
				springCustomContextLoader.loadApplicationContexts((GenericApplicationContext) Registry.getCoreApplicationContext());
				springCustomContextLoader.loadApplicationContextByConvention((GenericApplicationContext) Registry
						.getCoreApplicationContext());
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e.getMessage(), e);
			}
		}
	}

	@Override
	public String getExtensionName()
	{
		return WarehousingwebservicesConstants.EXTENSIONNAME;
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}
}
