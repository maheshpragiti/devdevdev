/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.configurablebundleservices.bundle.impl;

import de.hybris.platform.configurablebundleservices.bundle.BundleRuleService;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.constants.ConfigurableBundleServicesConstants;
import de.hybris.platform.configurablebundleservices.daos.BundleRuleDao;
import de.hybris.platform.configurablebundleservices.daos.ChangeProductPriceBundleRuleDao;
import de.hybris.platform.configurablebundleservices.daos.OrderEntryDao;
import de.hybris.platform.configurablebundleservices.enums.BundleRuleTypeEnum;
import de.hybris.platform.configurablebundleservices.model.AbstractBundleRuleModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.ChangeProductPriceBundleRuleModel;
import de.hybris.platform.configurablebundleservices.model.DisableProductBundleRuleModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

/**
 * Default implementation of the bundle rule service {@link BundleRuleService}. It uses Flexible Search queries and Java
 * Code to find the lowest price (based on bundle price rules) for a product that is or will be part of a bundle.
 * 
 */
public class DefaultBundleRuleService extends DefaultBundleCommerceRuleService
{

	@Override
	@Nullable
	public ChangeProductPriceBundleRuleModel getChangePriceBundleRuleForOrderEntry(@Nonnull final AbstractOrderEntryModel entry)
	{
		validateParameterNotNullStandardMessage("entry", entry);
		validateParameterNotNull(entry.getBundleTemplate(), "Cart entry model does not have a bundle template");

		final Integer bundleNo = entry.getBundleNo();

		if (bundleNo != null && bundleNo.intValue() != ConfigurableBundleServicesConstants.NO_BUNDLE)
		{
			final AbstractOrderModel order = entry.getOrder();
			final CurrencyModel currency = order.getCurrency();
			final ProductModel targetProduct = entry.getProduct();
			final AbstractOrderModel masterAbstractOrder = (entry.getOrder().getParent() == null) ? entry.getOrder() : entry
					.getOrder().getParent();
			final Set<ProductModel> otherProductsInSameBundle = getCartProductsInSameBundle(masterAbstractOrder, targetProduct,
					bundleNo.intValue());

			return getLowestPriceForTargetProductAndTemplate(entry.getBundleTemplate(), targetProduct, currency,
					otherProductsInSameBundle);
		}
		return null;
	}
}
