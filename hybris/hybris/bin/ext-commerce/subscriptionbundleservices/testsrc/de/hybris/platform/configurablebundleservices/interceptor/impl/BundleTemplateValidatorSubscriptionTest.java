/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.configurablebundleservices.interceptor.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Tests for the bundle template validator {@link BundleTemplateValidator}
 */
@IntegrationTest
@Deprecated
public class BundleTemplateValidatorSubscriptionTest
{
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Mock
	private InterceptorContext ctx;

	private BundleTemplateValidator vlad;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		vlad = new BundleTemplateValidator();
	}

	@Test
	public void testBundleTemplateValidateActsOnlyOnBundleTemplates() throws InterceptorException
	{
		ProductModel product = new ProductModel();
		product.setSubscriptionTerm(new SubscriptionTermModel());
		vlad.onValidate(product, ctx);
	}
}
