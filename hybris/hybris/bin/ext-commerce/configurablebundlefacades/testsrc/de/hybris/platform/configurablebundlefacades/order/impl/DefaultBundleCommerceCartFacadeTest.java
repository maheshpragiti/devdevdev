/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */


package de.hybris.platform.configurablebundlefacades.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.order.hook.BundleAddToCartMethodHook;
import de.hybris.platform.core.enums.GroupType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.order.EntryGroup;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.EntryGroupService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * JUnit test suite for {@link DefaultBundleCommerceCartFacade}
 */
@UnitTest
public class DefaultBundleCommerceCartFacadeTest
{
	@Mock
	private EntryGroupService entryGroupService;
	@Mock
	private CartService cartService;
	@Mock
	private ProductSearchFacade productSearchFacade;
	@Mock
	private BundleAddToCartMethodHook bundleAddToCartMethodHook;
	@Mock
	private BundleTemplateService bundleTemplateService;
	@Mock
	private CommerceCartService commerceCartService;
	@Mock
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;
	@Mock
	private ProductService productService;
	@Mock
	private ModelService modelService;
	@InjectMocks
	private DefaultBundleCommerceCartFacade defaultBundleCommerceCartFacade;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	private PageableData pageableData;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		final EntryGroup entryGroup = new EntryGroup();
		entryGroup.setGroupNumber(Integer.valueOf(1));
		entryGroup.setGroupType(GroupType.CONFIGURABLEBUNDLE);
		entryGroup.setExternalReferenceId("bundleId");
		final CartModel cartModel = new CartModel();
		final ProductSearchPageData<SearchStateData, ProductData> productSearchPageData = new ProductSearchPageData<>();
		productSearchPageData.setBreadcrumbs(Collections.emptyList());
		productSearchPageData.setFacets(Collections.emptyList());
		SearchStateData searchStateData = new SearchStateData();
		SearchQueryData searchQuery = new SearchQueryData();
		searchQuery.setValue("");
		searchStateData.setQuery(searchQuery);
		productSearchPageData.setCurrentQuery(searchStateData);
		pageableData = new PageableData();

		given(cartService.getSessionCart()).willReturn(cartModel);
		given(entryGroupService.getGroup(any(CartModel.class), any(Integer.class))).willReturn(entryGroup);
		given(productSearchFacade.textSearch(any(SearchStateData.class), any(PageableData.class))).willReturn(productSearchPageData);
	}

	@Test
	public void shouldCreateBundleStructureForOldCart() throws CommerceCartModificationException
	{
		final CartModel cart = new CartModel();
		final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
		entry.setOrder(cart);
		entry.setBundleNo(Integer.valueOf(2));
		final BundleTemplateModel component = new BundleTemplateModel();
		component.setId("BUNDLE");
		entry.setBundleTemplate(component);
		cart.setEntries(Collections.singletonList(entry));
		given(cartService.getSessionCart()).willReturn(cart);
		final EntryGroup group = new EntryGroup();
		group.setExternalReferenceId("BUNDLE");
		group.setGroupNumber(Integer.valueOf(10));
		given(bundleTemplateService.getBundleEntryGroup(entry)).willReturn(group);
		given(entryGroupService.getRoot(any(), any())).willReturn(group);
		given(entryGroupService.getLeaves(any())).willReturn(Collections.singletonList(group));

		defaultBundleCommerceCartFacade.addToCart("PRODUCT", 1L, 2, "BUNDLE", false);

		verify(bundleAddToCartMethodHook).createBundleTree(any(), any());
		assertThat(entry.getEntryGroupNumbers(), hasItem(group.getGroupNumber()));
	}

	@Test
	public void shouldFailIfEntryGroupIsNotBundle()
	{
		final EntryGroup testTypeEntryGroup = new EntryGroup();
		testTypeEntryGroup.setGroupNumber(Integer.valueOf(1));
		testTypeEntryGroup.setGroupType(GroupType.valueOf("TEST"));

		given(entryGroupService.getGroup(any(CartModel.class), any(Integer.class))).willReturn(testTypeEntryGroup);
		thrown.expect(IllegalArgumentException.class);

		defaultBundleCommerceCartFacade.getAllowedProducts(Integer.valueOf(1), "query", new PageableData());
	}

	@Test
	public void shouldCreateNewQueryIfEmpty()
	{
		defaultBundleCommerceCartFacade.getAllowedProducts(Integer.valueOf(1), "", pageableData);

		ArgumentCaptor<SearchStateData> searchStateDataCaptor = ArgumentCaptor.forClass(SearchStateData.class);
		verify(productSearchFacade).textSearch(searchStateDataCaptor.capture(), eq(pageableData));
		Assertions.assertThat(searchStateDataCaptor.getValue().getQuery().getValue()).isEqualTo("*::bundleTemplates:bundleId");
	}

	@Test
	public void shouldAppendOldQueryWithColon()
	{
		defaultBundleCommerceCartFacade.getAllowedProducts(Integer.valueOf(1), ":oldQuery", pageableData);

		ArgumentCaptor<SearchStateData> searchStateDataCaptor = ArgumentCaptor.forClass(SearchStateData.class);
		verify(productSearchFacade).textSearch(searchStateDataCaptor.capture(), eq(pageableData));
		Assertions.assertThat(searchStateDataCaptor.getValue().getQuery().getValue()).isEqualTo(":oldQuery:bundleTemplates:bundleId");
	}

	@Test
	public void shouldAppendOldQueryWithStar()
	{
		defaultBundleCommerceCartFacade.getAllowedProducts(Integer.valueOf(1), "*oldQuery", pageableData);

		ArgumentCaptor<SearchStateData> searchStateDataCaptor = ArgumentCaptor.forClass(SearchStateData.class);
		verify(productSearchFacade).textSearch(searchStateDataCaptor.capture(), eq(pageableData));
		Assertions.assertThat(searchStateDataCaptor.getValue().getQuery().getValue()).isEqualTo("*oldQuery:bundleTemplates:bundleId");
	}

	@Test
	public void shouldNotAppendTemplateIfExists()
	{
		defaultBundleCommerceCartFacade.getAllowedProducts(Integer.valueOf(1), ":bundleTemplates:bundleId", pageableData);

		ArgumentCaptor<SearchStateData> searchStateDataCaptor = ArgumentCaptor.forClass(SearchStateData.class);
		verify(productSearchFacade).textSearch(searchStateDataCaptor.capture(), eq(pageableData));
		Assertions.assertThat(searchStateDataCaptor.getValue().getQuery().getValue()).isEqualTo(":bundleTemplates:bundleId");
	}
}
