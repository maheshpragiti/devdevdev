/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.util;

import com.google.common.collect.Lists;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.insurance.data.CustomerFormSessionData;
import de.hybris.platform.commercefacades.insurance.data.FormSessionData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.financialfacades.strategies.CustomerFormPrePopulateStrategy;
import de.hybris.platform.storefront.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsservices.enums.YFormDataTypeEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;


@UnitTest
public class DefaultInsuranceCheckoutHelperTest
{

	private static final String ORDER_CODE = "orderCode";
	private static final String APPLICATION_ID = "applicationId";
	private static final String FORM_ID = "formId";
	private static final String FORM_ID_1 = "formId1";
	private static final String FORM_ID_2 = "formId2";
	private static final String FORM_ID_3 = "formId3";
	private static final String CONTENT = "content";

	@InjectMocks
	private DefaultInsuranceCheckoutHelper helper;

	@Mock
	private CustomerFormPrePopulateStrategy prePopulateStrategy;

	@Mock
	private YFormFacade yFormFacade;


	@Before
	public void setup()
	{
		helper = new DefaultInsuranceCheckoutHelper();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testBuildYFormDataRefId()
	{
		final Integer orderEntryNumber = Integer.valueOf(2);
		Assert.assertEquals(ORDER_CODE + "_" + orderEntryNumber, helper.buildYFormDataRefId(ORDER_CODE, orderEntryNumber));
	}

	@Test
	public void testCreateFormDetailData()
	{
		final YFormDefinitionData yFormDefinitionData = new YFormDefinitionData();
		yFormDefinitionData.setApplicationId(APPLICATION_ID);
		yFormDefinitionData.setFormId(FORM_ID);
		final Integer orderEntryNumber = Integer.valueOf(2);

		final FormDetailData formDetailData = helper.createFormDetailData(yFormDefinitionData, ORDER_CODE, orderEntryNumber);

		Assert.assertNotNull(formDetailData);
		Assert.assertEquals(APPLICATION_ID, formDetailData.getApplicationId());
		Assert.assertEquals(FORM_ID, formDetailData.getFormId());
		Assert.assertEquals(ORDER_CODE + "_" + orderEntryNumber, formDetailData.getRefId());
	}

	@Test
	public void testCreateFormDetailDataList()
	{
		final YFormDefinitionData yFormDefinitionData1 = new YFormDefinitionData();
		yFormDefinitionData1.setApplicationId(APPLICATION_ID);
		yFormDefinitionData1.setFormId(FORM_ID_1);

		final YFormDefinitionData yFormDefinitionData2 = new YFormDefinitionData();
		yFormDefinitionData2.setApplicationId(APPLICATION_ID);
		yFormDefinitionData2.setFormId(FORM_ID_2);


		final Integer orderEntryNumber = Integer.valueOf(2);

		final String expectedRefId = ORDER_CODE + "_" + orderEntryNumber;

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList(yFormDefinitionData1, yFormDefinitionData2);

		Mockito.when(prePopulateStrategy.hasCustomerFormDataStored()).thenReturn(Boolean.FALSE);

		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Assert.assertNotNull(formDetailDataList);
		Assert.assertEquals(2, formDetailDataList.size());

		final FormDetailData formDetailData1 = formDetailDataList.get(0);

		Assert.assertEquals(APPLICATION_ID, formDetailData1.getApplicationId());
		Assert.assertEquals(FORM_ID_1, formDetailData1.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData1.getRefId());

		final FormDetailData formDetailData2 = formDetailDataList.get(1);

		Assert.assertEquals(APPLICATION_ID, formDetailData2.getApplicationId());
		Assert.assertEquals(FORM_ID_2, formDetailData2.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData2.getRefId());

	}

	@Test
	public void testCreateFormDetailDataListFromSessionWithSameFormDefinitionAndSameVersion() throws YFormServiceException
	{
		final YFormDefinitionData yFormDefinitionData1 = new YFormDefinitionData();
		yFormDefinitionData1.setApplicationId(APPLICATION_ID);
		yFormDefinitionData1.setFormId(FORM_ID_1);
		yFormDefinitionData1.setVersion(1);

		final YFormDefinitionData yFormDefinitionData2 = new YFormDefinitionData();
		yFormDefinitionData2.setApplicationId(APPLICATION_ID);
		yFormDefinitionData2.setFormId(FORM_ID_2);
		yFormDefinitionData2.setVersion(2);

		final Integer orderEntryNumber = Integer.valueOf(2);

		final String expectedRefId = ORDER_CODE + "_" + orderEntryNumber;
		final String sessionRefId = ORDER_CODE + "_" + (orderEntryNumber + 1);

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList(yFormDefinitionData1, yFormDefinitionData2);

		Mockito.when(prePopulateStrategy.hasCustomerFormDataStored()).thenReturn(Boolean.TRUE);

		final CustomerFormSessionData customerFormSessionData = new CustomerFormSessionData();
		final FormSessionData formSessionData = new FormSessionData();
		formSessionData.setYFormDataId(FORM_ID_3);
		formSessionData.setYFormDefinition(yFormDefinitionData1);
		formSessionData.setYFormDataRefId(sessionRefId);
		customerFormSessionData.setFormSessionData(Lists.newArrayList(formSessionData));

		Mockito.when(prePopulateStrategy.getCustomerFormData()).thenReturn(customerFormSessionData);

		final YFormDataData yFormData = new YFormDataData();
		yFormData.setId(FORM_ID_3);
		yFormData.setFormDefinition(yFormDefinitionData1);
		yFormData.setContent(CONTENT);

		Mockito.when(yFormFacade.getYFormData(FORM_ID_3)).thenReturn(yFormData);

		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Mockito.verify(yFormFacade, Mockito.times(1)).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DRAFT), Mockito.eq(StringUtils.EMPTY), Mockito.eq(CONTENT));

		Mockito.verify(yFormFacade, Mockito.times(1)).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DATA), Mockito.eq(expectedRefId), Mockito.eq(StringUtils.EMPTY));

		Assert.assertNotNull(formDetailDataList);
		Assert.assertEquals(2, formDetailDataList.size());

		final FormDetailData formDetailData1 = formDetailDataList.get(0);

		Assert.assertEquals(APPLICATION_ID, formDetailData1.getApplicationId());
		Assert.assertEquals(FORM_ID_1, formDetailData1.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData1.getRefId());

		final FormDetailData formDetailData2 = formDetailDataList.get(1);

		Assert.assertEquals(APPLICATION_ID, formDetailData2.getApplicationId());
		Assert.assertEquals(FORM_ID_2, formDetailData2.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData2.getRefId());

	}

	@Test
	public void testCreateFormDetailDataListFromSessionWithDifferentFormDefinitionAndSameVersion() throws YFormServiceException
	{
		final YFormDefinitionData yFormDefinitionData1 = new YFormDefinitionData();
		yFormDefinitionData1.setApplicationId(APPLICATION_ID);
		yFormDefinitionData1.setFormId(FORM_ID_1);
		yFormDefinitionData1.setVersion(1);

		final YFormDefinitionData yFormDefinitionData2 = new YFormDefinitionData();
		yFormDefinitionData2.setApplicationId(APPLICATION_ID);
		yFormDefinitionData2.setFormId(FORM_ID_2);
		yFormDefinitionData2.setVersion(2);

		final Integer orderEntryNumber = Integer.valueOf(2);

		final String expectedRefId = ORDER_CODE + "_" + orderEntryNumber;
		final String sessionRefId = ORDER_CODE + "_" + (orderEntryNumber + 1);

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList(yFormDefinitionData1, yFormDefinitionData2);

		Mockito.when(prePopulateStrategy.hasCustomerFormDataStored()).thenReturn(Boolean.TRUE);

		final CustomerFormSessionData customerFormSessionData = new CustomerFormSessionData();
		final FormSessionData formSessionData = new FormSessionData();
		formSessionData.setYFormDataId(FORM_ID_3);
		formSessionData.setYFormDefinition(yFormDefinitionData1);
		formSessionData.setYFormDataRefId(sessionRefId);
		customerFormSessionData.setFormSessionData(Lists.newArrayList(formSessionData));

		Mockito.when(prePopulateStrategy.getCustomerFormData()).thenReturn(customerFormSessionData);

		final YFormDataData yFormData = new YFormDataData();
		yFormData.setId(FORM_ID_3);
		yFormData.setFormDefinition(yFormDefinitionData2);
		yFormData.setContent(CONTENT);

		Mockito.when(yFormFacade.getYFormData(FORM_ID_3, YFormDataTypeEnum.DRAFT)).thenReturn(yFormData);

		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Mockito.verify(yFormFacade, Mockito.never()).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DRAFT), Mockito.eq(StringUtils.EMPTY), Mockito.eq(CONTENT));

		Mockito.verify(yFormFacade, Mockito.never()).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DATA), Mockito.eq(expectedRefId), Mockito.eq(StringUtils.EMPTY));

		Assert.assertNotNull(formDetailDataList);
		Assert.assertEquals(2, formDetailDataList.size());

		final FormDetailData formDetailData1 = formDetailDataList.get(0);

		Assert.assertEquals(APPLICATION_ID, formDetailData1.getApplicationId());
		Assert.assertEquals(FORM_ID_1, formDetailData1.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData1.getRefId());

		final FormDetailData formDetailData2 = formDetailDataList.get(1);

		Assert.assertEquals(APPLICATION_ID, formDetailData2.getApplicationId());
		Assert.assertEquals(FORM_ID_2, formDetailData2.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData2.getRefId());

	}

	@Test
	public void testCreateFormDetailDataListFromSessionWithSameFormDefinitionAndDifferentVersion() throws YFormServiceException
	{
		final YFormDefinitionData yFormDefinitionData1 = new YFormDefinitionData();
		yFormDefinitionData1.setApplicationId(APPLICATION_ID);
		yFormDefinitionData1.setFormId(FORM_ID_1);
		yFormDefinitionData1.setVersion(1);

		final YFormDefinitionData yFormDefinitionData2 = new YFormDefinitionData();
		yFormDefinitionData2.setApplicationId(APPLICATION_ID);
		yFormDefinitionData2.setFormId(FORM_ID_2);
		yFormDefinitionData2.setVersion(2);

		final Integer orderEntryNumber = Integer.valueOf(2);

		final String expectedRefId = ORDER_CODE + "_" + orderEntryNumber;
		final String sessionRefId = ORDER_CODE + "_" + (orderEntryNumber + 1);

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList(yFormDefinitionData1, yFormDefinitionData2);

		Mockito.when(prePopulateStrategy.hasCustomerFormDataStored()).thenReturn(Boolean.TRUE);

		final CustomerFormSessionData customerFormSessionData = new CustomerFormSessionData();
		final FormSessionData formSessionData = new FormSessionData();
		formSessionData.setYFormDataId(FORM_ID_3);
		formSessionData.setYFormDefinition(yFormDefinitionData1);
		formSessionData.setYFormDataRefId(sessionRefId);
		customerFormSessionData.setFormSessionData(Lists.newArrayList(formSessionData));

		Mockito.when(prePopulateStrategy.getCustomerFormData()).thenReturn(customerFormSessionData);

		final YFormDefinitionData yFormDefinitionData1Version2 = new YFormDefinitionData();
		yFormDefinitionData1Version2.setApplicationId(APPLICATION_ID);
		yFormDefinitionData1Version2.setFormId(FORM_ID_1);
		yFormDefinitionData1Version2.setVersion(2);

		final YFormDataData yFormData = new YFormDataData();
		yFormData.setId(FORM_ID_3);
		yFormData.setFormDefinition(yFormDefinitionData1Version2);
		yFormData.setContent(CONTENT);

		Mockito.when(yFormFacade.getYFormData(FORM_ID_3, YFormDataTypeEnum.DATA)).thenReturn(yFormData);

		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Mockito.verify(yFormFacade, Mockito.never()).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DRAFT), Mockito.eq(StringUtils.EMPTY), Mockito.eq(CONTENT));

		Mockito.verify(yFormFacade, Mockito.never()).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DATA), Mockito.eq(expectedRefId), Mockito.eq(StringUtils.EMPTY));

		Assert.assertNotNull(formDetailDataList);
		Assert.assertEquals(2, formDetailDataList.size());

		final FormDetailData formDetailData1 = formDetailDataList.get(0);

		Assert.assertEquals(APPLICATION_ID, formDetailData1.getApplicationId());
		Assert.assertEquals(FORM_ID_1, formDetailData1.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData1.getRefId());

		final FormDetailData formDetailData2 = formDetailDataList.get(1);

		Assert.assertEquals(APPLICATION_ID, formDetailData2.getApplicationId());
		Assert.assertEquals(FORM_ID_2, formDetailData2.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData2.getRefId());

	}

	@Test
	public void testCreateFormDetailDataListEmptyList()
	{
		final Integer orderEntryNumber = Integer.valueOf(2);

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList();
		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Assert.assertNotNull(formDetailDataList);
		Assert.assertTrue(formDetailDataList.isEmpty());
	}

	@Test
	public void testGetOrderEntryDataByFormPageId()
	{
		final List<OrderEntryData> orderEntries = Lists.newArrayList();

		final OrderEntryData orderEntryData1 = new OrderEntryData();
		orderEntryData1.setBundleNo(2);
		orderEntries.add(orderEntryData1);

		final OrderEntryData orderEntryData2 = new OrderEntryData();
		orderEntryData2.setBundleNo(5);
		orderEntries.add(orderEntryData2);

		final OrderEntryData orderEntryData3 = new OrderEntryData();
		orderEntryData3.setBundleNo(7);
		orderEntries.add(orderEntryData3);

		final CartData cartData = new CartData();
		cartData.setEntries(orderEntries);

		final OrderEntryData orderEntryDataByFormPageId = helper.getOrderEntryDataByFormPageId(cartData, "5");

		Assert.assertEquals(orderEntryData2, orderEntryDataByFormPageId);

	}

	@Test
	public void testGetOrderEntryDataByFormPageIdNoOrderEntyr()
	{
		final List<OrderEntryData> orderEntries = Lists.newArrayList();

		final OrderEntryData orderEntryData1 = new OrderEntryData();
		orderEntryData1.setBundleNo(2);
		orderEntries.add(orderEntryData1);

		final OrderEntryData orderEntryData2 = new OrderEntryData();
		orderEntryData2.setBundleNo(5);
		orderEntries.add(orderEntryData2);


		final CartData cartData = new CartData();
		cartData.setEntries(orderEntries);

		final OrderEntryData orderEntryDataByFormPageId = helper.getOrderEntryDataByFormPageId(cartData, "1");

		Assert.assertNull(orderEntryDataByFormPageId);

	}

	@Test
	public void testGetFormPageIdByOrderEntryData()
	{
		final OrderEntryData orderEntryData = new OrderEntryData();
		orderEntryData.setBundleNo(2);

		Assert.assertEquals("2", helper.getFormPageIdByOrderEntryData(orderEntryData));
	}

	@Test
	public void shouldNotCloneFormDataWhenRefIdsAreSame() throws YFormServiceException
	{
		final YFormDefinitionData yFormDefinitionData1 = new YFormDefinitionData();
		yFormDefinitionData1.setApplicationId(APPLICATION_ID);
		yFormDefinitionData1.setFormId(FORM_ID_1);
		yFormDefinitionData1.setVersion(1);

		final YFormDefinitionData yFormDefinitionData2 = new YFormDefinitionData();
		yFormDefinitionData2.setApplicationId(APPLICATION_ID);
		yFormDefinitionData2.setFormId(FORM_ID_2);
		yFormDefinitionData2.setVersion(2);

		final Integer orderEntryNumber = Integer.valueOf(2);

		final String expectedRefId = ORDER_CODE + "_" + orderEntryNumber;
		final String sessionRefId = expectedRefId;

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList(yFormDefinitionData1, yFormDefinitionData2);

		Mockito.when(prePopulateStrategy.hasCustomerFormDataStored()).thenReturn(Boolean.TRUE);

		final CustomerFormSessionData customerFormSessionData = new CustomerFormSessionData();
		final FormSessionData formSessionData = new FormSessionData();
		formSessionData.setYFormDataId(FORM_ID_3);
		formSessionData.setYFormDefinition(yFormDefinitionData1);
		formSessionData.setYFormDataRefId(sessionRefId);
		customerFormSessionData.setFormSessionData(Lists.newArrayList(formSessionData));

		Mockito.when(prePopulateStrategy.getCustomerFormData()).thenReturn(customerFormSessionData);

		final YFormDataData yFormData = new YFormDataData();
		yFormData.setId(FORM_ID_3);
		yFormData.setFormDefinition(yFormDefinitionData1);
		yFormData.setContent(CONTENT);

		Mockito.when(yFormFacade.getYFormData(FORM_ID_3)).thenReturn(yFormData);

		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Mockito.verify(yFormFacade, Mockito.never()).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DRAFT), Mockito.eq(StringUtils.EMPTY), Mockito.eq(CONTENT));

		Mockito.verify(yFormFacade, Mockito.never()).createYFormData(Mockito.eq(APPLICATION_ID), Mockito.eq(FORM_ID_1),
				Mockito.anyString(), Mockito.eq(YFormDataTypeEnum.DATA), Mockito.eq(expectedRefId), Mockito.eq(StringUtils.EMPTY));

		Assert.assertNotNull(formDetailDataList);
		Assert.assertEquals(2, formDetailDataList.size());

		final FormDetailData formDetailData1 = formDetailDataList.get(0);

		Assert.assertEquals(APPLICATION_ID, formDetailData1.getApplicationId());
		Assert.assertEquals(FORM_ID_1, formDetailData1.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData1.getRefId());

		final FormDetailData formDetailData2 = formDetailDataList.get(1);

		Assert.assertEquals(APPLICATION_ID, formDetailData2.getApplicationId());
		Assert.assertEquals(FORM_ID_2, formDetailData2.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData2.getRefId());
	}

}
