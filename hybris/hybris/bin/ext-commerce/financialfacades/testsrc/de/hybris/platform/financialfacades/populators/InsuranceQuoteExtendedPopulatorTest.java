/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.quotation.InsuranceQuoteData;
import de.hybris.platform.financialfacades.constants.FinancialfacadesConstants;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.Maps;


/**
 * The class of InsuranceQuoteExtendedPopulatorTest.
 */
@UnitTest
public class InsuranceQuoteExtendedPopulatorTest
{
	@InjectMocks
	private InsuranceQuoteExtendedPopulator populator;

	private static final String EXPECT_DATE_FORMAT = "dd-MM-yyyy";

	@Before
	public void setup()
	{
		populator = new InsuranceQuoteExtendedPopulator();

		final List<InsuranceDataPopulatorStrategy> strategyList = new ArrayList<InsuranceDataPopulatorStrategy>();
		final TravelDataPopulatorStrategy travelDataPopulatorStrategy = new TravelDataPopulatorStrategy();

		travelDataPopulatorStrategy.setDateFormatForDisplay(EXPECT_DATE_FORMAT);

		strategyList.add(travelDataPopulatorStrategy);
		populator.setInsuranceDataPopulatorStrategies(strategyList);

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPopulate()
	{
		final String destination = "London";
		final Integer noOfTravellers = Integer.valueOf(2);
		final Integer noOfDays = Integer.valueOf(2);
		final String startDate = "18/09/2014";
		final String endDate = "22/09/2014";
		final String expStartDate = "18-09-2014";
		final String expEndDate = "22-09-2014";

		final DateTimeFormatter formatter = DateTimeFormat
				.forPattern(FinancialfacadesConstants.TEST_TRAVEL_INSURANCE_DATE_DISPLAY_FORMAT);
		final Date tripStartDate = formatter.parseDateTime(startDate).toDate();
		final Date tripEndDate = formatter.parseDateTime(endDate).toDate();

		final InsuranceQuoteModel quoteModel = new InsuranceQuoteModel();
		final InsuranceQuoteData quoteData = new InsuranceQuoteData();

		final Map<String, Object> properties = Maps.newHashMap();
		properties.put(FinancialfacadesConstants.TRIP_DETAILS_DESTINATION, destination);
		properties.put(FinancialfacadesConstants.TRIP_DETAILS_START_DATE, tripStartDate);
		properties.put(FinancialfacadesConstants.TRIP_DETAILS_END_DATE, tripEndDate);
		properties.put(FinancialfacadesConstants.TRIP_DETAILS_NO_OF_TRAVELLERS, noOfTravellers);
		properties.put(FinancialfacadesConstants.TRIP_DETAILS_NO_OF_DAYS, noOfDays);

		quoteModel.setProperties(properties);
		populator.populate(quoteModel, quoteData);

		Assert.assertEquals(destination, quoteData.getTripDestination());
		Assert.assertEquals(expStartDate, quoteData.getFormattedStartDate());
		Assert.assertEquals(expEndDate, quoteData.getTripEndDate());
		Assert.assertEquals(noOfTravellers, quoteData.getTripNoOfTravellers());
		Assert.assertEquals(noOfDays, quoteData.getTripNoOfDays());

	}
}
