/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.facades.impl;

import de.hybris.platform.commercefacades.insurance.data.QuoteItemRequestData;
import de.hybris.platform.commercefacades.insurance.data.QuoteItemResponseData;
import de.hybris.platform.commercefacades.insurance.data.QuoteRequestData;
import de.hybris.platform.commercefacades.insurance.data.QuoteResponseData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.financialfacades.facades.QuoteServiceFacade;
import de.hybris.platform.financialservices.enums.QuoteWorkflowStatus;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * The type Mock quote service facade.
 */
public class MockQuoteServiceFacade implements QuoteServiceFacade
{
	public static final String START_DATE = "startDate";
	public static final String END_DATE = "endDate";
	public static final String QUOTE_STATE = "state";
	public static final String QUOTE_STATE_BIND = "bind";
	public static final String QUOTE_STATE_UNBIND = "unbind";
	protected static final int DAYS = 30;
	protected static final String MOCK_ID_PREFIX = "QREF";
	protected static final int MAX_ID_NUM = 999999;
	protected static final int MIN_ID_NUM = 100000;
	protected static final String CATEGORY_ID_LIFE = "insurances_life";
	protected static final String CATEGORY_LIFE = "LIFE";
	protected static final String CATEGORY_DEFAULT = "DEFAULT";

	private String dateFormat;

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	/**
	 * Request to update a quote to a external system, and return to hybris the information about the switch from bind
	 * quote
	 *
	 * @param requestData the quote request data
	 * @return quote response data
	 */
	@Override
	public QuoteResponseData requestQuoteUnbind(final QuoteRequestData requestData)
	{
		final QuoteResponseData responseData = new QuoteResponseData();
		final QuoteItemResponseData itemData = new QuoteItemResponseData();
		final Map<String, String> properties = new HashMap<>();
		itemData.setId("");
		properties.put(START_DATE, createMockQuoteStartDate());
		properties.put(END_DATE, null);
		properties.put(QUOTE_STATE, QUOTE_STATE_UNBIND);
		itemData.setProperties(properties);
		final List<QuoteItemResponseData> responseDataList = new ArrayList<>();
		responseDataList.add(itemData);
		responseData.setItems(responseDataList);
		return responseData;
	}

	/**
	 * Request to update a quote to an external system, and return to hybris the information about an unbind quote quote
	 *
	 * @param requestData the quote request data
	 * @return quote response data
	 */
	@Override
	public QuoteResponseData requestQuoteBind(final QuoteRequestData requestData)
	{
		final QuoteResponseData responseData = new QuoteResponseData();
		final QuoteItemResponseData itemData = new QuoteItemResponseData();
		final Map<String, String> properties = new HashMap<>();
		itemData.setId(createMockQuoteId());
		properties.put(START_DATE, createMockQuoteStartDate());
		properties.put(END_DATE, createMockExpireDate());
		properties.put(QUOTE_STATE, QUOTE_STATE_BIND);
		itemData.setProperties(properties);
		final List<QuoteItemResponseData> responseDataList = new ArrayList<>();
		responseDataList.add(itemData);
		responseData.setItems(responseDataList);
		return responseData;
	}

	/**
	 * Request to get information about the Quote WorkFlow type based on the requested category id.
	 *
	 * @param requestData the quote request data
	 * @return quote response data
	 */
	@Override
	public QuoteResponseData requestQuoteWorkFlowType(final QuoteRequestData requestData)
	{
		final QuoteResponseData responseData = new QuoteResponseData();
		final List<QuoteItemResponseData> responseDataList = new ArrayList<>();

		QuoteItemResponseData itemData;

		for (final QuoteItemRequestData quoteItem : requestData.getItems())
		{
			itemData = new QuoteItemResponseData();

			if (CATEGORY_ID_LIFE.equalsIgnoreCase(quoteItem.getId()))
			{
				itemData.setId(CATEGORY_LIFE);
			}
			else
			{
				itemData.setId(CATEGORY_DEFAULT);
			}

			responseDataList.add(itemData);
		}

		responseData.setItems(responseDataList);

		return responseData;
	}

	@Override
	public QuoteResponseData requestQuoteWorkFlowStatus(final QuoteRequestData requestData)
	{
		final QuoteResponseData responseData = new QuoteResponseData();
		final List<QuoteItemResponseData> responseDataList = new ArrayList<>();

		for (final QuoteItemRequestData quoteItem : requestData.getItems())
		{
			final QuoteItemResponseData itemData = new QuoteItemResponseData();

			if (StringUtils.isNotEmpty(quoteItem.getId()))
			{
				final CartModel exampleCart = new CartModel();
				exampleCart.setCode(quoteItem.getId());
				final List<CartModel> carts = flexibleSearchService.getModelsByExample(exampleCart);

				if (areValidCarts(carts))
				{
					itemData.setId(carts.get(0).getInsuranceQuote().getQuoteWorkflowStatus().toString());
				}
				else
				{
					itemData.setId(QuoteWorkflowStatus.APPROVED.toString());
				}
			}

			responseDataList.add(itemData);
		}

		responseData.setItems(responseDataList);

		return responseData;
	}

	private boolean areValidCarts(List<CartModel> carts)
	{
		return cartsExist(carts) && hasValidDefaultCategory(carts) && isQuoteWorkflowStatusValid(carts)
				&& isLifeInsuranceInCart(carts);
	}

	private boolean cartsExist(List<CartModel> carts)
	{
		return CollectionUtils.isNotEmpty(carts) && CollectionUtils.isNotEmpty(carts.get(0).getEntries());
	}

	private boolean hasValidDefaultCategory(List<CartModel> carts)
	{
		return carts.get(0).getEntries().get(0).getProduct() != null
				&& carts.get(0).getEntries().get(0).getProduct().getDefaultCategory() != null;
	}

	private boolean isQuoteWorkflowStatusValid(List<CartModel> carts)
	{
		return carts.get(0).getInsuranceQuote() != null
				&& carts.get(0).getInsuranceQuote().getQuoteWorkflowStatus() != null;
	}

	private boolean isLifeInsuranceInCart(List<CartModel> carts)
	{
		return CATEGORY_ID_LIFE
				.equalsIgnoreCase(carts.get(0).getEntries().get(0).getProduct().getDefaultCategory().getCode());
	}

	/**
	 * @deprecated Since 6.4. use
	 * {@link de.hybris.platform.financialfacades.facades.impl.MockQuoteServiceFacade#createMockQuoteStartDate()}
	 * instead.
	 */
	@Deprecated
	protected String createMockQuoteStartDate(final QuoteItemResponseData itemResponseData)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(getDateFormat());
		return sdf.format(new Date());
	}

	protected String createMockQuoteStartDate()
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(getDateFormat());
		return sdf.format(new Date());
	}

	/**
	 * @deprecated Since 6.4. use
	 * {@link de.hybris.platform.financialfacades.facades.impl.MockQuoteServiceFacade#createMockExpireDate()}
	 * instead.
	 */
	@Deprecated
	protected String createMockExpireDate(final QuoteItemResponseData itemResponseData)
	{
		final DateTime expireDate = DateTime.now().plusDays(DAYS);
		final SimpleDateFormat sdf = new SimpleDateFormat(getDateFormat());
		return sdf.format(expireDate.toDate());
	}

	protected String createMockExpireDate()
	{
		final DateTime expireDate = DateTime.now().plusDays(DAYS);
		final SimpleDateFormat sdf = new SimpleDateFormat(getDateFormat());
		return sdf.format(expireDate.toDate());
	}

	/**
	 * @deprecated Since 6.4. use
	 * {@link de.hybris.platform.financialfacades.facades.impl.MockQuoteServiceFacade#createMockQuoteId()}
	 * instead.
	 */
	@Deprecated
	protected String createMockQuoteId(final QuoteItemResponseData itemResponseData)
	{
		final SecureRandom rand = new SecureRandom();
		rand.setSeed(new Date().getTime());
		final int randomNum = rand.nextInt(MAX_ID_NUM) % (MAX_ID_NUM - MIN_ID_NUM + 1) + MIN_ID_NUM;
		return MOCK_ID_PREFIX + randomNum;
	}

	protected String createMockQuoteId()
	{
		final SecureRandom rand = new SecureRandom();
		rand.setSeed(new Date().getTime());
		final int randomNum = rand.nextInt(MAX_ID_NUM) % (MAX_ID_NUM - MIN_ID_NUM + 1) + MIN_ID_NUM;
		return MOCK_ID_PREFIX + randomNum;
	}

	protected String getDateFormat()
	{
		return dateFormat;
	}

	@Required
	public void setDateFormat(final String dateFormat)
	{
		this.dateFormat = dateFormat;
	}

}
