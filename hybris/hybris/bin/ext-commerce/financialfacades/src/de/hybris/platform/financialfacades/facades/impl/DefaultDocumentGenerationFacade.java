/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.facades.impl;

import de.hybris.platform.commercefacades.insurance.data.InsurancePolicyData;
import de.hybris.platform.financialfacades.facades.DocumentGenerationFacade;
import de.hybris.platform.financialfacades.services.document.generation.pdf.fop.DocumentGenerationService;
import de.hybris.platform.financialfacades.services.document.generation.pdf.fop.PolicyDocumentDataProcessService;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.NotImplementedException;
import org.apache.fop.apps.MimeConstants;
import org.springframework.beans.factory.annotation.Required;


/**
 * Document Generation Facade
 */
public class DefaultDocumentGenerationFacade implements DocumentGenerationFacade
{
	/**
	 * The Document generation service.
	 */
	DocumentGenerationService documentGenerationService;
	/**
	 * The Policy document data process service.
	 */
	PolicyDocumentDataProcessService policyDocumentDataProcessService;

	/**
	 * The constant CONTENT_DISPOSITION.
	 */
	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	/**
	 * The constant INLINE_PDF.
	 */
	public static final String INLINE_PDF = "inline; filename=";

	/**
	 * Generates pdf from policy data and writes it to HTTP response.
	 *
	 * @param documentType
	 * @param policyData
	 * @param response
	 * @return pdf
	 * @throws IOException {@link NotImplementedException}
	 */
	public byte[] generate(final String documentType, final InsurancePolicyData policyData, final HttpServletResponse response)
			throws IOException
	{
		if (documentType.equals(DocumentGenerationFacade.PDF_DOCUMENT))
		{
			final byte[] pdf = getDocumentGenerationService().generatePdf(policyData);
			if (response != null)
			{
				writePdfToResponse(pdf, response);
			}
			return pdf;
		}
		else
		{
			throw new NotImplementedException();
		}
	}

	@Override
	public byte[] generate(final String documentType, final String itemRefId, final HttpServletResponse response)
			throws IOException
	{
		final InsurancePolicyData policyData = getPolicyDocumentDataProcessService().getPolicyDocumentData(itemRefId);
		return generate(documentType, policyData, response);
	}

	/**
	 * Writes pdf to response.
	 *
	 * @param pdf      array of bytes representing the pdf document
	 * @param response HTTP response where pdf is written
	 * @throws IOException
	 */
	protected void writePdfToResponse(final byte[] pdf, final HttpServletResponse response) throws IOException
	{
		if (pdf != null)
		{
			final Date date = new Date();

			final String filename = date.getTime() + ".pdf";
			response.setContentType(MimeConstants.MIME_PDF);
			response.setHeader(CONTENT_DISPOSITION, INLINE_PDF + filename);
			response.setContentLength(pdf.length);
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
		}
	}

	/**
	 * Gets document generation service.
	 *
	 * @return {@link DocumentGenerationService}
	 */
	protected DocumentGenerationService getDocumentGenerationService()
	{
		return documentGenerationService;
	}

	/**
	 * Sets document generation service.
	 *
	 * @param documentGenerationService the document generation service
	 */
	@Required
	public void setDocumentGenerationService(final DocumentGenerationService documentGenerationService)
	{
		this.documentGenerationService = documentGenerationService;
	}

	/**
	 * Gets policy document data process service.
	 *
	 * @return the policy document data process service
	 */
	protected PolicyDocumentDataProcessService getPolicyDocumentDataProcessService()
	{
		return policyDocumentDataProcessService;
	}

	/**
	 * Sets policy document data process service.
	 *
	 * @param policyDocumentDataProcessService the policy document data process service
	 */
	@Required
	public void setPolicyDocumentDataProcessService(final PolicyDocumentDataProcessService policyDocumentDataProcessService)
	{
		this.policyDocumentDataProcessService = policyDocumentDataProcessService;
	}
}
