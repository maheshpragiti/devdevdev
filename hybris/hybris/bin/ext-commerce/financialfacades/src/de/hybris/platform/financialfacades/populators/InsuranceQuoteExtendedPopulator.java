/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.populators;

import de.hybris.platform.commercefacades.quotation.InsuranceQuoteData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;


public class InsuranceQuoteExtendedPopulator<SOURCE extends InsuranceQuoteModel, TARGET extends InsuranceQuoteData> implements
		Populator<SOURCE, TARGET>
{

	private List<InsuranceDataPopulatorStrategy> insuranceDataPopulatorStrategies;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final InsuranceQuoteModel source, final InsuranceQuoteData target) throws ConversionException
	{
		Map<String, Object> infoMap = null;

		if (MapUtils.isNotEmpty(source.getProperties()))
		{
			infoMap = source.getProperties();
		}
		else
		{
			infoMap = new HashMap<String, Object>();
		}

		if (getInsuranceDataPopulatorStrategies() != null)
		{
			for (final InsuranceDataPopulatorStrategy insuranceDataPopulatorStrategy : getInsuranceDataPopulatorStrategies())
			{
				insuranceDataPopulatorStrategy.processInsuranceQuoteData(target, infoMap);
			}
		}
	}

	/**
	 * @return the insuranceDataPopulatorStrategies
	 */
	public List<InsuranceDataPopulatorStrategy> getInsuranceDataPopulatorStrategies()
	{
		return insuranceDataPopulatorStrategies;
	}

	/**
	 * @param insuranceDataPopulatorStrategies
	 *           the insuranceDataPopulatorStrategies to set
	 */
	public void setInsuranceDataPopulatorStrategies(final List<InsuranceDataPopulatorStrategy> insuranceDataPopulatorStrategies)
	{
		this.insuranceDataPopulatorStrategies = insuranceDataPopulatorStrategies;
	}

}
