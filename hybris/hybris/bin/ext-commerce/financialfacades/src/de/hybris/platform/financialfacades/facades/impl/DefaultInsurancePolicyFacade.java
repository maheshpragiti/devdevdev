/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.facades.impl;

import de.hybris.platform.commercefacades.insurance.data.InsurancePolicyListingData;
import de.hybris.platform.financialfacades.facades.InsurancePolicyFacade;
import de.hybris.platform.financialfacades.strategies.CustomerInsurancePolicyStrategy;
import de.hybris.platform.financialfacades.strategies.QuotePolicyStrategy;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * The type Default insurance policy facade.
 */
public class DefaultInsurancePolicyFacade implements InsurancePolicyFacade
{

	/**
	 * The Quote policy strategy.
	 */
	public QuotePolicyStrategy quotePolicyStrategy;

	private CustomerInsurancePolicyStrategy customerInsurancePolicyStrategy;


	@Override
	public List<InsurancePolicyListingData> getPoliciesForCustomer(final String customerId)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("customerId", customerId);
		return getCustomerInsurancePolicyStrategy().getPolicyDataForUID(customerId);
	}

	@Override
	public List<InsurancePolicyListingData> getPoliciesForCurrentCustomer()
	{
		return getCustomerInsurancePolicyStrategy().getPolicyDataForCurrentCustomer();
	}

	protected QuotePolicyStrategy getQuotePolicyStrategy()
	{
		return quotePolicyStrategy;
	}

	@Required
	public void setQuotePolicyStrategy(final QuotePolicyStrategy quotePolicyStrategy)
	{
		this.quotePolicyStrategy = quotePolicyStrategy;
	}

	protected CustomerInsurancePolicyStrategy getCustomerInsurancePolicyStrategy()
	{
		return customerInsurancePolicyStrategy;
	}

	@Required
	public void setCustomerInsurancePolicyStrategy(final CustomerInsurancePolicyStrategy customerInsurancePolicyStrategy)
	{
		this.customerInsurancePolicyStrategy = customerInsurancePolicyStrategy;
	}
}
