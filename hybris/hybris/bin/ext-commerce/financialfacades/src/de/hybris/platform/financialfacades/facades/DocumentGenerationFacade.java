/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.facades;

import de.hybris.platform.commercefacades.insurance.data.InsurancePolicyData;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;


/**
 * Document Generation Facade
 */
public interface DocumentGenerationFacade
{
	/**
	 * The constant PDF_DOCUMENT.
	 */
    String PDF_DOCUMENT = "PDF";

	/**
	 * Retrieve the byte[] with desired policy data and document type and write to the response if not null.
	 *
	 * @param documentType the document type
	 * @param policyData   the policy data
	 * @param response     the response
	 * @return the byte [ ]
	 * @throws IOException the io exception
	 */
	byte[] generate(final String documentType, final InsurancePolicyData policyData, final HttpServletResponse response)
			throws IOException;

	/**
	 * Retrieve the byte[] with desired itemRefId and document type and write to the response if not null.
	 *
	 * @param documentType the document type
	 * @param itemRefId    the item ref id
	 * @param response     the response
	 * @return the byte [ ]
	 * @throws IOException the io exception
	 */
	byte[] generate(final String documentType, final String itemRefId, final HttpServletResponse response) throws IOException;
}
