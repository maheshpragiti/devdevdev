/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.services.document.generation.pdf.fop.exceptions;

/**
 * Generic Exception thrown when document can not be generated.
 */
public class DocumentGenerationException extends RuntimeException
{
	public DocumentGenerationException(String message)
	{
		super(message);
	}

	public DocumentGenerationException(String message, Exception e)
	{
		super(message, e);
	}
}
