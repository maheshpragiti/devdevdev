/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.util;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.storefront.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;

import java.util.List;


public interface InsuranceCheckoutHelper
{

	/**
	 * Builds up a refId for YFormData
	 *
	 * @param orderCode
	 * @param orderEntryNumber
	 * @return String YFormData refId
	 */
    String buildYFormDataRefId(final String orderCode, final Integer orderEntryNumber);

	/**
	 * Transforms the inputs into a FormDetailData object
	 *
	 * @param yFormDefinitionData
	 * @param orderCode
	 * @param orderEntryNumber
	 * @return FormDetailData
	 */
    FormDetailData createFormDetailData(final YFormDefinitionData yFormDefinitionData, final String orderCode,
                                        final Integer orderEntryNumber);

	/**
	 * Transforms the inputs into a FormDetailData objects
	 *
	 * @param yFormDefinitionDataList
	 * @param orderCode
	 * @param orderEntryNumber
	 * @return List<FormDetailData>
	 */
    List<FormDetailData> createFormDetailData(final List<YFormDefinitionData> yFormDefinitionDataList,
                                              final String orderCode, final Integer orderEntryNumber);

	/**
	 * Returns the OrderEntryData based on the formPageId
	 *
	 * @param cartData
	 * @param formPageId
	 * @return OrderEntryData
	 */
    OrderEntryData getOrderEntryDataByFormPageId(final CartData cartData, final String formPageId);

	/**
	 * Returns the FormPageId from the OrderEntryData
	 *
	 * @param orderEntryData
	 * @return String
	 */
    String getFormPageIdByOrderEntryData(final OrderEntryData orderEntryData);
}
