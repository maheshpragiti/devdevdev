/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.facades;

import de.hybris.platform.commercefacades.insurance.data.InsuranceQuoteListingData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.financialservices.enums.QuoteBindingState;
import de.hybris.platform.financialservices.enums.QuoteWorkflowStatus;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;

import java.util.List;


/**
 * The interface Insurance quote facade.
 */
public interface InsuranceQuoteFacade extends QuoteFacade
{
	/**
	 * Create a quote object in session cart.
	 */
	void createQuoteInSessionCart();

	/**
	 * Unbind quote for the quote
	 *
	 * @param quoteModel the quote model
	 * @return the insurance quote model
	 */
	InsuranceQuoteModel unbindQuote(final InsuranceQuoteModel quoteModel);

	/**
	 * Bind quote for the quote
	 *
	 * @param insuranceQuote the insurance quote
	 * @return the insurance quote model
	 */
	InsuranceQuoteModel bindQuote(final InsuranceQuoteModel insuranceQuote);

	/**
	 * Get quote state from the session cart
	 *
	 * @return the quote state from session cart
	 */
	QuoteBindingState getQuoteStateFromSessionCart();


	/**
	 * Sort the cart data list based on the quote listing
	 *
	 * @param cartDataList the cart data list
	 * @return the list
	 */
	List<InsuranceQuoteListingData> sortCartListForQuoteListing(List<CartData> cartDataList);

	/**
	 * Get the quote workflow type.
	 *
	 * @return the quote workflow type
	 */
	String getQuoteWorkflowType();

	/**
	 * Get the quote workflow status for session cart.
	 *
	 * @return the quote workflow status
	 */
	QuoteWorkflowStatus getQuoteWorkflowStatus();
}
