/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.quotation.InsuranceQuoteData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.financialservices.model.InsuranceQuoteModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;


/**
 * The class of InsuranceOrderEntryInsuranceInformationPopulator. This populator is temporary class that needed to extra
 * form data from the OrderEntry, this should later be replaced with populators that populates from the Quote Object.
 */
public class InsuranceOrderQuotePopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	private Converter<InsuranceQuoteModel, InsuranceQuoteData> insuranceQuoteConverter;


	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param orderModel
	 *           the source object
	 * @param orderData
	 *           the target to fill
	 */
	@Override
	public void populate(final AbstractOrderModel orderModel, final AbstractOrderData orderData)
	{
		if (orderModel.getInsuranceQuote() != null)
		{
			final InsuranceQuoteModel quoteModel = orderModel.getInsuranceQuote();

			if (quoteModel != null)
			{
				final InsuranceQuoteData quoteData = getInsuranceQuoteConverter().convert(quoteModel);
				orderData.setInsuranceQuote(quoteData);
			}

		}
	}

	protected Converter<InsuranceQuoteModel, InsuranceQuoteData> getInsuranceQuoteConverter()
	{
		return insuranceQuoteConverter;
	}

	@Required
	public void setInsuranceQuoteConverter(final Converter<InsuranceQuoteModel, InsuranceQuoteData> insuranceQuoteConverter)
	{
		this.insuranceQuoteConverter = insuranceQuoteConverter;
	}

}
