/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialfacades.facades;


import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartResultData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.quotation.InsuranceQuoteData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.configurablebundlefacades.order.BundleCartFacade;
import de.hybris.platform.order.InvalidCartException;

import java.util.List;


/**
 * The interface Insurance cart facade.
 */
public interface InsuranceCartFacade extends CartFacade, BundleCartFacade
{

	/**
	 * Set the visited step to the quote in the current cart.
	 *
	 * @param progressStepId
	 * 		the progress step id
	 */
	void setVisitedStepToQuoteInCurrentCart(String progressStepId);

	/**
	 * Check the isVisited status for the step against the quote from the current cart
	 *
	 * @param progressStepId
	 * 		the progress step id
	 * @return boolean value
	 */
	boolean checkStepIsVisitedFromQuoteInCurrentCart(String progressStepId);

	/**
	 * Get the quote state from the current cart.
	 *
	 * @return boolean value
	 */
	boolean checkStepIsEnabledFromQuoteInCurrentCart();

	/**
	 * Check if the give product has the same default category as session cart product.
	 *
	 * @param productCode
	 * 		the product code
	 * @return boolean value
	 * @throws de.hybris.platform.order.InvalidCartException
	 * 		if there is no session cart or if session cart is empty
	 */
	boolean isSameInsuranceInSessionCart(final String productCode) throws InvalidCartException;

	/**
	 * Unbind the quote object in the session cart.
	 *
	 * @return boolean value
	 */
	boolean unbindQuoteInSessionCart();

	/**
	 * Bind the quote object in the session cart.
	 *
	 * @return boolean value
	 */
	boolean bindQuoteInSessionCart();

	/**
	 * @param code cart id
	 * @param isSaveCurrentCart
	 * @throws de.hybris.platform.commerceservices.order.CommerceSaveCartException
	 **/
	void restoreCart(String code, Boolean isSaveCurrentCart) throws CommerceSaveCartException;

	/**
	 * Method saves a insurance cart for current user.
	 *
	 * @return {@link CommerceSaveCartResultData}
	 * @throws de.hybris.platform.commerceservices.order.CommerceSaveCartException
	 */
	CommerceSaveCartResultData saveCurrentUserCart() throws CommerceSaveCartException;


	/**
	 * Gets saved carts for current user.
	 *
	 * @return the saved carts for current user
	 */
	List<CartData> getSavedCartsForCurrentUser();

	/**
	 * Check if the cart with sessionCartCode is inside the saved cart list.
	 *
	 * @param sessionCartCode
	 * 		the session cart code
	 * @return boolean value
	 */
	boolean isSavedCart(String sessionCartCode);

	/**
	 * Clear all items in the session cart by main bundle. For insurance products it expects with only 1 bundle in the
	 * cart.
	 *
	 * @throws CommerceCartModificationException
	 * 		the commerce cart modification exception
	 */
	void removeMainBundleFromSessionCart() throws CommerceCartModificationException;

	/**
	 * Returns the category chosen by the current user.
	 *
	 * @return {@link CategoryData}
	 */
	CategoryData getSelectedInsuranceCategory();

	/**
	 * Check if the bundle is allowed to be added to the cart, depending on the current contents of the cart. It is only allowed
	 * to have one bundle in the cart.
	 *
	 * @param bundleTemplateId
	 * 		to be added to the cart
	 * @return boolean value
	 */
	boolean isBundleAllowedInTheCart(String bundleTemplateId);

	/**
	 * Returns the insurance quote data from session cart.
	 *
	 * @return {@link InsuranceQuoteData}
	 */
	InsuranceQuoteData getInsuranceQuoteFromSessionCart();
}
