/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.hybris.yprofile.services.impl;

import com.hybris.yprofile.common.Utils;
import com.hybris.yprofile.rest.clients.ProfileClient;
import com.hybris.yprofile.services.ProfileConfigurationService;
import com.hybris.yprofile.services.RetrieveRestClientStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.yaasconfiguration.model.YaasProjectModel;
import de.hybris.platform.yaasconfiguration.model.YaasServiceModel;
import de.hybris.platform.yaasconfiguration.service.YaasConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

/**
 * Default implementation for the {@link ProfileConfigurationService}.
 */
public class DefaultProfileConfigurationService implements ProfileConfigurationService {

    private static final Logger LOG = Logger.getLogger(DefaultProfileConfigurationService.class);

    private YaasConfigurationService yaasConfigurationService;
    private BaseSiteService baseSiteService;
    private RetrieveRestClientStrategy retrieveRestClientStrategy;

    @Override
    public boolean isYaaSConfigurationPresentForBaseSiteId(final String siteId){

        if (!getCurrentBaseSiteModel().isPresent()) {
            if (getBaseSiteForUID(siteId).isPresent()) {
                getBaseSiteService().setCurrentBaseSite(Utils.remapSiteId(siteId), true);
            } else {
                LOG.warn("Failed to load base site: '" + Utils.remapSiteId(siteId) + "'");
                return false;
            }
        }

        try {
            getRetrieveRestClientStrategy().getProfileRestClient();
        } catch (SystemException e){
            LOG.warn("Cannot retrieve YaaS Configuration for service: '" + ProfileClient.class.getSimpleName() + "' and base site: '" + Utils.remapSiteId(siteId) + "'");
            return false;
        }

        return true;
    }

    @Override
    public String getYaaSTenant(final String siteId) {
        return getYaasProject(siteId).isPresent() ? getYaasProject(siteId).get().getIdentifier() : StringUtils.EMPTY;
    }


    protected Optional<YaasProjectModel> getYaasProject(final String siteId) {

        if (isYaaSConfigurationPresentForBaseSiteId(siteId)) {
            return ofNullable(getYaasConfigurationService().getBaseSiteServiceMappingForId(siteId, getYaasServiceModel().get()).getYaasClientCredential().getYaasProject());
        }

        return empty();
    }

    protected Optional<YaasServiceModel> getYaasServiceModel()
    {
        try {
            return ofNullable(getYaasConfigurationService().getYaasServiceForId(ProfileClient.class.getSimpleName()));
        } catch (ModelNotFoundException e){
            LOG.warn("Cannot retrieve YaaS Configuration for service: '" + ProfileClient.class.getSimpleName() + "'");
        }

        return empty();
    }

    protected Optional<BaseSiteModel> getCurrentBaseSiteModel() {
        return ofNullable(getBaseSiteService().getCurrentBaseSite());
    }

    protected Optional<BaseSiteModel> getBaseSiteForUID(final String siteId) {
        return ofNullable(getBaseSiteService().getBaseSiteForUID(siteId));
    }

    public YaasConfigurationService getYaasConfigurationService() {
        return yaasConfigurationService;
    }

    @Required
    public void setYaasConfigurationService(YaasConfigurationService yaasConfigurationService) {
        this.yaasConfigurationService = yaasConfigurationService;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    public RetrieveRestClientStrategy getRetrieveRestClientStrategy() {
        return retrieveRestClientStrategy;
    }

    @Required
    public void setRetrieveRestClientStrategy(RetrieveRestClientStrategy retrieveRestClientStrategy) {
        this.retrieveRestClientStrategy = retrieveRestClientStrategy;
    }
}
