/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.hybris.yprofile.common;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
/**
 * Created by d064321 on 01.12.15.
 *
 * Formatting utility to create correct data format for yaas.
 */
public class Utils {

    private final static Map<String, String> SITE_ID_MAP = new HashMap<String, String>();

    static {
        SITE_ID_MAP.put("1", "electronics");
        SITE_ID_MAP.put("2", "apparel-de");
        SITE_ID_MAP.put("3", "apparel-uk");
    }

    private Utils() {
        //Default private constructor
    }

    public static String formatDouble(Double d){
        if(d == null){
            return "";
        }
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(',');
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
        return decimalFormat.format(d);
    }

    public static String formatDate(Date d){
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return df2.format(d);
    }

    public static String remapSiteId(String siteId) {
        String result = SITE_ID_MAP.get(siteId);
        if (StringUtils.isNotBlank(result)) {
            return result;
        }
        return siteId;
    }
}
