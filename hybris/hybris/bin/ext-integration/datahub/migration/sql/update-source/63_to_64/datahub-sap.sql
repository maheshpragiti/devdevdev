--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- ==========================================================================================
-- Add new columns
-- ==========================================================================================

ALTER TABLE "RawItem" ADD ("batchid" VARCHAR(255), "traceid" VARCHAR(36), "uuid" VARCHAR(36));

ALTER TABLE "CanonicalItem" ADD ("batchid" VARCHAR(255), "traceid" VARCHAR(36), "uuid" VARCHAR(36), "documentid" VARCHAR(255));

ALTER TABLE "CanonicalItemMeta" ADD ("documentid" VARCHAR(4000));

-- ==========================================================================================
-- Migrate PublicationError table
-- ==========================================================================================
-- 1. backup and convert data
-- 2. drop old table
-- 3. rename backup and add constraints
-- ==========================================================================================

CREATE TABLE "PublicationError_tmp"
AS(
  SELECT
    "canonicalitempublicationstatus",
    "code",
    "modifiedtime",
    "targetsystempublication",
    "id",
    "creationtime",
    TO_NCLOB("message") "message",
    "typecode"
  FROM "PublicationError");

DROP TABLE  "PublicationError";

RENAME TABLE "PublicationError_tmp" TO "PublicationError";
ALTER TABLE "PublicationError" ADD PRIMARY KEY ("id");
CREATE INDEX "FK_PublicationE_licationstatus" ON "PublicationError" ("canonicalitempublicationstatus");
CREATE INDEX "FK_PublicationE_tempublication" ON "PublicationError" ("targetsystempublication");

-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================

TRUNCATE TABLE "DataHubVersion";

INSERT INTO "DataHubVersion" values ('6.4.0');
