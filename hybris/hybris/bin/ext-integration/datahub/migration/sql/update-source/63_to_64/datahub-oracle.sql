--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- ==========================================================================================
-- Add new columns
-- ==========================================================================================

ALTER TABLE "RawItem" ADD ("batchid" VARCHAR2(255), "traceid" VARCHAR2(36), "uuid" VARCHAR2(36));

ALTER TABLE "CanonicalItem" ADD ("batchid" VARCHAR2(255), "traceid" VARCHAR2(36), "uuid" VARCHAR2(36), "documentid" VARCHAR2(255));

ALTER TABLE "CanonicalItemMeta" ADD "documentid" VARCHAR2(4000);

-- ==========================================================================================
-- Migrate PublicationError table
-- ==========================================================================================
-- 1. backup and convert data
-- 2. drop old table
-- 3. rename backup and add constraints
-- ==========================================================================================

CREATE TABLE "PublicationError_tmp"
AS(
  SELECT
    "canonicalitempublicationstatus",
    "code",
    "modifiedtime",
    "targetsystempublication",
    "id",
    "creationtime",
    TO_NCLOB("message") "message",
    "typecode"
  FROM "PublicationError");

DROP TABLE  "PublicationError" CASCADE CONSTRAINTS;

ALTER TABLE "PublicationError_tmp" RENAME TO "PublicationError";
ALTER TABLE "PublicationError" ADD PRIMARY KEY ("id");
CREATE INDEX "FK_PublicationE_licationstatus" ON "PublicationError" ("canonicalitempublicationstatus");
CREATE INDEX "FK_PublicationE_tempublication" ON "PublicationError" ("targetsystempublication");

-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================

TRUNCATE TABLE "DataHubVersion";

INSERT INTO "DataHubVersion" values ('6.4.0');
