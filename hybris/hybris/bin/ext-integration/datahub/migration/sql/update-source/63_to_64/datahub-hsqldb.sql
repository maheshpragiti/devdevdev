--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- ==========================================================================================
-- Add new columns
-- ==========================================================================================

ALTER TABLE "RawItem" ADD COLUMN "batchid" VARCHAR(255);
ALTER TABLE "RawItem" ADD COLUMN "traceid" VARCHAR(36);
ALTER TABLE "RawItem" ADD COLUMN "uuid" VARCHAR(36);

ALTER TABLE "CanonicalItem" ADD COLUMN "batchid" VARCHAR(255);
ALTER TABLE "CanonicalItem" ADD COLUMN "traceid" VARCHAR(36);
ALTER TABLE "CanonicalItem" ADD COLUMN "uuid" VARCHAR(36);
ALTER TABLE "CanonicalItem" ADD COLUMN "documentid" VARCHAR(255);

ALTER TABLE "CanonicalItemMeta" ADD COLUMN "documentid" VARCHAR(4000);

-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================

TRUNCATE TABLE "DataHubVersion" AND COMMIT;

INSERT INTO "DataHubVersion" values ('6.4.0');
