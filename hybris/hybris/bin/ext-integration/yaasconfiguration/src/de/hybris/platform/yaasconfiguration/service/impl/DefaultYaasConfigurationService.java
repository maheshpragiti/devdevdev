/*
* [y] hybris Platform
*
* Copyright (c) 2017 SAP SE or an SAP affiliate company.
* All rights reserved.
*
* This software is the confidential and proprietary information of SAP
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with SAP.
*
*/

package de.hybris.platform.yaasconfiguration.service.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static de.hybris.platform.yaasconfiguration.constants.YaasconfigurationConstants.YAAS_CLIENT_SCOPE;
import static de.hybris.platform.yaasconfiguration.constants.YaasconfigurationConstants.YAAS_CLIENT_URL;
import static de.hybris.platform.yaasconfiguration.constants.YaasconfigurationConstants.YAAS_OAUTH_CLIENTID;
import static de.hybris.platform.yaasconfiguration.constants.YaasconfigurationConstants.YAAS_OAUTH_CLIENTSECRET;
import static de.hybris.platform.yaasconfiguration.constants.YaasconfigurationConstants.YAAS_OAUTH_URL;
import static de.hybris.platform.yaasconfiguration.constants.YaasconfigurationConstants.YAAS_TENANT;
import static de.hybris.platform.yaasconfiguration.model.YaasApplicationModel._TYPECODE;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.yaasconfiguration.model.BaseSiteServiceMappingModel;
import de.hybris.platform.yaasconfiguration.model.YaasApplicationModel;
import de.hybris.platform.yaasconfiguration.model.YaasClientCredentialModel;
import de.hybris.platform.yaasconfiguration.model.YaasClientModel;
import de.hybris.platform.yaasconfiguration.model.YaasServiceModel;
import de.hybris.platform.yaasconfiguration.service.YaasConfigurationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Default implementation of {@link YaasConfigurationService}
 *
 * @param <T>
 */
public class DefaultYaasConfigurationService implements YaasConfigurationService
{
	private FlexibleSearchService flexibleSearchService;

	@Deprecated
	@Override
	public YaasApplicationModel getYaasApplicationForId(final String applicationId)
	{
		checkArgument(applicationId != null, "appId must not be null");

		final YaasApplicationModel model = new YaasApplicationModel();

		model.setIdentifier(applicationId);
		return getFlexibleSearchService().getModelByExample(model);
	}


	@Deprecated
	@Override
	public Optional<YaasApplicationModel> takeFirstModel()
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(format("select {pk} from {%s}", _TYPECODE));
			query.setCount(1);
			return ofNullable(getFlexibleSearchService().searchUnique(query));
		}
		catch (final ModelNotFoundException exception)
		{
			return empty();
		}
	}

	@Deprecated
	@Override
	public List<YaasApplicationModel> getYaaSApplications()
	{
		List<YaasApplicationModel> yaasApplcations = new ArrayList<>();
		final FlexibleSearchQuery query = new FlexibleSearchQuery("select {pk} from {" + YaasApplicationModel._TYPECODE + "}");
		yaasApplcations = getFlexibleSearchService().<YaasApplicationModel> search(query).getResult();
		return yaasApplcations;
	}

	@Deprecated
	@Override
	public YaasClientModel getYaasClientForId(final String clientId)
	{
		checkArgument(clientId != null, "clientId must not be null");

		final YaasClientModel model = new YaasClientModel();

		model.setIdentifier(clientId);
		return getFlexibleSearchService().getModelByExample(model);
	}

	@Deprecated
	@Override
	public Map<String, String> buildYaasConfig(final String yaasAppId, final Class clientType)
	{
		checkArgument(yaasAppId != null, "appId must not be null");
		checkArgument(clientType != null, "clientType must not be null");

		final Map<String, String> config = new HashMap();

		buildApplicationConfig(yaasAppId, config);

		buildClientConfig(clientType.getSimpleName(), config);

		return config;
	}


	@Override
	public Map<String, String> buildYaasConfig(final YaasClientCredentialModel clientCredential, final Class serviceType)
	{
		checkArgument(clientCredential != null, "clientCredential must not be null");
		checkArgument(serviceType != null, "serviceType must not be null");

		final Map<String, String> config = new HashMap();

		buildClientCredentialConfig(clientCredential, config);

		buildServiceConfig(serviceType.getSimpleName(), config);

		return config;
	}

	protected void buildClientCredentialConfig(final YaasClientCredentialModel clientCredential, final Map<String, String> config)
	{
		checkArgument(clientCredential != null, "clientCredential must not be null");
		checkArgument(config != null, "config must not be null");

		config.put(YAAS_OAUTH_URL, clientCredential.getOauthURL());
		config.put(YAAS_OAUTH_CLIENTID, clientCredential.getClientId());
		config.put(YAAS_OAUTH_CLIENTSECRET, clientCredential.getClientSecret());
		config.put(YAAS_TENANT, clientCredential.getYaasProject().getIdentifier());
	}

	/**
	 * Helper method to build the yass configuration from persisted YaasApplication for the given yaas applicationId
	 *
	 * @param yaasAppId
	 * @param config
	 */
	@Deprecated
	protected void buildApplicationConfig(final String yaasAppId, final Map<String, String> config)
	{
		checkArgument(yaasAppId != null, "yaasAppId must not be null");
		checkArgument(config != null, "config must not be null");

		final YaasApplicationModel yaasApplication = getYaasApplicationForId(yaasAppId);

		config.put(YAAS_OAUTH_URL, yaasApplication.getOauthURL());
		config.put(YAAS_OAUTH_CLIENTID, yaasApplication.getClientId());
		config.put(YAAS_OAUTH_CLIENTSECRET, yaasApplication.getClientSecret());
		config.put(YAAS_TENANT, yaasApplication.getYaasProject().getIdentifier());
	}

	/**
	 * Helper methd to build the yaas configuration from persisted YaasClient for the given yass client name.
	 *
	 * @param clientId
	 * @param config
	 */
	protected void buildClientConfig(final String clientId, final Map<String, String> config)
	{
		checkArgument(clientId != null, "clientId must not be null");
		checkArgument(config != null, "config must not be null");

		final YaasClientModel yaasClient = getYaasClientForId(clientId);

		config.put(YAAS_CLIENT_URL, yaasClient.getClientURL());
		config.put(YAAS_CLIENT_SCOPE, yaasClient.getClientScope());

		if (MapUtils.isNotEmpty(yaasClient.getAdditionalConfigurations()))
		{
			config.putAll(yaasClient.getAdditionalConfigurations());
		}
	}

	/**
	 *
	 * @param serviceId
	 * @param config
	 */
	protected void buildServiceConfig(final String serviceId, final Map<String, String> config)
	{
		checkArgument(serviceId != null, "serviceId must not be null");
		checkArgument(config != null, "config must not be null");

		final YaasServiceModel yaasService = getYaasServiceForId(serviceId);

		config.put(YAAS_CLIENT_URL, yaasService.getServiceURL());
		config.put(YAAS_CLIENT_SCOPE, yaasService.getServiceScope());

		if (MapUtils.isNotEmpty(yaasService.getAdditionalConfigurations()))
		{
			config.putAll(yaasService.getAdditionalConfigurations());
		}
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}


	@Override
	public YaasServiceModel getYaasServiceForId(final String id)
	{
		checkArgument(id != null, "Yaas Servcie configuration must not be null");

		final YaasServiceModel model = new YaasServiceModel();

		model.setIdentifier(id);
		return getFlexibleSearchService().getModelByExample(model);
	}

	@Override
	public YaasClientCredentialModel getYaasClientCredentialForId(final String id)
	{
		checkArgument(id != null, "Yaas Client Credential configuration must not be null");

		final YaasClientCredentialModel model = new YaasClientCredentialModel();

		model.setIdentifier(id);
		return getFlexibleSearchService().getModelByExample(model);
	}

	@Override
	public BaseSiteServiceMappingModel getBaseSiteServiceMappingForId(final String id, final YaasServiceModel serviceModel)
	{
		checkArgument(id != null, "id must not be null");
		checkArgument(serviceModel != null, "serviceModel must not be null");

		final BaseSiteServiceMappingModel model = new BaseSiteServiceMappingModel();

		model.setBaseSite(id);
		model.setYaasService(serviceModel);
		return getFlexibleSearchService().getModelByExample(model);
	}


	@Override
	public List<YaasClientCredentialModel> getYaasClientCredentials()
	{
		List<YaasClientCredentialModel> yaasClientCredentials = new ArrayList<>();

		final FlexibleSearchQuery query = new FlexibleSearchQuery("select {pk} from {" + YaasClientCredentialModel._TYPECODE + "}");
		yaasClientCredentials = getFlexibleSearchService().<YaasClientCredentialModel> search(query).getResult();
		return yaasClientCredentials;
	}

}
