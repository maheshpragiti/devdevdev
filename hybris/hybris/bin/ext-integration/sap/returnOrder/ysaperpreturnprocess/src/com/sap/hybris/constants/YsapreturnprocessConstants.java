/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.hybris.constants;

/**
 * Global class for all Ysapreturnprocess constants. You can add global
 * constants for your extension into this class.
 */
public final class YsapreturnprocessConstants extends GeneratedYsaperpreturnprocessConstants {
    public static final String EXTENSIONNAME = "ysapreturnprocess";
    public static final String RETURNORDER_CONFIRMATION_EVENT = "ReturnRequestCreationEvent_";
    public static final String RETURNORDER_GOOD_EVENT = "ApproveOrCancelGoodsEvent_";
    public static final String RETURNORDER_PAYMENT_REVERSAL_EVENT= "PaymentReversalEvent_";
    public static final String CODE = "code";
    public static final String STR="#";

    private YsapreturnprocessConstants() {
        // empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}
