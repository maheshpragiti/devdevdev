/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.hybris.inbound.impl;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import com.sap.hybris.constants.YsapreturnprocessConstants;
import com.sap.hybris.inbound.DataHubInboundOrderHelper;

public class DefaultDataHubInboundOrderHelper implements DataHubInboundOrderHelper {
    private BusinessProcessService businessProcessService;
    private FlexibleSearchService flexibleSearchService;
    private ModelService modelService;

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @SuppressWarnings("javadoc")
    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    @SuppressWarnings("javadoc")
    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    @Override
    public void processOrderConfirmationFromHub(final String orderNumber) {

        String eventName = null;
        if (!orderNumber.isEmpty()) {
            eventName = YsapreturnprocessConstants.RETURNORDER_CONFIRMATION_EVENT + orderNumber;
        }
        getBusinessProcessService().triggerEvent(eventName);
    }

    @Override
    public void processOrderDeliveryNotififcationFromHub(final String orderNumber, final String delivInfo) {
        final String deliveryDocumentNo = determineDeliveryDocumentNo(delivInfo);
        String eventName = null;
        if (!deliveryDocumentNo.isEmpty()) {
            final ReturnRequestModel requestModel = readReturnOrder(orderNumber);
            requestModel.setDeliveryDocNumber(deliveryDocumentNo);
            getModelService().save(requestModel);
            eventName = YsapreturnprocessConstants.RETURNORDER_GOOD_EVENT + orderNumber;
        }
        getBusinessProcessService().triggerEvent(eventName);
    }

    @Override
    public void processOrderCreditMemoNotififcationFromHub(final String orderNumber, final String creditinfo) {
        // To be used for implementing credit memo notification logic.
    }

    public String determineDeliveryDocumentNo(final String deliveryInfo) {
        String result = null;
        final int delviLength = deliveryInfo.length();
        final int delivIndex = deliveryInfo.indexOf(YsapreturnprocessConstants.STR) + 1;
        if (delviLength != delivIndex) {
            result = deliveryInfo.substring(delivIndex, delviLength);
        }
        return result;
    }

    protected ReturnRequestModel readReturnOrder(final String returnOrderCode) {
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(
                "SELECT {rr:pk} FROM {ReturnRequest AS rr} WHERE  {rr.code} = ?code");
        flexibleSearchQuery.addQueryParameter("code", returnOrderCode);

        final ReturnRequestModel returnOrder = getFlexibleSearchService().searchUnique(flexibleSearchQuery);
        if (returnOrder == null) {
            final String msg = "Error while IDoc processing. Called with not existing order for order code : "
                    + returnOrderCode;
            throw new IllegalArgumentException(msg);
        }
        return returnOrder;
    }
}
