/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.hybris.outbound.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.PartnerCsvColumns;
import de.hybris.platform.sap.orderexchange.outbound.B2CCustomerHelper;
import de.hybris.platform.sap.orderexchange.outbound.RawItemContributor;
import de.hybris.platform.sap.orderexchange.constants.PartnerRoles;

public class DefaultReturnOrderPartnerContributor implements RawItemContributor<ReturnRequestModel> {

    private B2CCustomerHelper b2CCustomerHelper;

    private B2BUnitService<B2BUnitModel,UserModel> b2bUnitService;

    @Override
    public Set<String> getColumns() {
        return new HashSet<>(Arrays.asList(OrderCsvColumns.ORDER_ID, PartnerCsvColumns.PARTNER_ROLE_CODE,
                PartnerCsvColumns.PARTNER_CODE, PartnerCsvColumns.DOCUMENT_ADDRESS_ID, PartnerCsvColumns.FIRST_NAME,
                PartnerCsvColumns.LAST_NAME, PartnerCsvColumns.STREET, PartnerCsvColumns.CITY,
                PartnerCsvColumns.TEL_NUMBER, PartnerCsvColumns.HOUSE_NUMBER, PartnerCsvColumns.POSTAL_CODE,
                PartnerCsvColumns.REGION_ISO_CODE, PartnerCsvColumns.COUNTRY_ISO_CODE, PartnerCsvColumns.EMAIL,
                PartnerCsvColumns.LANGUAGE_ISO_CODE, PartnerCsvColumns.MIDDLE_NAME, PartnerCsvColumns.MIDDLE_NAME2,
                PartnerCsvColumns.DISTRICT, PartnerCsvColumns.BUILDING, PartnerCsvColumns.APPARTMENT,
                PartnerCsvColumns.POBOX, PartnerCsvColumns.FAX, PartnerCsvColumns.TITLE));
    }

    @Override
    public List<Map<String, Object>> createRows(ReturnRequestModel model) {
        List<Map<String, Object>> rows = createSoldtoPartyRows(model.getOrder());
        for (Map<String, Object> row : rows) {
            row.put(OrderCsvColumns.ORDER_ID, model.getCode());
        }
        return rows;
    }

    public List<Map<String, Object>> createSoldtoPartyRows(final OrderModel order) {
        return isB2BOrder(order) ? createB2BRows(order) : createRowsB2C(order);
    }

    protected List<Map<String, Object>> createB2BRows(final OrderModel order) {
        final Map<String, Object> row1 = createPartnerRow(order, PartnerRoles.SOLD_TO, soldToFromOrder(order));
        return Arrays.asList(row1);
    }

    protected Map<String, Object> createPartnerRow(final OrderModel order, final PartnerRoles partnerRole,
            final String partnerId) {
        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(PartnerCsvColumns.PARTNER_ROLE_CODE, partnerRole.getCode());
        row.put(PartnerCsvColumns.PARTNER_CODE, partnerId);
        row.put(PartnerCsvColumns.DOCUMENT_ADDRESS_ID, "");
        return row;
    }

    protected String soldToFromOrder(final OrderModel order) {
        final CompanyModel rootUnit = getB2bUnitService().getRootUnit(order.getUnit());
        return rootUnit.getUid();
    }

    protected boolean isB2BOrder(final OrderModel orderModel) {
        return orderModel.getSite().getChannel() == SiteChannel.B2B;
    }

    public List<Map<String, Object>> createRowsB2C(final OrderModel order) {
        final List<Map<String, Object>> result = new ArrayList<>(3);
        Map<String, Object> row = new HashMap<String, Object>();
        final String b2cCustomer = b2CCustomerHelper.determineB2CCustomer(order);
        final String sapcommonCustomer = b2cCustomer != null ? b2cCustomer
                : order.getStore().getSAPConfiguration().getSapcommon_referenceCustomer();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(PartnerCsvColumns.PARTNER_ROLE_CODE, PartnerRoles.SOLD_TO.getCode());
        row.put(PartnerCsvColumns.PARTNER_CODE, sapcommonCustomer);
        result.add(row);
        return result;
    }

    @SuppressWarnings("javadoc")
    public B2CCustomerHelper getB2CCustomerHelper() {
        return b2CCustomerHelper;
    }

    @SuppressWarnings("javadoc")
    public void setB2CCustomerHelper(final B2CCustomerHelper b2cCustomerHelper) {
        b2CCustomerHelper = b2cCustomerHelper;
    }

    @SuppressWarnings("javadoc")
    public B2BUnitService<B2BUnitModel,UserModel> getB2bUnitService() {
        return b2bUnitService;
    }

    @SuppressWarnings("javadoc")
    public void setB2bUnitService(final B2BUnitService<B2BUnitModel,UserModel> b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }
}
