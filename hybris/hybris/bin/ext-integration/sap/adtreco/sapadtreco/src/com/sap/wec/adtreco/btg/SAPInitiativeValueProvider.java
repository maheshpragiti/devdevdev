/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.btg;

import de.hybris.platform.btg.condition.operand.types.StringSet;
import de.hybris.platform.btg.condition.operand.valueproviders.CollectionOperandValueProvider;
import de.hybris.platform.btg.enums.BTGConditionEvaluationScope;
import de.hybris.platform.cockpit.model.editor.impl.DefaultSAPInitiativeUIEditor;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ConfigurationException;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.sap.wec.adtreco.bo.ADTUserIdProvider;
import com.sap.wec.adtreco.bo.impl.SAPInitiative;
import com.sap.wec.adtreco.bo.intf.SAPInitiativeReader;
import com.sap.wec.adtreco.model.BTGSAPInitiativeOperandModel;


public class SAPInitiativeValueProvider implements CollectionOperandValueProvider<BTGSAPInitiativeOperandModel>
{
	private static final Logger LOG = Logger.getLogger(DefaultSAPInitiativeUIEditor.class); // NOPMD
	private static final String INITIATIVES_PREFIX = "Initiatives_";
	protected SAPInitiativeReader sapInitiativeReader;
	protected ADTUserIdProvider userIdProvider;
	protected SessionService sessionService;

	public String getPiwikId(HttpServletRequest request)
	{
		final Cookie[] cookies = request.getCookies();

		if (cookies == null)
		{
			return "";
		}

		for (Cookie cookie : cookies)
		{
			if (cookie.getName().startsWith("_pk_id"))
			{
				return cookie.getValue().substring(0, 16);
			}
		}

		//return empty String if no cookies are matched
		return "";
	}

	@Override
	public Object getValue(final BTGSAPInitiativeOperandModel operand, final UserModel user,
			final BTGConditionEvaluationScope scope)
	{
		if (user == null)
		{
			return new StringSet(Collections.emptyList());
		}

		try
		{
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			String cookieId = this.getPiwikId(request);
			String userId = userIdProvider.getUserId(user);

			cookieId = cookieId == null ? "" : cookieId;
			userId = userId == null ? "" : userId;

			if (cookieId.isEmpty() && userId.isEmpty())
			{
				return new StringSet(Collections.emptyList());
			}

			List<String> result = sessionService.getAttribute(INITIATIVES_PREFIX + cookieId + userId);
			if (result != null)
			{
				return new StringSet(result);
			}

			if (!cookieId.isEmpty() && !userId.isEmpty())
			{
				String[] userIds = new String[]
				{ cookieId, userId };
				result = this.getValueForMultiUsers(userIds);
			}
			else
			{
				result = this.getValueForUser(cookieId + userId, !cookieId.isEmpty());
			}
			return new StringSet(result);
		}
		catch (ConfigurationException e)
		{
			LOG.error("SAP Hybris Segementation component configuration missing", e);
			return new StringSet(Collections.emptyList());
		}
	}

	protected List<String> getValueForUser(String userId, boolean isAnon)
	{
		try
		{
			final List<SAPInitiative> initiativesForBP = this.sapInitiativeReader.searchInitiativesForBP(userId, isAnon);
			final List<String> result = initiativesForBP.stream().map(SAPInitiative::getId).collect(Collectors.toList());
			sessionService.setAttribute(INITIATIVES_PREFIX + userId, result);
			return result;
		}
		catch (final Exception e)
		{
			LOG.error("Error reading value for user='" + userId + "' isAnon=" + isAnon, e);
			return Collections.emptyList();
		}
	}

	protected List<String> getValueForMultiUsers(String[] users)
	{
		try
		{
			final List<SAPInitiative> initiativesForMultiBP = this.sapInitiativeReader.searchInitiativesForMultiBP(users);
			final List<String> result = initiativesForMultiBP.stream().map(SAPInitiative::getId).collect(Collectors.toList());
			//If the logged on user doesn't belong to any campaigns, a "null" value is placed in cache
			//to avoid making the check against the backend again within the session
			sessionService.setAttribute(INITIATIVES_PREFIX + users[0] + users[1], result);
			return result;
		}
		catch (final Exception e)
		{
			LOG.error("Error reading value for MultiUsers='" + Arrays.toString(users), e);
			return Collections.emptyList();
		}
	}

	@Override
	public Class<SAPInitiativeSet> getValueType(final BTGSAPInitiativeOperandModel operand)
	{
		return SAPInitiativeSet.class;
	}

	@Override
	public Class<String> getAtomicValueType(final BTGSAPInitiativeOperandModel operand)
	{
		return String.class;
	}

	@Required
	public void setSapInitiativeReader(final SAPInitiativeReader sapInitiativeReader)
	{
		this.sapInitiativeReader = sapInitiativeReader;
	}

	@Required
	public void setUserIdProvider(final ADTUserIdProvider userIdProvider)
	{
		this.userIdProvider = userIdProvider;
	}

	@Required
	public void setsessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
