/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.bo.impl;

import de.hybris.platform.sap.core.bol.backend.BackendType;
import de.hybris.platform.sap.core.bol.businessobject.BusinessObjectBase;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.olingo.odata2.api.exception.ODataException;

import com.sap.wec.adtreco.be.HMCConfigurationReader;
import com.sap.wec.adtreco.be.intf.ADTInitiativesBE;
import com.sap.wec.adtreco.bo.intf.SAPInitiativeReader;


/**
 *
 */
@BackendType("CEI")
public class SAPInitiativeReaderImpl extends BusinessObjectBase implements SAPInitiativeReader
{
	protected static final String DT_SELECT = "Name,Description,InitiativeId,InitiativeIdExt,LifecycleStatus,TargetGroup/CustomerMemberCount";
	protected static final String RT_SELECT = "Name,Description,InitiativeId,InitiativeIdExt";
	protected static final String INITIATIVES = "Initiatives";
	protected static final String TARGETGROUPS = "TargetGroup";
	protected static final String IN_PREPARATION = "1";
	protected static final String RELEASED = "2";
	protected static final String ACTIVE = "1";
	protected static final String PLANNED = "2";
	protected static final String EQ_QUOT = " eq '";
	protected static final String QUOT = "'";
	protected static final String AND = " and ";
	protected static final String OR = " or ";

	protected ADTInitiativesBE accessBE;
	protected HMCConfigurationReader configuration;

	@Override
	public List<SAPInitiative> getAllInitiatives() throws ODataException, URISyntaxException, IOException
	{
		final String filterStatus = "LifecycleStatus/StatusCode" + EQ_QUOT + "2" + QUOT;
		final ODataFeed feed = accessBE.getInitiatives(DT_SELECT, filterStatus, INITIATIVES, "TargetGroups", null);
		return extractInitiatives(feed.getEntries());
	}

	@Override
	public List<SAPInitiative> searchInitiatives(final String search) throws ODataException, URISyntaxException, IOException
	{
		final String filterDescription = "Search/SearchTerm" + EQ_QUOT + search + QUOT;

		final String filterStatus = "(" + getSearchTileFilterCategoryTerm(ACTIVE) + OR
				+ getSearchTileFilterCategoryTerm(PLANNED) + ")";

		final String filterTerms = filterDescription + AND + getFilterCategory() + AND + filterStatus;
		
		return getInitiativesFromFeed(accessBE.getInitiatives(DT_SELECT, filterTerms, INITIATIVES, TARGETGROUPS, null));
	}

	@Override
	public List<SAPInitiative> searchInitiativesForBP(final String businessPartner, final boolean isAnonymous) throws ODataException, URISyntaxException, IOException
	{
		return getRTInitiativesForFilter(getFilterTerms(getBusinessPartnerFilter(businessPartner, isAnonymous)));
	}

	@Override
	public List<SAPInitiative> searchInitiativesForMultiBP(final String[] businessPartners) throws ODataException, URISyntaxException, IOException
	{
		return getRTInitiativesForFilter(getFilterTerms(getMultiBusinessPartnerFilter(businessPartners)));
	}
	
	protected String getMultiBusinessPartnerFilter(final String[] businessPartners)
	{
		String filter = "(Filter/InteractionContactId" + EQ_QUOT + businessPartners[0] + QUOT;
		for(int i = 1; i < businessPartners.length; i++)
		{
			filter += " or Filter/InteractionContactId" + EQ_QUOT + businessPartners[i] + QUOT;
		}
		
		filter += ") and (Filter/InteractionContactIdOrigin" + EQ_QUOT + configuration.getIdOrigin() + QUOT + 
				" or Filter/InteractionContactIdOrigin" + EQ_QUOT + configuration.getAnonIdOrigin() + QUOT + ")";
		
		return filter;
	}

	protected String getBusinessPartnerFilter(final String businessPartner, final boolean isAnonymous)
	{
		if (StringUtils.isNotEmpty(businessPartner))
		{
			final String idOrigin = isAnonymous ? configuration.getAnonIdOrigin() : configuration.getIdOrigin();
			return "Filter/InteractionContactIdOrigin" + EQ_QUOT + idOrigin + QUOT
					+ " and Filter/InteractionContactId" + EQ_QUOT + businessPartner + QUOT;
		}
		return "";
	}

	protected List<SAPInitiative> getRTInitiativesForFilter(final String filter) throws IOException, ODataException, URISyntaxException
	{
		return this.getInitiativesFromFeed(this.accessBE.getInitiatives(RT_SELECT, filter, INITIATIVES, null, null));
	}

	protected List<SAPInitiative> getInitiativesFromFeed(final ODataFeed feed) {
		if (feed == null)
		{
			return Collections.emptyList();
		}
		return extractInitiatives(feed.getEntries());
	}

	protected String getFilterTerms(final String filterBP)
	{
		return this.getFilterCategory() + AND + this.getSearchTileFilterCategoryTerm(ACTIVE) + AND + filterBP;
	}

	protected String getFilterCategory()
	{
		return "Category/CategoryCode" + EQ_QUOT + configuration.getFilterCategory() + QUOT;
	}

	protected String getSearchTileFilterCategoryTerm(final String term) {
		return "Search/TileFilterCategory" + EQ_QUOT + term + QUOT;
	}

	protected String convertToInternalKey(final String id)
	{
		final Integer in = Integer.valueOf(id);
		return String.format("%010d", in);
	}

	@Override
	public SAPInitiative getSelectedInitiative(final String id) throws ODataException, IOException, URISyntaxException
	{
		final String keyValue = "'" + convertToInternalKey(id) + "'";
		final ODataEntry entry = accessBE.getInitiative(DT_SELECT, keyValue, INITIATIVES);
		if (entry != null)
		{
			return extractInitiative(entry);
		}
		return null;
	}

	@Override
	public SAPInitiative getInitiative(final String id) throws ODataException, IOException, URISyntaxException
	{
		final String keyValue = "'" + convertToInternalKey(id) + "'";
		final ODataEntry entry = accessBE.getInitiative(RT_SELECT, keyValue, INITIATIVES);
		if (entry != null)
		{
			return extractInitiative(entry);
		}
		return null;
	}

	protected List<SAPInitiative> extractInitiatives(final List<ODataEntry> foundEntities)
	{
		final List<SAPInitiative> initiatives = new ArrayList<SAPInitiative>();
		if (foundEntities != null)
		{
			for(ODataEntry entity : foundEntities)
			{
				initiatives.add(extractInitiative(entity));
			}
		}
		return initiatives;
	}

	protected SAPInitiative extractInitiative(final ODataEntry entity)
	{
		final SAPInitiative initiative = new SAPInitiative();
		final Map<String, Object> props = entity.getProperties();
		if (props != null)
		{
			initiative.setName(props.get("Name").toString());
			initiative.setDescription(props.get("Description").toString());
			initiative.setId(props.get("InitiativeIdExt").toString());
			final Map<?, ?> status = (Map<?, ?>) props.get("LifecycleStatus");
			if (status != null)
			{
				initiative.setStatus((String) status.get("StatusDescription"));
			}

			final ODataEntry tg = (ODataEntry) props.get("TargetGroup");
			if (tg != null)
			{
				final Map<String, Object> tgProps = tg.getProperties();
				initiative.setMemberCount(tgProps.get("CustomerMemberCount").toString());
			}
		}
		return initiative;
	}

	@Override
	public void setAccessBE(final ADTInitiativesBE accessBE)
	{
		this.accessBE = accessBE;
	}

	@Override
	public void setConfiguration(HMCConfigurationReader configuration)
	{
		this.configuration = configuration;
	}
	
}
