/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.be.intf;

import de.hybris.platform.sap.core.bol.backend.BackendBusinessObject;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.olingo.odata2.api.exception.ODataException;


/**
 *
 */
public interface ADTInitiativesBE extends BackendBusinessObject
{
	/**
	 * Get a single campaign from the hybris Marketing server
	 *
	 * @param select
	 * @param keyValue
	 * @param entitySetName
	 * @return ODataEntry
	 * @throws ODataException
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	ODataEntry getInitiative(String select, String keyValue, String entitySetName)
			throws ODataException, URISyntaxException, IOException;

	/**
	 * Get a list of campaigns from the hybris Marketing server
	 * 
	 * @param select
	 * @param filter
	 * @param entitySetName
	 * @param expand
	 * @return ODataFeed
	 * @throws ODataException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	ODataFeed getInitiatives(String select, String filter, String entitySetName, String expand, String orderBy)
			throws ODataException, IOException, URISyntaxException;
}
