/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.btg.condition.impl;

import de.hybris.platform.btg.condition.impl.PlainCollectionExpressionEvaluator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sap.wec.adtreco.btg.SAPInitiativeSet;


public class SAPInitiativesExpressionEvaluator extends PlainCollectionExpressionEvaluator
{
	public SAPInitiativesExpressionEvaluator()
	{
		super(createOperatorMap());
	}

	private static Map<String, Set<Class>> createOperatorMap()
	{
		Map<String, Set<Class>> operatorMap = new HashMap<>();
		operatorMap.put(CONTAINS_ALL, Collections.singleton(SAPInitiativeSet.class));
		operatorMap.put(CONTAINS_ANY, Collections.singleton(SAPInitiativeSet.class));
		operatorMap.put(NOT_CONTAINS, Collections.singleton(SAPInitiativeSet.class));
		return operatorMap;
	}

	@Override
	public Class<SAPInitiativeSet> getLeftType()
	{
		return SAPInitiativeSet.class;
	}
}
