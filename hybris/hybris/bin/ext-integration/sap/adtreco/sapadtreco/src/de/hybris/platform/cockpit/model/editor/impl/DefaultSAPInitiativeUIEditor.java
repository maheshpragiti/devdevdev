/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cockpit.model.editor.impl;

import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.core.Registry;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;

import com.sap.wec.adtreco.bo.impl.SAPInitiative;
import com.sap.wec.adtreco.bo.intf.SAPInitiativeReader;


/**
 * Simple text editor.
 */
public class DefaultSAPInitiativeUIEditor extends AbstractTextBasedUIEditor
{
	private static final Logger LOG = Logger.getLogger(DefaultSAPInitiativeUIEditor.class); // NOPMD
	private static final String ENUM_EDITOR_SCLASS = "enumEditor";

	private List<? extends Object> availableValues = Collections.emptyList();
	private List<? extends Object> originalAvailableValues = Collections.emptyList();
	private final Combobox editorView = new Combobox();
	private String searchString = "";
	private SAPInitiativeReader initiativeReader;


	//Add a single search result to the drop down list box.
	protected Comboitem addInitiativeToCombo(final SAPInitiative initiative, final Combobox box)
	{
		final String label = initiative.getId() + " " + initiative.getName() + " (" + initiative.getMemberCount() + ")";
		final String value = initiative.getId();
		final Comboitem comboitem = new Comboitem();

		comboitem.setLabel(label);
		comboitem.setValue(value);
		comboitem.setTooltiptext(label);
		box.appendChild(comboitem);
		return comboitem;
	}

	protected void setEnumValue(final Combobox combo, final Object value)
	{
		final int index = this.availableValues.indexOf(value);
		if (index >= 0)
		{
			combo.setSelectedIndex(index);
		}
	}

	@Override
	public HtmlBasedComponent createViewComponent(final Object initialValue, final Map<String, ? extends Object> parameters,
			final EditorListener listener)
	{
		parseInitialInputString(parameters);
		SAPInitiative initiative = null;

		editorView.setConstraint("strict");
		editorView.setSclass("initiative-combo");
		editorView.setAutodrop(true);

		final String intialValueString = (String) initialValue;
		if (intialValueString != null && !intialValueString.isEmpty())
		{
			try
			{
				initiative = getInitiativeReader().getSelectedInitiative(intialValueString);
			}
			catch (final Exception e)
			{
				LOG.error("Error reading SelectedInitiative '" + intialValueString + "'", e);
			}
		}

		if (isEditable())
		{
			if (initiative != null)
			{
				final Comboitem item = addInitiativeToCombo(initiative, editorView);
				editorView.setSelectedItem(item);
			}

			final CancelButtonContainer ret = new CancelButtonContainer(listener, new CancelListener()
			{
				@Override
				public void cancelPressed()
				{
					setEnumValue(editorView, initialEditValue);
					setValue(initialEditValue);
					fireValueChanged(listener);
					listener.actionPerformed(EditorListener.ESCAPE_PRESSED);
				}
			});

			ret.setSclass(ENUM_EDITOR_SCLASS);
			ret.setContent(editorView);

			editorView.addEventListener(Events.ON_FOCUS, event -> {
				if (editorView.getSelectedItem() != null)
				{
					initialEditValue = editorView.getSelectedItem().getValue();
				}
				ret.showButton(true);
			});

			editorView.addEventListener(Events.ON_CHANGE, event -> validateAndFireEvent(listener));
			editorView.addEventListener(Events.ON_BLUR, event -> ret.showButton(false));

			editorView.addEventListener(Events.ON_OK, event -> {
				validateAndFireEvent(listener);
				listener.actionPerformed(EditorListener.ENTER_PRESSED);
			});

			editorView.addEventListener(Events.ON_CANCEL, event -> {
				ret.showButton(false);
				DefaultSAPInitiativeUIEditor.this.setEnumValue(editorView, initialEditValue);
				setValue(initialEditValue);
				fireValueChanged(listener);
				listener.actionPerformed(EditorListener.ESCAPE_PRESSED);
			});

			editorView.addEventListener(Events.ON_CHANGING, event -> {
				ret.showButton(true);
				handleChangingEvents(listener, event);
			});


			if (UISessionUtils.getCurrentSession().isUsingTestIDs())
			{
				String id = "Enum_";
				String attributeQualifier = (String) parameters.get(AbstractUIEditor.ATTRIBUTE_QUALIFIER_PARAM);
				if (attributeQualifier != null)
				{
					attributeQualifier = attributeQualifier.replaceAll("\\W", "");
					id = id + attributeQualifier;
				}
				UITools.applyTestID(editorView, id);
			}

			return ret;
		}
		else
		{
			editorView.setDisabled(true);

			final Label ret;
			if (initiative != null)
			{
				ret = new Label(initiative.getId() + " " + initiative.getName());
			}
			else
			{
				ret = new Label(" ");
			}
			return ret;
		}
	}

	@Override
	public boolean isInline()
	{
		return true;
	}


	@Override
	public String getEditorType()
	{
		return PropertyDescriptor.TEXT;
	}

	/**
	 * @param availableValues
	 */
	public void setAvailableValues(final List<? extends Object> availableValues)
	{
		if (availableValues == null || availableValues.isEmpty())
		{
			editorView.setValue(Labels.getLabel("general.nothingtodisplay"));
			editorView.setDisabled(true);
			this.availableValues = Collections.emptyList();
			this.originalAvailableValues = Collections.emptyList();
		}
		else
		{
			this.availableValues = new ArrayList<Object>(availableValues);
			if (isOptional())
			{
				this.availableValues.add(0, null);
			}
			this.originalAvailableValues = new ArrayList<Object>(availableValues);
		}
	}

	@Override
	public void setFocus(final HtmlBasedComponent rootEditorComponent, final boolean selectAll)
	{
		final Combobox element = (Combobox) ((CancelButtonContainer) rootEditorComponent).getContent();
		element.setFocus(true);

		if (initialInputString != null)
		{
			element.setText(initialInputString);
		}
	}

	@Override
	public void setOptional(final boolean optional)
	{
		if (!optional)
		{
			availableValues = originalAvailableValues;
		}
		super.setOptional(optional);
	}

	protected void validateAndFireEvent(final EditorListener listener)
	{
		if (editorView.getSelectedItem() == null)
		{
			setEnumValue(editorView, initialEditValue);
		}
		else
		{
			DefaultSAPInitiativeUIEditor.this.setValue(editorView.getSelectedItem().getValue());
			editorView.setTooltiptext(ObjectUtils.toString(editorView.getSelectedItem().getValue()));
			listener.valueChanged(getValue());
		}
	}

	protected void handleChangingEvents(final EditorListener listener, final Event event)
	{
		final String newSearchString = ((InputEvent) event).getValue();
		final String lastSearch = newSearchString;
		if (newSearchString.length() >= 3 && !this.searchString.equals(newSearchString))
		{
			final List<SAPInitiative> initiatives = searchValues(newSearchString);
			synchronized (this)
			{
				this.searchString = lastSearch;
				clearComboBox();
				fillComboBox(initiatives);
				listener.valueChanged(getValue());
			}
		}
	}

	protected List<SAPInitiative> searchValues(final String newSearchString)
	{
		try
		{
			return this.getInitiativeReader().searchInitiatives(newSearchString);
		}
		catch (final Exception e)
		{
			LOG.debug("Error searching with '" + newSearchString + "'", e);
			showErrorPopup();
			return Collections.emptyList();
		}
	}

	private void showErrorPopup()
	{
		try
		{
			final String exceptionText = Localization.getLocalizedString("adt.connectionError.description");
			final String exceptionTitle = Localization.getLocalizedString("adt.connectionError.title");
			Messagebox.show(exceptionText, exceptionTitle, Messagebox.OK, Messagebox.ERROR);
		}
		catch (final InterruptedException e)
		{
			LOG.error("Messagebox Exception", e);
		}
	}

	/**
	 * Fill the search results drop down list box with the list of initiatives retrieved from the SAP backend
	 */
	protected void fillComboBox(final List<SAPInitiative> initiatives)
	{
		for (final SAPInitiative initiative : initiatives)
		{
			addInitiativeToCombo(initiative, editorView);
		}
	}

	protected SAPInitiativeReader getInitiativeReader()
	{
		if (this.initiativeReader != null)
		{
			return this.initiativeReader;
		}
		this.initiativeReader = (SAPInitiativeReader) Registry.getApplicationContext().getBean("sapInitiativeReader");
		return this.initiativeReader;
	}

	protected void clearComboBox()
	{
		final int size = this.editorView.getChildren().size();
		for (int i = 0; i < size; i++)
		{
			this.editorView.removeItemAt(0);
		}
	}
}
