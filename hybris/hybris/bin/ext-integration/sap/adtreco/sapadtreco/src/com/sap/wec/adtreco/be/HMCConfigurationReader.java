/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.be;

import java.net.MalformedURLException;
import java.net.URL;

import de.hybris.platform.sap.core.configuration.SAPConfigurationService;
import de.hybris.platform.sap.core.configuration.global.SAPGlobalConfigurationService;
import de.hybris.platform.sap.core.configuration.http.HTTPDestination;
import de.hybris.platform.sap.core.configuration.http.impl.HTTPDestinationServiceImpl;
import de.hybris.platform.sap.core.module.ModuleConfigurationAccess;
import de.hybris.platform.servicelayer.exceptions.ConfigurationException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 */
public class HMCConfigurationReader
{
	private static final Logger LOG = Logger.getLogger(HMCConfigurationReader.class.getName());
	
	@Resource(name = "sapCoreDefaultSAPGlobalConfigurationService")
	private SAPGlobalConfigurationService globalConfigurationService;
	
	@Resource(name = "sapADTModuleConfigurationAccess")
	private ModuleConfigurationAccess baseStoreConfigurationService;
	
	@Resource(name = "sapCoreHTTPDestinationService")
	private HTTPDestinationServiceImpl httpDestinationService;

	private String anonIdOrigin = "";

	private String getGlobalConfigurationProperty(final String propertyName){
		String value = "";
		if (globalConfigurationService.sapGlobalConfigurationExists())
		{
			value = globalConfigurationService.getProperty(propertyName);
		}
		
		if (StringUtils.isNotEmpty(value))
		{
			return value;
		} 
		else
		{
			throw new ConfigurationException(propertyName);
		}
	}

	public String getBaseStoreConfigurationProperty(final String propertyName){
		if (baseStoreConfigurationService.isSAPConfigurationActive())
		{
			String value = String.valueOf(baseStoreConfigurationService.getProperty(propertyName));
			if (StringUtils.isNotEmpty(value))
			{
				return value;
			} 
		}

		throw new ConfigurationException(propertyName);
	}
	
	/**
	 * Get backoffice global configuration service
	 * @return SAPGlobalConfigurationService globalConfigurationService
	 */
	public SAPGlobalConfigurationService getGlobalConfigurationService()
	{
		return globalConfigurationService;
	}

	public void setGlobalConfigurationService(final SAPGlobalConfigurationService globalConfigurationService)
	{
		this.globalConfigurationService = globalConfigurationService;
	}
	
	/**
	 * Get backoffice base store configuration service
	 * @return
	 */
	public SAPConfigurationService getBaseStoreConfigurationService()
	{
		return baseStoreConfigurationService;
	}

	public void setBaseStoreConfigurationService(final ModuleConfigurationAccess baseStoreConfigurationService)
	{
		this.baseStoreConfigurationService = baseStoreConfigurationService;
	}
	
	/**
	 * Get Contact ID Origin backoffice configuration
	 * @return String idOrigin
	 */
	public String getIdOrigin()
	{
		return this.getBaseStoreConfigurationProperty("sapadtreco_idOrigin");
	}
	
	public String getAnonIdOrigin() {
		return anonIdOrigin;
	}

	public void setAnonIdOrigin(String anonIdOrigin) {
		this.anonIdOrigin = anonIdOrigin;
	}

	/**
	 * Get Campaign Filter Category from backoffice configuration
	 * @return String filterCategory
	 */
	public String getFilterCategory()
	{
		//filterCategory can be blank, therefore exception handling is not needed.
		String filterCategory = globalConfigurationService.getProperty("sapadtreco_filterCategory");
		if (filterCategory == null)
		{
			filterCategory = "";
		}
		return filterCategory;
	}
	
	/**
	 * Get HTTP Destination object containing the backoffice HTTP Destination configuration
	 * @return HTTPDestination httpDestination
	 */
	public HTTPDestination getHttpDestination()
	{
		//HTTPDestination is required, a ConfigurationException will be thrown if not maintained in the Backoffice
		return httpDestinationService.getHTTPDestination(this.getGlobalConfigurationProperty("sapadtreco_httpdest"));
	}
	
	public String getHttpDestinationURL()
	{
		try
		{
			URL url = new URL(this.getHttpDestination().getTargetURL());
			return url.getProtocol() + "://" + url.getAuthority();
		}
		catch (MalformedURLException e)
		{
			LOG.error("Configured HTTP Destination contains a malformed target URL", e);
		}
		return "";
	}
	
	public String getHttpDestinationSAPClient()
	{
		try
		{
			URL url = new URL(getHttpDestination().getTargetURL());
			String query = url.getQuery();
			if (StringUtils.isNotEmpty(query))
			{
				return query.substring(query.lastIndexOf('=') + 1);
			}
		}
		catch (MalformedURLException e)
		{
			LOG.warn("Configured URL is malformed, does not contain a client number", e);
		}
		return "";
	}
}