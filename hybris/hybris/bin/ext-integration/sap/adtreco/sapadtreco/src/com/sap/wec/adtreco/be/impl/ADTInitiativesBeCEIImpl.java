/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.be.impl;

import de.hybris.platform.sap.core.bol.backend.BackendBusinessObjectBase;
import de.hybris.platform.sap.core.bol.backend.BackendType;
import de.hybris.platform.sap.core.odata.util.ODataClientService;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.olingo.odata2.api.exception.ODataException;

import com.sap.wec.adtreco.be.HMCConfigurationReader;
import com.sap.wec.adtreco.be.intf.ADTInitiativesBE;


@BackendType("CEI")
public class ADTInitiativesBeCEIImpl extends BackendBusinessObjectBase implements ADTInitiativesBE
{
	protected static final String APPLICATION_XML = "application/xml";

	protected ODataClientService clientService;
	protected HMCConfigurationReader configuration;
	protected String serviceURL;

	@Override
	public ODataEntry getInitiative(final String select, final String keyValue, final String entitySetName)
			throws ODataException, IOException, URISyntaxException
	{
		final String client = configuration.getHttpDestinationSAPClient();
		final String user = configuration.getHttpDestination().getUserid();
		final String password = configuration.getHttpDestination().getPassword();

		return this.clientService.readEntry(configuration.getHttpDestinationURL() + this.serviceURL, //
				APPLICATION_XML, entitySetName, select, null, "TargetGroup", keyValue, user, password, client);
	}

	@Override
	public ODataFeed getInitiatives(final String select, final String filter, final String entitySetName, final String expand,
			String orderBy) throws ODataException, URISyntaxException, IOException
	{
		final String client = configuration.getHttpDestinationSAPClient();
		final String user = configuration.getHttpDestination().getUserid();
		final String password = configuration.getHttpDestination().getPassword();

		return this.clientService.readFeed(configuration.getHttpDestinationURL() + this.serviceURL, //
				APPLICATION_XML, entitySetName, expand, select, filter, orderBy, user, password, client);
	}

	public void setClientService(final ODataClientService clientService)
	{
		this.clientService = clientService;
	}

	public void setConfiguration(final HMCConfigurationReader configuration)
	{
		this.configuration = configuration;
	}

	public void setServiceURL(String serviceURL)
	{
		this.serviceURL = serviceURL;
	}

}
