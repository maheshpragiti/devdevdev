/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cockpit.model.editor.impl;

import de.hybris.platform.cockpit.constants.ImageUrls;
import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.ListUIEditor;
import de.hybris.platform.cockpit.model.editor.UIEditor;
import de.hybris.platform.cockpit.model.meta.PropertyEditorDescriptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;


/**
 * Removed when https://jira.hybris.com/browse/PLA-13857 will be fixed !!!
 */
public class InitiativeCollectionUIEditorFixed extends GenericCollectionUIEditor
{
	protected final class InitiativeListitemRenderer implements ListitemRenderer
	{
		private final Map<String, ? extends Object> parameters;
		private final EditorListener listener;

		private InitiativeListitemRenderer(Map<String, ? extends Object> parameters, EditorListener listener)
		{
			this.parameters = parameters;
			this.listener = listener;
		}

		private Listcell createListCell(final Component editorView, final Image image)
		{
			final Listcell cellItem = new Listcell();

			final Div cellContainerDiv = new Div();
			cellContainerDiv.setSclass("collectionUIEditorItem");

			final Div firstCellDiv = new Div();
			firstCellDiv.appendChild(editorView);
			cellContainerDiv.appendChild(firstCellDiv);

			if (image != null)
			{
				firstCellDiv.setSclass("editor");
				final Div secondCellDiv = new Div();
				secondCellDiv.appendChild(image);
				secondCellDiv.setSclass("image");
				cellContainerDiv.appendChild(secondCellDiv);
			}
			cellItem.appendChild(cellContainerDiv);
			return cellItem;
		}

		@Override
		public void render(final Listitem itemRow, final Object value) throws Exception //NOPMD: ZK specific
		{
			if (value instanceof Collection && ((Collection<?>) value).isEmpty())
			{
				return;
			}

			// add new value editor
			final UIEditor editor = createSingleValueEditor(parameters);
			editor.setEditable(isEditable());
			if (editor instanceof ListUIEditor && !getAvailableValues().isEmpty())
			{
				((ListUIEditor) editor).setAvailableValues(getAvailableValues());
			}

			if (value == null)
			{
				Image addImage = null;
				if (isEditable())
				{
					addImage = new Image(ImageUrls.GREEN_ADD_PLUS_IMAGE);
					addImage.setTooltiptext(Labels.getLabel("collectionEditor.button.add.tooltip"));
					addImage.addEventListener(Events.ON_CLICK, event -> {
					});
				}

				final Component editorView = editor.createViewComponent(value, parameters, new EditorListener()
				{
					@Override
					public void actionPerformed(final String actionCode)
					{
						if (EditorListener.ENTER_PRESSED.equals(actionCode) && editor.getValue() != null)
						{
							addCollectionElement(editor, listener);
						}
					}

					@Override
					public void valueChanged(final Object value)
					{
						if (editor.getValue() != null)
						{
							addCollectionElement(editor, listener);
						}
					}
				});

				itemRow.appendChild(createListCell(editorView, addImage));
			}
			else
			{
				final Component editorView = editor.createViewComponent(value, parameters, new EditorListener()
				{
					@Override
					public void actionPerformed(final String actionCode)
					{
						// No action.
					}

					@Override
					public void valueChanged(final Object value)
					{
						if (itemRow.getIndex() >= 0 && collectionValues.size() > itemRow.getIndex())
						{
							if (!collectionValues.get(itemRow.getIndex()).equals(value))
							{
								collectionValues = createNewCollectionValuesList(collectionValues);
							}
							if (value == null)
							{
								collectionValues.remove(itemRow.getIndex());
								collectionValues = createNewCollectionValuesList(collectionValues);
								setValue(collectionValues);
								updateCollectionItems();
							}
							else
							{
								collectionValues.set(itemRow.getIndex(), value);
								setValue(collectionValues);
							}
						}
						listener.valueChanged(getValue());
					}
				});

				Image removeImage = null;
				if (isEditable())
				{
					removeImage = new Image(ImageUrls.REMOVE_BUTTON_IMAGE);
					removeImage.setTooltiptext(Labels.getLabel("collectionEditor.button.remove.tooltip"));
					removeImage.addEventListener(Events.ON_CLICK, event -> {
						collectionValues.remove(itemRow.getIndex());
						collectionValues = createNewCollectionValuesList(collectionValues);
						setValue(collectionValues);
						updateCollectionItems();
						listener.valueChanged(getValue());
					});
				}
				itemRow.appendChild(createListCell(editorView, removeImage));
			}
		}
	}

	protected static final String SINGLE_VALUE_EDITOR_CODE = "singleValueEditorCode";

	private List<? extends Object> availableValues;
	private transient Listbox collectionItems;
	private transient List<Object> collectionValues = new ArrayList<>();
	private PropertyEditorDescriptor singleValueEditorDescriptor;

	private void addCollectionElement(final UIEditor editor, final EditorListener listener)
	{
		collectionValues.add(editor.getValue());
		collectionValues = createNewCollectionValuesList(collectionValues);
		setValue(collectionValues);
		updateCollectionItems();
		listener.valueChanged(getValue());
	}

	@Override
	protected ListitemRenderer createCollectionItemListRenderer(final Map<String, ? extends Object> parameters,
			final EditorListener listener)
	{
		return new InitiativeListitemRenderer(parameters, listener);
	}

	@Override
	protected List<Object> createNewCollectionValuesList(final Object values)
	{
		return new ArrayList<Object>(values instanceof Collection ? (Collection<?>) values : Collections.emptyList());
	}

	@Override
	protected UIEditor createSingleValueEditor(final Map<String, ? extends Object> parameters)
	{

		String editorCode = PropertyEditorDescriptor.SINGLE;
		if (parameters.containsKey(SINGLE_VALUE_EDITOR_CODE))
		{
			editorCode = ObjectUtils.toString(parameters.get(SINGLE_VALUE_EDITOR_CODE));
		}
		
		return getSingleValueEditorDescriptor().createUIEditor(editorCode);
	}

	@Override
	public HtmlBasedComponent createViewComponent(final Object initialValue, final Map<String, ? extends Object> parameters,
			final EditorListener listener)
	{
		collectionValues = createNewCollectionValuesList(initialValue);

		final Div listContainer = new Div();
		collectionItems = new Listbox();
		collectionItems.setSclass("collectionUIEditorItems");
		collectionItems.setOddRowSclass("oddRowRowSclass");
		collectionItems.setDisabled(!isEditable());
		collectionItems.setModel(getCollectionSimpleListModel(collectionValues));
		collectionItems.setFixedLayout(false);
		collectionItems.setItemRenderer(createCollectionItemListRenderer(parameters, listener));
		listContainer.appendChild(collectionItems);

		return listContainer;
	}

	@Override
	public List<? extends Object> getAvailableValues()
	{
		return this.availableValues == null ? Collections.emptyList() : this.availableValues;
	}

	private SimpleListModel getCollectionSimpleListModel(final List<Object> collectionValues)
	{
		final List<Object> newCollectionValues = new ArrayList<Object>(collectionValues);

		// add empty element to the list for the 'add element' editor
		if ((!newCollectionValues.isEmpty() && newCollectionValues.get(newCollectionValues.size() - 1) != null)
				|| newCollectionValues.isEmpty())
		{
			newCollectionValues.add(null);
		}

		return new SimpleListModel(newCollectionValues);
	}

	@Override
	public String getEditorType()
	{
		final PropertyEditorDescriptor desc = getSingleValueEditorDescriptor();
		return desc == null ? null : desc.getEditorType();
	}

	@Override
	public PropertyEditorDescriptor getSingleValueEditorDescriptor()
	{
		return this.singleValueEditorDescriptor;
	}

	@Override
	public boolean isInline()
	{
		return false;
	}

	@Override
	public void setAvailableValues(final List<? extends Object> availableValues)
	{
		this.availableValues = availableValues;
	}


	@Override
	public void setSingleValueEditorDescriptor(final PropertyEditorDescriptor singleValueEditorDescriptor)
	{
		this.singleValueEditorDescriptor = singleValueEditorDescriptor;
	}

	@Override
	public void updateCollectionItems()
	{
		collectionValues = new ArrayList<Object>(collectionValues);
		collectionItems.setModel(getCollectionSimpleListModel(collectionValues));
	}
}
