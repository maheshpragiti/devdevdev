/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.facades.populator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.reset;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.productconfig.runtime.interf.KBKey;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.ConfigModelImpl;
import de.hybris.platform.sap.productconfig.services.SessionAccessService;
import de.hybris.platform.sap.productconfig.services.intf.ProductConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;


@UnitTest
public class CartConfigurationPopulatorTest extends AbstractOrderConfigurationPopulatorTest
{
	private CartConfigurationPopulator classUnderTest;
	private ConfigModel configModel;
	private ProductModel productModel;

	@Mock
	private SessionAccessService sessionAccessService;
	@Mock
	private ProductConfigurationService productConfigurationService;


	@Override
	@Before
	public void setup()
	{
		super.setup();
		source = new CartModel();
		source.setEntries(entryList);

		target = new CartData();
		target.setEntries(targetEntryList);

		configModel = new ConfigModelImpl();

		productModel = new ProductModel();
		productModel.setCode("Product");


		classUnderTest = new CartConfigurationPopulator();
		super.classUnderTest = classUnderTest;
		classUnderTest.setSessionAccessService(sessionAccessService);
		classUnderTest.setProductConfigurationService(productConfigurationService);
	}

	@Test(expected = RuntimeException.class)
	public void testPopulateNoTargetItems()
	{

		Mockito.when(sourceEntry.getProduct()).thenReturn(productModel);
		productModel.setSapConfigurable(Boolean.TRUE);
		//entry numbers do not match->exception
		classUnderTest.populate((CartModel) source, (CartData) target);
	}

	@Test
	public void testPopulate()
	{
		final String configId = "123";
		final PK value = initializeSourceItem("Ext", configId);


		targetEntryList.add(targetEntry);

		classUnderTest.populate((CartModel) source, (CartData) target);
		assertTrue("ItemPK not set", targetEntry.getItemPK().equals(value.toString()));
		assertTrue("Configuration not marked as attached", targetEntry.isConfigurationAttached());
		assertTrue("Configuration should be consistent", targetEntry.isConfigurationConsistent());
	}


	@Test
	public void testPopulate_notConfigurable()
	{
		initializeSourceItem("Ext", null);

		targetEntryList.add(targetEntry);

		classUnderTest.populate((CartModel) source, (CartData) target);
		assertFalse("Configuration marked as attached", targetEntry.isConfigurationAttached());
		assertFalse("Configuration should be inconsistent", targetEntry.isConfigurationConsistent());
		assertEquals("ErrorCount should be zero for non configurable products", 0, targetEntry.getConfigurationErrorCount());
	}



	@Test
	public void testPopulate_numberErrors_conflicts()
	{
		final Map<ProductInfoStatus, Integer> cpqSummary = new HashMap<>();
		cpqSummary.put(ProductInfoStatus.ERROR, Integer.valueOf(numberOfErrors));
		Mockito.when(sourceEntry.getCpqStatusSummaryMap()).thenReturn(cpqSummary);
		final String configId = "123";
		initializeSourceItem("Ext", configId);

		targetEntryList.add(targetEntry);

		classUnderTest.populate((CartModel) source, (CartData) target);
		assertEquals(numberOfErrors, targetEntry.getConfigurationErrorCount());
	}

	@Test
	public void testPopulateConfigCompleteAndConsistent()
	{
		final String configId = "123";

		initializeSourceItem("Ext", configId);

		targetEntryList.add(targetEntry);

		classUnderTest.populate((CartModel) source, (CartData) target);

		assertTrue("Configuration should be consistent", targetEntry.isConfigurationConsistent());

	}



	@Test
	public void testPopulateConfigNoConfigModelInSession()
	{
		final String configId = "123";

		initializeSourceItem("Ext", configId);

		targetEntryList.add(targetEntry);
		Mockito.when(productConfigurationService.retrieveConfigurationModel(configId)).thenReturn(null);
		Mockito.when(
				productConfigurationService.createConfigurationFromExternal((KBKey) Mockito.anyObject(), Mockito.matches("Ext")))
				.thenReturn(configModel);
		classUnderTest.populate((CartModel) source, (CartData) target);

		assertTrue("Configuration should be consistent", targetEntry.isConfigurationConsistent());

	}

	protected Answer setExtConfigToCartEntry(final String extConfig)
	{
		final Answer answer = new Answer<Void>()
		{
			@Override
			public Void answer(final InvocationOnMock invocation) throws Throwable
			{
				Mockito.when(sourceEntry.getExternalConfiguration()).thenReturn(extConfig);
				return null;
			}
		};
		return answer;
	}

	@Test
	public void testPopulateNoConfigurationAttached()
	{
		final String configId = "";
		initializeSourceItem("", configId);
		final Answer answer = setExtConfigToCartEntry("externalConfig");
		Mockito.doAnswer(answer).when(sourceEntry).setExternalConfiguration(Mockito.any());
		targetEntryList.add(targetEntry);
		classUnderTest.populate((CartModel) source, (CartData) target);
		assertTrue("Configuration must be marked as attached, as a default config is created",
				targetEntry.isConfigurationAttached());
	}

	private PK initializeSourceItem(final String extConfig, final String configId)
	{
		final PK value = PK.fromLong(123);

		Mockito.when(sourceEntry.getPk()).thenReturn(value);
		Mockito.when(sourceEntry.getExternalConfiguration()).thenReturn(extConfig);
		Mockito.when(sourceEntry.getProduct()).thenReturn(productModel);
		Mockito.when(sourceEntry.getEntryNumber()).thenReturn(entryNo);
		Mockito.when(sessionAccessService.getConfigIdForCartEntry(targetEntry.getItemPK())).thenReturn(configId);
		if (configId == null)
		{
			productModel.setSapConfigurable(Boolean.FALSE);
		}
		else
		{
			productModel.setSapConfigurable(Boolean.TRUE);
		}
		return value;
	}

	@Override
	@Test(expected = IllegalArgumentException.class)
	public void testWriteToTargetEntryIllegalArgument()
	{
		super.testWriteToTargetEntryIllegalArgument();
	}

	@Override
	@Test
	public void testWriteToTargetEntry()
	{
		super.testWriteToTargetEntry();
	}

	@Override
	@Test
	public void testWriteToTargetEntryInconsistent()
	{
		super.testWriteToTargetEntryInconsistent();
	}

	@Override
	@Test
	public void testCreateConfigurationInfos()
	{
		super.testCreateConfigurationInfos();
	}

	@Override
	@Test(expected = ConversionException.class)
	public void testCreateConfigurationInfosException()
	{
		super.testCreateConfigurationInfosException();
	}

	@Override
	@Test(expected = IllegalStateException.class)
	public void testWriteToTargetEntrySummaryMapNull()
	{
		super.testWriteToTargetEntrySummaryMapNull();
	}

	@Override
	@Test(expected = IllegalStateException.class)
	public void testValidateAndSetConfigAttachedNullExternal()
	{
		super.testValidateAndSetConfigAttachedNullExternal();
	}

	@Override
	@Test(expected = IllegalStateException.class)
	public void testValidateAndSetConfigAttachedEmptyExternal()
	{
		super.testValidateAndSetConfigAttachedEmptyExternal();
	}

	@After
	public void tearDown()
	{
		// This is necessary to delete answer objects between test-method execution
		reset(sourceEntry);
	}
}
