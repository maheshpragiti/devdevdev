/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.model.constants;

/**
 * Global class for all Sapproductconfigmodel constants. You can add global constants for your extension into this
 * class.
 */
public final class SapproductconfigmodelConstants extends GeneratedSapproductconfigmodelConstants
{
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigmodel";

	public static final String CONFIGURATION_PRICING_SUPPORTED = "sapproductconfig_enable_pricing";
	public static final String CONFIGURATION_PRICING_PROCEDURE = "sapproductconfig_pricingprocedure";
	public static final String CONFIGURATION_CONDITION_FUNCTION_BASE_PRICE = "sapproductconfig_condfunc_baseprice";
	public static final String CONFIGURATION_CONDITION_FUNCTION_SECLECTED_OPTIONS = "sapproductconfig_condfunc_selectedoptions";

	public static final String START_DELTA_LOAD_AFTER_INITIAL = "sapproductconfigmodel.startDeltaloadAfterInitial";


	private SapproductconfigmodelConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
