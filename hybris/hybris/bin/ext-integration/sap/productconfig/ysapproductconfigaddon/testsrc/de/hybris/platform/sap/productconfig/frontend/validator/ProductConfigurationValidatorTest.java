/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.frontend.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.productconfig.facades.ConfigurationData;
import de.hybris.platform.sap.productconfig.facades.CsticData;
import de.hybris.platform.sap.productconfig.facades.SessionAccessFacade;
import de.hybris.platform.sap.productconfig.facades.UiType;
import de.hybris.platform.sap.productconfig.frontend.util.ConfigDataMergeProcessor;
import de.hybris.platform.sap.productconfig.frontend.util.impl.ConfigDataMergeProcessorImpl;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.ConfigModelImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.Errors;


@UnitTest
public class ProductConfigurationValidatorTest
{
	private final ProductConfigurationValidator validator = new ProductConfigurationValidator();

	@Mock
	private Errors errorObj;

	@Mock
	private NumericChecker numericChecker;

	@Mock
	private SessionAccessFacade sessionAccessFacade;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		validator.setNumericChecker(numericChecker);

		final ConfigDataMergeProcessor mergeProcessor = new ConfigDataMergeProcessorImpl();
		validator.setMergeProcessor(mergeProcessor);

		validator.setSessionAccessFacade(sessionAccessFacade);
		Mockito.when(sessionAccessFacade.getConfigurationModelEngineState(Mockito.anyString())).thenReturn(new ConfigModelImpl());
	}

	@Test
	public void validateNoGrups() throws Exception
	{
		final ConfigurationData configuration = ValidatorTestData.createEmptyConfigurationWithDefaultGroup();
		// happens if all is collapsed
		configuration.setGroups(null);
		validator.validateSubGroups(configuration.getGroups(), errorObj, "groups");
		// should run through without NPE

	}

	@Test
	public void notSupportingString() throws Exception
	{
		final boolean supports = validator.supports(String.class);
		assertFalse("Should not support everything", supports);
	}

	@Test
	public void supportingConfigurationData() throws Exception
	{
		final boolean supports = validator.supports(ConfigurationData.class);
		assertTrue("Must support ConfigurationData", supports);
	}

	@Test
	public void testEmptyConfiguration() throws Exception
	{
		final ConfigurationData configuration = ValidatorTestData.createEmptyConfigurationWithDefaultGroup();
		configuration.setInputMerged(true);
		validator.validate(configuration, errorObj);
	}


	@Test
	public void tesValidateNumeric() throws Exception
	{
		final String fieldName = "numeric";
		final ConfigurationData configuration = ValidatorTestData.createConfigurationWithNumeric(fieldName, "123");
		configuration.setInputMerged(true);
		validator.validate(configuration, errorObj);

		final CsticData cstic = configuration.getGroups().get(0).getCstics().get(0);
		Mockito.verify(numericChecker, Mockito.times(1)).validate(cstic, errorObj);
		Mockito.verify(numericChecker, Mockito.times(0)).validateAdditionalValue(cstic, errorObj);
	}

	@Test
	public void tesValidateNumeric_additionalEmpty() throws Exception
	{
		final String fieldName = "numeric";
		final ConfigurationData configuration = ValidatorTestData.createConfigurationWithNumeric(fieldName, "123");
		final CsticData csticData = configuration.getGroups().get(0).getCstics().get(0);
		csticData.setType(UiType.RADIO_BUTTON_ADDITIONAL_INPUT);
		csticData.setAdditionalValue("");

		configuration.setInputMerged(true);

		validator.validate(configuration, errorObj);

		final CsticData cstic = csticData;
		Mockito.verify(numericChecker, Mockito.times(0)).validate(cstic, errorObj);
		Mockito.verify(numericChecker, Mockito.times(0)).validateAdditionalValue(cstic, errorObj);
	}

	@Test
	public void tesValidateNumeric_additional() throws Exception
	{
		final String fieldName = "numeric";
		final ConfigurationData configuration = ValidatorTestData.createConfigurationWithNumeric(fieldName, "123");
		final CsticData csticData = configuration.getGroups().get(0).getCstics().get(0);
		csticData.setType(UiType.RADIO_BUTTON_ADDITIONAL_INPUT);
		csticData.setAdditionalValue("123456");

		configuration.setInputMerged(true);

		validator.validate(configuration, errorObj);

		final CsticData cstic = csticData;
		Mockito.verify(numericChecker, Mockito.times(0)).validate(cstic, errorObj);
		Mockito.verify(numericChecker, Mockito.times(1)).validateAdditionalValue(cstic, errorObj);
	}


	@Test
	public void tesValidateNumericInSubGroup() throws Exception
	{
		final String fieldName = "numeric";
		final ConfigurationData configuration = ValidatorTestData.createConfigurationWithNumericInSubGroup(fieldName, "123");
		configuration.setInputMerged(true);
		validator.validate(configuration, errorObj);

		final CsticData cstic = configuration.getGroups().get(0).getSubGroups().get(0).getCstics().get(0);
		Mockito.verify(numericChecker, Mockito.times(1)).validate(cstic, errorObj);
		Mockito.verify(numericChecker, Mockito.times(0)).validateAdditionalValue(cstic, errorObj);

	}
}
