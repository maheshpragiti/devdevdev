/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.frontend.constants;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.catalog.enums.ConfiguratorType;


/**
 * Global class for all ysapproductconfigaddon web constants. You can add global constants for your extension into this
 * class.
 */
public final class SapproductconfigfrontendWebConstants
{
	public static final String ROOT = AbstractController.ROOT;
	public static final String REDIRECT_PREFIX = AbstractController.REDIRECT_PREFIX;
	public static final String CONFIG_OVERVIEW_URL = "/configOverview";
	public static final String GENERIC_CONFIG_DISPLAY = "/configurationDisplay";
	public static final String VARIANT_OVERVIEW_URL = "/variantOverview";
	public static final String CONFIG_URL = "/configuratorPage/" + ConfiguratorType.CPQCONFIGURATOR.toString();
	public static final String CART_CONFIG_URL = "/configuration/" + ConfiguratorType.CPQCONFIGURATOR.toString();
	public static final String ADDON_PREFIX = "addon:";
	public static final String LOG_URL = "Redirect to: '";
	public static final String CONFIGURATOR_TYPE = "CPQCONFIGURATOR";
	public static final String PRODUCT_ATTRIBUTE = "product";
	public static final String LOG_CONFIG_DATA = "configuration data with [CONFIG_ID: '";
	public static final String CONFIG_PAGE_ROOT = "/pages/configuration/configurationPage";
	public static final String CONFIG_ERROR_ROOT = "/pages/configuration/errorForwardForAJAXRequests";
	public static final String OVERVIEW_PAGE_ROOT = "/pages/configuration/configurationOverviewPage";
	public static final String AJAX_SUFFIX = "ForAJAXRequests";
	public static final String CONFIG_PAGE_VIEW_NAME = ADDON_PREFIX + ROOT + SapproductconfigaddonConstants.EXTENSIONNAME
			+ CONFIG_PAGE_ROOT;
	public static final String OVERVIEW_PAGE_VIEW_NAME = ADDON_PREFIX + ROOT + SapproductconfigaddonConstants.EXTENSIONNAME
			+ OVERVIEW_PAGE_ROOT;
	public static final String ORDER_DETAILS = "/my-account/order/";
	public static final String QUOTES = "/my-account/my-quotes/";
	public static final String SAVED_CARTS = "/my-account/saved-carts/";

	private SapproductconfigfrontendWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
