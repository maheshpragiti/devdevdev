/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcommonbol.businesspartner.businessobject.interf;

import de.hybris.platform.sap.core.bol.businessobject.BusinessObject;

import java.io.Serializable;
import java.util.List;


/**
 * BO representation of an address.
 *
 */
public interface Address extends BusinessObject, Cloneable, Comparable<Address>, Serializable
{

	/**
	 * Address type corresponding to an organization
	 */
	String TYPE_ORGANISATION = "1";

	/**
	 * Address type corresponding to a business partner
	 */
	String TYPE_PERSON = "2";

	/**
	 * All (delta) attributes indicating that bean attributes have been changed are set to true
	 */
	void setAllXFields();

	/**
	 * Returns the attribute value for a given bean attribute
	 *
	 * @param field
	 *           Name of the field that should be returned
	 * @return attribute value
	 */
	String get(String field);

	/**
	 * Set the property id
	 *
	 * @param id
	 *           the id
	 */
	void setId(String id);

	/**
	 * Returns the property id
	 *
	 * @return id
	 */
	String getId();

	/**
	 * Set the property type
	 *
	 * @param type
	 *           the type
	 */
	void setType(String type);

	/**
	 * Returns the property type
	 *
	 * @return type
	 */
	String getType();

	/**
	 * Set the property origin
	 *
	 * @param origin
	 *           the origin
	 */
	void setOrigin(String origin);

	/**
	 * Returns the property origin
	 *
	 * @return origin
	 */
	String getOrigin();

	/**
	 * Set the property personNumber
	 *
	 * @param personNumber
	 *           the person number (BAS)
	 */
	void setPersonNumber(String personNumber);

	/**
	 * Returns the property personNumber
	 *
	 * @return personNumber
	 */
	String getPersonNumber();

	/**
	 * @param titleKey
	 *           short ID of title
	 */
	void setTitleKey(String titleKey);

	/**
	 * @param title
	 *           title description
	 */
	void setTitle(String title);

	/**
	 * @param titleAca1Key
	 *           short ID of academic title
	 */
	void setTitleAca1Key(String titleAca1Key);

	/**
	 * @param titleAca1
	 *           academic title description
	 */
	void setTitleAca1(String titleAca1);

	/**
	 * @param firstName
	 *           the fist name
	 */
	void setFirstName(String firstName);

	/**
	 * @param lastName
	 *           the last name
	 */
	 void setLastName(String lastName);

	/**
	 * @param birthName
	 *           the birth name
	 */
	void setBirthName(String birthName);

	/**
	 * @param secondName
	 *           the second name
	 */
	void setSecondName(String secondName);

	/**
	 * @param middleName
	 *           the middle name
	 */
	void setMiddleName(String middleName);

	/**
	 * Setter for bean attribute
	 *
	 * @param nickName
	 *           the nickname
	 */
	void setNickName(String nickName);

	/**
	 * Setter for bean attribute
	 *
	 * @param initials
	 *           the initials
	 */
	void setInitials(String initials);

	/**
	 * Setter for bean attribute
	 *
	 * @param name1
	 *           the name 1
	 */
	void setName1(String name1);

	/**
	 * Setter for bean attribute
	 *
	 * @param name2
	 *           the name 2
	 */
	void setName2(String name2);

	/**
	 * Setter for bean attribute
	 *
	 * @param name3
	 *           the name 3
	 */
	void setName3(String name3);

	/**
	 * Setter for bean attribute
	 *
	 * @param name4
	 *           the name 4
	 */
	void setName4(String name4);

	/**
	 * Setter for bean attribute
	 *
	 * @param coName
	 *           company name
	 */
	void setCoName(String coName);

	/**
	 * Setter for bean attribute
	 *
	 * @param city
	 *           the city
	 */
	void setCity(String city);

	/**
	 * Setter for bean attribute
	 *
	 * @param district
	 *           geographic entity for tax jurisdiction code determination
	 */
	void setDistrict(String district);

	/**
	 * Setter for bean attribute
	 *
	 * @param postlCod1
	 *           the postal code 1
	 */
	void setPostlCod1(String postlCod1);

	/**
	 * Setter for bean attribute
	 *
	 * @param postlCod2
	 *           the postal code 2
	 */
	void setPostlCod2(String postlCod2);

	/**
	 * Setter for bean attribute
	 *
	 * @param postlCod3
	 *           the postal code 3
	 */
	void setPostlCod3(String postlCod3);

	/**
	 * Setter for bean attribute
	 *
	 * @param pcode1Ext
	 *           the postal code 1 ext
	 */
	void setPcode1Ext(String pcode1Ext);

	/**
	 * Setter for bean attribute
	 *
	 * @param pcode2Ext
	 *           the postal code 2 ext
	 */
	void setPcode2Ext(String pcode2Ext);

	/**
	 * Setter for bean attribute
	 *
	 * @param pcode3Ext
	 *           the postal code 1 ext
	 */
	void setPcode3Ext(String pcode3Ext);

	/**
	 * Setter for bean attribute
	 *
	 * @param poBox
	 *           the post box
	 */
	void setPoBox(String poBox);

	/**
	 * Setter for bean attribute
	 *
	 * @param poWoNo
	 *           Flag: PO Box without number
	 */
	void setPoWoNo(String poWoNo);

	/**
	 * Setter for bean attribute
	 *
	 * @param poBoxCit
	 *           the city of the post box
	 */
	void setPoBoxCit(String poBoxCit);

	/**
	 * Setter for bean attribute
	 *
	 * @param poBoxReg
	 *           the post box region
	 */
	void setPoBoxReg(String poBoxReg);

	/**
	 * Setter for bean attribute
	 *
	 * @param poBoxCtry
	 *           the post box country
	 */
	void setPoBoxCtry(String poBoxCtry);

	/**
	 * Setter for bean attribute
	 *
	 * @param poCtryISO
	 *           the country iso code
	 */
	void setPoCtryISO(String poCtryISO);

	/**
	 * Setter for bean attribute
	 *
	 * @param street
	 *           The street
	 */
	void setStreet(String street);

	/**
	 * Setter for bean attribute
	 *
	 * @param strSuppl1
	 *           The street supplement 1
	 */
	void setStrSuppl1(String strSuppl1);

	/**
	 * Setter for bean attribute
	 *
	 * @param strSuppl2
	 *           The street supplement 2
	 */
	void setStrSuppl2(String strSuppl2);

	/**
	 * Setter for bean attribute
	 *
	 * @param strSuppl3
	 *           The street supplement 3
	 */
	void setStrSuppl3(String strSuppl3);

	/**
	 * Setter for bean attribute
	 *
	 * @param location
	 *           The location
	 */
	void setLocation(String location);

	/**
	 * Setter for bean attribute
	 *
	 * @param houseNo
	 *           The house no
	 */
	void setHouseNo(String houseNo);

	/**
	 * Setter for bean attribute
	 *
	 * @param houseNo2
	 *           The house no 2
	 */
	void setHouseNo2(String houseNo2);

	/**
	 * Setter for bean attribute
	 *
	 * @param houseNo3
	 *           The house no3
	 */
	void setHouseNo3(String houseNo3);

	/**
	 * Setter for bean attribute
	 *
	 * @param building
	 *           The building
	 */
	void setBuilding(String building);

	/**
	 * Setter for bean attribute
	 *
	 * @param floor
	 *           The floor
	 */
	void setFloor(String floor);

	/**
	 * Setter for bean attribute
	 *
	 * @param roomNo
	 *           The room number
	 */
	void setRoomNo(String roomNo);

	/**
	 * Setter for bean attribute
	 *
	 * @param country
	 *           The Country
	 */
	void setCountry(String country);

	/**
	 * Setter for bean attribute
	 *
	 * @param countryISO
	 *           The country ISO code
	 */
	void setCountryISO(String countryISO);

	/**
	 * Setter for bean attribute
	 *
	 * @param region
	 *           The region
	 */
	void setRegion(String region);

	/**
	 * Setter for bean attribute
	 *
	 * @param homeCity
	 *           The home city
	 */
	void setHomeCity(String homeCity);

	/**
	 * Setter for bean attribute
	 *
	 * @param taxJurCode
	 *           tax jurisdiction code. Can be determined from full address or from district
	 */
	void setTaxJurCode(String taxJurCode);

	/**
	 * Setter for bean attribute
	 *
	 * @param tel1Numbr
	 *           the telephone number
	 */
	void setTel1Numbr(String tel1Numbr);

	/**
	 * Setter for bean attribute
	 *
	 * @param tel1Ext
	 *           the telephone extension
	 */
	void setTel1Ext(String tel1Ext);

	/**
	 * Setter for bean attribute
	 *
	 * @param faxNumber
	 *           The fax number
	 */
	void setFaxNumber(String faxNumber);

	/**
	 * Setter for bean attribute
	 *
	 * @param faxExtens
	 *           the fax number extension
	 */
	void setFaxExtens(String faxExtens);

	/**
	 * Setter for bean attribute
	 *
	 * @param email
	 *           the email address
	 */
	void setEmail(String email);

	/**
	 * Setter for bean attribute
	 *
	 * @param partner
	 *           business partner owning the address
	 */
	void setAddressPartner(String partner);

	/**
	 * Setter for bean attribute
	 *
	 * @param category
	 *           the address category
	 */
	void setCategory(String category);

	/**
	 * @return respective bean attribute
	 */
	String getTitleKey();

	/**
	 * @return title in language dependent format
	 */
	String getTitle();

	/**
	 * @return academic title key (language independent)
	 */
	String getTitleAca1Key();

	/**
	 * @return academic title
	 */
	String getTitleAca1();

	/**
	 * @return respective bean attribute
	 */
	String getFirstName();

	/**
	 * @return respective bean attribute
	 */
	String getLastName();

	/**
	 * @return respective bean attribute
	 */
	String getBirthName();

	/**
	 * @return respective bean attribute
	 */
	String getSecondName();

	/**
	 * @return respective bean attribute
	 */
	String getMiddleName();

	/**
	 * @return respective bean attribute
	 */
	String getNickName();

	/**
	 * @return respective bean attribute
	 */
	String getInitials();

	/**
	 * @return respective bean attribute
	 */
	String getName1();

	/**
	 * @return respective bean attribute
	 */
	String getName2();

	/**
	 * @return respective bean attribute
	 */
	String getName3();

	/**
	 * @return respective bean attribute
	 */
	String getName4();

	/**
	 * @return company name
	 */
	String getCoName();

	/**
	 * @return respective bean attribute
	 */
	String getCity();

	/**
	 * @return geographic entity used for tax jurisdiction code determination
	 */
	String getDistrict();

	/**
	 * @return respective bean attribute
	 */
	String getPostlCod1();

	/**
	 * @return respective bean attribute
	 */
	String getPostlCod2();

	/**
	 * @return respective bean attribute
	 */
	String getPostlCod3();

	/**
	 * @return respective bean attribute
	 */
	String getPcode1Ext();

	/**
	 * @return respective bean attribute
	 */
	String getPcode2Ext();

	/**
	 * @return respective bean attribute
	 */
	String getPcode3Ext();

	/**
	 * @return respective bean attribute
	 */
	String getPoBox();

	/**
	 * @return respective bean attribute
	 */
	String getPoWoNo();

	/**
	 * @return respective bean attribute
	 */
	String getPoBoxCit();

	/**
	 * @return respective bean attribute
	 */
	String getPoBoxReg();

	/**
	 * @return respective bean attribute
	 */
	String getPoBoxCtry();

	/**
	 * @return respective bean attribute
	 */
	String getPoCtryISO();

	/**
	 * @return respective bean attribute
	 */
	String getStreet();

	/**
	 * @return respective bean attribute
	 */
	String getStrSuppl1();

	/**
	 * @return respective bean attribute
	 */
	String getStrSuppl2();

	/**
	 * @return respective bean attribute
	 */
	String getStrSuppl3();

	/**
	 * @return respective bean attribute
	 */
	String getLocation();

	/**
	 * @return respective bean attribute
	 */
	String getHouseNo();

	/**
	 * @return respective bean attribute
	 */
	String getHouseNo2();

	/**
	 * @return respective bean attribute
	 */
	String getHouseNo3();

	/**
	 * @return respective bean attribute
	 */
	String getBuilding();

	/**
	 * @return respective bean attribute
	 */
	String getFloor();

	/**
	 * @return respective bean attribute
	 */
	String getRoomNo();

	/**
	 * @return respective bean attribute
	 */
	String getCountry();

	/**
	 * @return respective bean attribute
	 */
	String getCountryISO();

	/**
	 * @return respective bean attribute
	 */
	String getRegion();

	/**
	 * @return respective bean attribute
	 */
	String getHomeCity();

	/**
	 * @return tax jurisdiction which can be derived from complete address or from country, regions city, street and
	 *         district
	 */
	String getTaxJurCode();

	/**
	 * @return respective bean attribute
	 */
	String getTel1Numbr();

	/**
	 * @return respective bean attribute
	 */
	String getTel1Ext();

	/**
	 * @return respective bean attribute
	 */
	String getFaxNumber();

	/**
	 * @return respective bean attribute
	 */
	String getFaxExtens();

	/**
	 * @return respective bean attribute
	 */
	String getEmail();

	/**
	 * @return respective bean attribute
	 */
	String getCategory();

	/**
	 * @return partner who owns this address
	 */
	String getAddressPartner();

	/**
	 * Set language dependent text for country
	 *
	 * @param countryText
	 *           the country text
	 */
	void setCountryText(String countryText);

	/**
	 * @return respective bean attribute
	 */
	String getCountryText();

	/**
	 * Set language dependent text for region in length 50
	 *
	 * @param regionText50
	 *           the region text length 50
	 */
	void setRegionText50(String regionText50);

	/**
	 * @return respective bean attribute
	 */
	String getRegionText50();

	/**
	 * Set language dependent text for region in length 15
	 *
	 * @param regionText15
	 *           the region text length 15
	 */
	void setRegionText15(String regionText15);

	/**
	 * @return respective bean attribute
	 */
	String getRegionText15();

	/**
	 * Sets operation mode on current address
	 *
	 * @param operation
	 *           the address operation
	 * @see Operation
	 */
	void setOperation(Operation operation);

	/**
	 * The operation which is possible on an address
	 *
	 */
	public enum Operation
	{
		/**
		 * Default value. Assigned if actual operation cannot be determined
		 */
		NONE,
		/**
		 * Add a new address
		 */
		ADD,
		/**
		 * Change an address
		 */
		CHANGE,
		/**
		 * Delete an address
		 */
		DELETE
	}

	/**
	 * @return current operation
	 * @see Operation
	 */
	Operation getOperation();

	/**
	 * @return is this a standard address for a business partner. Cannot be deleted. This only applies to CRM where
	 *         multiple addresses are available
	 */
	boolean isStdAddress();

	/**
	 * States that current address is a standard address. See {@link Address#isStdAddress()}
	 *
	 * @param stdAddress
	 *           std address flag
	 */
	void setStdAddress(boolean stdAddress);

	/**
	 * @return respective bean attribute
	 */
	String getTelmob1();

	/**
	 * Sets first mobile number
	 *
	 * @param telmob1
	 *           the telmob1
	 */
	void setTelmob1(String telmob1);

	/**
	 * @return address number from CRM or ERP backend if available
	 */
	String getAddrnum();

	/**
	 * Sets address number which is available in the CRM or ERP backend
	 *
	 * @param addrnum
	 *           the address number (BAS)
	 */
	void setAddrnum(String addrnum);

	/**
	 * @return guid of address, only available in CRM case
	 */
	String getAddrguid();

	/**
	 * Sets address guid (only relevant for CRM backend)
	 *
	 * @param addrguid
	 *           the address guid
	 */
	void setAddrguid(String addrguid);

	/**
	 * @return respective bean attribute
	 */
	String getFunction();

	/**
	 * Sets job function
	 *
	 * @param function
	 *           the function
	 */
	void setFunction(String function);

	/**
	 * @return list of available counties. Relevant for tax jurisdiction code determination
	 */
	List<County> getCountyList();

	/**
	 * Sets lists of available counties. Relevant for tax jurisdiction code determination
	 *
	 * @param countyList
	 *           the county list
	 */
	void setCountyList(List<County> countyList);

	/**
	 * @return respective bean change attribute
	 */
	boolean getLastName_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getFirstName_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getName1_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getName2_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getMiddleName_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getCity_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getDistrict_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean isStdAddress_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getPoBox_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getCountry_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getStreet_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getRegion_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getFunction_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getEmail_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getTelmob1_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getFaxNumber_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getStrSuppl1_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getHouseNo_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getFaxExtens_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getPostlCod1_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getPostlCod2_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getTel1Numbr_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getTel1Ext_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getTitleKey_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getTaxJurCode_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getTitle_X();

	/**
	 * @return respective bean change attribute
	 */
	boolean getCoName_X();

	/**
	 * Sets country dialing code
	 *
	 * @param dialingCode
	 *           the dialing code
	 */
	void setDialingCode(String dialingCode);

	/**
	 * @return respective bean attribute
	 */
	String getDialingCode();

	/**
	 * @return respective bean attribute
	 */
	String getAddrText();

	/**
	 * Sets address short text
	 *
	 * @param addrText
	 *           the address text
	 */
	void setAddrText(String addrText);

	/**
	 * @return respective bean attribute
	 */
	String getCompanyName();

	/**
	 * Sets company name
	 *
	 * @param companyName
	 *           the company name
	 */
	void setCompanyName(String companyName);

	/**
	 * @return respective bean change attribute
	 */
	boolean getCompanyName_X();

	/**
	 * <b>clear_X</b> This method is to dynamically clear the values of all the <i>"_X"</i> variables except the ones in
	 * the except array. Dynamically fetches the fields names and clears their values.
	 */
	void clear_X();

	/**
	 * @return respective bean attribute
	 */
	String getFullName();

	/**
	 * @return has this address been changed
	 */
	boolean isChanged();

	/**
	 * @return respective bean attribute
	 */
	String getTel1NumbrSeq();

	/**
	 * Sets respective bean attribute
	 *
	 * @param tel1Seq
	 *           the tel1 sequence number
	 */
	void setTel1NumbrSeq(String tel1Seq);

	/**
	 * @return respective bean attribute
	 */
	String getTelmob1Seq();

	/**
	 * Sets respective bean attribute
	 *
	 * @param telmob1Seq
	 *           the telmob1 sequence number
	 */
	void setTelmob1Seq(String telmob1Seq);

	/**
	 * @return respective bean attribute
	 */
	String getFaxNumberSeq();

	/**
	 * Sets respective bean attribute
	 *
	 * @param string
	 *           the fax sequence number
	 */
	void setFaxNumberSeq(String string);

	/**
	 * Sets address in short format
	 *
	 * @param string
	 *           the address string
	 */
	void setAddressString(String string);

	/**
	 * @return respective bean attribute
	 */
	String getName();

	/**
	 * @return address in string format
	 */
	String getAddressString();

	/**
	 * @return is it an address specific to a certain document, e.g. sales transaction
	 */
	boolean isDocumentAddress();

	/**
	 * Indicates that this address is a document address which means that it's not available in master data but specific
	 * to a sales transaction e.g.
	 *
	 * @param documentAddress
	 *           flag for document address
	 */
	void setDocumentAddress(boolean documentAddress);

	/**
	 * Sets address string including name
	 *
	 * @param addressStringC
	 *           address in string format, including name
	 */
	void setAddressStringC(String addressStringC);

	/**
	 * @return address string including name
	 */
	String getAddressStringC();

	/**
	 * @return has this address a deviating name
	 */
	boolean isDeviatingName();

	/**
	 * Setter for the deviating name flag
	 *
	 * @param deviatingName
	 *           flag for deviating name
	 */
	void setDeviatingName(boolean deviatingName);

	/**
	 * @return the email sequence number
	 */
	String getEmailSeq();

	/**
	 * Setter for the email sequence number
	 *
	 * @param emailSeq
	 *           the email sequence numebr
	 */
	void setEmailSeq(String emailSeq);

	/**
	 * Compares all address content fields
	 *
	 * @param address
	 *           the address to be compared
	 *
	 * @return true, if all address content fields are equal
	 */
	boolean isAddressfieldsEqualTo(Address address);

	/**
	 * @return respective bean change attribute
	 */
	boolean getTitleAca1Key_X();

	/**
	 * @return the title supplement key
	 */
	String getTitleSupplKey();

	/**
	 * Setter for the title supplement key
	 *
	 * @param titleSuppl
	 *           the title supplement key
	 */
	void setTitleSupplKey(String titleSuppl);

	/**
	 * @return the title supplement key change flag
	 */
	boolean getTitleSupplKey_X();

	/**
	 * @return the prefix 1 key change flag
	 */
	boolean getPrefix1Key_X();

	/**
	 * @return the prefix 1 key change flag
	 */
	boolean getPrefix2Key_X();

	/**
	 * @return the prefix 1
	 */
	String getPrefix1();

	/**
	 * @return the prefix 2
	 */
	String getPrefix2();

	/**
	 * @param prefix2
	 */
	void setPrefix2(String prefix2);

	/**
	 * Setter for the name prefix 2 key
	 *
	 * @param prefix2Key
	 *           the prefix 2 key
	 */
	void setPrefix2Key(String prefix2Key);

	/**
	 * @return the prefix 2 key
	 */
	String getPrefix2Key();

	/**
	 * Setter for the name prefix text
	 *
	 * @param prefix1
	 *           the name 1 prefix test
	 */
	void setPrefix1(String prefix1);

	/**
	 * Setter for the name prefix 1 key
	 *
	 * @param prefix1Key
	 *           the prefix 1 key
	 */
	void setPrefix1Key(String prefix1Key);

	/**
	 * @return the prefix 1 key
	 */
	String getPrefix1Key();

	/**
	 * Setter for the full name text
	 *
	 * @param fullName
	 *           the full name string
	 */
	void setFullName(String fullName);
	
	@SuppressWarnings("squid:S1161")
	Address clone();
}