/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

(function(global) {
	
	//global variable to keep track of functions
	var sapprodrecoaddon = {};
	
	sapprodrecoaddon.oIntervalIds = {};
		
	function retrieveRecommendations(id) {
		var oElement = $("#" + id);
		var sProductCode = oElement.data("prodcode");
		var sComponentId = oElement.data("componentid");
	
		var sBaseUrl = oElement.data("baseUrl");
		var urlPathName = window.parent.location.pathname;
		ajaxUrl = sBaseUrl + '/action/recommendations/';
		$.get(ajaxUrl, {
			id : id,
			productCode : sProductCode,
			componentId : sComponentId
		}, addRecommendation(id)).done(function() {
	
			//we need to check if we are NOT in smartEdit mode && if the component has been initialized.
			if (urlPathName !== "/smartedit/" && oElement.hasClass("initialized")) {
					detectElementFromViewport(oElement, sComponentId);
			}
	
		});
	};
	
	function registerClickthrough(id, productCode, componentId) {
		var baseUrl = $("#" + id).parent().data("baseUrl")
		ajaxUrl = baseUrl + '/action/interaction/';
		$.post(ajaxUrl, {
			id : productCode,
			componentId : componentId
		}, null);
	};
	
	function postImpression(element, componentId) {
		var baseUrl = element.data("baseUrl");
		ajaxUrl = baseUrl + '/action/impression/';
	
		$.post(ajaxUrl, {
			itemCount : element.find(".owl-item").length,
			componentId : componentId
		}, null);
	}
	
	function detectElementFromViewport(element, componentId) {
	
		/**
		 * This prototype function checks if the element is on the current user
		 * screen
		 */
		$.fn.bElementIsViewedByUser = function(element) {
	
			var win = $(window);
	
			// gets the user browser screen dimension in pixel
			var viewport = {
				top : win.scrollTop(),
				left : win.scrollLeft()
			};
			viewport.right = viewport.left + win.width();
			viewport.bottom = viewport.top + win.height();
	
			var elementBounds = element.offset();
			elementBounds.right = elementBounds.left + element.outerWidth();
			elementBounds.bottom = elementBounds.top + element.outerHeight();
	
			// Check if one pixel of element is visible
			return (viewport.right >= elementBounds.left
					&& viewport.left <= elementBounds.right
					&& viewport.bottom >= elementBounds.top && viewport.top <= elementBounds.bottom)
		};
	
			if(!sapprodrecoaddon.oIntervalIds.hasOwnProperty(element.attr("id"))){
				sapprodrecoaddon.oIntervalIds[element.attr("id")] = setInterval(checkVisibility, 500, element, componentId);
			}
			
		
	
		// this function checks if the input element is on the screen periodically
		function checkVisibility(oElement, componentId) {
			try {
				if ($.prototype.bElementIsViewedByUser(oElement)) {
					clearInterval(sapprodrecoaddon.oIntervalIds[oElement.attr("id")]);
					postImpression(oElement, componentId);
				}
			} catch (e) {
				console.log(e);
				clearInterval(sapprodrecoaddon.oIntervalIds[oElement.attr("id")]);
			}
		}
	}
	
	
	/* This is the function that creates the component */
	function loadRecommendations() {
	    var divs = document.getElementsByClassName("sap-product-reco");
	
	    for (var i = 0; i < divs.length; i++) {
	        if (divs[i].id.search("reco") > -1) {
	            var productCode = $("#" + divs[i].id).attr("data-prodcode");
	            var componentId = $("#" + divs[i].id).attr("data-componentId");
	
	            retrieveRecommendations(divs[i].id, productCode, componentId);
	        }
	    }
	}
	
	function addRecommendation(recoId) {
	    return function (data) {
	        var $recoComponent = $("#" + recoId);
	        
			 if (data !== '') {
		    	if($recoComponent && $recoComponent.hasClass("initialized")){
		   			 return;
		   		 }
		        $recoComponent.append(data);
		    	jQuery('#recommendationUL' + recoId).owlCarousel(ACC.carousel.carouselConfig.default);
		        $recoComponent.addClass('initialized');
			 }
	        else {
	            $recoComponent.empty();
	        }
	    }
	}
	
	$(function() {
		//trigger recommendation retrieval for initial page load
		loadRecommendations();
		
		//add our function to the SmartEdit reprocessPage
		window.smartedit.addOnReprocessPageListener(loadRecommendations);
		window.smartedit.reprocessPage();
	});


})(this);
