/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecoaddon.facade;

import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Optional;

import com.hybris.ymkt.sapprodreco.dao.ImpressionContext;
import com.hybris.ymkt.sapprodreco.dao.RecommendationContext;


/**
 * to do
 */
public interface ProductRecommendationManagerFacade
{
	/**
	 * Get recommended products based on recommendation context
	 * 
	 * @param context
	 * @return {@link List} of {@link ProductReferenceData}
	 */
	List<ProductReferenceData> getProductRecommendation(RecommendationContext context);

	/**
	 * Save impression
	 * 
	 * @param context
	 */
	void saveImpression(ImpressionContext context);

	/**
	 * @param productCode
	 * @return
	 */
	Optional<ProductModel> findProduct(String productCode);
}
