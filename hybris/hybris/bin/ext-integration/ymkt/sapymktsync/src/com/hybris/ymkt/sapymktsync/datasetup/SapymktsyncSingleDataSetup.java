/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapymktsync.datasetup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.scripting.enums.ScriptType;
import de.hybris.platform.scripting.model.ScriptModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.hybris.ymkt.sapymktsync.constants.SapymktsyncConstants;


/**
 *
 * Configure the y2ySync tool to capture all the changes in the hybris platform to sync them to the hybris marketing
 * datahub extension
 */
@SystemSetup(extension = SapymktsyncConstants.EXTENSIONNAME)
public class SapymktsyncSingleDataSetup extends AbstractSystemSetup
{

	private static final String REMOVE_MARKER = "import de.hybris.platform.servicelayer.search.FlexibleSearchQuery\n\n" + //
			"def fQuery = new FlexibleSearchQuery('SELECT {PK} FROM {ItemVersionMarker}')\n" + //
			"def result = flexibleSearchService.search(fQuery)\n\n" + //
			"result.getResult().forEach {\n" + //
			"modelService.remove(it)\n}"; //

	private static final String SYNC = "import de.hybris.platform.servicelayer.search.FlexibleSearchQuery\n" + //
			"import de.hybris.y2ysync.services.SyncExecutionService.ExecutionMode\n\n" + //
			"def syncToDatahubJob = findJob '%s' \n" + //
			"syncExecutionService.startSync(syncToDatahubJob, ExecutionMode.SYNC)\n\n" + //
			"def findJob(code) {\n\t" + //
			"def fQuery = new FlexibleSearchQuery('SELECT {PK} FROM {Y2YSyncJob} WHERE {code}=?code')\n\t" + //
			"fQuery.addQueryParameter('code', code)\n\t" + //
			"flexibleSearchService.searchUnique(fQuery)\n}";

	private ModelService modelService;

	private void createScript(final String code, final String content)
	{
		final ScriptModel script = modelService.create(ScriptModel.class);
		script.setScriptType(ScriptType.GROOVY);
		script.setContent(content);
		script.setCode(code);
		modelService.save(script);
	}

	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		return Collections.emptyList();
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.ALL)
	public void setup(final SystemSetupContext context)
	{
		this.importImpexFile(context, "/sapymktsync/import/sapymktsync-single-data-streams.impex");

		createScript("syncYmktSingleToDataHub", String.format(SYNC, "ymktToDatahubJob"));
		createScript("syncYmktSingleToZip", String.format(SYNC, "ymktToZipJob"));
		createScript("removeYmktSingleVersionMarkers", REMOVE_MARKER);
	}
}
