/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.common.http;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class HttpURLConnectionResponse
{
	private static final byte[] ZERO_BYTES = new byte[0];

	protected Map<String, List<String>> headerFields = Collections.emptyMap();
	protected IOException ioException;
	protected byte[] payload = ZERO_BYTES;
	protected byte[] payloadError = ZERO_BYTES;
	protected int responseCode = -1;
	protected long timeEnd;
	protected final long timeStart = System.currentTimeMillis();

	public long getDuration()
	{
		if (this.timeEnd == 0)
		{
			this.timeEnd = System.currentTimeMillis();
		}
		return this.timeEnd - this.timeStart;
	}

	public Map<String, List<String>> getHeaderFields()
	{
		return headerFields;
	}

	public IOException getIOException()
	{
		return this.ioException;
	}

	public byte[] getPayload()
	{
		return this.payload;
	}

	public byte[] getPayloadError()
	{
		return this.payloadError;
	}

	public int getResponseCode()
	{
		return this.responseCode;
	}

	public void setHeaderFields(Map<String, List<String>> headerFields)
	{
		this.headerFields = headerFields;
	}

	public void setIOException(IOException ioException)
	{
		this.ioException = ioException;
	}

	public void setPayload(byte[] payload)
	{
		this.payload = payload;
	}

	public void setPayloadError(byte[] payloadError)
	{
		this.payloadError = payloadError;
	}

	public void setResponseCode(int responseCode)
	{
		this.responseCode = responseCode;
	}

}
