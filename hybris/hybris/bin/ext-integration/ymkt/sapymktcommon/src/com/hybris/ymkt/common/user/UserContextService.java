/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.common.user;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.Cookie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Provide utility methods such as {@link #getUserOrigin()} and {@link #getUserId()}.
 *
 */
public class UserContextService
{
	private static final Logger LOG = LoggerFactory.getLogger(UserContextService.class);

	protected UserService userService;
	protected String anonymousUserOrigin;
	protected String loggedInUserOrigin;

	protected String getAnonymousUserId()
	{
		final ServletRequestAttributes attributes;
		try
		{
			attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		}
		catch (final IllegalStateException e)
		{
			LOG.info("Not executing within a web request", e);
			return "";
		}

		final Cookie[] cookies = attributes.getRequest().getCookies();

		if (cookies == null)
		{
			return "";
		}

		for (final Cookie cookie : cookies)
		{
			if (cookie.getName().startsWith("_pk_id"))
			{
				return cookie.getValue().substring(0, 16);
			}
		}
		return "";
	}

	/**
	 * @return User ID according to the {@link #getUserOrigin()}.
	 */
	public String getUserId()
	{
		return this.isAnonymousUser() ? this.getAnonymousUserId() : this.userService.getCurrentUser().getUid();
	}

	/**
	 * ID Origin is a synonyms of User Type.
	 *
	 * @return COOKIE_ID or EMAIL
	 */
	public String getUserOrigin()
	{
		return this.isAnonymousUser() ? this.anonymousUserOrigin : this.loggedInUserOrigin;
	}

	public boolean isAnonymousUser()
	{
		final UserModel currentUser = userService.getCurrentUser();
		return currentUser == null || userService.isAnonymousUser(currentUser);
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Required
	public void setAnonymousUserOrigin(final String anonymousUserOrigin)
	{
		LOG.debug("anonymousUserOrigin={}", anonymousUserOrigin);
		assert anonymousUserOrigin != null;
		assert !anonymousUserOrigin.isEmpty();
		this.anonymousUserOrigin = anonymousUserOrigin.intern();
	}

	@Required
	public void setLoggedInUserOrigin(final String loggedInUserOrigin)
	{
		LOG.debug("loggedInUserOrigin={}", loggedInUserOrigin);
		this.loggedInUserOrigin = loggedInUserOrigin.intern();
	}
}
