/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.c4c.quote.inbound.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.comments.model.CommentTypeModel;
import de.hybris.platform.comments.model.ComponentModel;
import de.hybris.platform.comments.model.DomainModel;
import de.hybris.platform.comments.services.CommentDao;
import de.hybris.platform.comments.services.CommentService;
import de.hybris.platform.commerceservices.service.data.CommerceCommentParameter;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.QuoteEntryModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;

/**
 * JUnit for DefaultInboundQuoteCommentConversionHelper
 * 
 * @author C5229488
 *
 */
@UnitTest
public class DefaultInboundQuoteCommentConversionHelperTest {
	@InjectMocks
	private final DefaultInboundQuoteCommentConversionHelper helper = new DefaultInboundQuoteCommentConversionHelper();

	@Mock
	private ModelService modelService;
	@Mock
	private CommentService commentService;
	@Mock
	private QuoteService quoteService;
	@Mock
	private UserService userService;
	@Mock
	private EnumerationService enumerationService;
	@Mock
	private CommentDao commentDao;
	@Mock
	private BaseStoreModel baseStore;
	@Mock
	private ItemModel item;
	@Mock
	private ItemModel previousItem;
	@Mock
	private QuoteModel quote;
	@Mock
	private QuoteEntryModel quoteEntry;

	private static String COMMENTTYPE = "10024";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldCreateComments() {
		final CommerceCommentParameter parameter = buildParameter("text", "domain", "component", "commentType");
		final List<DomainModel> domainList = listStub(DomainModel.class, "domain");
		final List<ComponentModel> componentList = listStub(ComponentModel.class, "component", "component2");
		final List<CommentTypeModel> typeList = listStub(CommentTypeModel.class, "commentType", "commentType2");
		setDomain(domainList.get(0), componentList, typeList);

		Mockito.when(commentDao.findDomainsByCode(parameter.getDomainCode())).thenReturn(domainList);
		Mockito.when(commentDao.findComponentsByDomainAndCode(domainList.get(0), parameter.getComponentCode()))
				.thenReturn(componentList);
		Mockito.when(modelService.create(CommentModel.class)).thenReturn(new CommentModel());
		helper.createComment(parameter, COMMENTTYPE, baseStore, previousItem);

		final List<CommentModel> comments = parameter.getItem().getComments();
		Assert.assertEquals(1, comments.size());
		final CommentModel comment = comments.get(0);
		Assert.assertEquals(parameter.getAuthor(), comment.getAuthor());
		Assert.assertEquals(parameter.getText(), comment.getText());
		Assert.assertEquals(parameter.getComponentCode(), comment.getComponent().getCode());
		Assert.assertEquals(parameter.getDomainCode(), comment.getComponent().getDomain().getCode());
		Assert.assertEquals(parameter.getCommentTypeCode(), comment.getCommentType().getCode());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidParameter() {
		helper.createComment(null, null, null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidItem() {
		final CommerceCommentParameter parameter = buildParameter("text", "domain", "component", "commentType");
		parameter.setItem(null);
		helper.createComment(parameter, COMMENTTYPE, baseStore, previousItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidText() {
		final CommerceCommentParameter parameter = buildParameter(null, "domain", "component", "commentType");
		helper.createComment(parameter, COMMENTTYPE, baseStore, previousItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidAuthor() {
		final CommerceCommentParameter parameter = buildParameter(new ItemModel(), null, "text", "domain", "component",
				"commentType");
		helper.createComment(parameter, COMMENTTYPE, baseStore, previousItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidDomain() {
		final CommerceCommentParameter parameter = buildParameter("text", null, "component", "commentType");
		helper.createComment(parameter, COMMENTTYPE, baseStore, previousItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidComponent() {
		final CommerceCommentParameter parameter = buildParameter("text", "domain", null, "commentType");
		helper.createComment(parameter, COMMENTTYPE, baseStore, previousItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidCommentType() {
		final CommerceCommentParameter parameter = buildParameter("text", "domain", "component", null);
		helper.createComment(parameter, COMMENTTYPE, baseStore, previousItem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveValidBasestore() {
		final CommerceCommentParameter parameter = buildParameter("text", "domain", "component", "commentType");
		helper.createComment(parameter, COMMENTTYPE, null, previousItem);
	}

	private CommerceCommentParameter buildParameter(final String text, final String domain, final String component,
			final String commentType) {
		return buildParameter(new ItemModel(), new UserModel(), text, domain, component, commentType);
	}

	private CommerceCommentParameter buildParameter(final ItemModel item, final UserModel author, final String text,
			final String domain, final String component, final String commentType)

	{
		final CommerceCommentParameter parameter = new CommerceCommentParameter();
		parameter.setItem(item);
		parameter.getItem().setComments(new ArrayList<CommentModel>());
		parameter.setAuthor(author);
		parameter.setText(text);
		parameter.setDomainCode(domain);
		parameter.setComponentCode(component);
		parameter.setCommentTypeCode(commentType);

		return parameter;
	}

	private <T extends ItemModel> List<T> listStub(final Class<T> clazz, final String... codes) {
		final List<T> models = new ArrayList<T>();
		for (final String code : codes) {
			try {
				final T model = clazz.newInstance();
				setCodeOnItem(model, code);
				models.add(model);
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return models;
	}

	public <T extends ItemModel> void setCodeOnItem(final T obj, final String code) {
		try {
			final Method m = obj.getClass().getMethod("setCode", code.getClass());
			m.invoke(obj, code);
		} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private void setDomain(final DomainModel domain, final List<ComponentModel> componentList,
			final List<CommentTypeModel> typeList) {
		domain.setCommentTypes(typeList);
		for (final ComponentModel component : componentList) {
			component.setDomain(domain);
		}
	}

}
