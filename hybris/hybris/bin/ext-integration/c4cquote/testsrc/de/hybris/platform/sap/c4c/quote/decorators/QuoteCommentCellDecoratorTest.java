/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.c4c.quote.decorators;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteCommentConversionHelper;

/**
 * JUnit for QuoteCommentCellDecorator
 * 
 * @author C5229488
 *
 */
@UnitTest
public class QuoteCommentCellDecoratorTest {

	private static final String COMMENT_INPUT = "620001701|More discount was expected|hybris.erp@c4c.com|10024";
	private static final int COMMENT_POSITION_INT = 1;
	private static final String COMMENT_CODE = "0000ED4";
	private static final Integer COMMENT_POSITION = Integer.valueOf(COMMENT_POSITION_INT);

	@InjectMocks
	private final QuoteCommentCellDecorator decorator = new QuoteCommentCellDecorator();

	@Mock
	private InboundQuoteCommentConversionHelper inboundQuoteCommentConversionHelper;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void decorateTest() {
		final Map<Integer, String> srcLine = new HashMap<Integer, String>();
		srcLine.put(COMMENT_POSITION, COMMENT_INPUT);
		when(inboundQuoteCommentConversionHelper.createHeaderComment("620001701", "More discount was expected",
				"hybris.erp@c4c.com", "10024")).thenReturn(COMMENT_CODE);
		String resultComment = decorator.decorate(COMMENT_POSITION_INT, srcLine);
		Assert.assertNotNull(resultComment);
		Assert.assertEquals(COMMENT_CODE, resultComment);
	}

}
