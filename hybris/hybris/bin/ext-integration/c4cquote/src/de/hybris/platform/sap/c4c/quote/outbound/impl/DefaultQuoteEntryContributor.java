/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.c4c.quote.outbound.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.sap.c4c.quote.constants.C4cquoteConstants;
import de.hybris.platform.sap.c4c.quote.constants.QuoteCsvColumns;
import de.hybris.platform.sap.c4c.quote.constants.QuoteEntryCsvColumns;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.sap.orderexchange.outbound.RawItemContributor;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 *
 */
public class DefaultQuoteEntryContributor implements RawItemContributor<QuoteModel> {

    private static final Logger LOG = Logger.getLogger(DefaultQuoteEntryContributor.class);

    private static final Set<String> COLUMNS = new HashSet<>(Arrays.asList(QuoteCsvColumns.QUOTE_ID,
            QuoteEntryCsvColumns.ENTRY_NUMBER, QuoteEntryCsvColumns.PRODUCT_CODE, QuoteEntryCsvColumns.PRODUCT_NAME,
            QuoteEntryCsvColumns.QUANTITY, QuoteEntryCsvColumns.PRODUCT_BASE_PRICE, QuoteCsvColumns.CURRENCY_CODE,
            QuoteEntryCsvColumns.PRODUCT_TOTAL_PRICE, QuoteEntryCsvColumns.ENTRY_UNIT_CODE,
            QuoteCsvColumns.CREATION_DATE, QuoteEntryCsvColumns.PRODUCT_ENTRY_COMMENT, QuoteCsvColumns.CONDITIONCODE,
            QuoteCsvColumns.LANGUAGE_ISO_CODE));

    @Override
    public Set<String> getColumns() {
        return COLUMNS;
    }

    @Override
    public List<Map<String, Object>> createRows(final QuoteModel quote) {
        final List<AbstractOrderEntryModel> entries = quote.getEntries();
        final List<Map<String, Object>> result = new ArrayList<>();

        for (final AbstractOrderEntryModel entry : entries) {
            validateMandatoryParameters(entry, quote);
            final Map<String, Object> row = new HashMap<>();
            row.put(QuoteCsvColumns.QUOTE_ID, quote.getCode());
            if (null != quote.getCurrency()) {
                row.put(QuoteCsvColumns.CURRENCY_CODE, quote.getCurrency().getSapCode());
            }
            row.put(QuoteCsvColumns.CREATION_DATE, entry.getCreationtime());
            row.put(QuoteEntryCsvColumns.ENTRY_NUMBER, entry.getEntryNumber());
            row.put(QuoteEntryCsvColumns.QUANTITY, entry.getQuantity());
            row.put(QuoteEntryCsvColumns.PRODUCT_CODE, entry.getProduct().getCode());
            final UnitModel unit = entry.getUnit();
            if (unit != null) {
                row.put(QuoteEntryCsvColumns.ENTRY_UNIT_CODE, unit.getCode());
            } else {
                LOG.info("Could not determine unit code for product " + entry.getProduct().getCode() + "as entry "
                        + entry.getEntryNumber() + "of order " + quote.getCode());
            }
            final String language = quote.getLocale() != null ? quote.getLocale()
                    : quote.getStore().getDefaultLanguage().getIsocode();
            final String shortText = determineItemShortText(entry, language);
            row.put(QuoteEntryCsvColumns.PRODUCT_NAME, shortText);
            row.put(QuoteEntryCsvColumns.PRODUCT_BASE_PRICE, entry.getBasePrice());
            row.put(QuoteEntryCsvColumns.PRODUCT_TOTAL_PRICE, entry.getTotalPrice());
            row.put(QuoteEntryCsvColumns.PRODUCT_ENTRY_COMMENT, getComment(entry.getComments()));
            row.put(QuoteCsvColumns.CONDITIONCODE, getGrossPriceCondition(quote));
            row.put(QuoteCsvColumns.LANGUAGE_ISO_CODE,
                    quote.getStore().getDefaultLanguage().getIsocode().toUpperCase());
            result.add(row);
        }
        return result;
    }

    private void validateMandatoryParameters(AbstractOrderEntryModel entry, QuoteModel quote) {
        validateParameterNotNullStandardMessage(QuoteCsvColumns.CREATION_DATE, entry.getCreationtime());
        validateParameterNotNullStandardMessage(QuoteEntryCsvColumns.ENTRY_NUMBER, entry.getEntryNumber());
        validateParameterNotNullStandardMessage(QuoteEntryCsvColumns.QUANTITY, entry.getQuantity());
        validateParameterNotNullStandardMessage(QuoteEntryCsvColumns.PRODUCT_CODE, entry.getProduct().getCode());
        validateParameterNotNullStandardMessage(QuoteEntryCsvColumns.ENTRY_UNIT_CODE, entry.getUnit().getCode());
        final String language = quote.getLocale() != null ? quote.getLocale()
                : quote.getStore().getDefaultLanguage().getIsocode();
        final String shortText = determineItemShortText(entry, language);
        validateParameterNotNullStandardMessage(QuoteEntryCsvColumns.PRODUCT_NAME, shortText);
        validateParameterNotNullStandardMessage(QuoteEntryCsvColumns.PRODUCT_BASE_PRICE, entry.getBasePrice());
        validateParameterNotNullStandardMessage(QuoteEntryCsvColumns.PRODUCT_TOTAL_PRICE, entry.getTotalPrice());
        validateParameterNotNullStandardMessage(QuoteCsvColumns.CONDITIONCODE, getGrossPriceCondition(quote));
        validateParameterNotNullStandardMessage(QuoteCsvColumns.LANGUAGE_ISO_CODE,
                quote.getStore().getDefaultLanguage().getIsocode());
    }

    protected String determineItemShortText(final AbstractOrderEntryModel item, final String language) {
        final String shortText = item.getProduct().getName(new java.util.Locale(language));
        return shortText == null ? "" : shortText;
    }

    /**
     * Append all comments in a quote to single comment separated by line
     */
    private String getComment(final List<CommentModel> comments) {
        final StringBuilder sb = new StringBuilder();
        if (comments != null && !comments.isEmpty()) {
            for (final CommentModel c : comments) {
                sb.append(c.getText());
                sb.append(Config.getParameter(C4cquoteConstants.C4CQUOTE_COMMENTS_LINE_SEPARATOR));
            }
        }
        return sb.toString();
    }

    /**
     * returns gross price(pricing condition) attached to base store
     * configuration
     */
    private String getGrossPriceCondition(final QuoteModel quoteModel) {
        final SAPConfigurationModel configurationModel = quoteModel.getStore().getSAPConfiguration();
        return configurationModel.getSaporderexchange_itemPriceConditionType();
    }
}
