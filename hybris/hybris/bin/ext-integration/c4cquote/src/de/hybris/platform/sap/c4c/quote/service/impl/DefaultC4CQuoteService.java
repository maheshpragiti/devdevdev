/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.c4c.quote.service.impl;

import de.hybris.platform.commerceservices.constants.GeneratedCommerceServicesConstants.Enumerations.QuoteState;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceQuoteService;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.sap.orderexchange.outbound.SendToDataHubHelper;

/**
 * Default C4C implementation.
 */
public class DefaultC4CQuoteService extends DefaultCommerceQuoteService {
    private SendToDataHubHelper<QuoteModel> sendQuoteToDataHubHelper;

    @Override
    public void cancelQuote(final QuoteModel quoteModel, final UserModel userModel) {
        if (quoteModel.getState().equals(QuoteState.BUYER_OFFER)) {
            super.cancelQuote(quoteModel, userModel);
            sendQuoteToDataHubHelper.createAndSendRawItem(quoteModel);
        } else {
            super.cancelQuote(quoteModel, userModel);
        }
    }

    public SendToDataHubHelper<QuoteModel> getSendQuoteToDataHubHelper() {
        return sendQuoteToDataHubHelper;
    }

    public void setSendQuoteToDataHubHelper(final SendToDataHubHelper<QuoteModel> sendQuoteToDataHubHelper) {
        this.sendQuoteToDataHubHelper = sendQuoteToDataHubHelper;
    }
}
