/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.c4c.quote.constants;

/**
 * Global class for all C4cquote constants. You can add global constants for your extension into this class.
 */
public final class C4cquoteConstants extends GeneratedC4cquoteConstants
{
	public static final String EXTENSIONNAME = "c4cquote";
	
	public static final String IGNORE = "<ignore>";
	
	public static final String PLATFORM_LOGO_CODE = "c4cquotePlatformLogo";
	
	public static final String C4CQUOTE_COMMENTS_LINE_SEPARATOR = "c4cquote.comments.lineSeparator";
	
	public static final String COMMENTS_CODE_SEPARATOR=",";


	private C4cquoteConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

}
