package de.hybris.platform.sap.c4c.quote.decorators;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.core.Registry;
import de.hybris.platform.sap.c4c.quote.constants.C4cquoteConstants;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteCommentConversionHelper;
import de.hybris.platform.util.CSVCellDecorator;

/**
 * Decorator class to convert inbound quote item comments to comment model
 * 
 * @author C5229488
 *
 */
public class QuoteEntryCommentCellDecorator implements CSVCellDecorator {

	private static final Logger LOG = LoggerFactory.getLogger(QuoteCommentCellDecorator.class);
	private InboundQuoteCommentConversionHelper inboundQuoteCommentConversionHelper = (InboundQuoteCommentConversionHelper) Registry
			.getApplicationContext().getBean("inboundQuoteCommentConversionHelper");

	@Override
	public String decorate(int position, Map<Integer, String> impexLine) {
		LOG.info("Decorating item level comments information from canonical into comments models.");
		final String comments = impexLine.get(Integer.valueOf(position));
		String result = null;
		if (comments != null && !comments.equals(C4cquoteConstants.IGNORE)) {
			final List<String> commentData = Arrays.asList(StringUtils.split(comments, '|'));
			final String quoteId = commentData.get(0);
			final String commentText = commentData.get(1);
			final Integer entryNumber = Integer.parseInt(commentData.get(2));
			final String commentType = commentData.get(3);
				result = getInboundQuoteCommentConversionHelper().createItemComment(quoteId, commentText, entryNumber,commentType);
		}
		return result;
	}

	public InboundQuoteCommentConversionHelper getInboundQuoteCommentConversionHelper() {
		return inboundQuoteCommentConversionHelper;
	}

	public void setInboundQuoteCommentConversionHelper(
			InboundQuoteCommentConversionHelper inboundQuoteCommentConversionHelper) {
		this.inboundQuoteCommentConversionHelper = inboundQuoteCommentConversionHelper;
	}

}
