/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.sap.c4c.quote.inbound.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.comments.model.CommentTypeModel;
import de.hybris.platform.comments.model.ComponentModel;
import de.hybris.platform.comments.model.DomainModel;
import de.hybris.platform.comments.services.CommentService;
import de.hybris.platform.commerceservices.service.data.CommerceCommentParameter;
import de.hybris.platform.commerceservices.util.CommerceCommentUtils;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.QuoteEntryModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.sap.c4c.quote.constants.C4cquoteConstants;
import de.hybris.platform.sap.c4c.quote.enums.C4CCommentType;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteCommentConversionHelper;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;

/**
 * Default implementation for InboundQuoteCommentConversionHelper
 * 
 * @author C5229488
 *
 */
public class DefaultInboundQuoteCommentConversionHelper implements InboundQuoteCommentConversionHelper {
	
	private ModelService modelService;
	private CommentService commentService;
	private QuoteService quoteService;
	private UserService userService;
	private EnumerationService enumerationService;

	public EnumerationService getEnumerationService() {
		return enumerationService;
	}

	public void setEnumerationService(EnumerationService enumerationService) {
		this.enumerationService = enumerationService;
	}

	public QuoteService getQuoteService() {
		return quoteService;
	}

	public void setQuoteService(QuoteService quoteService) {
		this.quoteService = quoteService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public ModelService getModelService() {
		return modelService;
	}

	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}

	@Override
	public String createHeaderComment(String quoteId, String text, String userId, String commentType) {
		QuoteModel quote = getQuoteService().getCurrentQuoteForCode(quoteId);

		return createComment(
				CommerceCommentUtils.buildQuoteCommentParameter(quote, getUserService().getUserForUID(userId), text),
				commentType, quote.getStore(), getQuoteHistory(quote));
	}

	@Override
	public String createItemComment(String quoteId, String text, Integer entryNumber, String commentType) {
		QuoteModel quote = getQuoteService().getCurrentQuoteForCode(quoteId);
		QuoteEntryModel entry = null;
		if (quote != null) {
			entry = (QuoteEntryModel) getEntryForEntryNumber(quote, (int) entryNumber);
		}
		QuoteEntryModel previousEntry = (QuoteEntryModel) getEntryForEntryNumber(getQuoteHistory(quote),
				(int) entryNumber);
		return createComment(CommerceCommentUtils.buildQuoteEntryCommentParameter(entry, quote.getUser(), text),
				commentType, quote.getStore(), previousEntry);
	}

	/**
	 * To get previos logs of comments
	 * 
	 * @param quoteId
	 * @param quote
	 * @return
	 */
	private QuoteModel getQuoteHistory(QuoteModel quote) {
		Integer previousVersion = null;
		QuoteModel history = null;
		if (quote.getVersion() == 1) {
			history = quote;
		} else {
			previousVersion = quote.getVersion() - 1;
			history = getQuoteService().getQuoteForCodeAndVersion(quote.getCode(), previousVersion);
		}
		return history;
	}

	/**
	 * Method to create a comment model with given parameters
	 * 
	 * @param parameter
	 * @param commentType
	 * @param baseStore
	 * @return List of comment codes
	 */
	protected String createComment(final CommerceCommentParameter parameter, final String commentType,
			BaseStoreModel baseStore, ItemModel item) {
		String result = null;
		CommentModel commentModel = null;
		StringBuilder commentCodes = new StringBuilder();
		validateCommentParameter(parameter);

		Set<C4CCommentType> commentTypes = baseStore.getSAPConfiguration().getC4cCommentTypes();
		if (commentTypes.contains(getEnumerationService().getEnumerationValue(C4CCommentType.class, commentType))) {
			final DomainModel domainModel = getCommentService().getDomainForCode(parameter.getDomainCode());
			final ComponentModel componentModel = getCommentService().getComponentForCode(domainModel,
					parameter.getComponentCode());
			final CommentTypeModel commentTypeModel = getCommentService().getCommentTypeForCode(componentModel,
					parameter.getCommentTypeCode());
			commentModel = getModelService().create(CommentModel.class);
			commentModel.setText(parameter.getText());
			commentModel.setAuthor(parameter.getAuthor());
			commentModel.setComponent(componentModel);
			commentModel.setCommentType(commentTypeModel);
			getModelService().save(commentModel);
		}
		// need to redeclare list as default list on model is immutable
		if (item != null) {
			List<CommentModel> comments = new ArrayList<CommentModel>(item.getComments());
			ItemModel currentItem = parameter.getItem();
			List<CommentModel> recentComments = new ArrayList<CommentModel>(currentItem.getComments());
			if (CollectionUtils.isNotEmpty(recentComments)) {
				if (commentModel != null) {
					recentComments.add(commentModel);
					currentItem.setComments(recentComments);
				}
			} else {
				if (commentModel != null) {
					comments.add(commentModel);
				}
				currentItem.setComments(comments);
			}
			getModelService().save(currentItem);
			final List<CommentModel> updatedComments = new ArrayList<CommentModel>(currentItem.getComments());
			if (CollectionUtils.isNotEmpty(updatedComments)) {
				for (CommentModel comment : updatedComments) {
					commentCodes = commentCodes.append(comment.getCode()).append(C4cquoteConstants.COMMENTS_CODE_SEPARATOR);
				}
			}
		}
		result = commentCodes.toString();
		return result;
	}

	/**
	 * Method to get quoteEnrty model for given quote and entry number
	 * 
	 * @param order
	 * @param number
	 * @return
	 */
	protected AbstractOrderEntryModel getEntryForEntryNumber(final AbstractOrderModel order, final int number) {
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (entries != null && !entries.isEmpty()) {
			final Integer requestedEntryNumber = Integer.valueOf(number);
			for (final AbstractOrderEntryModel entry : entries) {
				if (entry != null && requestedEntryNumber.equals(entry.getEntryNumber())) {
					return entry;
				}
			}
		}
		return null;
	}

	/**
	 * Method to validate the CommerceComment parameters to generate a quote
	 * comment
	 * 
	 * @param parameter
	 */
	protected void validateCommentParameter(final CommerceCommentParameter parameter) {
		ServicesUtil.validateParameterNotNullStandardMessage("parameter", parameter);
		ServicesUtil.validateParameterNotNullStandardMessage("author", parameter.getAuthor());
		checkArgument(isNotEmpty(parameter.getText()), "Text cannot not be empty");
		checkArgument(isNotEmpty(parameter.getDomainCode()), "Domain cannot not be empty");
		checkArgument(isNotEmpty(parameter.getComponentCode()), "Component cannot not be empty");
		checkArgument(isNotEmpty(parameter.getCommentTypeCode()), "CommentType cannot not be empty");
	}

}
