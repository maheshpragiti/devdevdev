/*
 * [y] hybris Platform
 *
<<<<<<< 51ee5e0afe1b2fea00354c6d2abb2ea043e70855
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
=======
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
>>>>>>> Fix for comment publication on hybris side
 */
package de.hybris.platform.sap.c4c.quote.inbound;

import de.hybris.platform.core.model.order.QuoteModel;

/**
 * Helper Class for updating the quote version in inbound scenarios
 * 
 * @author C5229488
 */
public interface InboundQuoteVersionControlHelper {

	/**
	 * Get quote for code
	 * 
	 * @param code
	 * @return quote
	 */
	QuoteModel getQuoteforCode(String code);

	/**
	 * Get updated version for given quote
	 * 
	 * @param quote
	 * @return
	 */
	Integer getUpdatedVersionNumber(QuoteModel quote);
}
