/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.c4c.quote.constants;

/**
 * Constants for Quote Csv
 */
public class QuoteCsvColumns
{		
	public static final String LANGUAGE_ISO_CODE = "languageIsoCode";
	public static final String QUOTE_ID = "quoteId";
	public static final String CREATION_DATE = "creationDate";
	public static final String QUOTE_TYPE = "quoteType";
	public static final String QUOTE_NAME = "quoteName";
	public static final String PARTNER_ROLE_CODE = "partnerRoleCode";
	public static final String PARTNER_CODE = "partnerCode";
	public static final String DESCRIPTION = "description";
	public static final String CURRENCY_CODE = "currencyCode";
	public static final String TOTAL_AMOUNT = "totalAmount";
	public static final String TOTAL_QUANTITY = "totalQuantity";
	public static final String COMMENT = "comment";
	public static final String CONDITIONCODE = "conditionCode";
	public static final String BASESTORE = "baseStore";
	public static final String STATUS = "status";
	
	private QuoteCsvColumns(){
		
	}
}
