/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.assistedserviceyprofilefacades.data.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.assistedserviceyprofilefacades.YProfileAffinityFacade;
import de.hybris.platform.assistedserviceyprofilefacades.data.DeviceAffinityData;
import de.hybris.platform.assistedserviceyprofilefacades.data.DeviceFingerprintAffinityData;
import de.hybris.platform.assistedserviceyprofilefacades.data.LoyaltyData;
import de.hybris.platform.assistedserviceyprofilefacades.data.ProfileLoyaltyHistoryData;
import de.hybris.platform.assistedserviceyprofilefacades.data.ProfileLoyaltyStatisticData;
import de.hybris.platform.assistedserviceyprofilefacades.data.TechnologyUsedData;
import de.hybris.platform.assistedserviceyprofilefacades.data.YProfileSummaryData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ConfigurationException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@UnitTest
public class ProfileSummaryProviderTest
{
	private static final String PHONE = "Phone";
	private static final String TABLET = "TABLET";
	private static final String DEVICE_VIEW_COUNT_10 = "10";
	private static final String COMPUTER = "Computer";
	private static final String WINDOWS = "Windows";
	private static final String CHROME = "Chrome";
	private static final String MISSING_CONFIG_VALUE_FOR_MAPPING = "2";
	private static final String LOYALTY_BALANCE_250 = "250";
	private static final String ACTIVITY_SCORE_10 = "10";
	private static final String EXPECTED_USER = "EXPECTED_USER";

	@InjectMocks
	private final ProfileSummaryProvider provider = new ProfileSummaryProvider();
	@Mock
	private YProfileAffinityFacade yProfileAffinityFacade;
	@Mock
	private UserService userService;
	@Mock
	private UserModel user;

	private NavigableMap<Integer, String> activityScoreMappingMap;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(user.getName()).thenReturn(EXPECTED_USER);

		prepareActivityScoreMappings();
		provider.setActivityScoreMappingMap(activityScoreMappingMap);
	}

	@Test
	public void getModelTest()
	{
		Mockito.when(yProfileAffinityFacade.getProfileLoyaltyStatisticAffinity(Mockito.any()))
				.thenReturn(prepareLoyaltyStatisticDataList());

		Mockito.when(yProfileAffinityFacade.getProfileLoyaltyHistoryAffinity(Mockito.any()))
				.thenReturn(prepareHistoricalDataList());

		Mockito.when(yProfileAffinityFacade.getDeviceFingerprintAffinities(Mockito.any()))
				.thenReturn(prepareDeviceFingerprintDataList());

		Mockito.when(yProfileAffinityFacade.getDeviceAffinities(Mockito.any())).thenReturn(prepareDeviceAfinityDataList());

		final YProfileSummaryData data = provider.getModel(null);

		Assert.assertNotNull(data);
		Assert.assertEquals(EXPECTED_USER, data.getName());
		Assert.assertNotNull(data.getCustomerLoyaltyData());

		checkLoyaltyData(data);
		checkDeviceData(data);
	}

	protected void checkDeviceData(final YProfileSummaryData data)
	{
		Assert.assertTrue(data.getTechnologyUsedData().size() == 2);
		final Iterator<TechnologyUsedData> technologyUsedDataIterator = data.getTechnologyUsedData().iterator();
		final TechnologyUsedData computerData = technologyUsedDataIterator.next();
		Assert.assertEquals(CHROME, computerData.getBrowser());
		Assert.assertEquals(WINDOWS, computerData.getOperatingSystem());
		Assert.assertEquals(COMPUTER, computerData.getDevice());

		final TechnologyUsedData tabletData = technologyUsedDataIterator.next();
		Assert.assertEquals(null, tabletData.getBrowser());
		Assert.assertEquals(null, tabletData.getOperatingSystem());
		Assert.assertEquals(TABLET, tabletData.getDevice());
	}

	protected void checkLoyaltyData(final YProfileSummaryData data)
	{
		final LoyaltyData loyaltyData = data.getCustomerLoyaltyData();
		Assert.assertEquals((activityScoreMappingMap.floorEntry(Integer.valueOf(ACTIVITY_SCORE_10)).getValue()),
				loyaltyData.getActivityScore());
		Assert.assertEquals(LOYALTY_BALANCE_250, loyaltyData.getCurrentLoyaltyBalance());
		Assert.assertTrue(loyaltyData.isGoldBadgeActive());
		Assert.assertFalse(loyaltyData.isSilverBadgeActive());
		Assert.assertFalse(loyaltyData.isPlatinumBadgeActive());
	}

	@Test(expected = ConfigurationException.class)
	public void getActivityScoreTestForMisingConfiguration()
	{
		final ProfileLoyaltyStatisticData data = new ProfileLoyaltyStatisticData();
		data.setActivityScore(MISSING_CONFIG_VALUE_FOR_MAPPING);
		provider.getActivityScore(data);
	}

	@Test
	public void populateLoaltyDataMissingDataTest()
	{
		final LoyaltyData loyaltyData = provider.populateLoaltyData(Collections.EMPTY_LIST, Collections.EMPTY_LIST);

		Assert.assertNull(loyaltyData.getActivityScore());
		Assert.assertNull(loyaltyData.getCurrentLoyaltyBalance());
		Assert.assertFalse(loyaltyData.isGoldBadgeActive());
		Assert.assertFalse(loyaltyData.isSilverBadgeActive());
		Assert.assertFalse(loyaltyData.isPlatinumBadgeActive());
	}

	protected void prepareActivityScoreMappings()
	{
		activityScoreMappingMap = new TreeMap<>();
		activityScoreMappingMap.put(Integer.valueOf(10), "low");
		activityScoreMappingMap.put(Integer.valueOf(20), "medium");
		activityScoreMappingMap.put(Integer.valueOf(30), "max");
	}

	protected List<ProfileLoyaltyStatisticData> prepareLoyaltyStatisticDataList()
	{
		final List<ProfileLoyaltyStatisticData> statisticDataList = new ArrayList<>();
		final ProfileLoyaltyStatisticData statisticData = new ProfileLoyaltyStatisticData();
		statisticData.setActivityScore(ACTIVITY_SCORE_10);
		statisticDataList.add(statisticData);
		return statisticDataList;
	}

	protected List<ProfileLoyaltyHistoryData> prepareHistoricalDataList()
	{
		final List<ProfileLoyaltyHistoryData> historyDataList = new ArrayList<>();
		final ProfileLoyaltyHistoryData profileLoyaltyHistoryData = new ProfileLoyaltyHistoryData();
		profileLoyaltyHistoryData.setCurrentLoyaltyBalance(LOYALTY_BALANCE_250);
		historyDataList.add(profileLoyaltyHistoryData);
		return historyDataList;
	}

	protected List<DeviceFingerprintAffinityData> prepareDeviceFingerprintDataList()
	{
		final List<DeviceFingerprintAffinityData> fingerprintAffinityDataList = new ArrayList<>();
		fingerprintAffinityDataList.add(createFingerprintAffinityData(CHROME, WINDOWS, COMPUTER));
		fingerprintAffinityDataList.add(createFingerprintAffinityData(null, null, TABLET));
		fingerprintAffinityDataList.add(createFingerprintAffinityData(CHROME, null, TABLET));
		fingerprintAffinityDataList.add(createFingerprintAffinityData(CHROME, WINDOWS, null));
		return fingerprintAffinityDataList;
	}

	protected DeviceFingerprintAffinityData createFingerprintAffinityData(final String browser, final String os,
			final String device)
	{
		final DeviceFingerprintAffinityData fingerprintAffinityData = new DeviceFingerprintAffinityData();
		fingerprintAffinityData.setBrowser(browser);
		fingerprintAffinityData.setOperatingSystem(os);
		fingerprintAffinityData.setDevice(device);
		return fingerprintAffinityData;
	}

	protected final List<DeviceAffinityData> prepareDeviceAfinityDataList()
	{
		final List<DeviceAffinityData> deviceAffinityDataList = new ArrayList<>();
		deviceAffinityDataList.add(createDeviceAffinityData(COMPUTER, DEVICE_VIEW_COUNT_10));
		deviceAffinityDataList.add(createDeviceAffinityData(TABLET, DEVICE_VIEW_COUNT_10));
		deviceAffinityDataList.add(createDeviceAffinityData(PHONE, DEVICE_VIEW_COUNT_10));
		return deviceAffinityDataList;
	}

	protected DeviceAffinityData createDeviceAffinityData(final String device, final String count)
	{
		final DeviceAffinityData deviceAffinityData = new DeviceAffinityData();
		deviceAffinityData.setDevice(device);
		deviceAffinityData.setViewCount(count);
		return deviceAffinityData;
	}
}
