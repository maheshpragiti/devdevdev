/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.assistedserviceyprofilefacades.data.provider;

import static java.util.Map.Entry.comparingByValue;

import de.hybris.platform.assistedservicefacades.customer360.FragmentModelProvider;
import de.hybris.platform.assistedserviceyprofilefacades.YProfileAffinityFacade;
import de.hybris.platform.assistedserviceyprofilefacades.data.CustomerLoyaltyParameterData;
import de.hybris.platform.assistedserviceyprofilefacades.data.DeviceAffinityData;
import de.hybris.platform.assistedserviceyprofilefacades.data.DeviceAffinityParameterData;
import de.hybris.platform.assistedserviceyprofilefacades.data.DeviceFingerprintAffinityData;
import de.hybris.platform.assistedserviceyprofilefacades.data.DeviceFingerprintAffinityParameterData;
import de.hybris.platform.assistedserviceyprofilefacades.data.LoyaltyData;
import de.hybris.platform.assistedserviceyprofilefacades.data.ProfileLoyaltyHistoryData;
import de.hybris.platform.assistedserviceyprofilefacades.data.ProfileLoyaltyStatisticData;
import de.hybris.platform.assistedserviceyprofilefacades.data.TechnologyUsedData;
import de.hybris.platform.assistedserviceyprofilefacades.data.YProfileSummaryData;
import de.hybris.platform.servicelayer.exceptions.ConfigurationException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;


/**
 * YProfile Summary fragment provider
 */
public class ProfileSummaryProvider implements FragmentModelProvider<YProfileSummaryData>
{
	private int deviceFetchLimit = 100;
	private int deviceLimit = 3;
	private static final int LOYALTY_LIMIT = 1;

	private YProfileAffinityFacade yProfileAffinityFacade;
	private UserService userService;
	private NavigableMap<Integer, String> activityScoreMappingMap;

	@Override
	public YProfileSummaryData getModel(final Map<String, String> parameters)
	{
		final YProfileSummaryData yProfileSummaryData = new YProfileSummaryData();
		yProfileSummaryData.setName(getUserService().getCurrentUser().getName());
		yProfileSummaryData.setTechnologyUsedData(prepareDeviceData());
		yProfileSummaryData.setCustomerLoyaltyData(prepareProfileLoyaltyData());
		return yProfileSummaryData;
	}

	protected List<TechnologyUsedData> prepareDeviceData()
	{
		final List<TechnologyUsedData> technologyUsedDataList = new ArrayList<>(deviceLimit);
		final Map<String, TechnologyUsedData> mostPopularTechnologyByDeviceMap = getTechnologyUsedByDevice();
		final Map<String, Integer> usedDeviceCounts = getUsedDeviceCounts();
		final int totalUsedDevice = getTotalUsedCount(usedDeviceCounts);

		usedDeviceCounts.entrySet().stream().sorted(comparingByValue(Collections.reverseOrder())).limit(deviceLimit)
				.collect(Collectors.toList()).forEach(entry -> {
					final TechnologyUsedData technologyUsedData = mostPopularTechnologyByDeviceMap.get(entry.getKey());
					if (technologyUsedData != null)
					{
						technologyUsedData.setPercentage(entry.getValue() * 100 / totalUsedDevice);
						technologyUsedDataList.add(technologyUsedData);
					}
				});

		return technologyUsedDataList;
	}

	protected Map<String, Integer> getUsedDeviceCounts()
	{
		final DeviceAffinityParameterData deviceAffinityParameterData = new DeviceAffinityParameterData();
		deviceAffinityParameterData.setSizeLimit(deviceFetchLimit);
		// get all device nodes up to recent deviceFetchLimit
		final List<DeviceAffinityData> deviceTypeList = getyProfileAffinityFacade()
				.getDeviceAffinities(deviceAffinityParameterData);

		// create Map Device -> Number of usage
		final Map<String, Integer> deviceUsedCounts = new HashMap<>();
		// can't use Java8 streams here because of total which should be also counted in one loop
		for (final DeviceAffinityData deviceAffinityData : deviceTypeList)
		{
			final Integer usedCount = Integer.valueOf(deviceAffinityData.getViewCount());
			deviceUsedCounts.computeIfPresent(deviceAffinityData.getDevice(),
					(device, totalDeviceCount) -> totalDeviceCount + usedCount);
			deviceUsedCounts.putIfAbsent(deviceAffinityData.getDevice(), usedCount);
		}
		return deviceUsedCounts;
	}

	protected Map<String, TechnologyUsedData> getTechnologyUsedByDevice()
	{
		final DeviceFingerprintAffinityParameterData deviceFingerprintAffinityParameterData = new DeviceFingerprintAffinityParameterData();
		deviceFingerprintAffinityParameterData.setSizeLimit(deviceFetchLimit);
		// get all device fingerprint nodes up to recent deviceFetchLimit
		final List<DeviceFingerprintAffinityData> deviceFingerprintList = getyProfileAffinityFacade()
				.getDeviceFingerprintAffinities(deviceFingerprintAffinityParameterData);

		// create Map Device -> latest OS + browser for this device
		final Map<String, TechnologyUsedData> mostPopularTechnologyByDevice = new HashMap<>();
		deviceFingerprintList.forEach(deviceFingerprintAffinityData -> {
			if (deviceFingerprintAffinityData.getDevice() != null
					&& !mostPopularTechnologyByDevice.containsKey(deviceFingerprintAffinityData.getDevice()))
			{
				final TechnologyUsedData technologyUsedData = new TechnologyUsedData();
				technologyUsedData.setBrowser(deviceFingerprintAffinityData.getBrowser());
				technologyUsedData.setOperatingSystem(deviceFingerprintAffinityData.getOperatingSystem());
				technologyUsedData.setDevice(deviceFingerprintAffinityData.getDevice());
				mostPopularTechnologyByDevice.put(deviceFingerprintAffinityData.getDevice(), technologyUsedData);
			}
		});
		return mostPopularTechnologyByDevice;
	}

	protected int getTotalUsedCount(final Map<String, Integer> deviceUsedCounts)
	{
		return deviceUsedCounts.values().stream().mapToInt(Integer::intValue).sum();
	}

	protected LoyaltyData prepareProfileLoyaltyData()
	{
		final CustomerLoyaltyParameterData customerLoyaltyParameterData = new CustomerLoyaltyParameterData();
		customerLoyaltyParameterData.setSizeLimit(LOYALTY_LIMIT);

		final List<ProfileLoyaltyStatisticData> profileLoyaltyStatisticDataList = getyProfileAffinityFacade()
				.getProfileLoyaltyStatisticAffinity(customerLoyaltyParameterData);

		final List<ProfileLoyaltyHistoryData> profileLoyaltyHistoryDataList = getyProfileAffinityFacade()
				.getProfileLoyaltyHistoryAffinity(customerLoyaltyParameterData);

		return populateLoaltyData(profileLoyaltyStatisticDataList, profileLoyaltyHistoryDataList);
	}

	protected LoyaltyData populateLoaltyData(final List<ProfileLoyaltyStatisticData> profileLoyaltyStatisticData,
			final List<ProfileLoyaltyHistoryData> profileLoyaltyHistoryData)
	{
		final LoyaltyData loyaltyData = new LoyaltyData();

		if (profileLoyaltyStatisticData.iterator().hasNext())
		{
			final ProfileLoyaltyStatisticData statisticData = profileLoyaltyStatisticData.iterator().next();
			loyaltyData.setActivityScore(getActivityScore(statisticData));
		}

		if (profileLoyaltyHistoryData.iterator().hasNext())
		{
			final ProfileLoyaltyHistoryData historyData = profileLoyaltyHistoryData.iterator().next();
			loyaltyData.setCurrentLoyaltyBalance(historyData.getCurrentLoyaltyBalance());
		}

		//TODO populate properties by enricher graph
		loyaltyData.setGoldBadgeActive(loyaltyData.getActivityScore() != null ? true : false);
		loyaltyData.setSilverBadgeActive(false);
		loyaltyData.setPlatinumBadgeActive(false);
		return loyaltyData;
	}

	String getActivityScore(final ProfileLoyaltyStatisticData statisticData)
	{
		final Entry<Integer, String> scoreData = activityScoreMappingMap
				.floorEntry(Integer.valueOf(statisticData.getActivityScore()));

		if (scoreData == null)
		{
			throw new ConfigurationException(
					"Broken spring configuration for profile activity score mapping. Check activityScoreMapping bean definition.");
		}
		return scoreData.getValue();
	}

	protected YProfileAffinityFacade getyProfileAffinityFacade()
	{
		return yProfileAffinityFacade;
	}

	@Required
	public void setyProfileAffinityFacade(final YProfileAffinityFacade yProfileAffinityFacade)
	{
		this.yProfileAffinityFacade = yProfileAffinityFacade;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public int getDeviceFetchLimit()
	{
		return deviceFetchLimit;
	}

	public void setDeviceFetchLimit(final int deviceFetchLimit)
	{
		this.deviceFetchLimit = deviceFetchLimit;
	}

	public void setDeviceLimit(final int deviceLimit)
	{
		this.deviceLimit = deviceLimit;
	}

	public int getDeviceLimit()
	{
		return deviceLimit;
	}

	protected NavigableMap<Integer, String> getActivityScoreMappingMap()
	{
		return activityScoreMappingMap;
	}

	@Required
	public void setActivityScoreMappingMap(final NavigableMap<Integer, String> activityScoreMappingMap)
	{
		this.activityScoreMappingMap = activityScoreMappingMap;
	}
}