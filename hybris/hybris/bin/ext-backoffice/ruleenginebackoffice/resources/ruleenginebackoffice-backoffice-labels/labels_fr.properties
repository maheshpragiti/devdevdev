# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------

ruleengine=Moteur de règles
ruleenginebackoffice_Rules=Règles
ruleenginebackoffice_RulesUpdated_success=Tous les modules de règles actifs ont été mis à jour : {0}.
ruleenginebackoffice_RulesUpdated_error=Echec de la mise à jour de ces modules de règles actifs : {0} \nDétails de l'erreur : {1}

tab.droolsruleengine.droolsrule.main=Propriétés
sec.droolsruleengine.droolsrule.runtime.attributes=Attributs d'exécution de règle
sec.droolsruleengine.droolsrule.runtime.attributes.description=Les attributs indiqués ici peuvent être modifiés lors de l'exécution (ils ne nécessitent donc aucun déploiement de règle).
panel.droolsruleengine.droolsrule.runtimedata=Objets de données d'exécution de règle
attribute.droolsruleengine.droolsrule.runtimeData.description=Les objets indiqués ici ont permis de définir la configuration de l'exécution de la règle. Ils peuvent être modifiés lors de l'exécution et ne nécessitent aucun déploiement de règle. 
sec.droolsruleengine.droolsrule.attributes=Attributs DRL
sec.droolsruleengine.droolsrule.drl=DRL
sec.droolsruleengine.droolsrule.drl.description=Les modifications au niveau du DRL nécessitent un redéploiement de la règle.
sec.configuration.ruleengine.parameters=Paramètres
tab.configuration.abstractrule=Propriétés de la règle
sec.configuration.abstractrule.details=Détails
sec.configuration.abstractrule.rulemanagement=Gestion des règles
sec.configuration.abstractrule.others=Autres
sec.configuration.abstractrule.ruleadministration=Administration des règles
sec.configuration.sourcerule.conditions=Conditions
sec.configuration.sourcerule.actions=Actions
tab.configuration.abstractruletemplate=Propriétés du modèle de règle
sec.configuration.abstractruletemplate.details=Détails
sec.configuration.abstractruletemplate.rulemanagement=Gestion des modèles de règles
sec.configuration.abstractruletemplate.others=Autres
sec.configuration.abstractruletemplate.ruletemplateadministration=Administration des modèles de règles
tab.configuration.conditionsandactions=Conditions et actions

ruleengine.abstractrule.startdate=Date/Heure de début (fuseau horaire - UTC/GMT)
ruleengine.abstractrule.enddate=Date/Heure de fin (fuseau horaire - UTC/GMT)

action.rulecompileallaction.name=Publier la règle
create.promotion.rule.from.template=Créer une règle à l’aide de ce modèle
hmc_typenode_drools_rules=Règles Drools
hmc_typenode_drools_kie_module=Modules Drools
tab.configuration.abstractrulesmodule=Modules
sec.configuration.abstractrulesmodule.details=Détails du module

create.drools.rule.label=Règles Drools
create.rule.group.label=Groupe de règles Drools

create.rule.label=Règle source

action.create.rule.ok=OK
action.create.rule.cancel=Annuler
action.create.rule.with.code=Code de la nouvelle règle

action.rule.for.module.selectModule=Sélectionner un module
action.rule.for.module.pleaseSelect=Veuillez sélectionner
action.rule.for.module.ok=OK
action.rule.for.module.cancel=Annuler

action.rules.module.sync.selectModule=Sélectionner un module pour la synchronisation
action.rules.module.sync.pleaseSelect=Module cible
action.rules.module.sync.ok=Synchroniser
action.rules.module.sync.cancel=Annuler

rules.module.sync.start=La synchronisation du module {0} vers {1} a commencé...
rules.module.sync.success=Le module {0} a bien été synchronisé vers {1}.

rules.module.name=Nom du module de règles

abstractrule.availability=Disponibilité

action.clone.rule.cancel=Annuler
action.clone.rule.ok=OK
action.clone.rule.with.code=Code de la nouvelle règle

rule.clone.succeed=La règle {0} a été clonée.
rule.archive.succeed=La règle {0} a été archivée.

rule.compileandpublish.swap.completed=La mise à jour du module du moteur de règles {0} est terminée. La version de module déployée a été modifiée de [{1}] en [{2}].
rule.compileandpublish.swap.failed=La mise à jour du module du Moteur de règles {0} a échoué avec le motif [{1}].

rules.compileandpublish.inprogress=La compilation et la publication des règles sont en cours.
rules.compileandpublish.compileall.error=Échec de la compilation de {0} des {1} règles.
rules.compileandpublish.success=La compilation et la publication des règles ont été effectuées.
rules.compileandpublish.publish.error=Échec de la publication des règles.
rule.compileandpublish.inprogress=La compilation et la publication de la règle {0} pour {1} sont en cours.
rule.compileandpublish.success=La règle {0} a bien été compilée et publiée pour {1}.
rule.compileandpublish.publish.error=Échec de la publication de la règle

rule.notification.exception={0}

rule.module.editorAreaLogicHandler.actions.save.disallowed=La mise à jour d'un module de règles est impossible ou non autorisée.

rule.undeploy.inprogress=L'annulation du déploiement de la règle {0} et le rechargement du module {1} sont en cours.
rule.undeploy.publish.error=L'annulation du déploiement de la règle à la publication de la nouvelle version du module a échoué.

rules.undeploy.inprogress=L'annulation du déploiement des règles sélectionnées et le rechargement du module {0} sont en cours.
rules.undeploy.error=L'annulation du déploiement des règles sélectionnées a échoué.

rules.module.catalog.version=Catalogue
rules.module.deployment.status=Statut de déploiement
rules.module.deployment.status.undeployed=Déploiement du module annulé
rules.module.deployment.status.deployed=Module déployé
rules.module.deployment.status.pending=Déploiement en attente

rules.module.initialization.started=L'initialisation du module du moteur de règles {0} a commencé.
rules.module.initialization.error=L'initialisation du module du moteur de règles {0} a échoué.

hmc_sourcerules=Règles sources
hmc_published_and_unpublished_source_rules_only=Afficher publiées et non publiées
hmc_all_source_rules=Afficher tout
