/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.integration.cis.aspect;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.hybris.cis.api.subscription.model.CisSubscriptionProfileResult;
import com.hybris.commons.client.RestResponseFactory;


/**
 * Check if calls of
 * de.hybris.platform.integration.cis.subscription.service.impl.DefaultCisSubscriptionService.getCustomerProfile throws
 * com.hybris.cis.api.exception.ServiceRequestException. If so, store the exception message in sessionScopedMessages map
 * by 'ServiceRequestExceptionMessage' key.
 *
 */
@Aspect
public class HandleServiceRequestExceptionAspect
{
	@Resource(name = "threadScopedMessages")
	private Map<String, String> threadScopedMessages;

	private static final Logger LOG = Logger.getLogger(HandleServiceRequestExceptionAspect.class);

	@Around("execution(* com.hybris.cis.client.rest.subscription.mock.SubscriptionMockClientImpl.getProfile(..))")
	public Object aroundSubscriptionMockClientImpl_getProfile(final ProceedingJoinPoint joinPoint) throws Throwable
	{
		return processJoinPoint(joinPoint);
	}

	protected Object processJoinPoint(final ProceedingJoinPoint joinPoint) throws Throwable // NOSONAR
	{
		try
		{
			getThreadScopedMessages().remove("ServiceRequestExceptionMessage");
			return joinPoint.proceed();
		}
		catch (final com.hybris.cis.api.exception.ServiceRequestException ex)
		{
			getThreadScopedMessages().put("ServiceRequestExceptionMessage", ex.getMessage());
			LOG.info(
					"ServiceRequestException was interrupted by de.hybris.platform.integration.cis.aspect.HandleServiceRequestExceptionAspect",
					ex);
			return RestResponseFactory.<CisSubscriptionProfileResult> newStubInstance();
		}
	}

	public Map<String, String> getThreadScopedMessages()
	{
		return threadScopedMessages;
	}

	public void setThreadScopedMessages(final Map<String, String> threadScopedMessages)
	{
		this.threadScopedMessages = threadScopedMessages;
	}
}
