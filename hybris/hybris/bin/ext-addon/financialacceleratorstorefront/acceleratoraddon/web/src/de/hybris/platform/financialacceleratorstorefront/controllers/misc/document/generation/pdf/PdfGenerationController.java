/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialacceleratorstorefront.controllers.misc.document.generation.pdf;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.financialfacades.facades.DocumentGenerationFacade;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * PDFGenerationController
 */
@Controller("PdfGenerationController")
public class PdfGenerationController extends AbstractController
{

	/**
	 * @deprecated Since 6.4. use
	 * {@link de.hybris.platform.financialacceleratorstorefront.controllers.misc.document.generation.pdf.PdfGenerationController#LOGGER}
	 * instead.
	 */
	@Deprecated
	protected static final Logger LOG = Logger.getLogger(PdfGenerationController.class);
	private static final Logger LOGGER = Logger.getLogger(PdfGenerationController.class);

	@Resource(name = "documentGenerationFacade")
	DocumentGenerationFacade documentGenerationFacade;

	/*
	 * http://financialservices.local:9001/yacceleratorstorefront/insurance/en/pdf/print?itemRefId=12345
	 */
	@RequestMapping(value = "/pdf/print", method = RequestMethod.GET)
	public void pdfPrint(@RequestParam("itemRefId") final String itemRefId, final HttpServletResponse response) throws IOException
	{
		LOGGER.info("Pdf printing.");
		documentGenerationFacade.generate(DocumentGenerationFacade.PDF_DOCUMENT, itemRefId, response);
	}
}
