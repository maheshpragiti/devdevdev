<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="policyData" required="true" type="de.hybris.platform.commercefacades.insurance.data.InsurancePolicyData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<spring:htmlEscape defaultHtmlEscape="true" />

<h2 class="review-accordion-item__valid js-toggle">
	<spring:theme code="checkout.orderConfirmation.optional.extras" text="Optional Extras"/>
	<span class="review-accordion-item__open" data-open="optionalExtras"></span>
</h2>

<div id="optionalExtras" class="review-accordion-item__body opened">
	<div class="col-md-9 col-sm-9 col-xs-12">
		<c:if test="${empty policyData.optionalProducts}">
			<p class="review-accordion-item__text">
				<spring:theme code="checkout.multi.quoteReview.extras.empty" text="No optional extras were chosen" />
			</p>
		</c:if>
		<ul class="review-accordion-item__list review-accordion-item__list--style-type">
			<c:set var="extraBenefints" value="${policyData.optionalProducts}"/>
			<c:forEach items="${extraBenefints}" var="benefit">
				<li class="review-accordion-item__list-item">${fn:escapeXml(benefit.coverageProduct.name)}</li>
			</c:forEach>
		</ul>
	</div>
</div>
