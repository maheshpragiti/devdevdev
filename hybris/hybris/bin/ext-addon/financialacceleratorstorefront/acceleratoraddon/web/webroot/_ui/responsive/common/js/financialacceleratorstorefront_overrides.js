ACC.FSA = {
	mobileTest:  /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent),
	iosTest:  /iPhone|iPad|iPod/i.test(navigator.userAgent),
	screenSm: 1023,
	maxHeight: -1,
	homePage: $('.banner__component.banner a').attr('href'),
	draging: false,
	viewPort: $('html,body'),
	bankingPage: $('.page-BankingCategoryLandingPage'),
	myAccountLinksHeader: $('.js-nav-links').find('.js-myAccount-toggle'),
	findAgent: $('.js-nav-links').find('.js-find-agent-link, .js-find-branch-link').parent(),
	mobileNav: $('.navigation__overflow'),
	mobileNavAccLnk: $('.navigation__overflow').find('.js-userAccount-Links'),
	productLinks: $('.navigation__overflow').find('.js-offcanvas-links'),
	languageSwitcher: $('#langCurrSwticher').find('.js-language'),
	currencySwitcher: $('#langCurrSwticher').find('.js-currency'),
	search: $('.site-search'),
	searchInput: $('.js-site-search-input'),
	searchBtn: $('.js-btn-search'),
	detailsTitle: $('.js-find-agent').find('.js-category-caption'),
	orbeonForm: $('.xforms-form'),
	orbeonButton: $('.fr-save-final-button button'),
    orbeonInputField: $('.xforms-input-input'),
	divHeightArray: ['.js-optional-products', '.js-product-grid-item .js-price', '.js-pay-on-checkout', '.js-details > .js-product-name', '.js-addToCartForm','.js-product-grid-item .js-details'],//Creating array of selectors needed to equalize element heights on checkout and product pages

	_autoload: [
		'mobileMenuShow',
		'searchShow',
		'mobileMenuHeight',
		'colorBoxOverride',
		'hoverOnMobile',
		'equalizeHeights',
		'heightsOnResize',
		'findAgentAccordion',
		'orbeonDate',
		'timerRedirect',
		'orbeonAccordion',
        'disableBankingLanguageSelector'
    ],
	
	//Adding my account link and language/currency switchers to mobile menu since the whole mobile menu is implemented through JS (acc.navigation.js)
	mobileMenuShow: function() {
		ACC.FSA.findAgent.clone().addClass('liUserSign').prependTo(ACC.FSA.mobileNavAccLnk);
		ACC.FSA.languageSwitcher.add(ACC.FSA.currencySwitcher)
			.clone().addClass('hidden-md hidden-lg').appendTo(ACC.FSA.productLinks);
	},
	
	//Show/hide search bar input field
	searchShow: function() {
		var inputCheck = (function checkInput (){
			if (ACC.FSA.search.hasClass('active')) {
				ACC.FSA.searchInput.focus();	
			} else {
				ACC.FSA.searchInput.blur();
			}
			return checkInput;
		})();
		ACC.FSA.searchBtn.on('click', function() {
			ACC.FSA.search.toggleClass('active');
			inputCheck();
			//if mobile menu is opened close it!
			var menuOpen = $(document).find('.offcanvas');
			menuOpen.removeClass('offcanvas');
		});
	},
	
	//Setting the height of the mobile menu based on the viewport height
	mobileMenuHeight: function() {
		enquire.register("screen and (max-width:"+ (ACC.FSA.screenSm) +"px)", {
			match : function() {
				var mobileMenuHeightCalc = (function heightCheck () {
					var paddingCalc = $('.branding-mobile').height() + 80;
					ACC.FSA.mobileNav.height(window.innerHeight - paddingCalc);
					return heightCheck;
				})();
				$(window).on('resize', function() {
					//resizing mobile menu on device's orientation change
					mobileMenuHeightCalc();
                    // ACC.FSA.footerAccordion();
					//resizing Colorbox popups on device's orientation change
                    if ($('body').hasClass('page-login')) {
                        var resizeTimer;
                        clearTimeout(resizeTimer);
                        resizeTimer = setTimeout(function() {
                            ACC.FSA.popUpPos();
                        }, 50);
                    }
				});
                $(document).on('click','.js-password-forgotten, #forgottenPwdForm button, .removePaymentDetailsButton',function(){
                    // resizing Colorbox popups on click
                    $(document).on('cbox_complete', function() {
                        ACC.FSA.popUpPos();
                    });
                    // resizing Colorbox when content inside it is changed without closing it
                    var resizeTimer;
                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function() {
                        ACC.FSA.popUpPos();
                    }, 200);
                });
                ACC.FSA.footerAccordion(true);
			},
			unmatch : function() {
                $(window).on('resize', function() {
					//returning menu to desktop size
					ACC.FSA.mobileNav.height('auto');
				});
                ACC.FSA.footerAccordion(false);
            }
		});	
	},
	
	//Override for Colorbox popup
	colorBoxOverride : function () {
		ACC.colorbox.config.height = '360px';
		cboxOptions = {width: '95%'};
	},

	//Disable :hover on mobile devices
	hoverOnMobile : function () {
		if (ACC.FSA.mobileTest) {
			try { // prevent exception on browsers not supporting DOM styleSheets properly
				for (var si in document.styleSheets) {
					var styleSheet = document.styleSheets[si];
					if (!styleSheet.rules) continue;
					for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
						if (!styleSheet.rules[ri].selectorText) continue;
						if (styleSheet.rules[ri].selectorText.match(':hover')) {
							styleSheet.deleteRule(ri);
						}
					}
				}
			} catch (ex) {}	
		}	
	},
	
	//Equalize heights on resize
	heightsOnResize : function () {
		$(window).on('resize', function() {
			setTimeout(function() {
				ACC.FSA.equalizeHeights();
			}, 300);
		});	
	},

	//Accordion for Find an Agent page and scrolling to active category
	findAgentAccordion : function () {
		ACC.FSA.detailsTitle.on('click', function() {
			var $that = $(this);
			if($that.hasClass('active')) {
				$that.removeClass('active').next().slideUp().removeClass('opened');
				ACC.FSA.animateScroll(0);
			} else {
				$that.addClass('active').siblings().removeClass('active');
				$that.next().slideDown().addClass('opened')
				.siblings('.js-category-content').slideUp().removeClass('opened');
				//wait for the animation slideUp or slideDown to finish then animate content into view
				setTimeout(function() {
					ACC.FSA.animateScroll($that.offset().top);
				},400);
			}	
		});
		//Scrolling to active category when directly accessing it
		if(ACC.FSA.detailsTitle.length > 1) {
			ACC.FSA.detailsTitle.each(function() {
				if($(this).hasClass('active')) {
					var active = $(this);
					setTimeout(function() {
						ACC.FSA.animateScroll(active.offset().top);
					},400);		
				}
			});
		}
	},
	
	//Function for equalizing heights of divs on product pages/checkout
	equalizeHeights : function () {
		$.each(ACC.FSA.divHeightArray, function(index, selector) {
			$(selector).each(function() {
				var that = $(this);
				$(selector).height('auto');
				ACC.FSA.maxHeight = ACC.FSA.maxHeight > that.height() ? ACC.FSA.maxHeight : that.height();
			});
			$(selector).each(function() {
				var that = $(this);
				that.height(ACC.FSA.maxHeight);
			});
			ACC.FSA.maxHeight = -1;
		});
	},
	
	//Function for animating content into view
	animateScroll : function (position) {
		ACC.FSA.viewPort.animate({
		  scrollTop: position
		}, 600);	
	},

	// Function for triggering Orbeon's hidden button
	clickTouch: function(selector) {
		var event;

        if(ACC.FSA.mobileTest) {
            event = 'touchend';
        } else {
            event = 'click';
        }
        selector.on(event, function(e) {
            var touch_pos = $(window).scrollTop();
			if (e.type === 'touchend' && (Math.abs(touch_pos-$(window).scrollTop())>3)) {
				return;
			}
            ACC.FSA.orbeonInputField.blur();
            ACC.FSA.orbeonButton.trigger('click');
            return false;
        });
	},

	//Reposition and resize Colorbox Popup
	popUpPos: function () {
		// var element = $.colorbox.element().context.ownerDocument.activeElement;
		var element = '#colorbox';
		var position = ($(window).height() - $('#colorbox').height())/2;
		$.colorbox.resize({'width': '95%'});
		$(element).css({'top': position});
	},
	
	//Orbeon input date fields on iOS workaround. Should be deleted when orbeon resolves it.
	orbeonDate: function() {
		if (ACC.FSA.iosTest && ACC.FSA.orbeonForm.length > 0) {
			ACC.FSA.orbeonForm.on('change', function() {
				var orbeonDate = $('.xforms-input-input.xforms-type-date');
				if(orbeonDate.length > 1) {
					orbeonDate.prop('type','date');
				}
			});
		}
	},
	
	//Disabling back button on "Contact an agent - thank you page" and implemented page redirect
	timerRedirect: function () {
		if ($('#timer').length > 0) {
			history.pushState({ page: 1 }, 'Title 1', '#no-back');
			window.onhashchange = function (event) {
				window.location.hash = 'no-back';
			};
			var timerHolder = $('#timer');
			var count = 6;
			setInterval(function() {
				count--;
				timerHolder.text(count + ' seconds');
				if (count === 0) {
					window.location = ACC.FSA.homePage; 
				}
			},1000);	
		}
	},
	//Overriding orbeon's conflicting accordion function. Should be deleted when/if orbeon resolves it.
	orbeonAccordion: function() {
		if($('.orbeon-portlet-body').length > 0) {
			var  myAccountLinksHeader = $('.js-myAccount-toggle');
			myAccountLinksHeader.attr('data-toggle','');
			myAccountLinksHeader.on('click touchend', function(e) {
				e.preventDefault();
				e.stopPropagation();
				var that = $(this);
				if (ACC.insurance.mobileTest) {
					that.off('click');
					var linkSpan = that.find('span');
					var linkMobile = that.attr('data-target');
					$(linkMobile).slideToggle(function() {
						if($(linkMobile).is(':visible')) {
							linkSpan.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
						} else {
							linkSpan.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
						}
					});
				}
				var linkDesktop = that.attr('href');
				$(linkDesktop).slideToggle();
			});
		}
	},

	//Disable form submit when language is changed.
	disableBankingLanguageSelector: function () {
		$('.page-BankingCategoryLandingPage #lang-selector').off('change');
    },

    //Accordion for footer
    footerAccordion : function (bool) {
		var $title = ACC.FSA.bankingPage.find('.footer__nav--container .title');
		var $links = ACC.FSA.bankingPage.find('.footer__nav--links');
		if(bool) {
            $title.on('touchend', function() {
            	var $that = $(this);
                if($that.hasClass('active')) {
                    $that.removeClass('active').next().slideUp().removeClass('opened');
                } else {
                    $that.addClass('active').siblings().removeClass('active');
                    $that.next().slideDown().addClass('opened');
                }
            });
            $links.hide();
        } else {
            $title.off('touchend').removeClass('active');
            $links.show();
		}
    }
};