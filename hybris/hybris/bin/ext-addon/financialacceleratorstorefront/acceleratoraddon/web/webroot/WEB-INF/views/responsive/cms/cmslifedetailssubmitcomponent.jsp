<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="life-infos">
    <c:choose>
        <c:when test="${param.viewStatus=='view'}">
			<ul class="life-infos__details">
				<li>
					<c:if test="${not empty sessionScope.lifeWhoCovered}">
						<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.covered" text="Who is being covered:" /></label>${fn:escapeXml(sessionScope.lifeWhoCovered)}
					</c:if>
				</li>
			
				<li>
					<c:if test="${not empty sessionScope.lifeCoverageRequire}">
						<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.covered.required" text="How much life insurance coverage do you require:" /></label>${fn:escapeXml(sessionScope.lifeCoverageRequire)}
					</c:if>
				</li>
			</ul>
			
			<ul class="life-infos__details">
				<li>
					<c:if test="${not empty sessionScope.lifeCoverageLast}">
						<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.coverage.last" text="How many years do you want the coverage to last:" /></label>${fn:escapeXml(sessionScope.lifeCoverageLast)}
					</c:if>
				</li>
			
				<li>
					<c:if test="${not empty sessionScope.lifeCoverageStartDate}">
						<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.coverage.start.date" text="Coverage start date:" /></label>${fn:escapeXml(sessionScope.lifeCoverageStartDate)}
					</c:if>
				</li>
			</ul>
			
			<ul class="life-infos__details">
				<li>
					<c:if test="${not empty sessionScope.lifeMainDob}">
						<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.date.of.birth" text="Your date of birth:" /></label>${fn:escapeXml(sessionScope.lifeMainDob)}
					</c:if>
				</li>
			
				<li>
					<c:if test="${not empty sessionScope.lifeMainSmoke}">
						<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.main.smoke" text="Do you smoke:" /></label>${fn:escapeXml(sessionScope.lifeMainSmoke)}
					</c:if>
				</li>
			</ul>
			<c:if test="${sessionScope.lifeWhoCovered eq 'yourself and second person'}">
				<ul class="life-infos__details">
					<li>
						<c:if test="${not empty sessionScope.lifeSecondDob}">
							<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.second.date.of.birth" text="Second persons date of birth:" /></label>${fn:escapeXml(sessionScope.lifeSecondDob)}
						</c:if>
					</li>
			
					<li>
						<c:if test="${not empty sessionScope.lifeSecondSmoke}">
							<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.second.smoke" text="Dose the second person smoke:" /></label>${fn:escapeXml(sessionScope.lifeSecondSmoke)}
						</c:if>
					</li>
				</ul>
				<ul class="life-infos__details">
					<li>
						<c:if test="${not empty sessionScope.lifeRelationship}">
							<label class="life-infos__details--right-margin"><spring:theme code="text.insurance.life.relationship" text="Relationship to the second person:" /></label>${fn:escapeXml(sessionScope.lifeRelationship)}
						</c:if>
					</li>
				</ul>
			</c:if>         
			
            <ycommerce:testId code="multicheckout_cancel_button">
        	    <a id="backBtn" class="secondary-button secondary-button__default secondary-button__infos" href="?viewStatus=edit">
            		<spring:theme code="text.cmsformsubmitcomponent.edit" text="Edit"/>
            	</a>
			</ycommerce:testId>                     
      		
      </c:when>

	<c:otherwise>
		<ycommerce:testId code="multicheckout_saveForm_button">
			<a id="continueBtn" class="primary-button primary-button__default primary-button__infos pull-right" href="?viewStatus=view">
				<spring:theme code="text.cmsformsubmitcomponent.find.prices" text="Find Prices"/>
			</a>
		</ycommerce:testId>
	</c:otherwise>
</c:choose>
</div>