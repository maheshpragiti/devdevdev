var carouselObj = ACC.carousel.carouselConfig["rotating-image"];
$.extend(carouselObj, {
	autoPlay: true,
	navigation: true,
	navigationText : ["<span class='glyphicon glyphicon-menu-left'></span>", "<span class='glyphicon glyphicon-menu-right'></span>"]
});