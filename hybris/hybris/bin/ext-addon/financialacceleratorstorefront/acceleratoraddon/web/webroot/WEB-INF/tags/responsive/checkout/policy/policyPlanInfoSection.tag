<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderData" required="false" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="isValidStep" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/addons/financialcheckout/responsive/checkout/multi/quote" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<spring:htmlEscape defaultHtmlEscape="true" />

    <c:set value="${orderData}" var="masterEntry"/>

    <h2 class="review-accordion-item__valid js-toggle">
        <spring:theme code="checkout.orderConfirmation.details.information" text="Information"/>
        <span class="review-accordion-item__open" data-open="policyCoverageLevel"></span>
    </h2>

    <div id="policyCoverageLevel" class="review-accordion-item__body opened">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <ul class="review-accordion-item__list">
                <c:if test="${not empty masterEntry.insuranceQuote.tripDestination}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.travel.destination" text="Destination"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.tripDestination)}</div>
                        </div>
                    </li>
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.travel.depart" text="Depart"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.tripStartDate)}</div>
                        </div>
                    </li>
					<c:if test="${not empty masterEntry.insuranceQuote.tripEndDate}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.travel.return" text="Return"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.tripEndDate)}</div>
                        </div>
                    </li>
                    </c:if>
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.travel.nooftravellers" text="No. of Travellers"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.tripNoOfTravellers)}</div>
                        </div>
                    </li>
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.travel.age" text="Age of Travellers"/>
                            </div>
                            <div class="col-xs-6">
                                <c:forEach items="${masterEntry.insuranceQuote.tripTravellersAge}" var="travellerAge"
                                           varStatus="status">
                                    ${fn:escapeXml(travellerAge)}${not status.last ? ',' : ''}
                                </c:forEach>
                            </div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.propertyAddressLine1}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.property.property.address" text="Property:"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.propertyAddressLine1)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.propertyCoverRequired}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.property.property.cover.required" text="Cover Required:"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.propertyCoverRequired)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.propertyStartDate}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.property.property.start.date" text="Start Date:"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.propertyStartDate)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.propertyType}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.property.property.type" text="Property Type:"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.propertyType)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.propertyValue}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.property.property.value" text="Property Value:"/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.propertyValue)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.autoDetail.autoMake}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.auto.vehicle.make" text="Vehicle Make: "/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.autoDetail.autoMake)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.autoDetail.autoModel}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.auto.vehicle.model" text="Vehicle Model: "/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.autoDetail.autoModel)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.autoDetail.autoLicense}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.auto.vehicle.license" text="Vehicle License: "/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.autoDetail.autoLicense)}</div>
                        </div>
                    </li>
                </c:if>
                <c:if test="${not empty masterEntry.insuranceQuote.autoDetail.autoPrice}">
                    <li class="review-accordion-item__list-item">
                        <div class="row">
                            <div class="col-xs-6">
                                <spring:theme code="checkout.multi.quoteReview.auto.vehicle.value" text="Vehicle Value: "/>
                            </div>
                            <div class="col-xs-6">${fn:escapeXml(masterEntry.insuranceQuote.autoDetail.autoPrice)}</div>
                        </div>
                    </li>
                </c:if>
                
                <c:if test="${masterEntry.insuranceQuote.quoteType eq 'LIFE'}">
                    <quote:quotePlanInfoLifeInsuranceSection quoteData="${masterEntry.insuranceQuote}"/> 
                </c:if>
            </ul>
        </div>
    </div>
