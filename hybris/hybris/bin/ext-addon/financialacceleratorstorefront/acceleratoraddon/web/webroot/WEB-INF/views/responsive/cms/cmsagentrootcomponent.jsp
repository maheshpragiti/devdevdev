<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="agent" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/agent"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="/find-agent" var="currentURL"/>
<c:set var="category" value="${activeCategory}"/>
<c:forEach items="${categories}" var="entry">
	<c:choose>
		<c:when test="${entry.category.code == category}">
				<c:set var="activeClass" value="active"/>
				<c:set var="openedClass" value="opened"/>
	    </c:when>
	    <c:otherwise>
				<c:set var="activeClass" value=""/>
				<c:set var="openedClass" value=""/>
	    </c:otherwise>
	</c:choose>
	<h2 class="category__caption js-category-caption ${activeClass}">
		<span class="category__image ${fn:escapeXml(entry.category.name)}"></span>${fn:escapeXml(entry.category.name)}
	</h2>
    <div class="category__content js-category-content ${openedClass}">
		<c:forEach items="${entry.agents}" var="person">
			<agent:agentInfo agent="${person}"/>
		</c:forEach>
     </div>
</c:forEach>