<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="true"/>


<c:set value="${product.bundleTemplates[0]}" var="productPackage" />
<c:set value="${product.price.oneTimeChargeEntries}" var="productComponents" />

<c:set value="${not empty product.potentialPromotions}"
	var="hasPromotion" />

<li class="product__list--item col-xs-12 col-sm-6 col-md-3">
	<div class="product__list--item-wrapper">
		<ycommerce:testId code="test_searchPage_wholeProduct">

			<ycommerce:testId code="searchPage_productName_link_${product.code}">
				<h3 class="product__list--category">${fn:escapeXml(productPackage.products[0].defaultCategory.name)}</h3>
				<a class="product__list--name">${fn:escapeXml(product.name)}</a>
			</ycommerce:testId>

			<div class="base__options">
				<c:set value="${fn:length(productComponents) - 1}" var="size" />
				<c:forEach var="billingEvent"
					items="${productComponents}" begin="1" end="3">
					<c:if test = "${billingEvent.billingTime.name != null}">
						<div class="eventName">${fn:escapeXml(billingEvent.billingTime.name)} up to ${fn:escapeXml(billingEvent.price.formattedValue)}</div>
					</c:if>
				</c:forEach>
				<c:choose>
				    <c:when test="${size >3}">
				       <div class="eventName">...</div>
				    </c:when>
				    <c:otherwise>
				   		<c:forEach var="line" begin="0" end="${3 - size}">
				        	<div class="eventName">&nbsp;</div>
				        </c:forEach>
				    </c:otherwise>
				</c:choose>
			</div>

<%--  		<button class="primary-button primary-button__default primary-button__edit">
				<spring:message code="text.search.learnMore" /> 
			</button> --%>

			<form method="get" action="${fn:escapeXml(request.contextPath)}${fn:escapeXml(productPackage.products[0].defaultCategory.url)}">
				<button type="submit" class="secondary-button secondary-button__default secondary-button__edit" >
					<spring:message code="text.search.getQuote" />
				</button>
			</form>

		</ycommerce:testId>
	</div>
</li>
