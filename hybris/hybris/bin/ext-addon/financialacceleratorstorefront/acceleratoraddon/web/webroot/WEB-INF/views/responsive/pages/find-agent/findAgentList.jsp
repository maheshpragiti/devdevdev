<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<spring:htmlEscape defaultHtmlEscape="true"/>


<template:page pageTitle="${pageTitle}">
    <div class="find-agent find-agent__main-page js-find-agent">
        <h1 class="find-agent__title"><spring:theme code="text.agent.title.findAnAgent" text="Find an Agent"/></h1>
        <cms:pageSlot position="Section1" var="component">
       	    <cms:component component="${component}" element="div" class="category" />
        </cms:pageSlot>
    </div>
</template:page>