<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderEntryData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="planProductData" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="col-md-12 cart-items-list__item">
    <div class="row">
        <div class="col-xs-12 col-sm-2 cart-items-list__thumb">
            <product:productPrimaryImage product="${orderEntryData.product}" format="96Wx96H"/>
        </div>
        <div class="col-xs-12 col-sm-7">
            <ycommerce:testId code="entry_product_productName">
                <h3 class="cart-items-list__product-name">${fn:escapeXml(orderEntryData.product.name)}</h3>
            </ycommerce:testId>
            <ycommerce:testId code="entry_product_productDescription">
                <p>${fn:escapeXml(orderEntryData.product.description)}</p>
            </ycommerce:testId>
        </div>
        <c:choose>
            <c:when test="${not orderEntryData.removeable and not orderEntryData.addable}">
                <div class="col-xs-12 col-sm-7 disabled-option"></div>
            </c:when>
        </c:choose>
        <div class="col-xs-12 col-sm-3">
            <c:choose>
                <c:when test="${not orderEntryData.removeable and not orderEntryData.addable}">
                    <button class="secondary-button secondary-button__default secondary-button__add-remove secondary-button__default--opacity-disabled" ${not orderEntryData.removeable and not orderEntryData.addable ? "disabled": ""}>
                        <c:choose>
                            <c:when test="${fn:length(orderEntryData.product.bundleTemplates) == 3}">
                                <spring:theme code="text.product.mandated"/>
                            </c:when>
                            <c:otherwise>
                                <spring:theme code="checkout.notAvailable"/>
                            </c:otherwise>
                        </c:choose>
                    </button>
                </c:when>
                <c:when test="${not orderEntryData.removeable and orderEntryData.addable}">
                    <spring:url var="cartUrl" value="/cart/addSingleProduct"/>

                    <form class="js-addPotentialProductToCartForm" action="${cartUrl}" method="post">
                        <c:choose>
                            <c:when test="${fn:length(orderEntryData.product.bundleTemplates) == 3}">
                                <input type="hidden" name="bundleTemplateId" value="${fn:escapeXml(orderEntryData.product.bundleTemplates[2].id)}">
                            </c:when>
                            <c:otherwise>
                                <input type="hidden" name="bundleTemplateId" value="${fn:escapeXml(orderEntryData.product.bundleTemplates[1].id)}">
                            </c:otherwise>
                        </c:choose>

                        <input type="hidden" name="productCodePost" value="${fn:escapeXml(orderEntryData.product.code)}">
                        <input type="hidden" name="bundleNo" value="${fn:escapeXml(orderEntryData.bundleNo)}">
                        <button type="submit" class="secondary-button secondary-button__default secondary-button__add-remove js-submit"><spring:theme code="checkout.add"/></button>
                    </form>
                </c:when>
            </c:choose>

            <c:if test="${orderEntryData.removeable}">
                <spring:url value="/cart/update" var="cartUpdateFormAction"/>
                <form:form id="updateCartForm${orderEntryData.entryNumber}" action="${cartUpdateFormAction}" method="post"
                           commandName="updateQuantityForm${orderEntryData.entryNumber}">
                    <input type="hidden" name="entryNumber" value="${orderEntryData.entryNumber}"/>
                    <input type="hidden" name="productCode" value="${fn:escapeXml(orderEntryData.product.code)}"/>
                    <input type="hidden" name="initialQuantity" value="${orderEntryData.quantity}"/>
                    <input type="hidden" name="quantity" value="0"/>
                </form:form>
                <ycommerce:testId code="cart_product_removeProduct">
                    <spring:theme code="text.iconCartRemove" var="iconCartRemove"/>
                    <button id="RemoveProduct_${orderEntryData.entryNumber}" class="secondary-button secondary-button__default secondary-button__add-remove secondary-button__default--opacity js-disable-after-click js-remove-entry-button">${iconCartRemove}</button>
                </ycommerce:testId>
            </c:if>
        </div>
    </div>
</div>
