<%@ attribute name="policyData" required="true" type="de.hybris.platform.commercefacades.insurance.data.InsurancePolicyData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty policyData.policyHolderDetail}">
    <c:set var="policyHolderDetail" value="${policyData.policyHolderDetail}"/>
    <h2 class="review-accordion-item__valid js-toggle">
		<spring:theme code="checkout.orderConfirmation.details.your.details" text="Your Details"/>
		<span class="review-accordion-item__open" data-open="policyDetails"></span>
	</h2>

	<div class="review-accordion-item__body opened" id="policyDetails">
	    <div class="col-md-9 col-sm-9 col-xs-12">
	        <h3 class="review-accordion-item__list-title"><spring:theme code="checkout.orderConfirmation.details.policy.holder" text="Policy Holder"/></h3>
	        <ul class="review-accordion-item__list">
	            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.firstName)} &nbsp; ${fn:escapeXml(policyHolderDetail.lastName)}</li>
	            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressLine1)}</li>
	            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressLine2)}</li>
	            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressCity)}</li>
	            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.postcode)}</li>
	            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressCountry)}</li>
	        </ul>
	        <c:if test="${not empty policyData.travellers}">
	            <h3 class="review-accordion-item__list-title"><spring:theme code="checkout.orderConfirmation.details.beneficiaries" text="Travellers"/></h3>
	            <c:forEach items="${policyData.travellers}" var="traveller" varStatus="status">
	                <ul class="review-accordion-item__list">
	                    <li class="review-accordion-item__list-item review-accordion-item__list-item--title"><spring:theme code="checkout.orderConfirmation.details.traveller"
	                                    text="Traveller"/>&nbsp;${fn:escapeXml(status.count)}</li>
	                    <li class="review-accordion-item__list-item">${fn:escapeXml(traveller.firstName)}&nbsp;${fn:escapeXml(traveller.lastName)}</li>
	                    <li class="review-accordion-item__list-item"><spring:theme code="checkout.orderConfirmation.details.age" text="Age:"/>&nbsp;${fn:escapeXml(traveller.age)}</li>
	                </ul>
	            </c:forEach>
	        </c:if>
	    </div>
    </div>
</c:if>

