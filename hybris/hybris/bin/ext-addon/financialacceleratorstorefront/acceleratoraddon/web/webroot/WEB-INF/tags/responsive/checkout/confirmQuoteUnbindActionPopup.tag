<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<div id="popup_confirm_unbind_quote_wrapper" style="display:none">
    <div id="popup_confirm_unbind_quote">
        <div class="popup-content-wrapper">
            <h3><spring:theme code="text.confirm.change.quote.notification" text="Change Quote Notification"/></h3>
            <spring:theme code="text.change.quote.notification.text" text="Making changes to your quote application could alter the quote price"/>
            <div class="modal-actions">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
                    <button id="yesButton" class="primary-button primary-button__default primary-button__popup">
                        <spring:theme code="text.dialog.confirm" text="Continue"/>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
