<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="carousel__component">
	<div class="carousel__component--headline">${title}</div>
	<c:choose>
		<c:when test="${component.popup}">
			<div class="carousel__component--carousel js-owl-carousel js-owl-rotating-image">
				<div id="quickViewTitle" class="quickView-header display-none">
					<div class="headline">
						<span class="headline-text"><spring:theme code="popup.quick.view.select"/></span>
					</div>
				</div>
				<c:forEach items="${banners}" var="banner" varStatus="status">
					<c:if test="${ycommerce:evaluateRestrictions(banner)}">
						<c:url value="${banner.urlLink}" var="encodedUrl" />
						<div class="carousel__item">
							<img src="${banner.media.url}" alt="${not empty banner.media.altText ? banner.media.altText : banner.media.headline}" title="${not empty banner.media.altText ? banner.media.altText : banner.headline}"/>
							<div class="carousel__item--title">
							<span class="title">
								<strong>${banner.headline}</strong>
				 			 </span>
								<span class="details">
								${banner.content}
							</span>		
							</div>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</c:when>
		<c:otherwise>
			<div class="carousel__component--carousel js-owl-carousel js-owl-rotating-image">
				<c:forEach items="${banners}" var="banner" varStatus="status">
					<c:if test="${ycommerce:evaluateRestrictions(banner)}">
						<c:url value="${banner.urlLink}" var="encodedUrl" />
						<div class="carousel__item">
							<img src="${banner.media.url}" alt="${not empty banner.media.altText ? banner.media.altText : banner.media.headline}" title="${not empty banner.media.altText ? banner.media.altText : banner.headline}"/>
							<div class="carousel__item--title">
							<span class="title">
								<strong>${banner.headline}</strong>
				 			 </span>
								<span class="details">
								${banner.content}
							</span>		
							</div>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</c:otherwise>
	</c:choose>
</div>
