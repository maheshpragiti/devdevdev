<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="account-section-content__headline">
	<spring:theme code="text.account.myPolicies" text="Policies" />
</div>

<c:choose>
	<c:when test="${empty policies}">
		<p>
			<spring:theme code="text.account.myPolicies.noPolicies" text="You have no policies" />
		</p>
	</c:when>
	<c:otherwise>
		<p>
			<spring:theme code="text.account.myPolicies.viewYourPolicies" text="View your Policies" />
		</p>
		<div class="table-responsive">
			<table class="my-policies">
				<tr>
					<th class="my-policies__table-head" colspan="2"><spring:theme code="text.account.myPolicies.policy.name" text="Policy Name"/></th>
					<th class="my-policies__table-head"></th>
				</tr>
				<c:forEach items="${policies}" var="policy">
					<c:forEach items="${policy.policyImages}" var="image">
						<c:if test="${image.format == '40Wx40H_policy_responsive'}">
							<c:set var="thumbnail_img" value="${image}"/>
						</c:if>
					</c:forEach>
					<tr>
						<td class="my-policies__table-body">
	                    	<span class="images images__wrapper">
	                    		<c:if test="${not empty thumbnail_img}"><img class="images__size" src="${thumbnail_img.url}"/></c:if>
                    		</span>
						</td>
						<td class="my-policies__table-body">
								${fn:escapeXml(policy.policyProduct.defaultCategory.name)}<br/>${fn:escapeXml(policy.policyNumber)}
						</td>
						<td class="my-policies__table-body">
							<a target="_blank" class="primary-button primary-button__default primary-button__account-policies" href="${fn:escapeXml(policy.policyUrl)}"><spring:theme code="text.account.myPolicies.policy.view" text="View Policy"/></a>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:otherwise>
</c:choose>