<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="financialMyAccount" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/account" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="account-section-content__headline">
    <spring:theme code="text.account.myQuotes" text="Quotes" />
</div>

<c:choose>
    <c:when test="${empty quotes}">
        <p>
            <spring:theme code="text.account.myQuotes.noQuotes" text="You have no quotes" />
        </p>
    </c:when>
    <c:otherwise>
        <p>
            <spring:theme code="text.account.myQuotes.viewAndActivate.section.header" text="View and activate your Saved Quotes" />
        </p>
        <table id="myQuotesList" class="my-quotes">
            <tr class="hidden-xs">
                <th colspan="2" class="my-quotes__table-head"><spring:theme code="text.account.myQuotes.plan.name" text="Plan Name"/></th>
                <th class="my-quotes__table-head"><spring:theme code="text.account.myQuotes.quote.status" text="Quote status"/></th>
                <th class="my-quotes__table-head"><spring:theme code="text.account.myQuotes.quote.expiry.date" text="Expires"/></th>
                <th class="my-quotes__table-head"><spring:theme code="text.account.myQuotes.quote.price" text="Price"/></th>
                <th class="my-quotes__table-head"></th>
            </tr>
            <spring:url var="retrieveQuoteUrl" value="/checkout/multi/retrieveQuote" htmlEscape="false"/>

            <c:forEach items="${quotes}" var="quote">
                <c:set var="thumbnail_img" value=""/>
                <c:set var="workflowStatus" value="${quote.quoteWorkflowStatus}"/>
                <c:set var="isExpired" value="${quote.quoteIsExpired}"/>

                <c:forEach items="${quote.quoteImages}" var="image">
                    <c:if test="${image.format == '40Wx40H_quote_responsive'}">
                        <c:set var="thumbnail_img" value="${image}"/>
                    </c:if>
                </c:forEach>
                <tr class="responsive-table-item my-quotes__responsive-table-item-mobile">
                    <td class="my-quotes__table-body">
                        <span class="images images__wrapper images__wrapper--size">
                            <c:if test="${not empty thumbnail_img}"><img class="images__size" src="${thumbnail_img.url}"/></c:if>
                        </span>
                    </td>
                    <td class="my-quotes__table-body hidden-sm hidden-md hidden-lg">
                        <spring:theme code="text.account.myQuotes.plan.name" text="Plan Name"/>
                    </td>
                    <td class="my-quotes__table-body responsive-table-cell">${fn:escapeXml(quote.planName)}<br/>${fn:escapeXml(quote.quoteNumber)}</td>
                    <c:set var="quoteStatus">
                        <spring:theme code="text.account.myQuotes.quote.status.${quote.quoteStatus}" text="${quote.quoteStatus}" />
                    </c:set>
                    <c:choose>
                        <c:when test="${workflowStatus eq 'REJECTED'}">
                            <c:set var="quoteStatus">
                                <spring:theme code="text.account.myQuotes.quote.status.rejected" text="Rejected" />
                            </c:set>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${isExpired}">
                                    <c:set var="quoteStatus">
                                        <spring:theme code="text.account.myQuotes.quote.status.expired" text="Expired"/>
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${quote.quoteIsBind and workflowStatus eq 'APPROVED'}">
                                            <c:set var="quoteStatus">
                                                <spring:theme code="text.account.myQuotes.quote.status.readyForPurchase" text="Ready for purchase"/>
                                            </c:set>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="quoteStatus">
                                                <spring:theme code="text.account.myQuotes.quote.status.unfinished" text="Unfinished"/>
                                            </c:set>
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                    <td class="my-quotes__table-body hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.myQuotes.quote.status" text="Quote status"/></td>
                    <td class="my-quotes__table-body responsive-table-cell" <c:if test="${isExpired or workflowStatus eq 'REJECTED'}">class="expired"</c:if>>${fn:escapeXml(quoteStatus)}</td>
                    <td class="my-quotes__table-body hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.myQuotes.quote.expiry.date" text="Expires"/></td>
                    <td class="my-quotes__table-body responsive-table-cell" <c:if test="${isExpired}">class="expired"</c:if>><c:if test="${workflowStatus ne 'REJECTED'}">${fn:escapeXml(quote.quoteExpiryDate)}</c:if></td>
                    <td class="my-quotes__table-body hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.myQuotes.quote.price" text="Price"/></td>
                    <td class="my-quotes__table-body responsive-table-cell"><c:choose>
                        <c:when test="${!isExpired and workflowStatus ne 'REJECTED'}">
                            ${fn:escapeXml(quote.quotePrice)}<br/>
                            <c:choose>
                                <c:when test="${quote.isMonthly}">
                                    <spring:theme code="checkout.cart.payment.frequency.monthly" text="Monthly" />
                                </c:when>
                                <c:otherwise>
                                    <spring:theme code="checkout.cart.payment.frequency.annual" text="Annual" />
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${workflowStatus ne 'REJECTED'}">
                                <spring:theme code="checkout.cart.payment.notApplicable" text="n/a" />
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    </td>
                    <c:choose>
                        <c:when test="${!isExpired and workflowStatus ne 'REJECTED'}">
                            <td class="my-quotes__table-body">
                                <form class="my-quotes__retrieve-quote-form" action="${retrieveQuoteUrl}" method="POST">
                                    <input name="code" value="${quote.retrieveQuoteCartCode}" type="hidden"/>
                                    <input name="cartCode" value="${quote.currentCartCode}" type="hidden"/>
                                    <input type="hidden" name="CSRFToken" value="${CSRFToken.token}">
                                    <a href="#" type="submit" class="primary-button primary-button__default primary-button__account-quotes js-retrieveBtn"><spring:theme code="text.account.myQuote.quote.retrieve" text="Retrieve"/></a>
                                </form>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <td>&nbsp;</td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </table>
    </c:otherwise>
</c:choose>

<financialMyAccount:confirmRetrieveQuoteActionPopup/>