<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="Section1" var="feature">
        <cms:component component="${feature}" element="div"/>
    </cms:pageSlot>
    <div class="row quotes">
        <cms:pageSlot position="Section2A" var="feature">
            <cms:component component="${feature}" element="div" class="col-xs-6 col-sm-4 col-md-2 service-links"/>
        </cms:pageSlot>
    </div>

    <cms:pageSlot position="Section2B" var="feature">
        <cms:component component="${feature}" element="div"/>
    </cms:pageSlot>

    <div class="review review__wrapper">
        <div class="row">
            <cms:pageSlot position="Section2C" var="feature">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6"/>
            </cms:pageSlot>
        </div>
    </div>

    <cms:pageSlot position="Section3" var="feature">
        <cms:component component="${feature}" element="div" class="col-xs-12"/>
    </cms:pageSlot>

    <cms:pageSlot position="Section4" var="feature">
        <cms:component component="${feature}" element="div" class="col-xs-12"/>
    </cms:pageSlot>

    <cms:pageSlot position="Section5" var="feature">
        <cms:component component="${feature}" element="div" class="col-xs-12"/>
    </cms:pageSlot>

</template:page>
