<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="agent" required="true" type="de.hybris.platform.financialfacades.findagent.data.AgentData"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="agent-info agent-info__wrapper col-xs-12 col-sm-6 col-md-4">
    <div class="agent-info__image">
        <img class="agent-info__image--color-border" src="${agent.thumbnail.url}"/>
    </div>
    <div class="agent-info__description">
        <ul class="agent-info__details">
            <li class="agent-info__name">${fn:escapeXml(agent.firstName)}&nbsp;${fn:escapeXml(agent.lastName)}</li>
            <c:if test="${not empty agent.enquiryData}">
                <li class="agent-info__detail">${fn:escapeXml(agent.enquiryData.line1)}</li>
                <li class="agent-info__detail">${fn:escapeXml(agent.enquiryData.line2)}</li>
                <li class="agent-info__detail">${fn:escapeXml(agent.enquiryData.town)}</li>
                <li class="agent-info__detail">${fn:escapeXml(agent.enquiryData.region.isocodeShort)}&nbsp;${fn:escapeXml(agent.enquiryData.postalCode)}</li>
            </c:if>
        </ul>
    </div>
    <div class="agent-info__button">
        <a id="backBtn" class="secondary-button secondary-button__default secondary-button__agent" href="contact-agent?agent=${fn:escapeXml(agent.uid)}&activeCategory=${fn:escapeXml(activeCategory)}"><spring:theme code="text.agent.contactExpert.request" text="Contact"/></a>
    </div>
</div>