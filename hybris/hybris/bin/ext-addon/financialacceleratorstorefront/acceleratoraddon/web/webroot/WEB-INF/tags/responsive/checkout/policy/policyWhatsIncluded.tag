<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="policyData" required="true" type="de.hybris.platform.commercefacades.insurance.data.InsurancePolicyData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<h2 class="review-accordion-item__valid js-toggle">
	<spring:theme code="checkout.multi.quoteReview.included" text="Whats Included" />
	<span class="review-accordion-item__open" data-open="whatsIncludedPanel"></span>
</h2>

<div id="whatsIncludedPanel" class="review-accordion-item__body opened">
	<div class="col-md-9 col-sm-9 col-xs-12">
		<h3 class="review-accordion-item__list-title">
			${fn:escapeXml(policyData.mainProduct.coverageProduct.name)}
		</h3>
		<c:set var="benefits" value="${policyData.mainProduct.benefits}"/>
		<ul class="review-accordion-item__list review-accordion-item__list--style-type">
			<c:forEach items="${benefits}" var="benefit">
				<li class="review-accordion-item__list-item">${fn:escapeXml(benefit.name)}</li>
			</c:forEach>
		</ul>
	</div>
</div>
