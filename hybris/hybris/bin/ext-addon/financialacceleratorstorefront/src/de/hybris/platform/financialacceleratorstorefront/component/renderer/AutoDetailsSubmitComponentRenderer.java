/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.financialacceleratorstorefront.component.renderer;

import de.hybris.platform.financialservices.constants.FinancialservicesConstants;
import de.hybris.platform.financialservices.model.components.CMSAutoDetailsSubmitComponentModel;
import org.w3c.dom.Document;

import javax.servlet.jsp.PageContext;
import javax.xml.xpath.XPath;


/**
 * The class of AutoDetailsSubmitComponentRenderer.
 */
public class AutoDetailsSubmitComponentRenderer<C extends CMSAutoDetailsSubmitComponentModel>
		extends AbstractFormSubmitComponentRenderer<C>
{

	@Override
	protected void setSessionAttributes(final String pString, final PageContext pPageContext, final C component)
	{
		//Create DOM document
		final Document document = createDocument(pString);

		// Create XPath object
		final XPath xpath = createXPath();

		// Stash the nodes..
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_DRIVER_DOB,
				getNodeList(xpath, document, "/form/section-1/driver-date-of-birth/text()"));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_COVER_START,
				getNodeList(xpath, document, "/form/section-1/coverage-start-date/text()"));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_VALUE,
				getNodeList(xpath, document, "/form/section-1/vehicle-value/text()"));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_MAKE,
				getNodeList(xpath, document, "/form/section-1/vehicle-make/text()"));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_MODEL,
				getNodeList(xpath, document, "/form/section-1/vehicle-model/text()"));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_LICENSE,
				getNodeList(xpath, document, "/form/section-1/vehicle-license/text()"));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_VEHICLE_YEAR,
				getNodeList(xpath, document, "/form/section-1/year-manufacture/text()"));
		safelyStashNodeDataInSession(pPageContext, FinancialservicesConstants.AUTO_STATE,
				getNodeList(xpath, document, "/form/section-1/state/text()"));
	}

}
