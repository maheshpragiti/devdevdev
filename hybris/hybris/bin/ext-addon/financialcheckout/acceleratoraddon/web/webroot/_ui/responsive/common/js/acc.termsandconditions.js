ACC.termsandconditions = {

	bindTermsAndConditionsLink: function() {
        $('.terms-and-conditions-link').click(function(e) {
            e.preventDefault();
            ACC.colorbox.config.height = '80%';
            ACC.colorbox.config.fixed = true;
            cboxOptions = {width: '100%', height: '80%'};
            ACC.colorbox.open("", {
                href: $(this).attr("href"),
                onOpen: function () {
                },
                onComplete: function ()
                {
                    ACC.common.refreshScreenReaderBuffer();
                    $('#cboxLoadingOverlay').remove();
                    $('#cboxLoadingGraphic').remove();
                },
                onClosed: function() {
                    ACC.common.refreshScreenReaderBuffer();
                }
            });
        });
	}
};

$(document).ready(function(){
	with(ACC.termsandconditions) {
		bindTermsAndConditionsLink();
	}
});
