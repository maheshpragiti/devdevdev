<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/financialcheckout/responsive/checkout/multi" %>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/addons/financialcheckout/responsive/checkout/multi/quote" %>
<%@ taglib prefix="insuranceCheckout" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/checkout" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}">

	<multi-checkout:checkoutProgressBar steps="${checkoutSteps}" progressBarId="${progressBarId}"/>
	<div class="col-md-8">
		<div id="checkoutContentPanel" class="review-accordion">
			<c:url value="${nextStepUrl}" var="nextStep"/>
			<c:url value="/checkout/multi/quote/back" var="backStep"/>
			<div class="col-md-12 col-sm-12 col-xs-12 review-accordion-header review-accordion-header__wrapper">
				<quote:quoteHeader insuranceQuoteReviews="${insuranceQuoteReviews}"/>
			</div>
			<div class="review-accordion-item">
				<c:if test="${cartData.insuranceQuote.quoteType ne 'EVENT'}">
					<div class="col-md-12 col-sm-12 col-xs-12 review-accordion-item__wrapper js-quotePlanInfoSection">
						<quote:quotePlanInfoSection insuranceQuoteReviews="${insuranceQuoteReviews}" isValidStep="${insuranceQuoteReviews[0].validation['0']}"/>
					</div>
				</c:if>
				<div class="col-md-12 col-sm-12 col-xs-12 review-accordion-item__wrapper">
					<quote:quoteIncluded insuranceQuoteReviews="${insuranceQuoteReviews}" isValidStep="${insuranceQuoteReviews[0].validation['0']}"/>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 review-accordion-item__wrapper">
					<quote:quoteExtras insuranceQuoteReviews="${insuranceQuoteReviews}" isValidStep="${insuranceQuoteReviews[0].validation['1']}"/>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 review-accordion-item__wrapper">
					<quote:quoteReviewDetails insuranceQuoteReviews="${insuranceQuoteReviews}" isValidStep="${insuranceQuoteReviews[0].validation['3']}"/>
				</div>

				<c:if test="${insuranceQuoteReviews[0].workFlowType eq 'LIFE'}">
					<div class="col-md-12 col-sm-12 col-xs-12 review-accordion-item__wrapper review-accordion-item__docs-flow">
						<quote:documentationFlow status="${insuranceQuoteReviews[0].documentationStatus }"/>
					</div>
				</c:if>
			</div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <form:form method="post" commandName="quoteReviewForm" class="review-accordion__form-button review-accordion__form-button--bottom">
                        <ycommerce:testId code="multicheckout_back_button">
                            <a class="secondary-button secondary-button__default secondary-button__checkout js-checkQuoteStatus" href="${fn:escapeXml(backStep)}">
                                <spring:theme code="checkout.multi.quoteReview.back" text="Back"/>
                            </a>
                        </ycommerce:testId>
                    </form:form>
                </div>
                <quote:quoteCertifySection checkoutUrl="${nextStep}" cartData="${cartData}"/>
            </div>
		</div>
	</div>

    <multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="true"/>

    <cms:pageSlot position="SideContent" var="feature" element="div" class="span-24 side-content-slot cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot>

	<insuranceCheckout:confirmQuoteUnbindActionPopup/>
	<insuranceCheckout:confirmQuoteBindActionPopup/>
</template:page>
