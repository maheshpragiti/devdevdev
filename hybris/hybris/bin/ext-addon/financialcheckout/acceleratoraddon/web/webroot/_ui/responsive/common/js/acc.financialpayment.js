ACC.financialpayment = {

	bindUseThisSavedCardButton: function ()
	{
		$(document).on("click",'.use_this_saved_card_button', function ()
		{
			var paymentId = $(this).attr('data-payment');
			$.postJSON(setPaymentDetailsUrl, {paymentId: paymentId}, ACC.financialpayment.handleSelectSavedCardSuccess);
			return false;
		});
	},

	handleSelectSavedCardSuccess: function (data)
	{
		if (data != null)
		{
			ACC.refresh.refreshPage(data);

			parent.$.colorbox.close();
		}
		else
		{
			alert("Failed to set payment details");
		}
	},

	refreshPaymentDetailsSection: function (data)
	{
		$('.js-summaryPayment').replaceWith($('#paymentSummaryTemplate').tmpl(data));

		//bind edit payment details button
		if (!typeof bindSecurityCodeWhatIs == 'undefined')
			bindSecurityCodeWhatIs();
	},

	showSavedPayments: function ()
	{
		$(document).on("click", "#viewSavedPayments", function(){

            var data = $("#savedPaymentListHolder").html();
            $.colorbox.settings.html = data;
			ACC.colorbox.open("", {
                // inline: true,
                // title: false
				height: '100px'
			});
		})
	},

	bindPaymentCardTypeSelect: function ()
	{
		$("#card_cardType").change(function ()
		{
			var cardType = $(this).val();
			if (cardType == '024')
			{
				$('#startDate, #issueNum').show();
			}
			else
			{
				$('#startDate, #issueNum').hide();
			}
		});
	}
};


$(document).ready(function ()
{
	ACC.financialpayment.bindPaymentCardTypeSelect();
	ACC.financialpayment.showSavedPayments();
	if (!typeof bindSecurityCodeWhatIs == 'undefined')
	{
		bindSecurityCodeWhatIs();
	}
});
