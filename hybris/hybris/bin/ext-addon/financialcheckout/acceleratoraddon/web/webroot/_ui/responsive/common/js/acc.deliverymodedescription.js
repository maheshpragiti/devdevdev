ACC.deliverydescription = {

	bindAll: function ()
	{
		this.bindDeliveryModeDes();
	},
	bindDeliveryModeDes: function ()
	{

		var str = $(".js-deliverymode-description").text();
		if (str.length > 180)
		{
			str = str.substr(0, 180) + '&hellip;';
		}
		$(".js-deliverymode-description").html(str);

	}

};

$(document).ready(function ()
{
	ACC.deliverydescription.bindAll();
});
