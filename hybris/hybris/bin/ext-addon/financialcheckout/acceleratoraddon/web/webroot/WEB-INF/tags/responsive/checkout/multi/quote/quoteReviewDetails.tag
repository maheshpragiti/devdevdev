<%@ attribute name="insuranceQuoteReviews" required="true" type="java.util.List<de.hybris.platform.commercefacades.insurance.data.InsuranceQuoteReviewData>" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="isValidStep" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty insuranceQuoteReviews}">
    <c:forEach items="${insuranceQuoteReviews}" var="quoteReview">
        <c:if test="${not empty quoteReview.policyHolderDetail}">
            <c:set var="policyHolderDetail" value="${quoteReview.policyHolderDetail}"/>
            <c:choose>
                <c:when test="${isValidStep == 'true'}"> <c:set var="isValidClass" value="review-accordion-item__valid js-toggle"/></c:when>
                <c:otherwise><c:set var="isValidClass" value="review-accordion-item__invalid js-toggle"/></c:otherwise>
            </c:choose>
            <h2 class="${isValidClass}">
                <c:if test="${cartData.insuranceQuote.state eq 'UNBIND'}">
                    <spring:theme code="checkout.multi.quoteReview.details.policy.holder.details" text="Policyholder Details"/>
                </c:if>
                <c:if test="${cartData.insuranceQuote.state eq 'BIND'}">
                    <spring:theme code="checkout.multi.quoteReview.details.your.details" text="Your Details"/>
                </c:if>
                <span class="review-accordion-item__open" data-open="quoteReviewYourDetails"></span>
            </h2>

            <div class="review-accordion-item__body opened" id="quoteReviewYourDetails">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h3 class="review-accordion-item__list-title"><spring:theme code="checkout.multi.quoteReview.details.policy.holder" text="Policy Holder"/></h3>
                    <ul class="review-accordion-item__list">
                        <li class="review-accordion-item__list-item">
                            <c:if test="${not empty policyHolderDetail.title}">${fn:escapeXml(policyHolderDetail.title)}. </c:if>${fn:escapeXml(policyHolderDetail.firstName)} ${fn:escapeXml(policyHolderDetail.lastName)}
                        </li>
                        <c:if test="${not empty policyHolderDetail.maritalStatus}">
                            <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.policy.holder.maritalStatus"/> : ${fn:escapeXml(policyHolderDetail.maritalStatus)}</li>
                        </c:if>
                        <c:if test="${not empty policyHolderDetail.emailAddress}">
                            <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.policy.holder.emailAddress"/> : ${fn:escapeXml(policyHolderDetail.emailAddress)}</li>
                        </c:if>
                        <c:if test="${not empty policyHolderDetail.phoneNumber}">
                            <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.policy.holder.phoneNumber"/> : ${fn:escapeXml(policyHolderDetail.phoneNumber)}</li>
                        </c:if>
                        <c:if test="${not empty policyHolderDetail.homePhoneNumber}">
                            <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.policy.holder.homePhoneNumber"/> : ${fn:escapeXml(policyHolderDetail.homePhoneNumber)}</li>
                        </c:if>
                        <c:if test="${not empty policyHolderDetail.mobilePhoneNumber}">
                            <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.policy.holder.mobilePhoneNumber"/> : ${fn:escapeXml(policyHolderDetail.mobilePhoneNumber)}</li>
                        </c:if>
                        <c:if test="${not empty policyHolderDetail.homePhoneNumber or policyHolderDetail.mobilePhoneNumber}">
                            <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.policy.holder.preferredContactNumber"/> : ${fn:escapeXml(policyHolderDetail.preferredContactNumber)}</li>
                        </c:if>
                        <c:if test="${not empty policyHolderDetail.propertyCorrespondenceAddress }">
                            <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.policy.holder.property.correspondance.address"/> : ${fn:escapeXml(policyHolderDetail.propertyCorrespondenceAddress)}</li>
                        </c:if>
                        <c:if test="${empty policyHolderDetail.propertyCorrespondenceAddress or policyHolderDetail.propertyCorrespondenceAddress eq 'no' }">
                            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressLine1)}</li>
                            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressLine2)}</li>
                            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressCity)}</li>
                            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.postcode)}</li>
                            <li class="review-accordion-item__list-item">${fn:escapeXml(policyHolderDetail.addressCountry)}</li>
                        </c:if>
                    </ul>
                    <c:if test="${not empty quoteReview.travellers}">
                        <c:forEach items="${quoteReview.travellers}" var="traveller" varStatus="status">
                            <ul class="review-accordion-item__list">
                                <li class="review-accordion-item__list-item review-accordion-item__list-item--title"><spring:theme code="checkout.multi.quoteReview.details.traveller" text="Traveller"/>&nbsp;${status.count}</li>
                                <li class="review-accordion-item__list-item">${fn:escapeXml(traveller.firstName)}&nbsp;${fn:escapeXml(traveller.lastName)}</li>
                                <li class="review-accordion-item__list-item"><spring:theme code="checkout.multi.quoteReview.details.age" text="Age:"/>&nbsp;${traveller.age}</li>
                            </ul>
                        </c:forEach>
                    </c:if>
                </div>
                <c:if test="${cartData.insuranceQuote.state eq 'UNBIND'}">
                    <spring:url var="updatePersonalDetailsUrl" value="/checkout/multi/form" htmlEscape="false"/>
                    <div class="col-md-3 col-sm-3 col-xs-12 review-accordion-item__button">
                        <a class="secondary-button secondary-button__default secondary-button__edit" href="${updatePersonalDetailsUrl}"><spring:theme code="text.cmsformsubmitcomponent.edit" text="Edit"/></a>
                    </div>
                </c:if>
            </div>
        </c:if>
    </c:forEach>
</c:if>
