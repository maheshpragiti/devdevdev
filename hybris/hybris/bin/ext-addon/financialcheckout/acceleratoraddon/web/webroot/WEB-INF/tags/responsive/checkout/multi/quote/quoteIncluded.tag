<%@ attribute name="insuranceQuoteReviews" required="true" type="java.util.List<de.hybris.platform.commercefacades.insurance.data.InsuranceQuoteReviewData>" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="financialCart" tagdir="/WEB-INF/tags/addons/financialacceleratorstorefront/responsive/cart" %>
<%@ attribute name="isValidStep" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty insuranceQuoteReviews}">
    <c:forEach items="${insuranceQuoteReviews}" var="quoteReview">
        <c:choose>
            <c:when test="${isValidStep == 'true'}"> <c:set var="isValidClass" value="review-accordion-item__valid js-toggle"/></c:when>
            <c:otherwise><c:set var="isValidClass" value="review-accordion-item__invalid js-toggle"/></c:otherwise>
        </c:choose>
        <h2 class="${isValidClass}">
            <c:if test="${cartData.insuranceQuote.state eq 'UNBIND'}">
                <spring:theme code="checkout.multi.quoteReview.included" text="Whats Included" />
            </c:if>
            <c:if test="${cartData.insuranceQuote.state eq 'BIND'}">
                <spring:theme code="checkout.multi.quoteReview.quoteDetails" text="Quote Details" /> 
            </c:if>
            <c:if test="${isValidStep == 'true'}">
                <span class="review-accordion-item__open" data-open="whatsIncludedPanel"></span>
            </c:if>
        </h2>

        <c:if test="${isValidStep == 'true'}">
            <div id="whatsIncludedPanel" class="review-accordion-item__body opened">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <c:if test="${empty quoteReview.mainProduct.benefits}">
                        <p class="review-accordion-item__text">
                            <spring:theme code="checkout.multi.quoteReview.included.empty" text="-" />
                        </p>
                    </c:if>
                    <ul class="review-accordion-item__list review-accordion-item__list--style-type">
                        <c:forEach var="includedItem" items="${quoteReview.mainProduct.benefits}">
                            <li class="review-accordion-item__list-item">${fn:escapeXml(includedItem.name)}</li>
                        </c:forEach>
                    </ul>
                </div>
                <c:if test="${cartData.insuranceQuote.state eq 'UNBIND'}">
                    <spring:url var="editInformationUrl" value="/c/${quoteReview.mainProduct.coverageProduct.defaultCategory.code}" htmlEscape="false">
                        <spring:param value="view" name="viewStatus"/>
                    </spring:url>
                    <div class="col-md-3 col-sm-3 col-xs-12 review-accordion-item__button">
                        <a class="secondary-button secondary-button__default secondary-button__edit" href="${editInformationUrl}"><spring:theme code="text.cmsformsubmitcomponent.edit" text="Edit"/></a>
                    </div>
                </c:if>
            </div>
        </c:if>
    </c:forEach>
</c:if>