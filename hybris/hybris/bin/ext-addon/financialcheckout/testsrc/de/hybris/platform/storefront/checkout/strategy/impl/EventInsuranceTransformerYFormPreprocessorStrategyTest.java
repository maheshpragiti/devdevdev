/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.storefront.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCustomerNameStrategy;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storefront.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Map;


/**
 * The class of EventInsuranceTransformerYFormPreprocessorStrategyTest.
 */
public class EventInsuranceTransformerYFormPreprocessorStrategyTest
{

	@InjectMocks
	private EventInsuranceTransformerYFormPreprocessorStrategy preprocessorStrategy;

	@Mock
	private UserService userService;

	@Mock
	private CustomerModel customerModel;

	@Mock
	private InsuranceYFormDataPreprocessorStrategy insuranceYFormDataPreprocessorStrategy;

	@Mock
	private DefaultCustomerNameStrategy defaultCustomerNameStrategy;

	@Before
	public void setup()
	{
		preprocessorStrategy = new EventInsuranceTransformerYFormPreprocessorStrategy();

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testTransform() throws YFormProcessorException
	{
		final String firstName = "Name";
		final String lastName = "LastName";
		final String email = "test@email.com";
		final String[] fullName = { firstName, lastName };

		final String xmlContent =
				"<form><eventDetails>"
						+ "<country/>"
						+ "<date/>"
						+ "<venue/>"
						+ "<address/>"
						+ "<city/>"
						+ "<first-name/>"
						+ "<surname/>"
						+ "<email/>"
						+ "<ciAddressL1/>"
						+ "<ciAddressL2/>"
						+ "<ciPostcode/>"
						+ "<ciCity/>"
						+ "</eventDetails></form>";

		final String expectedXmlContent =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
						+ "<form><eventDetails>"
						+ "<country/>"
						+ "<date/>"
						+ "<venue/>"
						+ "<address/>"
						+ "<city/>"
						+ "<first-name>" + firstName + "</first-name>"
						+ "<surname>" + lastName + "</surname>"
						+ "<email>" + email + "</email>"
						+ "<ciAddressL1/>"
						+ "<ciAddressL2/>"
						+ "<ciPostcode/>"
						+ "<ciCity/>"
						+ "</eventDetails></form>";

		final Integer cartEntryNumber = Integer.valueOf(0);

		final FormDetailData data = new FormDetailData();
		data.setOrderEntryNumber(cartEntryNumber);

		final Map<String, Object> params = Maps.newHashMap();
		params.put(InsuranceYFormDataPreprocessorStrategy.FORM_DETAIL_DATA, data);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);

		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy()).thenReturn(defaultCustomerNameStrategy);

		Mockito.when(insuranceYFormDataPreprocessorStrategy.getCustomerNameStrategy().splitName(customerModel.getName()))
				.thenReturn(fullName);

		Mockito.when(customerModel.getContactEmail()).thenReturn(email);

		final String resultXml = preprocessorStrategy.transform(xmlContent, params);

		Assert.assertEquals(expectedXmlContent, resultXml);
	}
}
