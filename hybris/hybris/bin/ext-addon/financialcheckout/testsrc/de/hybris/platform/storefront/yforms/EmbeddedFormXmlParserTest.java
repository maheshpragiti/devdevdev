/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.storefront.yforms;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * Tests for the EmbeddedFormXmlParser to ensure that valid / invalid content result in either a DOM document or null
 */
public class EmbeddedFormXmlParserTest
{

	private static final Logger LOG = Logger.getLogger(EmbeddedFormXmlParserTest.class);

	@InjectMocks
	private EmbeddedFormXmlParser formParser;

	@Mock
	private DocumentBuilder builder;

	@Mock
	private Document document;

	@Before
	public void setup() throws SAXException, IOException
	{
		this.formParser = new EmbeddedFormXmlParser();
		MockitoAnnotations.initMocks(this);
		Mockito.when(builder.parse((InputSource) Matchers.isNotNull())).thenReturn(document);
	}

	@Test
	public void validXmlParseTest()
	{
		final String xmlContent = "<test><item>something</item><test>";
		final Document parsedDocument = this.formParser.parseContent(xmlContent);
		assertEquals(document, parsedDocument);
	}

	@Test
	public void emptyXmlParseTest()
	{
		final String xmlContent = "";
		final Document parsedDocument = this.formParser.parseContent(xmlContent);
		assertNull(parsedDocument);
	}

	@Test
	public void nullXmlParseTest()
	{
		final String xmlContent = null;
		final Document parsedDocument = this.formParser.parseContent(xmlContent);
		assertNull(parsedDocument);
	}

	@Test
	public void nullBuilderTest()
	{
		this.formParser.setBuilder(null); // simulates init() failing
		final String xmlContent = "<test><item>something</item><test>";
		final Document parsedDocument = this.formParser.parseContent(xmlContent);
		assertNull(parsedDocument);
	}

	@Test
	public void multiThreadParseTest()
	{
		final EmbeddedFormXmlParser xmlParser = new EmbeddedFormXmlParser();
		xmlParser.init();
		final String xmlToParse1 = "<init><node1><node2>value in node2</node2><node2>second value in node 2</node2></node1><trailer>value in trailer 1</trailer></init>";
		final String xmlToParse2 = "<init><node1><node2>2nd value in node2</node2><node2>2nd second value in node 2</node2></node1><trailer>value in trailer 2</trailer></init>";
		final String xmlToParse3 = "<init><node1><node2>3rd value in node2</node2><node2>3rd second value in node 2</node2></node1><trailer>value in trailer 3</trailer></init>";
		final String xmlToParse = "<container>" + xmlToParse1 + xmlToParse2 + xmlToParse3 + "</container>";

		final long timeNow = new Date().getTime();


		final ParserThread thread1 = new ParserThread(xmlParser, getXmlToParse(1, xmlToParse), timeNow + 2000);
		final ParserThread thread2 = new ParserThread(xmlParser, getXmlToParse(2, xmlToParse), timeNow + 2000);
		final ParserThread thread3 = new ParserThread(xmlParser, getXmlToParse(3, xmlToParse), timeNow + 2000);
		final ParserThread thread4 = new ParserThread(xmlParser, getXmlToParse(4, xmlToParse), timeNow + 2000);
		final ParserThread thread5 = new ParserThread(xmlParser, getXmlToParse(5, xmlToParse), timeNow + 2000);
		final ParserThread thread6 = new ParserThread(xmlParser, getXmlToParse(6, xmlToParse), timeNow + 2000);
		final ParserThread thread7 = new ParserThread(xmlParser, getXmlToParse(7, xmlToParse), timeNow + 2000);
		final ParserThread thread8 = new ParserThread(xmlParser, getXmlToParse(8, xmlToParse), timeNow + 2000);
		final ParserThread thread9 = new ParserThread(xmlParser, getXmlToParse(9, xmlToParse), timeNow + 2000);

		final Thread t1 = new Thread(thread1);
		final Thread t2 = new Thread(thread2);
		final Thread t3 = new Thread(thread3);
		final Thread t4 = new Thread(thread4);
		final Thread t5 = new Thread(thread5);
		final Thread t6 = new Thread(thread6);
		final Thread t7 = new Thread(thread7);
		final Thread t8 = new Thread(thread8);
		final Thread t9 = new Thread(thread9);

		try
		{
			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t5.start();
			t6.start();
			t7.start();
			t8.start();
			t9.start();
		}
		catch (final Exception e)
		{
			LOG.error(e);
		}

	}

	public class ParserThread implements Runnable
	{
		private final EmbeddedFormXmlParser threadParser;
		private final String xmlToParse;
		private Document xmlDocument;

		private final long waitTime;

		public ParserThread(final EmbeddedFormXmlParser threadParser, final String xmlToParse, final long waitTime)
		{
			this.threadParser = threadParser;
			this.xmlToParse = xmlToParse;
			this.waitTime = waitTime;
			this.xmlDocument = null;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			// wait until the right time
			long timeNow = new Date().getTime();

			while (timeNow < waitTime)
			{
				try
				{
					Thread.sleep(1);
					timeNow = new Date().getTime();
				}
				catch (final Exception e)
				{
					LOG.error(e);
				}
			}

			this.setXmlDocument(this.threadParser.parseContent(xmlToParse));
			LOG.info("finished");
		}

		/**
		 * @return the threadParser
		 */
		public EmbeddedFormXmlParser getThreadParser()
		{
			return threadParser;
		}

		/**
		 * @return the xmlToParse
		 */
		public String getXmlToParse()
		{
			return xmlToParse;
		}

		/**
		 * @return the xmlDocument
		 */
		public Document getXmlDocument()
		{
			return xmlDocument;
		}

		public void setXmlDocument(final Document xmlDocument)
		{
			this.xmlDocument = xmlDocument;
		}
	}

	private static String getXmlToParse(final int testId, final String xmlToParse)
	{
		return "<test id=\"" + testId + "\">" + xmlToParse + "</test>";
	}

}
