/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.storefront.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import java.util.Locale;
import java.util.Map;


/**
 * The class of PropertyInsuranceTransformerYFormPreprocessorStrategy.
 */
public class PropertyInsuranceTransformerYFormPreprocessorStrategy extends InsuranceYFormDataPreprocessorStrategy
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(PropertyInsuranceTransformerYFormPreprocessorStrategy.class);

	private static final String FORM_HOLDER_XPATH = "/form/holder-details-section";
	private static final String FIRST_NAME_XPATH = "/first-name";
	private static final String LAST_NAME_XPATH = "/last-name";
	private static final String ADDRESS_XPATH = "/email-address";

	/**
	 * Applies the actual transformation to a formData
	 *
	 * @param xmlContent
	 * @param params
	 * @throws YFormProcessorException
	 */
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);

		if (!validation(params))
		{
			return xmlString;
		}

		final Map<String, Object> propertyInsuranceParams = Maps.newHashMap();

		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();

		if (!getUserService().isAnonymousUser(currentUser))
		{
			final String[] fullName = getCustomerNameStrategy().splitName(currentUser.getName());

			final Document document = createDocument(xmlString);
			final XPath xpath = createXPath();

			if (getTextValue(xpath, document, FORM_HOLDER_XPATH + FIRST_NAME_XPATH).isEmpty() && currentUser.getTitle()!=null)
			{
				final String currentLanguage = currentUser.getSessionLanguage().getIsocode();
				propertyInsuranceParams
						.put(FORM_HOLDER_XPATH + "/title",
								currentUser.getTitle().getName(Locale.forLanguageTag(currentLanguage)));
			}

			if (getTextValue(xpath, document, FORM_HOLDER_XPATH + FIRST_NAME_XPATH).isEmpty())
			{
				propertyInsuranceParams.put(FORM_HOLDER_XPATH + FIRST_NAME_XPATH, fullName[0]);
			}

			if (getTextValue(xpath, document, FORM_HOLDER_XPATH + LAST_NAME_XPATH).isEmpty())
			{
				propertyInsuranceParams.put(FORM_HOLDER_XPATH + LAST_NAME_XPATH, fullName[1]);
			}

			if (getTextValue(xpath, document, FORM_HOLDER_XPATH + ADDRESS_XPATH).isEmpty())
			{
				propertyInsuranceParams.put(FORM_HOLDER_XPATH + ADDRESS_XPATH, currentUser.getContactEmail());
			}
		}

		return updateXmlContent(xmlString, propertyInsuranceParams);
	}

}
