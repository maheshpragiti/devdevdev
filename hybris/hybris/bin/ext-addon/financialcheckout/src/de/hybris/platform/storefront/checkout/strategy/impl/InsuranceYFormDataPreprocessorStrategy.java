/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.storefront.checkout.strategy.impl;

import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storefront.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.ReferenceIdTransformerYFormPreprocessorStrategy;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;


/**
 * The class of InsuranceYFormDataPreprocessorStrategy.
 */
public class InsuranceYFormDataPreprocessorStrategy extends ReferenceIdTransformerYFormPreprocessorStrategy
{
	private static final Logger LOG = Logger.getLogger(InsuranceYFormDataPreprocessorStrategy.class);

	public static final String FORM_DETAIL_DATA = "formDetailData";
	protected static final String XML_DISABLE_DOCTYPE_DECL = "http://apache.org/xml/features/disallow-doctype-decl";
	private static final String XPATH_TEXT = "/text()";

	private UserService userService;
	private CustomerNameStrategy customerNameStrategy;

	/**
	 * Update the XML string by using the given params.
	 *
	 * @param xmlContent XML content string
	 * @param params     The params map
	 * @return Updated XML content string
	 * @throws YFormProcessorException
	 */
	protected String updateXmlContent(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		if (xmlContent == null || MapUtils.isEmpty(params))
		{
			return xmlContent;
		}

		final Document document = createDocument(xmlContent);
		final XPath xpath = createXPath();

		for (final Map.Entry<String, Object> entry : params.entrySet())
		{
			if (entry.getValue() == null)
			{
				continue;
			}

			final NodeList nodeList = getNodeList(xpath, document, entry.getKey());

			if (nodeList == null || nodeList.item(0) == null)
			{
				LOG.warn("Cannot find node with xpath [" + entry.getKey() + "]!");
				continue;
			}

			final NodeList nodeTextList = getNodeList(xpath, document, entry.getKey() + XPATH_TEXT);

			if (nodeTextList.item(0) != null)
			{
				nodeTextList.item(0).setNodeValue(entry.getValue().toString());
			}
			else
			{
				LOG.debug("Cannot find node with xpath [" + entry.getKey() + "], append the node.");
				final Text text = document.createTextNode(entry.getValue().toString());
				final Element p = document.createElement(nodeList.item(0).getNodeName());

				p.appendChild(text);
				nodeList.item(0).getParentNode().insertBefore(p, nodeList.item(0));
				nodeList.item(0).getParentNode().removeChild(nodeList.item(0));
			}
		}

		String xmlResult = xmlContent;

		try
		{
			final ByteArrayOutputStream os = new ByteArrayOutputStream();
			final TransformerFactory factory = TransformerFactory.newInstance();
			factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			final Transformer transformer = factory.newTransformer();
			transformer.transform(new DOMSource(document), new StreamResult(os));

			xmlResult = os.toString("UTF-8");
		}
		catch (TransformerException | UnsupportedEncodingException e)
		{
			LOG.error(e.getMessage(), e);
		}

		return xmlResult;
	}

	protected NodeList getNodeList(final XPath xpath, final Document document, final String expression)
	{
		XPathExpression expr;
		NodeList nodes = null;

		try
		{
			expr = xpath.compile(expression);
			nodes = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
		}
		catch (final XPathExpressionException e)
		{
			LOG.error(e.getMessage(), e);
		}

		return nodes;
	}

	protected XPath createXPath()
	{
		final XPathFactory xpathFactory = XPathFactory.newInstance();
		return xpathFactory.newXPath();
	}

	protected Document createDocument(final String xml)
	{
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document document = null;

		try
		{
			factory.setFeature(XML_DISABLE_DOCTYPE_DECL, true);
			builder = factory.newDocumentBuilder();
			document = builder.parse(new InputSource(new StringReader(xml)));
		}
		catch (final ParserConfigurationException | SAXException | IOException e)
		{
			LOG.error(e.getMessage(), e);
		}

		return document;
	}

	protected String getTextValue(final XPath xpath, final Document document, final String expression)
	{
		final NodeList nodeList = getNodeList(xpath, document, expression + XPATH_TEXT);
		if (nodeList != null && nodeList.getLength() > NumberUtils.INTEGER_ZERO)
		{
			final Node item = getNodeList(xpath, document, expression + XPATH_TEXT).item(0);
			return StringEscapeUtils.escapeXml(item.getNodeValue());
		}
		else
		{
			return StringUtils.EMPTY;
		}
	}

	protected boolean validation(final Map<String, Object> params)
	{
		return params.containsKey(FORM_DETAIL_DATA) && params.get(FORM_DETAIL_DATA) instanceof FormDetailData;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CustomerNameStrategy getCustomerNameStrategy()
	{
		return customerNameStrategy;
	}

	@Required
	public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy)
	{
		this.customerNameStrategy = customerNameStrategy;
	}

}
