/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.storefront.checkout.strategy.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import java.util.Map;


/**
 * The class of AutoInsuranceTransformerYFormPreprocessorStrategy.
 */
public class AutoInsuranceTransformerYFormPreprocessorStrategy extends InsuranceYFormDataPreprocessorStrategy
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(AutoInsuranceTransformerYFormPreprocessorStrategy.class);

	/**
	 * Applies the actual transformation to a formData
	 *
	 * @param xmlContent
	 * @param params
	 * @throws YFormProcessorException
	 */
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);

		if (!validation(params))
		{
			return xmlString;
		}

		final Map<String, Object> autoInsuranceParams = Maps.newHashMap();

		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();

		if (!getUserService().isAnonymousUser(currentUser))
		{
			String[] fullName = getCustomerNameStrategy().splitName(currentUser.getName());

			final Document document = createDocument(xmlString);
			final XPath xpath = createXPath();

			if (getTextValue(xpath, document, "/form/personal-details/first-name").isEmpty())
			{
				autoInsuranceParams.put("/form/personal-details/first-name", fullName[0]);
			}

			if (getTextValue(xpath, document, "/form/personal-details/last-name").isEmpty())
			{
				autoInsuranceParams.put("/form/personal-details/last-name", fullName[1]);
			}

			if (getTextValue(xpath, document, "/form/personal-details/email").isEmpty())
			{
				autoInsuranceParams.put("/form/personal-details/email", currentUser.getContactEmail());
			}
		}

		return updateXmlContent(xmlString, autoInsuranceParams);
	}

}
