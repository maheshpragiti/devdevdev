/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmssmarteditwebservices.structures.controller;

import static de.hybris.platform.webservicescommons.testsupport.client.WebservicesAssert.assertResponse;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.restrictions.CMSTimeRestrictionModel;
import de.hybris.platform.cmsfacades.data.OptionData;
import de.hybris.platform.cmsfacades.data.StructureTypeCategory;
import de.hybris.platform.cmssmarteditwebservices.constants.CmssmarteditwebservicesConstants;
import de.hybris.platform.cmssmarteditwebservices.data.StructureTypeMode;
import de.hybris.platform.cmssmarteditwebservices.dto.StructureAttributeWsDTO;
import de.hybris.platform.cmssmarteditwebservices.dto.StructureListWsDTO;
import de.hybris.platform.cmssmarteditwebservices.dto.StructureWsDTO;
import de.hybris.platform.cmssmarteditwebservices.util.ApiBaseIntegrationTest;
import de.hybris.platform.cmswebservices.constants.CmswebservicesConstants;
import de.hybris.platform.oauth2.constants.OAuth2Constants;
import de.hybris.platform.webservicescommons.testsupport.server.NeedsEmbeddedServer;

import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;


@NeedsEmbeddedServer(webExtensions =
{ CmssmarteditwebservicesConstants.EXTENSIONNAME, OAuth2Constants.EXTENSIONNAME, CmswebservicesConstants.EXTENSIONNAME })
@IntegrationTest
public class StructureControllerWebServiceTest extends ApiBaseIntegrationTest
{
	// Time Restriction
	private static final String NAME = "name";
	private static final String ACTIVE_FROM = "activeFrom";
	private static final String ACTIVE_UNTIL = "activeUntil";

	// Cms Link Component
	private static final String LINK_NAME = "linkName";
	private static final String LINK_TO = "linkTo";
	private static final String PRODUCT = "product";
	private static final String PRODUCT_CMS_STRUCTURE_TYPE = "SingleOnlineProductSelector";
	private static final String CATEGORY = "category";
	private static final String CATEGORY_CMS_STRUCTURE_TYPE = "SingleOnlineCategorySelector";
	private static final String CMS_LINK_TO_OPTION_PREFIX = "cms.linkto.option.";
	private static final String CMS_LINK_CONTENT_OPTION = "content";
	private static final String CMS_LINK_PRODUCT_OPTION = "product";
	private static final String CMS_LINK_CATEGORY_OPTION = "category";
	private static final String CMS_LINK_EXTERNAL_OPTION = "external";
	private static final String PRODUCT_CATALOG_URI = "/cmssmarteditwebservices/v1/sites/CURRENT_CONTEXT_SITE_ID/productcatalogs";

	private static final String MODE = "mode";
	private static final String URI = "/v1/structures";

	@Test
	public void shouldFindTimeRestrictionStructureWithRequiredModes()
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(CMSTimeRestrictionModel._TYPECODE).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureListWsDTO entityList = response.readEntity(StructureListWsDTO.class);

		final StructureWsDTO defaultStructure = findStructureByMode(entityList, StructureTypeMode.DEFAULT.name());
		assertThat(defaultStructure.getAttributes(), hasSize(3));
		final StructureWsDTO addStructure = findStructureByMode(entityList, StructureTypeMode.ADD.name());
		assertThat(addStructure.getAttributes(), hasSize(3));
		final StructureWsDTO editStructure = findStructureByMode(entityList, StructureTypeMode.EDIT.name());
		assertThat(editStructure.getAttributes(), hasSize(3));
		final StructureWsDTO createStructure = findStructureByMode(entityList, StructureTypeMode.CREATE.name());
		assertThat(createStructure.getAttributes(), hasSize(3));

	}

	protected StructureWsDTO findStructureByMode(final StructureListWsDTO dtos, final String mode)
	{
		return dtos.getStructures().stream().filter(structure -> structure.getAttributes().stream()
				.filter(attribute -> attribute.getMode().equals(mode)).findFirst().isPresent()).findFirst().get();
	}

	@Test
	public void shouldFindTimeRestrictionStructureForAddModeAndAttributesAreOrdered() throws Exception
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(CMSTimeRestrictionModel._TYPECODE) //
				.queryParam(MODE, StructureTypeMode.ADD.name()).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureListWsDTO entityList = response.readEntity(StructureListWsDTO.class);
		assertNotNull(entityList);

		final StructureWsDTO entity = entityList.getStructures().get(0);
		assertThat(entity.getAttributes(), hasSize(3));

		final List<StructureAttributeWsDTO> attributes = entity.getAttributes();

		final StructureAttributeWsDTO name = attributes.get(0);
		assertThat(name.getQualifier(), equalTo(NAME));
		assertThat(name.isEditable(), is(false));
		assertThat(name.getMode(), equalTo(StructureTypeMode.ADD.name()));

		final StructureAttributeWsDTO activeFrom = attributes.get(1);
		assertThat(activeFrom.getQualifier(), equalTo(ACTIVE_FROM));
		assertThat(activeFrom.isEditable(), is(false));
		assertThat(activeFrom.getMode(), equalTo(StructureTypeMode.ADD.name()));

		final StructureAttributeWsDTO activeUntil = attributes.get(2);
		assertThat(activeUntil.getQualifier(), equalTo(ACTIVE_UNTIL));
		assertThat(activeUntil.isEditable(), is(false));
		assertThat(activeUntil.getMode(), equalTo(StructureTypeMode.ADD.name()));
	}

	@Test
	public void shouldFindTimeRestrictionStructureForDefaultModeAndAttributesAreOrdered() throws Exception
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(CMSTimeRestrictionModel._TYPECODE) //
				.queryParam(MODE, StructureTypeMode.DEFAULT.name()).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureListWsDTO entityList = response.readEntity(StructureListWsDTO.class);
		assertNotNull(entityList);

		final StructureWsDTO entity = entityList.getStructures().get(0);
		assertTrue(entity.getAttributes().size() > 1);

		final List<StructureAttributeWsDTO> attributes = entity.getAttributes();

		final StructureAttributeWsDTO name = attributes.get(0);
		assertThat(name.getQualifier(), equalTo(NAME));
		assertThat(name.isEditable(), is(true));
		assertThat(name.getMode(), equalTo(StructureTypeMode.DEFAULT.name()));

		final StructureAttributeWsDTO activeFrom = attributes.get(1);
		assertThat(activeFrom.getQualifier(), equalTo(ACTIVE_FROM));
		assertThat(activeFrom.isEditable(), is(true));
		assertThat(activeFrom.getMode(), equalTo(StructureTypeMode.DEFAULT.name()));

		final StructureAttributeWsDTO activeUntil = attributes.get(2);
		assertThat(activeUntil.getQualifier(), equalTo(ACTIVE_UNTIL));
		assertThat(activeUntil.isEditable(), is(true));
		assertThat(activeUntil.getMode(), equalTo(StructureTypeMode.DEFAULT.name()));
	}

	@Test
	public void shouldFindTimeRestrictionStructureForCreateModeAndAttributesAreOrdered() throws Exception
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(CMSTimeRestrictionModel._TYPECODE) //
				.queryParam(MODE, StructureTypeMode.CREATE.name()).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureListWsDTO entityList = response.readEntity(StructureListWsDTO.class);
		assertNotNull(entityList);

		final StructureWsDTO entity = entityList.getStructures().get(0);
		assertTrue(entity.getAttributes().size() > 1);

		final List<StructureAttributeWsDTO> attributes = entity.getAttributes();

		final StructureAttributeWsDTO name = attributes.get(0);
		assertThat(name.getQualifier(), equalTo(NAME));
		assertThat(name.isEditable(), is(true));
		assertThat(name.getMode(), equalTo(StructureTypeMode.CREATE.name()));

		final StructureAttributeWsDTO activeFrom = attributes.get(1);
		assertThat(activeFrom.getQualifier(), equalTo(ACTIVE_FROM));
		assertThat(activeFrom.isEditable(), is(true));
		assertThat(activeFrom.getMode(), equalTo(StructureTypeMode.CREATE.name()));

		final StructureAttributeWsDTO activeUntil = attributes.get(2);
		assertThat(activeUntil.getQualifier(), equalTo(ACTIVE_UNTIL));
		assertThat(activeUntil.isEditable(), is(true));
		assertThat(activeUntil.getMode(), equalTo(StructureTypeMode.CREATE.name()));
	}

	@Test
	public void shouldFindCmsLinkComponentStructureAndLinkToOptionsAreAddedCorrectly() throws Exception
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(CMSLinkComponentModel._TYPECODE) //
				.queryParam(MODE, StructureTypeMode.DEFAULT.name()).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureWsDTO entity = response.readEntity(StructureListWsDTO.class).getStructures().get(0);
		assertNotNull(entity);

		final List<StructureAttributeWsDTO> attributes = entity.getAttributes();
		assertTrue(attributes.size() > 1);

		final StructureAttributeWsDTO name = attributes.get(0);
		assertThat(name.getQualifier(), equalTo(LINK_NAME));
		assertThat(name.isEditable(), is(true));
		assertThat(name.getMode(), equalTo(StructureTypeMode.DEFAULT.name()));

		final StructureAttributeWsDTO linkTo = attributes.get(1);
		assertThat(linkTo.getQualifier(), equalTo(LINK_TO));
		final List<OptionData> linkToOptions = linkTo.getOptions();
		assertTrue(linkToOptions.size() == 4);

		assertTrue(linkToOptions.get(0).getLabel().equals(CMS_LINK_TO_OPTION_PREFIX + CMS_LINK_CONTENT_OPTION));
		assertTrue(linkToOptions.get(0).getId().equals(CMS_LINK_CONTENT_OPTION));

		assertTrue(linkToOptions.get(1).getLabel().equals(CMS_LINK_TO_OPTION_PREFIX + CMS_LINK_PRODUCT_OPTION));
		assertTrue(linkToOptions.get(1).getId().equals(CMS_LINK_PRODUCT_OPTION));

		assertTrue(linkToOptions.get(2).getLabel().equals(CMS_LINK_TO_OPTION_PREFIX + CMS_LINK_CATEGORY_OPTION));
		assertTrue(linkToOptions.get(2).getId().equals(CMS_LINK_CATEGORY_OPTION));

		assertTrue(linkToOptions.get(3).getLabel().equals(CMS_LINK_TO_OPTION_PREFIX + CMS_LINK_EXTERNAL_OPTION));
		assertTrue(linkToOptions.get(3).getId().equals(CMS_LINK_EXTERNAL_OPTION));
	}

	@Test
	public void shouldFindCmsLinkComponentStructureAndProductAreAddedCorrectly() throws Exception
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(CMSLinkComponentModel._TYPECODE) //
				.queryParam(MODE, StructureTypeMode.PRODUCT.name()).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureWsDTO entity = response.readEntity(StructureListWsDTO.class).getStructures().get(0);
		assertNotNull(entity);

		final List<StructureAttributeWsDTO> attributes = entity.getAttributes();
		assertTrue(attributes.size() > 2);

		final StructureAttributeWsDTO product = attributes.get(2);
		assertThat(product.getCmsStructureType(), equalTo(PRODUCT_CMS_STRUCTURE_TYPE));
		assertThat(product.getQualifier(), equalTo(PRODUCT));
		assertThat(product.isEditable(), is(true));
		assertThat(product.isPaged(), is(true));
		assertThat(product.getMode(), equalTo(StructureTypeMode.PRODUCT.name()));
	}

	@Test
	public void shouldFindCmsLinkComponentStructureAndCategoryAreAddedCorrectly() throws Exception
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(CMSLinkComponentModel._TYPECODE) //
				.queryParam(MODE, StructureTypeMode.CATEGORY.name()).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureWsDTO entity = response.readEntity(StructureListWsDTO.class).getStructures().get(0);
		assertNotNull(entity);

		final List<StructureAttributeWsDTO> attributes = entity.getAttributes();
		assertTrue(attributes.size() > 2);

		final StructureAttributeWsDTO category = attributes.get(2);
		assertThat(category.getCmsStructureType(), equalTo(CATEGORY_CMS_STRUCTURE_TYPE));
		assertThat(category.getQualifier(), equalTo(CATEGORY));
		assertThat(category.isEditable(), is(true));
		assertThat(category.isPaged(), is(true));
		assertThat(category.getMode(), equalTo(StructureTypeMode.CATEGORY.name()));
	}

	/**
	 * A custom page structure type structure is defined in the registry
	 * (cmssmarteditwebservices-structuretype-spring.xml) which overrides the structure provided by the base structure
	 * (cmsfacades). This test validates that the resulted page structure is correct.
	 */
	@Test
	public void shouldFindContentPageStructureForDefaultMode()
	{
		final Response response = getWsSecuredRequestBuilder() //
				.path(URI) //
				.path(ContentPageModel._TYPECODE) //
				.queryParam(MODE, StructureTypeMode.DEFAULT.name()).build() //
				.accept(MediaType.APPLICATION_JSON) //
				.get();

		assertResponse(Status.OK, response);

		final StructureListWsDTO entityList = response.readEntity(StructureListWsDTO.class);
		assertThat(entityList, notNullValue());

		final StructureWsDTO entity = entityList.getStructures().get(0);
		assertThat(entity.getCategory(), equalTo(StructureTypeCategory.PAGE.name()));
		assertThat(entity.getType(), equalTo("contentPageWsDTO"));
	}

}
